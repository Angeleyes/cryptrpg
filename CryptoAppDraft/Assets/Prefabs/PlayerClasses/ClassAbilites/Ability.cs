﻿using System.Text;
using System;
using UnityEngine;

[SerializeField]
public abstract class Ability : ScriptableObject {

    public string AbilityName;
    public Sprite AbilityIcon;
    public AudioClip AbilitySoundEffect;
    public float AbilityBaseCoolDown;
    public int ResourceCost;
    public bool IsAvaliable = false;
    internal bool HasResource = false;
    [TextArea]
    public string AbilityDescription;
    internal string TooltipDesc;
    internal StringBuilder Sb = new StringBuilder();

    public delegate void ResourceUsed(int amount);
    public ResourceUsed OnResourceUsed;

    public abstract void Initalize(PlayerEntity P);

    public abstract void TriggerAbility();
   

    public string GetAbilityData()
    {
        Sb.Length = 0;
        Sb.Append(AbilityName);
        Sb.Append(Environment.NewLine);
        Sb.Append("Cool down: ");
        Sb.Append(AbilityBaseCoolDown);
        Sb.Append(Environment.NewLine);
        Sb.Append("Resource cost: ");
        Sb.Append(ResourceCost);
        Sb.Append(Environment.NewLine);
        Sb.Append(Environment.NewLine);
        Sb.Append(TooltipDesc);

        return Sb.ToString();
    }


}
