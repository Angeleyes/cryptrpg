﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileAbility : Ability {

    public float ProjectiveForce;
    public Rigidbody2D ProjectileBody;

    private GameObject WeaponObject;


    public override void Initalize(PlayerEntity player)
    {
        
    }

    public override void TriggerAbility()
    {

        //shoot
        Debug.Log("shooting ability: " + this.AbilityName);
    }
}
