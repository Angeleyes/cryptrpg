﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilites/AOEAbility")]
public class AOEAbility : Ability
{
    public override void Initalize(PlayerEntity player)
    {
    }

    public override void TriggerAbility()
    {
        Debug.Log("triggerings aoe ability");
    }
}
