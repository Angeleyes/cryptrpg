﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Abilites/RaycastAbility")]
public class RaycastAbility : Ability {

    public float Damage;
    public Color EffectColor;
    public float MaxRange;
    public float ForceAmount;
    //public Sprite RaySprite;

    //public InGameEffect; //some particle system sprite thing


    public override void Initalize(PlayerEntity player)
    {
        //the object is like weapon data and shit
        Debug.Log(this.AbilityName + " ability initalized");
    }

    public override void TriggerAbility() //on button press pretty much
    {
        //shoot ray from shootrayclass and shootrayclass does all the ray work
        Debug.Log("triggering ability: " + this.AbilityName);
    }

}
