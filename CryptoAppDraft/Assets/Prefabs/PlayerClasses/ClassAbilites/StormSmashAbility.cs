﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Abilites/StormSmashAbility")]
public class StormSmashAbility : Ability
{
    internal PlayerEntity ThePlayer;

    public float SmashRadius = 5f;
    public SimpleSpriteAnimator EffectClip;
    public int LightningDamage = 50;

    //private int Monsterlayer = 23;
    Collider2D[] Cols = new Collider2D[10];

    public override void Initalize(PlayerEntity P)
    {
        ThePlayer = P;
    }

    private void LightningSmash()
    {
        ContactFilter2D C = new ContactFilter2D();
        C.layerMask = 23;

        Physics2D.OverlapCircle(ThePlayer.transform.position, SmashRadius, C, Cols);

        foreach (Collider2D Collider in Cols)
        {
            if (Collider != null)
            {
                if (Collider.gameObject.GetComponent<MonsterEntity>() != null && Collider.gameObject.activeInHierarchy)
                {
                    MonsterEntity M = Collider.gameObject.GetComponent<MonsterEntity>();
                    M.EntityEffectHandler.SetEffectAnimation(EffectClip);
                    M.TakeDamage(LightningDamage, ThePlayer, AttackHitBox.DamageSpec.SpellDamage);
                }
            }
        }

    }

    public override void TriggerAbility()
    {
        ThePlayer.StartCoroutine(AnimateSmash());
        LightningSmash();  
    }



    IEnumerator AnimateSmash()
    {
        Debug.Log("animating smash");
        ThePlayer.EntityAnimator.SetBool("Ability1Trigger", true);
        ThePlayer.EntityAnimator.SetBool("BarbStomping", true);
        yield return new WaitForSeconds(0.5f);
        ThePlayer.EntityAnimator.SetBool("Ability1Trigger", false);
        ThePlayer.EntityAnimator.SetBool("BarbStomping", false);
        yield break;
    }


}
