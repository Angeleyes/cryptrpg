﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilites/PowerUpAbility")]
public class PowerUpAbility : Ability
{

    PlayerEntity ThePlayer;
    public SimpleSpriteAnimator PowerUpSpriteAnim;

    public Buff PowerUpBuff;

    //public float BuffDuration = 5f;
    //public string StatToIncrease = "Strength";
    //public float AmountToIncrease = 50f;
    //public Color BuffAnimColor = Color.red;

    public override void Initalize(PlayerEntity P)
    {
        ThePlayer = P;
    }


    public override void TriggerAbility()
    {
        ThePlayer.StartCoroutine(PowerUp());
    }

    IEnumerator PowerUp()
    {
        ThePlayer.EntityAnimator.SetBool("Ability1Trigger", true);
        ThePlayer.EntityAnimator.SetBool("BarbPoweringUp", true);

        if(PowerUpSpriteAnim != null) ThePlayer.EntityEffectHandler.SetEffectAnimation(PowerUpSpriteAnim);

        ThePlayer.ApplyBuff(PowerUpBuff);
        ThePlayer.CanMove = false;
        yield return new WaitForSeconds(0.5f);
        ThePlayer.CanMove = true;
        ThePlayer.PlayerSpriteRend.color = Color.white;
        ThePlayer.EntityAnimator.SetBool("Ability1Trigger", false);
        ThePlayer.EntityAnimator.SetBool("BarbPoweringUp", false);


        yield break;

    }


}
