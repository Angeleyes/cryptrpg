﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilites/DashAbility")]
public class DashAbility : Ability
{
    PlayerEntity ThePlayer;
    public float DashDistance = 5f;
    public int DashImpactDamage = 100;
    public ParticleSystem DashEffect;
    public float DashSpeed = 1f;

    //IEnumerator DashCoroutine;
    internal Vector3 WorldDashPoint = new Vector3();
    internal MonsterEntity HitMonster;
    internal ContactFilter2D ContactFilter = new ContactFilter2D();

    public override void Initalize(PlayerEntity player)
    {
        ThePlayer = player;
        HitMonster = null;
        ContactFilter.SetLayerMask(LayerMask.NameToLayer("Default"));
        ContactFilter.SetLayerMask(LayerMask.NameToLayer("RoomTileMap"));
    }

    public override void TriggerAbility()
    {
        DoDashFunc();
    }

    private Vector3 GetRayPoint()
    {

        Vector3 PlayerDir = ThePlayer.MoveMotor.LastMove.normalized;

        if (PlayerDir == Vector3.zero) PlayerDir = Vector2.one;

        PlayerDir.z = 0;
        Vector3 PlayerPos = ThePlayer.GetEntityPosition();
        PlayerPos.z = 0;

        
        RaycastHit2D hit = Physics2D.Raycast(PlayerPos, PlayerDir, DashDistance, 1<<9); //should add a laymask argument to this so I dont have to turn off player collision, test later

        Debug.DrawRay(PlayerPos, PlayerDir*DashDistance, Color.cyan, 3f);


        if (hit.collider != null && hit.collider.gameObject.GetComponent<PlayerEntity>() == null && hit.collider.isTrigger == false)
        {
            MonsterEntity M = hit.collider.gameObject.GetComponent<MonsterEntity>();

            if (M != null)
            {
                HitMonster = M;
            }

            Vector3 DashHitPoint = Vector3.Distance(hit.point, PlayerPos) * PlayerDir;
            DashHitPoint = ShaveVector(DashHitPoint, 0.2f);
            Debug.Log("dash hit point: " + DashHitPoint);
            return DashHitPoint; //return the float value of distance to the collision point times the player dir to put in vector form
        }
        else
        {
            return PlayerDir * DashDistance;
        }

    }

    private Vector3 ShaveVector(Vector3 V, float reduction)
    {
        //might need to shave more off in the y axis cause of lerp and player col box being too big
        Vector3 ShavedVector = V;
        ShavedVector.Normalize();

        ShavedVector *= reduction;

        ShavedVector = V - ShavedVector;
        //Debug.Log("dif reduction:" + ShavedVector); //this one works
        return ShavedVector;
    }

    private void TogglePlayerCollisions(bool toggle)
    {
        ThePlayer.OffhandPosition.gameObject.SetActive(toggle);
        ThePlayer.WeaponPosition.gameObject.SetActive(toggle);
        ThePlayer.EntityCollider.enabled = toggle;
    }

    private void DoDashFunc()
    {
        ThePlayer.StartCoroutine(AnimateDash());
    }

    IEnumerator AnimateDash()
    {
        ThePlayer.CanMove = false;
        ThePlayer.EntityEffectHandler.AddParticle(DashEffect, 0);
        Vector3 DashPoint = GetRayPoint();
        DashPoint = ThePlayer.GetEntityPosition() + DashPoint;
        float timer = 0;

        //Debug.Log("animating dash");
        ThePlayer.EntityAnimator.SetBool("Ability1Trigger", true);
        ThePlayer.EntityAnimator.SetBool("BarbStomping", true);

        if(HitMonster != null)
        {
            HitMonster.TakeDamage(DashImpactDamage, ThePlayer, AttackHitBox.DamageSpec.PhysicalDamage);
            ThePlayer.BasicAttack();
            HitMonster = null;
        }

        while ((ThePlayer.GetEntityPosition() - (new Vector3(DashPoint.x, DashPoint.y, ThePlayer.GetEntityPosition().z))).sqrMagnitude > 0.1f*0.1f)
        {
            if (timer > 0.7f || (DashPoint - ThePlayer.GetEntityPosition()).sqrMagnitude < 0.2f*0.2f)
            {
                ThePlayer.EntityAnimator.SetBool("Ability1Trigger", false);
                ThePlayer.EntityAnimator.SetBool("BarbStomping", false);
                ThePlayer.CanMove = true;
                yield break;
            }
            ThePlayer.SetEntityPosition(Vector3.Lerp(ThePlayer.GetEntityPosition(), new Vector3(DashPoint.x, DashPoint.y, ThePlayer.GetEntityPosition().z), DashSpeed * Time.deltaTime));
            timer += Time.deltaTime;

            yield return null;
        } // end of while

        ThePlayer.EntityAnimator.SetBool("Ability1Trigger", false);
        ThePlayer.EntityAnimator.SetBool("BarbStomping", false);
        ThePlayer.CanMove = true;
        yield break;
    }


}
