﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilites/ShieldAbility")]

public class ShieldAbility : Ability
{
    PlayerEntity ThePlayer;
    Offhand TheOffhand;

    public override void Initalize(PlayerEntity player)
    {
        ThePlayer = player;
        TheOffhand = player.OffHand;
    }

    public override void TriggerAbility()
    {
        
        if(TheOffhand != null) TheOffhand.Active();
    }

}
