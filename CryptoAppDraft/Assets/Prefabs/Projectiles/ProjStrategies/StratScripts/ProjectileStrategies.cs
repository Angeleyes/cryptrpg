﻿using UnityEngine;

public abstract class ProjectileStrategies : ScriptableObject
{
    public int AdditionalProjectiles;
    public abstract void ShootFunction(Projectile P, Vector3 TargetPos, float timer, ProjectileShooter Shooter);

}
