﻿using UnityEngine;

[CreateAssetMenu(menuName = "ProjStrats/POrbit")]
public class POrbit : ProjectileStrategies
{
    public float XSpread = 5f;
    public float YSpread = 5f;

    public override void ShootFunction(Projectile P, Vector3 TargetPos, float timer, ProjectileShooter Shooter)
    {

        float x = Mathf.Cos(timer) * XSpread;
        float y = Mathf.Sin(timer) * YSpread;

        Vector3 pos = new Vector3(x, y, 0);
        P.transform.position = pos + Shooter.transform.position;
    }
}
