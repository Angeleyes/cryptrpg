﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ProjStrats/PNormalShoot")]
public class PNormalShoot : ProjectileStrategies
{
    public override void ShootFunction(Projectile P, Vector3 TargetPos, float timer, ProjectileShooter Shooter)
    {
        Debug.Log("doing normal shoot with proj: " + P.name + " target pos: " + TargetPos + " and timer: " + timer + " and speed: " + P.Speed * 0.017f);
        P.gameObject.transform.position = Vector3.MoveTowards(P.gameObject.transform.position, P.TargetAim * 50, P.Speed * 0.017f);
    }
}
