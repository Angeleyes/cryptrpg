﻿using UnityEngine;

[CreateAssetMenu(menuName = "ProjStrats/PGrowOverTimeStrat")]
public class PGrowOverTime : ProjectileStrategies
{

    public float ScaleLimit = 3;
    public float InverseGrowthRate = 5;

    public override void ShootFunction(Projectile P, Vector3 TargetPos, float timer, ProjectileShooter Shooter)
    {
        if (P.transform.localScale.magnitude < P.OriginScale.magnitude * ScaleLimit) P.transform.localScale += new Vector3(timer / InverseGrowthRate, timer / InverseGrowthRate, 0);
    }
}
