﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New ItemData/Armor Item")]
public class ArmorItemData : ItemDataContainer
{
    public ScriptableGearSprites GearSpritesData;
}
