﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GearSpriteList")]
public class ScriptableGearSprites : ScriptableObject {

    public Sprite SpriteToReassignDown;
    public Sprite SpriteToReassignUp;
    public Sprite SpriteToReassignLeft;
    public Sprite SpriteToReassignRight;

    public int SpriteToInsertIndex = 0;
    public Sprite SpriteToInsert;

    public List<Sprite> IdleDown;
    public List<Sprite> IdleUp;
    public List<Sprite> IdleLeft;
    public List<Sprite> IdleRight;

    public List<Sprite> WalkDown = new List<Sprite>(4);
    public List<Sprite> WalkUp = new List<Sprite>(4);
    public List<Sprite> WalkLeft = new List<Sprite>(4);
    public List<Sprite> WalkRight = new List<Sprite>(4);

    public List<Sprite> DownAttacks = new List<Sprite>(4);
    public List<Sprite> UpAttacks = new List<Sprite>(4);
    public List<Sprite> LeftAttacks = new List<Sprite>(4);
    public List<Sprite> RightAttacks = new List<Sprite>(4);


    public List<Sprite> Special1Down = new List<Sprite>(4);
    public List<Sprite> Special1Up = new List<Sprite>(4);
    public List<Sprite> Special1Left = new List<Sprite>(4);
    public List<Sprite> Special1Right = new List<Sprite>(4);

    public List<Sprite> Special2Down = new List<Sprite>(4);
    public List<Sprite> Special2Up = new List<Sprite>(4);
    public List<Sprite> Special2Left = new List<Sprite>(4);
    public List<Sprite> Special2Right = new List<Sprite>(4);


    public List<Sprite> DownAttacks2 = new List<Sprite>(4);
    public List<Sprite> UpAttacks2 = new List<Sprite>(4);
    public List<Sprite> LeftAttacks2  = new List<Sprite>(4);
    public List<Sprite> RightAttacks2 = new List<Sprite>(4);


    public List<Sprite> DownAttacks3 = new List<Sprite>(4);
    public List<Sprite> UpAttacks3 = new List<Sprite>(4);
    public List<Sprite> LeftAttacks3 = new List<Sprite>(4);
    public List<Sprite> RightAttacks3 = new List<Sprite>(4);


    public enum ListChoice
    {
        Downs = 0,
        Ups = 1,
        Lefts = 2,
        Rights = 3,
    }

    public ListChoice ListType;

    internal Dictionary<ListChoice, List<List<Sprite>>> ListLookUp = new Dictionary<ListChoice, List<List<Sprite>>>();
    //INSTEAD OF ASSIGNING EACH FRAME FOR A NEW PIECE OF ARMOR SPRITE, MAKE A FUNCTION TO REASSIGN THEM ALL



    //this function is for convience instead of having to drag each frame to the slot in the inspector
    public void ReassignSprites()
    {
        List<List<Sprite>> Downs = new List<List<Sprite>>() { IdleDown, WalkDown, DownAttacks, Special1Down, Special2Down, DownAttacks2, DownAttacks3 };
        List<List<Sprite>> Ups = new List<List<Sprite>>() { IdleUp, WalkUp, UpAttacks, Special1Up, Special2Up, UpAttacks2, UpAttacks3 };
        List<List<Sprite>> Lefts = new List<List<Sprite>>() { IdleLeft, WalkLeft, LeftAttacks, Special1Left, Special2Left, LeftAttacks2, LeftAttacks3 };
        List<List<Sprite>> Rights = new List<List<Sprite>>() { IdleRight, WalkRight, RightAttacks, Special1Right, Special2Right, RightAttacks2, RightAttacks3 };

        ListLookUp.Add(ListChoice.Downs, Downs);
        ListLookUp.Add(ListChoice.Ups, Ups);
        ListLookUp.Add(ListChoice.Lefts, Lefts);
        ListLookUp.Add(ListChoice.Rights, Rights);

        foreach(KeyValuePair<ListChoice, List<List<Sprite>>> K in ListLookUp)
        {
            foreach(List<Sprite> S in K.Value)
            {
                S[0] = SpriteToReassignDown;
            }
        }
    }

    internal List<List<Sprite>> GearSpriteFrameList = new List<List<Sprite>>();

    public void SetupGearList()
    {
        GearSpriteFrameList.Insert(0, IdleDown);
        GearSpriteFrameList.Insert(1, IdleUp);
        GearSpriteFrameList.Insert(2, IdleLeft);
        GearSpriteFrameList.Insert(3, IdleRight);


        GearSpriteFrameList.Insert(4, WalkDown);
        GearSpriteFrameList.Insert(5, WalkUp);
        GearSpriteFrameList.Insert(6, WalkLeft);
        GearSpriteFrameList.Insert(7, WalkRight);


        GearSpriteFrameList.Insert(8, DownAttacks);
        GearSpriteFrameList.Insert(9, UpAttacks);
        GearSpriteFrameList.Insert(10, LeftAttacks);
        GearSpriteFrameList.Insert(11, RightAttacks);


        GearSpriteFrameList.Insert(12, Special1Down);
        GearSpriteFrameList.Insert(13, Special1Up);
        GearSpriteFrameList.Insert(14, Special1Left);
        GearSpriteFrameList.Insert(15, Special1Right);


        GearSpriteFrameList.Insert(16, Special2Down);
        GearSpriteFrameList.Insert(17, Special2Up);
        GearSpriteFrameList.Insert(18, Special2Left);
        GearSpriteFrameList.Insert(19, Special2Right);





        if (DownAttacks2.Count > 0)
        {


            GearSpriteFrameList.Insert(20, DownAttacks2);
            GearSpriteFrameList.Insert(21, UpAttacks2);
            GearSpriteFrameList.Insert(22, LeftAttacks2);
            GearSpriteFrameList.Insert(23, RightAttacks2);


            GearSpriteFrameList.Insert(24, DownAttacks3);
            GearSpriteFrameList.Insert(25, UpAttacks3);
            GearSpriteFrameList.Insert(26, LeftAttacks3);
            GearSpriteFrameList.Insert(27, RightAttacks3);


        }
    }

}
