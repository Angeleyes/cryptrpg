﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System;

[RequireComponent(typeof(SpriteRenderer))]
public class Item : MonoBehaviour {

    internal int ItemID { get; set; }
    internal string ItemHash = " ";
    //public Sprite ItemIcon = null;
    [TextArea]
    internal string ItemDescription;
    internal string ItemName;
    internal SpriteRenderer Renderer;
    internal Sprite ItemSprite;
    internal Sprite ItemIcon;
    internal Color SpriteColorTint;
    internal StringBuilder Sb = new StringBuilder();
    internal PlayerEntity ItemOwner;

    internal int WordsBeforeNewLine = 5;

    public virtual void Awake()
    {       
        Renderer = GetComponent<SpriteRenderer>();
        Debug.Log("inside item awak with a null rend?:" + (Renderer == null));
        SpriteColorTint = Renderer.color;
        ItemIcon = Renderer.sprite;
        //This is set in the roll item function of equipableitem.cs
    }

    protected void ProcessItemDesc()
    {
        if (ItemDescription == "") return;
        StringBuilder NewString = new StringBuilder();
        string[] P = ItemDescription.Split(' ');

        NewString.Append(Environment.NewLine);
        NewString.Append("<color=yellow>");
        for (int i = 0; i < P.Length; i++)
        {
            NewString.Append(P[i]);
            NewString.Append(" ");
            if ((i + 1) % 5 == 0) NewString.Append(Environment.NewLine);
        }

        //Debug.Log("new string is: " + NewString.ToString());
        NewString.Append("</color>");
        ItemDescription = NewString.ToString();
    }

    public virtual string GetItemData()
    {
        Sb.Length = 0;
        //Sb.Append(this.name);
        Sb.Append(Environment.NewLine);
        Sb.Append("Description: ");
        Sb.Append(this.ItemDescription);
        Sb.Append(Environment.NewLine);

        return Sb.ToString();
    }

    //the use on an item needs inventory context, equipment slot or emblem slot or some kind of bank slot
    public virtual void Use(Entity User)
    {
        Debug.Log("using " + this.name);
    }

    protected virtual void SetLootData(ItemDataContainer ItemData)
    {
        Debug.Log("calling item loot data");
        ItemID = ItemData.ItemDataID;
        ItemDescription = ItemData.ItemDataDescription;
        ItemSprite = ItemData.ItemDataSprite;
        ItemName = ItemData.ItemDataName;
    }

    //public virtual void SpawnItem(ItemDataContainer ItemData)
    //{
    //    SetLootData(ItemData);
    //    gameObject.AddComponent<Pickup>();
    //}


    //public virtual void OnMouseHover()
    //{
    //    //show info box
    //}

}
