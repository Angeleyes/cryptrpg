﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : EquipableItem {


    //special properties
    internal Vector2 WeaponDamageRange = new Vector2();
    internal float WeaponSpeed = 1.0f;

    public override string GetItemData()
    {
        Debug.Log("getting weapon item data");
        string AppendedData = base.GetItemData();

        Sb.Length = 0;
        Sb.AppendLine(ItemName);
        Sb.Append("Damage: ");
        Sb.Append(WeaponDamageRange.x);
        Sb.Append("-");
        Sb.Append(WeaponDamageRange.y);
        Sb.Append("    Speed: ");
        Sb.Append(WeaponSpeed);
        Sb.Append(System.Environment.NewLine);
        Sb.Append("(DPS): ");
        Sb.AppendLine(((WeaponDamageRange.y / WeaponSpeed)).ToString());
        Sb.Append(AppendedData);
        //Sb.Insert(0, WeaponDamageRange.x + "-" + WeaponDamageRange.y + System.Environment.NewLine + "Speed: " + WeaponSpeed + "(DPS: " + WeaponDamageRange.y/WeaponSpeed);
        return Sb.ToString();
    }

    public override void RollItem(ItemDataContainer item, int TableCoefficient)
    {
        Debug.Log("inside weapon roll item");

        //roll weapon range
        WeaponItemData WeaponData = item as WeaponItemData;
        //EquipmentStats["AttackSpeed"] = new Stat((item as WeaponItemData).WeaponSpeed);
        WeaponSpeed = WeaponData.WeaponSpeed;
        float Min2 = WeaponData.WeaponDamageMin * 1.2f;
        int Min = (int)Random.Range(WeaponData.WeaponDamageMin, Min2 + 1);
        float Max2 = WeaponData.WeaponDamageMax * 1.2f;
        int Max = (int)Random.Range(WeaponData.WeaponDamageMax, Max2 + 1);
        WeaponDamageRange = new Vector2(Min, Max); //maybe roll, yes roll
        base.RollItem(item, TableCoefficient);
    }

}
