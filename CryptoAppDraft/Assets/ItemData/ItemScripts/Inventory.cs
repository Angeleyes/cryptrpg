﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {


    //public static Inventory PlayerInventory;

    public List<Item> ListOfItems;
    public int MaxCapacity = 20;

    public delegate void OnItemChanged();
    public OnItemChanged OnItemChangedCallback;

    private PlayerEntity InventoryOwner;
    private InventoryUI InventoryInterface;

    public void Awake()
    {
        //if (PlayerInventory == null) PlayerInventory = this;
    }

    public bool AddItem(Item I)
    {
        if (ListOfItems.Count >= MaxCapacity) return false;
        ListOfItems.Add(I);
        if(OnItemChangedCallback != null) OnItemChangedCallback.Invoke();
        return true;
    }

    public void RemoveItem(Item I)
    {
        if (!ListOfItems.Contains(I))
        {
            Debug.Log("no item to remove");
            return;
        }
        ListOfItems.Remove(I);
        if (OnItemChangedCallback != null) OnItemChangedCallback.Invoke();
    }

    public void SetUI(InventoryUI UI)
    {
        InventoryInterface = UI;
        OnItemChangedCallback += InventoryInterface.UpdateUI;
        InventoryInterface.SetInventoryRef(this);
    }


}
