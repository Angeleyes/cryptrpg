﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class SpellEffect : ScriptableObject
{

    public abstract void DoEffect();

}
