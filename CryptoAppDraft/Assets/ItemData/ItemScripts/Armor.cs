﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : EquipableItem {

    public bool IsBarbArmor()
    {
        if (GearSprites.DownAttacks2.Count > 0) return true;
        else return false;
    }


    public override void RollItem(ItemDataContainer item, int TableCoefficient)
    {
        Debug.Log("inside armor roll item");
        base.RollItem(item, TableCoefficient);
    }
}
