﻿using UnityEngine;
public class UsableItem : Item {

    public Buff BuffEffect;


    public override void Use(Entity User)
    {
        Debug.Log("in using usable item :" + this.name);
        base.Use(User);

        User.ApplyBuff(BuffEffect);

        PlayerEntity P = User as PlayerEntity;
        P.PlayerInventory.RemoveItem(this);

    }

    public override string GetItemData()
    {
        string data = base.GetItemData();

        Sb.Length = 0;
        Sb.Append(data);
        Sb.Append(System.Environment.NewLine);
        if (BuffEffect != null) Sb.Append(BuffEffect.BuffToolTip);

        return Sb.ToString();
    }

}
