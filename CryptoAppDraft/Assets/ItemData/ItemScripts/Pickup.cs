﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider2D))]
public class Pickup : MonoBehaviour {

    //public AudioClip PickUpSound;
    //public AudioSource PickUpSoundFX;
    //public AudioClip DropSound;

    internal Item TheItem;

    //public OnHitEffect Hit;
    //ParticleSystem ParticleDummy;

    //public event Action PickedMeUp;
    internal Collider2D BoxTrigger;

    public void Start()
    {
        //Debug.Log("starting pickup item " + this.name);
        TheItem = GetComponent<Item>();
        //DropMe += MonsterAI.DroppingLoot;
        BoxTrigger = GetComponent<Collider2D>();

    }

    public void Awake() // on drop
    {
        OnDrop();
    }

    public virtual void OnDrop()
    {
        //Debug.Log("im " + this.gameObject.name + "..have been dropped");
        //ondropanim.soundfx.play(DropSound); will just do on awake in anim script
    }

    public virtual void OnPickUp(PlayerEntity PlayerThatisPickingUp)
    {
        //send data to inventory
        //pickupanim.soundfx.play(pickupsound);
        bool PickedUp = PlayerThatisPickingUp.PlayerInventory.AddItem(TheItem);
       
        if (PickedUp)
        {
            TheItem.ItemOwner = PlayerThatisPickingUp;
            //if(Hit != null) ParticleDummy = Instantiate(Hit.ParticleEffect, transform.position, Quaternion.identity);
            Invoke("EndLife", 1f);
            gameObject.SetActive(false);
        }
    }

    public void EndLife()
    {
        //Destroy(gameObject);
        
        //Destroy(ParticleDummy.gameObject);
        BoxTrigger.enabled = false;
        Destroy(this.GetComponent<Pickup>());
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            PlayerEntity P = collision.gameObject.GetComponent<PlayerEntity>();
            OnPickUp(P);
        }
    }

}
