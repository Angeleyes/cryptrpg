﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
[RequireComponent(typeof(SpriteRenderer))]
public abstract class EquipableItem : Item {

    internal Entity Owner;

    internal bool IsEquipped = false;

    public Action OnEquipAction;

    internal SlotType ItemType;

    internal Stat AttackPower;
    internal Stat SpellPower;

    internal Stat Armor;
    internal Stat Strength;
    internal Stat Intelligence;
    internal Stat Agility;

    internal Stat CritChance;
    internal Stat AttackSpeed;
    internal Stat LifeSteal;
    internal Stat ResourceRegen;
    internal Stat HealthRegen;
    internal Stat HealthMax;
    //internal Stat Resource;
    internal Stat ResourceMax;

    private List<string> StatNames = new List<string>() { "HealthMax", "ResourceMax", "Strength", "Intelligence", "Agility",
        "SpellPower", "AttackPower", "AttackSpeed", "Armor", "LifeSteal", "ResourceRegen"};

    //public List<ItemEffect> SpecialEffects; //maybe not a list

    internal Dictionary<string, Stat> EquipmentStats = new Dictionary<string, Stat>();

    internal Aura AuraEffect = null;

    //public SpellEffect Effect1, Effect2, Effect3, Effect4;

    internal ScriptableGearSprites GearSprites;

    public override void Awake()
    {
        base.Awake();
        InitEquipableItem();
    }

    internal void InitEquipableItem()
    {

        EquipmentStats.Add("HealthMax", HealthMax);
        //EquipmentStats.Add("Resource", Resource);
        EquipmentStats.Add("ResourceMax", ResourceMax);
        EquipmentStats.Add("Strength", Strength);
        EquipmentStats.Add("Intelligence", Intelligence);
        EquipmentStats.Add("Agility", Agility);
        EquipmentStats.Add("AttackPower", AttackPower);
        EquipmentStats.Add("SpellPower", SpellPower);
        EquipmentStats.Add("AttackSpeed", AttackSpeed);
        EquipmentStats.Add("Armor", Armor);
        //EquipmentStats.Add("CriticalHitChance", CritChance);
        EquipmentStats.Add("LifeSteal", LifeSteal);
        EquipmentStats.Add("ResourceRegen", ResourceRegen);

        Debug.Log("finished init stat dict with count: " + EquipmentStats.Count);
    }

    public virtual void FinishUnEquip()
    {

    }

    public Dictionary<string, Stat> GetStats() //make const or read only
    {
        return EquipmentStats;
    }

    public Stat GetSpecificStat(string StatName) //read only?
    {
        Stat s = null;
        EquipmentStats.TryGetValue(StatName, out s);
        return s;
    }

    private void IncreaseStat(string StatName, float value)
    {
        EquipmentStats[StatName].AddModifier(value);
    }

    private void AddStat(string StatName, float value)
    {
        EquipmentStats[StatName] = new Stat(value);
    }

    private void AddRandomStat(int value)
    {
        int randomindex = UnityEngine.Random.Range(0, EquipmentStats.Count);
        //if (EquipmentStats != null && EquipmentStats.Count > 0) Debug.Log("passed tests and chekcing for stat: " + StatNames[randomindex]);
        EquipmentStats[StatNames[randomindex]] = new Stat(value);
        Debug.Log("added stat: " + StatNames[randomindex] + " with value: " + value);
    }
    public override string GetItemData()
    {
        Sb.Length = 0;
        //Sb.Append(this.name);

        foreach(KeyValuePair<string, Stat> S in EquipmentStats)
        {
            if (S.Value != null)
            {
                if (S.Value.GetValue() > 0)
                {
                    Sb.Append(Environment.NewLine);
                    Sb.Append(" + ");
                    Sb.Append(S.Value.GetValue());
                    Sb.Append(" ");
                    Sb.Append(S.Key);
                    //s += "\n" + " + " + S.Value.GetValue() + " " + S.Key;
                }
            }
        }
        Sb.Append(Environment.NewLine);
        if (AuraEffect != null)
        {
            Sb.AppendLine(AuraEffect.name);
            Sb.AppendLine(AuraEffect.AuraTipData);        
        }

        
        Sb.AppendLine(ItemDescription);

        return Sb.ToString();
    }


    //current thinking on this, instead of having a use function override, have delegate in the item class
    //the delegate will get its use function reassigned based on what type of inventory ui is open, char sheet, bank, crafting ect...
    public override void Use(Entity User)
    {   
        OnEquipAction.Invoke();
    }


    public virtual void RollItem(ItemDataContainer item, int TableCoefficient)
    {
        //EquipableItem E = null;
        Debug.Log("inside equip roll item");



        if (item.ItemDataAura != null) AuraEffect = item.ItemDataAura;

        ItemDescription = item.ItemDataDescription;
        ItemID = item.ItemDataID;
        ItemName = item.ItemDataName;
        Renderer.sprite = item.ItemDataSprite;
        ItemType = item.ItemDataType;
        ItemSprite = item.ItemDataSprite;
        ItemIcon = ItemSprite;      

        ProcessItemDesc();
        //internal Color SpriteColorTint;

        int EnchantNum = UnityEngine.Random.Range(1, item.RandomEnchantments);
        int RandomStatValue = 0;
        //remove int/spell power for barbs and strength and attack power for witches here maybe
        //int coefficient = (int)(item.ItemQualityCoefficient * TableCoefficient);
        int RangeMin = 0;
        int RangeMax = 0;

        if(item.CustomStats.Count > 0)
        {
            foreach(string Statname in item.CustomStats)
            {
                RangeMin = (int)(TableCoefficient + item.ItemQualityCoefficient);
                RangeMax = (int)Mathf.Round((RangeMin * 1.8f)); //1.8 magic number for testing
                RandomStatValue = UnityEngine.Random.Range(RangeMin, RangeMax);
                AddStat(Statname, RandomStatValue);

            }
        }


        for (int i = 0; i < EnchantNum; i++)
        {
            RangeMin = (int)(TableCoefficient + item.ItemQualityCoefficient);
            RangeMax = (int)Mathf.Round((RangeMin * 1.8f)); //1.8 magic number for testing

            RandomStatValue = UnityEngine.Random.Range(RangeMin, RangeMax);
            AddRandomStat(RandomStatValue);
        }

    }


    //public override void SpawnItem(ItemDataContainer ItemData)
    //{
    //    //Debug.Log("inside equip spawn item");
    //    //ArmorItemData ArmorData;
    //    //EmblemItemData EmblemData;
    //    //if (ItemData is WeaponItemData)
    //    //{
    //    //    WeaponItemData WeaponData = ItemData as WeaponItemData;
    //    //    Instantiate(this.gameObject as Weapon, Vector3.zero, Quaternion.identity);
    //    //}
    //    //else if (ItemData is EmblemItemData) EmblemData = ItemData as EmblemItemData;
    //    //else ArmorData = ItemData as ArmorItemData;


    //    //assign stats and stuff here

    //    base.SpawnItem(ItemData);
    //}

}
