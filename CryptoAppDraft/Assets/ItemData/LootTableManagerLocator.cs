﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTableManagerLocator 
{
    static LootTableManager Service;

    public static LootTableManager GetLootManagerService() { return Service; }
    public static void Provide(LootTableManager service) { Service = service; }
}
