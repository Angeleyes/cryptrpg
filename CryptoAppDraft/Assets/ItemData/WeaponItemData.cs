﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="New ItemData/Weapon Item")]

public class WeaponItemData : ItemDataContainer
{
    public ScriptableGearSprites GearSpritesData;

    public float WeaponDamageMin = 1.0f;
    public float WeaponDamageMax = 3.0f;

    public float WeaponSpeed = 1.5f;
}
