﻿
using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "LootTable")]
public class LootTable : ScriptableObject
{
    [SerializeField]
    private List<ItemDataContainer> ItemList = new List<ItemDataContainer>();

    [SerializeField]
    private float TableQualityCoefficient = 1.0f;

    internal List<ItemDataContainer> GetListCopy()
    {
        return ItemList;
    }

    internal void SetTableQuality(float Coefficient)
    {
        TableQualityCoefficient = Coefficient;
    }
    internal float GetTableChanceSum()
    {
        float result = 0f;

        foreach(ItemDataContainer I in ItemList)
        {
            result += I.ItemDropChance;
        }

        return result;
    }


    internal int GetTableCountSum()
    {
        return ItemList.Count;
    }
}
