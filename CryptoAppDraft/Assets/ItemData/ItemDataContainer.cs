﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "NewItemData")]
public abstract class ItemDataContainer : ScriptableObject
{
    public string ItemDataName;
    [TextArea]
    public string ItemDataDescription;
    public Sprite ItemDataSprite;
    public SlotType ItemDataType;
    public float ItemDropChance;
    public int ItemDataID;
    [Tooltip("Number of random enchantment slots this item can have")]
    public int RandomEnchantments = 1;
    [Tooltip("This number is multiplied by the min/max range that this item can roll\n" + "Default range rolls are 1-3")]
    public float ItemQualityCoefficient = 1.0f;

    public List<string> CustomStats = new List<string>();

    public Aura ItemDataAura;

    public enum ClassTypeEnum
    {
        Barbarian = 0,
        Witch = 1,
        Archer = 2,
    }

    public ClassTypeEnum ClassType;

    public float GetPercentDrop()
    {
        return ItemDropChance * 0.01f;
    }

    public float GetPercentDrop(float PlusModifier)
    {
        float chance = (ItemDropChance + PlusModifier);
        if (chance > 100) chance = 100;
        return chance * 0.01f;
    }
}
