﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTableManager : MonoBehaviour
{
    //table index 0 is always main generic table
    //table index 2 is always somewhat special loot table connected to the dungeon type
    //table index 3-x is extra stuff
    public Armor ArmorObject;
    public Weapon WeaponObject;
    public Emblem EmblemObject;

    public int DungeonCoefficient = 1;
    
    public List<LootTable> LootTables = new List<LootTable>();
    public LootTable GenericsLootTable; //for like gold and health pots and stuff
    public ItemDataContainer TestItem;
    public bool DebugTesting = false;
    public int SimTests = 10000;

    private Queue<LootRequest> LootRequestQue = new Queue<LootRequest>();
    
    private System.Diagnostics.Stopwatch Timer = new System.Diagnostics.Stopwatch();

    GameObject LootParent;
    //private int ChanceModifier = 2; //based on monster dop chance, at 5% it cuts all drop chances in half so you gota multiply by 2 to make it correct again

    private void Start()
    {
        LootParent = new GameObject();
        LootParent.transform.position = Vector3.zero;
        LootParent.name = "LootParent";     
    }

    public void TestRoulette()
    {
        Timer.Start();
        float FitnessSum = 0;
        foreach(ItemDataContainer I in LootTables[0].GetListCopy())
        {
            FitnessSum += I.ItemDropChance;
        }

        Debug.Log("using fitness sum: " + FitnessSum);
        if (TestItem != null && DebugTesting)
        {
            int id = 0;
            int Hit = 0;
            int Miss = 0;
            int NegMiss = 0;
            int Other = 0;
            int Other2 = 0;
            float sims = SimTests;
            ItemDataContainer theitem = null;
            Debug.Log("checking for test item id: " + TestItem.ItemDataID + " with a drop chance of : " + TestItem.ItemDropChance);
            for (int i = 0; i < SimTests; i++)
            {
                //if (Random.Range(0, 101) <= MobDropChance)
                //{
                theitem = SelectOne(LootTables[0].GetListCopy());
                if (theitem == null) NegMiss++;
                else if (theitem.ItemDataID == TestItem.ItemDataID) Hit++;

 
                //}
                //else Miss++;
            }
            Timer.Stop();
            Debug.Log("Time: " + Timer.Elapsed);
            Debug.Log("item was found: " + Hit + " times, a percentage of " + (Hit / sims) * 100.00f + "%");
            //Debug.Log("Misses: " + Miss + "  NegMiss: " + NegMiss + "  Other hits: " + Other +  " other percent: " + (Other/sims)*100.00f + "%");
            //Debug.Log("Other2: " + Other2 + "  " + (Other2/sims)*100.00f + "%");

            if (theitem == null) return;

            //could do a factory pattern here, but it seems unneccessary
            EquipableItem DroppedItem = null;
            if (theitem != null && theitem.ItemDataID == TestItem.ItemDataID)
            {
                Debug.Log("item id found");
                switch (theitem.ItemDataType)
                {
                    case SlotType.Weapon:
                        DroppedItem = Instantiate(WeaponObject, Vector3.zero, Quaternion.identity, LootParent.transform);
                        Debug.Log("creating weapon");
                        break;
                    case SlotType.Emblem:
                        DroppedItem = Instantiate(EmblemObject, Vector3.zero, Quaternion.identity, LootParent.transform);
                        break;
                    default:
                        DroppedItem = Instantiate(ArmorObject, Vector3.zero, Quaternion.identity, LootParent.transform);
                        Debug.Log("creating armor");
                        break;
                }

                Debug.Log("preparing to drop item");
                DroppedItem.RollItem(theitem, DungeonCoefficient);
            }

        }
    }


    public bool RequestLoot(LootRequest TheRequest)
    {
        LootRequestQue.Enqueue(TheRequest);
        //ProcessLootRequest(LootRequestQue.Dequeue());

        ItemDataContainer ItemData = null;
        LootTable TableTemp;
        List<ItemDataContainer> MainList = new List<ItemDataContainer>();

        for (int i = 0; i < TheRequest.TableIDs.Count; i++)
        {
            TableTemp = LootTables[TheRequest.TableIDs[i]];
            MainList.AddRange(TableTemp.GetListCopy());
        }

        //creates a big list of all the tables
        if (TheRequest.CustomLootTable != null)
        {
            MainList.AddRange(TheRequest.CustomLootTable.GetListCopy());
        }


        ItemData = SelectOne(MainList);

        if (ItemData == null) return false;

        EquipableItem DroppedItem = null;
        if (ItemData != null)
        {
            switch (ItemData.ItemDataType)
            {
                case SlotType.Weapon:
                    DroppedItem = Instantiate(WeaponObject, TheRequest.DropLocation, Quaternion.identity, LootParent.transform);
                    Debug.Log("creating weapon");
                    break;
                case SlotType.Emblem:
                    DroppedItem = Instantiate(EmblemObject, TheRequest.DropLocation, Quaternion.identity, LootParent.transform);
                    break;
                default:
                    DroppedItem = Instantiate(ArmorObject, TheRequest.DropLocation, Quaternion.identity, LootParent.transform);
                    Debug.Log("creating armor");
                    break;
            }

            DroppedItem.RollItem(ItemData, DungeonCoefficient);
        }

        return true;
    }

    //public int RequestLoot(LootRequest TheRequest)
    //{
    //    //Item TheItem = null;
    //    ItemDataContainer ItemData = null;
    //    LootTable TableTemp;
    //    List<ItemDataContainer> MainList = new List<ItemDataContainer>();
    //    List<ItemDataContainer> PotentialItemList = new List<ItemDataContainer>();

    //    float ChanceFitnessCount = 0;

    //    //collect all the tables items into one big list
    //    for (int i = 0; i < TheRequest.TableIDs.Count; i++)
    //    {
    //        TableTemp = LootTables[TheRequest.TableIDs[i]];
    //        MainList.AddRange(TableTemp.GetListCopy());
    //        ChanceFitnessCount += TableTemp.GetTableChanceSum();
    //    }
    //    //Debug.Log("using fitness sum: " + ChanceFitnessCount);

    //    //creates a big list of all the tables
    //    if (TheRequest.CustomLootTable != null)
    //    {
    //        MainList.AddRange(TheRequest.CustomLootTable.GetListCopy());
    //        ChanceFitnessCount += TheRequest.CustomLootTable.GetTableChanceSum();
    //    }


    //    //ItemData = RollRoulette(ChanceFitnessCount, MainList, TheRequest);

    //    //NEED TO TEST WITH MERGED TABLES, then with passed in tables from loot request 

    //    //EquipableItem DroppedItem = null;

    //    //switch (ItemData.ItemDataType)
    //    //{
    //    //    case SlotType.Weapon: DroppedItem = Instantiate(WeaponObject, TheRequest.DropLocation, Quaternion.identity, LootParent.transform);
    //    //        break;
    //    //    case SlotType.Emblem: DroppedItem = Instantiate(EmblemObject, TheRequest.DropLocation, Quaternion.identity, LootParent.transform);
    //    //        break;
    //    //    default: DroppedItem = Instantiate(ArmorObject, TheRequest.DropLocation, Quaternion.identity, LootParent.transform);
    //    //        break;
    //    //}

    //    //DroppedItem.RollItem(ItemData, DungeonCoefficient);


    //    return ItemData.ItemDataID;

    //}


    //public ItemDataContainer RollRoulette(float FitnessSum, List<ItemDataContainer> ItemList, LootRequest TheRequest)
    //{
    //    //to get the item
    //    //get the sum of fitness aka chancecount
    //    //set the previous probability from 0
    //    //break the list into "groups" with a count equal to the itemlist count
        
    //    //float FitnessSum = 100;

    //    int ListCount = ItemList.Count;
    //    float PreviousProbability = 0.0f;
    //    //float ItemFitness = 0.0f;
    //    float MaxFitness = 0.0f;
    //    //make x number of groups where x is itemlist count
    //    List<float> Groups = new List<float>(ListCount);
    //    List<float> Probabilites = new List<float>();
    //    Dictionary<float, ItemDataContainer> ItemTable = new Dictionary<float, ItemDataContainer>();
    //    List<ItemDataContainer> PotentialItems = new List<ItemDataContainer>();

    //    //building the roulette list
    //    for (int i = 0; i < ListCount; i++)
    //    {
    //        float DropChance = ItemList[i].ItemDropChance;
    //        float Probability = PreviousProbability + (DropChance / FitnessSum); //how does the previous prob affect an item of the same fitness...?
    //        Groups.Add(Probability);
    //        if (ItemTable.ContainsKey(Probability)) Debug.Log("WHOA WHOA WHOA, COLLISION");
    //        else ItemTable.Add(Probability, ItemList[i]);
    //        //Debug.Log("prob: " + Probability + " with previous prob: " + PreviousProbability + " with vars: " + (ItemList[i].ItemDropChance / FitnessSum));
    //        PreviousProbability = Groups[i];
    //        //if (!Probabilites.Contains(PreviousProbability)) Probabilites.Add(PreviousProbability); //version of prob list without duplicates
    //        Probabilites.Add(PreviousProbability);
    //        if (Probability > MaxFitness) MaxFitness = Probability;
    //    }
    //    //Debug.Log("max fitness is: " + MaxFitness);
    //    //float RandomPick1 = Random.Range(0.0f, MaxFitness);
    //    float RandomPick1 = Random.Range(0.0f, 1.0f);
    //    Debug.Log("random pick = " + RandomPick1);
    //    //float RandomPick2 = Random.Range(0, 101);
    //    float ThePick = 0.0f; //all that items that have this prob number get picked and a list is made from that
    //    //Debug.Log("Max fitness: " + MaxFitness + " with RandomPick1: " + RandomPick1);
    //    PreviousProbability = 0.0f;

    //    //this part is weird
    //    for (int i = 0; i < Groups.Count; i++)
    //    {
    //        Debug.Log("group index : " + i + " is " + Groups[i] + " with probability: " + Probabilites[i]);
    //        Debug.Log("checking dif: " + (Probabilites[i] - PreviousProbability));
    //        if (RandomPick1 < Probabilites[i] - PreviousProbability)
    //        {
    //            ThePick = Groups[i];
    //            Debug.Log("choose pick: " + ThePick + ", prob was: " + Probabilites[i]);
    //            break;
    //        }
    //        //else Debug.Log("no pick, prob was: " + Probabilites[i]);
    //    }
    //    //make a list of all the items that rolled the randompick (items can have the same drop chance)

    //    Debug.Log("checking table...");
    //    foreach (KeyValuePair<float, ItemDataContainer> Pair in ItemTable)
    //    {
    //        //THIS IS ALL WRONG, WRONG WRON WRONG
    //        //I DONT WANA USE A DICT, NEED LISTS SO I CAN HAVE MORE THAN 1 ITEM WITH SAME DROP CHANCE
    //        //FITNESS A RATIO OF COUNT????
    //        int itemmod = 0;
    //        if (Pair.Value.ItemDataType == SlotType.Weapon) itemmod = TheRequest.WeaponModifier;
    //        else if (Pair.Value.ItemDataType == SlotType.Emblem) itemmod = TheRequest.EmblemModifier;
    //        else itemmod = TheRequest.ArmorModifier;
    //        //if(((item.ItemDropChance/FitnessSum) == ThePick)) PotentialItems.Add(item);
    //        Debug.Log("checking if pick : " + ThePick + " equals the key : " + Pair.Key);
    //        PotentialItems.Add(ItemTable[ThePick]);
    //    }
    //    Debug.Log("potential item list created with count: " + PotentialItems.Count + ", the item id picked was: " + PotentialItems[0].ItemDataID + " with a drop chance of: " + PotentialItems[0].ItemDropChance);
    //    //now just flip a coin to see which item gets picked
    //    int Pick = Random.Range(0, PotentialItems.Count);

    //    ItemDataContainer Choice = PotentialItems[Pick];
    //    Debug.Log("end of roulette");
    //    return Choice;

    //}

    public ItemDataContainer SelectOne(List<ItemDataContainer> Population)
    {

        //this function works better with more speed, but now i need to test:
        //1. Change in table size
        //2. along with a fitness over 100 and over 200 ect
        //3. items with the same chance and flipping a coin  (works)

        //when the fitness is over 100 negmiss = 0
        //when the fitness is under 100 negmiss > 0

        //when the fitness is over 100 and I add artificial values to it, chance goes down
        //and when I add to drop chance, chance goes down....wtf
        //THIS IS ONLY TRUE FOR ITEMS THAT SHARE THE SAME DROP CHANCE...OK GOOD

        //float FitnessSum = 0.0f;
        //float FitnessSum1 = 1.0f;
        //float FitnessSum2 = 2.0f;
        List<ItemDataContainer> PotentialItems = new List<ItemDataContainer>();

        //foreach (ItemDataContainer I in Population)
        //{
        //    FitnessSum += I.GetPercentDrop();
        //}

        float RandomPick = Random.Range(0, 1.0f);
        float Current = 0.0f;
        //Debug.Log("using fitness sum: " + FitnessSum + " with random pick: " + RandomPick);
        foreach(ItemDataContainer I in Population)
        {
            Current += I.GetPercentDrop();
            //Debug.Log("current: " + Current + " just added: " + I.GetPercentDrop() + ":" + I.ItemDataID);
            if (Current > RandomPick)
            {
                PotentialItems.Add(I);
                break;
            }
        }

        //fill the potentialitemslist with all items that have the same drop chance
        if(PotentialItems.Count > 0)
        {
            foreach(ItemDataContainer I in Population)
            {
                if (I.ItemDropChance == PotentialItems[0].ItemDropChance && I.ItemDataID != PotentialItems[0].ItemDataID) PotentialItems.Add(I);
            }
            //Debug.Log("potential items count: " + PotentialItems.Count);

            //flip a coin
            return PotentialItems[Random.Range(0, PotentialItems.Count)];
        }


        //return SelectOne....if no hits
        return null;

    }

    public Item RequestGenericLoot()
    {
        Item I = null;
        return I;
    }

}


public struct LootRequest
{
    //ScriptableClass.ClassID ClientClass;
    public LootRequest(List<int> Tableids, int SourceId, Vector3 pos, LootTable customtable)
    {
        DropLocation = pos;
        SourceID = SourceId;
        TableIDs = Tableids;
        //WeaponModifier = Modifiers.x;
        //ArmorModifier = Modifiers.y;
        //EmblemModifier = Modifiers.z;
        CustomLootTable = customtable;
    }

    readonly public LootTable CustomLootTable;
    readonly public Vector3 DropLocation;
    readonly public int SourceID;
    readonly public List<int> TableIDs;
    //readonly public int WeaponModifier;
    //readonly public int ArmorModifier;
    //readonly public int EmblemModifier;

}