﻿using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(RoomGenerator))]
public class RoomGeneratorEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        RoomGenerator Rg = target as RoomGenerator;

        if(GUILayout.Button("Generate Room"))
        {
            Rg.GenerateRoom();
        }
    }

}

[CustomEditor(typeof(ScriptableGearSprites))]
public class GearSpriteEditor: Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ScriptableGearSprites G = target as ScriptableGearSprites;

        if(GUILayout.Button("Reassign"))
        {
            G.ReassignSprites();
        }
    }
}


[CustomEditor(typeof(DungeonGenerator))]
public class DungeonGeneratorEditor: Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        DungeonGenerator Dg = target as DungeonGenerator;

        if(GUILayout.Button("Generate Dungeon"))
        {
            Dg.GenerateDungeon(null, new Vector3(0,0,0));
        }
    }
}

[CustomEditor(typeof(RoomPiece))]
public class RoomPieceEditor: Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        RoomPiece R = target as RoomPiece;

        if(GUILayout.Button("Block Joints"))
        {
            R.BlockOpenJoints();
        }

        if(GUILayout.Button("SetTile"))
        {
            R.SetDumbTile();
        }
    }
}

[CustomEditor(typeof(MonsterClosetSpawner))]
public class MonsterSpawnerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        MonsterClosetSpawner M = target as MonsterClosetSpawner;

        if (GUILayout.Button("Spawn Monster"))
        {
            M.TriggerMonseterSpawn();
        }


    }
}

[CustomEditor(typeof(LootTableManager))]
public class LootTableEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LootTableManager L = target as LootTableManager;
        if(GUILayout.Button("Simulate item drop"))
        {
            L.TestRoulette();
        }
    }
}
//[CustomEditor(typeof(GroundGrid))]
//public class GridEditor: Editor
//{
//    public override void OnInspectorGUI()
//    {
//        base.OnInspectorGUI();

//        GroundGrid G = target as GroundGrid;

//        if(GUILayout.Button("create grid"))
//        {
//            //G.CreateGrid();
//        }
//    }
//}


[CustomEditor(typeof(FractleRoomPiece))]
public class FractleRoomPieceEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        FractleRoomPiece R = target as FractleRoomPiece;

        if (GUILayout.Button("Block F Joints"))
        {
            R.BlockOpenJoints();
        }

        if(GUILayout.Button("settile"))
        {
            R.SetDumbTile();
        }
    }
}

[CustomPropertyDrawer(typeof(PreviewSpriteAttribute))]
public class PreviewSpriteDrawer : PropertyDrawer
{
    const float imageHeight = 100;

    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.ObjectReference &&
            (property.objectReferenceValue as Sprite) != null)
        {
            return EditorGUI.GetPropertyHeight(property, label, true) + imageHeight + 10;
        }
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    static string GetPath(SerializedProperty property)
    {
        string path = property.propertyPath;
        int index = path.LastIndexOf(".");
        return path.Substring(0, index + 1);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //Draw the normal property field
        EditorGUI.PropertyField(position, property, label, true);

        if (property.propertyType == SerializedPropertyType.ObjectReference)
        {
            var sprite = property.objectReferenceValue as Sprite;
            if (sprite != null)
            {
                position.y += EditorGUI.GetPropertyHeight(property, label, true) + 5;
                position.height = imageHeight;

                //GUI.DrawTexture(position, sprite.texture, ScaleMode.ScaleToFit);
                DrawTexturePreview(position, sprite);
            }
        }
    }

    private void DrawTexturePreview(Rect position, Sprite sprite)
    {
        Vector2 fullSize = new Vector2(sprite.texture.width, sprite.texture.height);
        Vector2 size = new Vector2(sprite.textureRect.width, sprite.textureRect.height);

        Rect coords = sprite.textureRect;
        coords.x /= fullSize.x;
        coords.width /= fullSize.x;
        coords.y /= fullSize.y;
        coords.height /= fullSize.y;

        Vector2 ratio;
        ratio.x = position.width / size.x;
        ratio.y = position.height / size.y;
        float minRatio = Mathf.Min(ratio.x, ratio.y);

        Vector2 center = position.center;
        position.width = size.x * minRatio;
        position.height = size.y * minRatio;
        position.center = center;

        GUI.DrawTextureWithTexCoords(position, sprite.texture, coords);
    }


}
