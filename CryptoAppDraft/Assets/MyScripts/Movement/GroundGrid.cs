﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGrid : MonoBehaviour
{

    //public LayerMask UnwalkableMask;
    public Vector2 GridWorldSize;
    public float NodeRaidus;
    public float NodeOverlapRadius;
    public float TileGap;
    public bool DebugTestGrid = true;
    Node[,] TheGrid;

    public Transform OverlapParent;
    public CircleCollider2D OverlapTool;

    internal List<CircleCollider2D> OverlapTools = new List<CircleCollider2D>();

    public bool DisplayGridGizmos = true;

    float NodeDiameter;

    int GridSizeX, GridSizeY;
    //int NodeID = 0;

    public int MaxSize { get { return GridSizeX * GridSizeY; } }

    private void Awake()
    {
        NodeDiameter = NodeRaidus * 2;
        GridSizeX = Mathf.RoundToInt(GridWorldSize.x / NodeDiameter);
        GridSizeY = Mathf.RoundToInt(GridWorldSize.y / NodeDiameter);

        if (DebugTestGrid) CreateGrid();

    }


    public void CreateGrid()
    {
        Debug.Log("creating grid");
        TheGrid = new Node[GridSizeX, GridSizeY];
        Vector3 WorldBottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.up * GridWorldSize.y / 2;
        Debug.Log("grid with world  bottom left: " + WorldBottomLeft);
        int nodeID = 0;

        for (int x = 0; x < GridSizeX; x++)
        {
            for (int y = 0; y < GridSizeY; y++)
            {
                Vector3 WorldPoint = WorldBottomLeft + Vector3.right * (x * NodeDiameter + NodeRaidus) + Vector3.up * (y * NodeDiameter + NodeRaidus);
                bool CanWalk = true;
                ContactFilter2D C = new ContactFilter2D();
                C.useTriggers = false;

                Collider2D[] Cols = new Collider2D[1];
                Physics2D.OverlapCircle(WorldPoint, NodeRaidus, C, Cols);


                if (Cols[0] != null) CanWalk = false;

                TheGrid[x, y] = new Node(CanWalk, WorldPoint, x, y, nodeID);
                nodeID++;

            }

        }

        //if (!DebugTestGrid)
        //{
        //    SetUpNodeWalk();
        //    StartCoroutine(GizmoDraw());
        //}

    }

    public void SetUpNodeWalk()
    {
        for (int i = 0; i < GridSizeX; i++)
        {
            Vector3 WorldBottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.up * GridWorldSize.y / 2;
            Vector3 WorldPoint = WorldBottomLeft + Vector3.right * (i * NodeDiameter + NodeRaidus) + Vector3.up * (1 * NodeDiameter + NodeRaidus);

            CircleCollider2D D = Instantiate(OverlapTool, new Vector3(WorldPoint.x, -248.8f, 0), Quaternion.identity);
            OverlapTools.Add(D);
            D.transform.SetParent(OverlapParent);
            //OverlapToolDict.Add(D, new Vector2(WorldPoint.x, 0));
            D.transform.position = new Vector2(WorldPoint.x, 0);
        }
    }



    //If I use contact points maybe i can use those to get the node from worldpoint
    //I can use a big ass circle collider over the whole map and get an arry of all colliders it hits,
    //then run in through a physics2d contactpoint func and get the vector3 world point of the contact point

    //IEnumerator BigSphere()
    //{
    //    CircleCollider2D C = this.gameObject.GetComponent<CircleCollider2D>();
    //    yield return new WaitForEndOfFrame();
    //    C.enabled = true;
    //    ContactFilter2D C1 = new ContactFilter2D();
    //    C1.useTriggers = false;
    //    yield return new WaitForEndOfFrame();
    //    //Collider2D[] Cols = new Collider2D[500];

    //    //Physics2D.OverlapCollider(C, C1, Cols);
    //    ContactPoint2D[] Cpoints = new ContactPoint2D[1000];

    //    Physics2D.GetContacts(C, Cpoints);
    //    int counter = 0;
    //    foreach (ContactPoint2D P in Cpoints)
    //    {
    //        Node N = GetNodeFromWorldPoint(P.point);
    //        N.SetWalkable(false);
    //        counter++;
    //    }

    //    Debug.Log("points: " + counter);
    //    yield return null;
    //}

    IEnumerator FlagWalkNodes()
    {

        for (int i = 1; i < GridSizeY; i++)
        {
            Vector3 WorldBottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.up * GridWorldSize.y / 2;
            Vector3 WorldPoint = WorldBottomLeft + Vector3.right * (1 * NodeDiameter + NodeRaidus) + Vector3.up * (i * NodeDiameter + NodeRaidus);

            if (i % 10 == 0) yield return new WaitForEndOfFrame();

            foreach (CircleCollider2D D in OverlapTools)
            {
                D.transform.position = new Vector2(D.transform.position.x, WorldPoint.y);
                Collider2D[] Cols = new Collider2D[1];
                ContactFilter2D C = new ContactFilter2D();
                C.useTriggers = false;
                Physics2D.OverlapCollider(D, C, Cols);
                //Physics2D.cont               

                if (Cols[0] != null)
                {
                    Node N = GetNodeFromWorldPoint(D.transform.position);
                    N.SetWalkable(false);
                }

            }

        }


        yield return null;
    }


    IEnumerator GizmoDraw()
    {
        yield return StartCoroutine(FlagWalkNodes());
        //yield return StartCoroutine(BigSphere());
        //yield return null;
        Destroy(OverlapParent.gameObject);

    }

    public Node GetNodeFromWorldPoint(Vector3 WorldPosition)
    {
        float PercentX = (WorldPosition.x + GridWorldSize.x / 2) / GridWorldSize.x;
        float PercentY = (WorldPosition.y + GridWorldSize.y / 2) / GridWorldSize.y;

        PercentX = Mathf.Clamp01(PercentX);
        PercentY = Mathf.Clamp01(PercentY);

        int Xindex = Mathf.RoundToInt((GridSizeX - 1) * PercentX);
        int Yindex = Mathf.RoundToInt((GridSizeY - 1) * PercentY);

        return TheGrid[Xindex, Yindex];

    }


    public List<Node> GetNeighbors(Node Node)
    {
        List<Node> NeighborList = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0) continue;

                int CheckX = Node.GridX + x;
                int CheckY = Node.GridY + y;

                if ((CheckX >= 0 && CheckX < GridSizeX) && (CheckY >= 0 && CheckY < GridSizeY))
                {
                    NeighborList.Add(TheGrid[CheckX, CheckY]);
                }

            }
        }

        return NeighborList;
    }

    //float gtimer = 5f;
    public void OnDrawGizmosSelected()
    {

        Color R = Color.red;
        Color W = Color.white;
        R.a = 0.4f;
        W.a = 0.4f;
        Gizmos.DrawWireCube(transform.position, new Vector3(GridWorldSize.x, GridWorldSize.y, 1));

        //Gizmos.DrawWireCube(transform.position, new Vector3(GridWorldSize.x, GridWorldSize.y, 1));
        if (TheGrid != null && DisplayGridGizmos)
        {
            foreach (Node N in TheGrid)
            {
                Gizmos.color = (N.Walkable) ? W : R;
                Gizmos.DrawCube(N.WorldPosition, Vector3.one * (NodeDiameter - TileGap));
            }

        }





    }



}
