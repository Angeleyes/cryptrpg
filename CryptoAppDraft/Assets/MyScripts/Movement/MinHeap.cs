﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public interface IHeapItem<T> : IComparable<T>
{
    int HeapIndex { get; set; }
}

public class MinHeap<T> where T : IHeapItem<T>
{

    T[] Items;
    int CurrentItemCount;

    public MinHeap(int MaxHeapSize)
    {
        Items = new T[MaxHeapSize];
    }


    public void Add(T item)
    {
        item.HeapIndex = CurrentItemCount;
        Items[CurrentItemCount] = item;
        SortUp(item);
        CurrentItemCount++;
    }

    public T RemoveFirst()
    {
        T FirstItem = Items[0];
        CurrentItemCount--;
        Items[0] = Items[CurrentItemCount];
        Items[0].HeapIndex = 0;
        SortDown(Items[0]);
        return FirstItem;
    }

    public void SortUp(T item)
    {
        int ParentIndex = (item.HeapIndex - 1)/ 2;

        while(true)
        {
            T ParentItem = Items[ParentIndex];
            if (item.CompareTo(ParentItem) > 0)
            {
                Swap(item, ParentItem);
            }
            else break;

            ParentIndex = (item.HeapIndex - 1 )/ 2;
        }

    }

    public void SortDown(T item)
    {
        while(true)
        {
            int ChildIndexLeft = item.HeapIndex * 2 + 1;
            int ChildIndexRight = item.HeapIndex * 2 + 2;

            int SwapIndex = 0;

            if (ChildIndexLeft < CurrentItemCount)
            {
                SwapIndex = ChildIndexLeft;

                if (ChildIndexRight < CurrentItemCount)
                {
                    if (Items[ChildIndexLeft].CompareTo(Items[ChildIndexRight]) < 0)
                    {
                        SwapIndex = ChildIndexRight;
                    }

                }

                if (item.CompareTo(Items[SwapIndex]) < 0)
                {
                    Swap(item, Items[SwapIndex]);
                }
                else return;
            }
            else return;
        }
    }

    public void UpdateItem(T item)
    {
        SortUp(item);
    }

    void Swap(T item1, T item2)
    {
        int tempindex = 0;

        Items[item1.HeapIndex] = item2;
        Items[item2.HeapIndex] = item1;

        tempindex = item1.HeapIndex;
        item1.HeapIndex = item2.HeapIndex;
        item2.HeapIndex = tempindex;
    }

    public int Count
    {
        get { return CurrentItemCount; }
    }


    public bool Contains(T item)
    {
        return Equals(Items[item.HeapIndex], item);
    }


}
