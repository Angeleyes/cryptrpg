﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Movement : MonoBehaviour
{

    public float Ysensitivity = 25;
    public float Xsensitivity = 25;

    public float ScrollSpeed = 1.05f;

    public bool IsMoving = false;
    //public int Speed = 15;
    internal int Acceleration;
    Vector3 Velocity;
    internal Vector3 Direction;

    public Vector2 PreviousFacing;
    public Vector2 LastMove;

    public delegate void DirectionChanged();
    public DirectionChanged OnDirectionChanged;

    internal float CustomDeltaTime = 0.017f;

    internal Entity TheEntity;

    private void Awake()
    {
        TheEntity = GetComponent<Entity>();
    }

    public enum DirectionEnum
    {
        Up = 1,
        Down = 2,
        Left = 3,
        Right = 4,
        UpRight = 5,
        DownRight = 6,
        UpLeft = 7,
        DownLeft = 8
    }


    //public void BasicMove(float x, float y, Rigidbody2D Body)
    //{
    //    Direction = new Vector3(x, y, 0);
    //    Velocity += Direction * CustomDeltaTime * Speed;

    //    //transform.Translate(Velocity*CustomDeltaTime, Space.World);
    //    Body.velocity = Velocity;
    //    Velocity *= 0;
    //}
    //setup movement for other entites later
    //IN USE BY PLAYER CONTROLLER
    public void BasicMove(Vector3 DirectionToMove, Rigidbody2D Body, float CustomSpeed)
    {
        Direction = DirectionToMove;

        Velocity += (DirectionToMove * CustomDeltaTime);
        Body.velocity = Velocity * CustomSpeed;
        Velocity *= 0;
    }

    //used by monsters
    public void BasicMove(Vector3 CurrentPosition, Vector3 TargetPosition, float CustomSpeed)
    {
        //Direction = CurrentPosition - TargetPosition;
        //Debug.Log("inside movment basic move");
        Direction = TargetPosition - CurrentPosition;
        IsMoving = !(Direction.x == 0 && Direction.y == 0);

        TheEntity.SetEntityPosition(Vector3.MoveTowards(CurrentPosition, TargetPosition, CustomSpeed * CustomDeltaTime));
        LastMove = Direction.normalized * 10f;

    }

    public Vector3 GetCurrentDirection()
    {
        return Direction;
    }

    internal DirectionEnum GetCurrentFacingDirectionEnum()
    {
        Vector2 Dir = Direction;
        Dir.Normalize();

        //if(Mathf.Abs(Dir.x) == Mathf.Abs(Dir.y))
        //{
        //    //going diagonal
        //    if(Dir.x > 0)
        //    {
        //        if (Dir.y > 0) return DirectionEnum.UpRight;
        //        else return DirectionEnum.DownRight;
        //    }
        //    else
        //    {
        //        if (Dir.y > 0) return DirectionEnum.UpLeft;
        //        else return DirectionEnum.DownLeft;
        //    }
        //}
        Debug.Log("norm direction: " + Direction.normalized + " with last move nomr: " + Dir);

        if (Mathf.Abs(Dir.x) > Mathf.Abs(Dir.y))
        {
            if (Dir.x > 0) return DirectionEnum.Right;
            else return DirectionEnum.Left;
        }
        else
        {
            if (Dir.y > 0) return DirectionEnum.Up;
            else return DirectionEnum.Down;
        }


    }

    public Vector2 GetNormCurrentDirection()
    {
        Vector2 Dir = Direction;
        Dir.Normalize();
        //Debug.Log("returning norm dir: " + Dir);
        return Dir;
    }

    private void OnCollisionStay2D(Collision2D collision) //should virtual and override this so that i can move it to player controller
    {
        Rigidbody2D OtherBody = collision.otherRigidbody;

        //getting jittering collision, this bug might be due to chancing player controller input from getaxis to getaxisraw
        //if(isLocalPlayer)
        //{
        //Debug.Log("colliding with " + collision.gameObject.name);
        if ((Input.GetAxisRaw("Horizontal") < 0.1f && OtherBody.velocity.x <= 0.1f) || (Input.GetAxisRaw("Horizontal") > 0.1f && OtherBody.velocity.x >= 0.1f)) //if your are trying to move left and your body isnt
        {
            //Debug.Log("stopping horizontal movement to " + OtherBody.name);
            OtherBody.velocity = new Vector2(0, OtherBody.velocity.y);
        }
        else if ((Input.GetAxisRaw("Vertical") < 0.1f && OtherBody.velocity.y <= 0) || (Input.GetAxisRaw("Vertical") > 0.1f && OtherBody.velocity.y >= 0))
        {
            //Debug.Log("stopping vertical movement to " + OtherBody.name);
            OtherBody.velocity = new Vector2(OtherBody.velocity.x, 0);
        }

        //StartCoroutine(Collide(OtherBody));
        //}

        else if (collision.gameObject.tag == "Monster")
        {
            MonsterAI M = collision.gameObject.GetComponent<MonsterAI>();
            if ((M.GetCurrentDirection().x < 0 && OtherBody.velocity.x <= 0) || (M.GetCurrentDirection().x > 0 && OtherBody.velocity.x >= 0.1f))
            {
                OtherBody.velocity = new Vector2(0, OtherBody.velocity.y);
            }
            else if (M.GetCurrentDirection().y < 0 && OtherBody.velocity.y <= 0 || (M.GetCurrentDirection().y > 0 && OtherBody.velocity.y >= 0.1f))
            {
                OtherBody.velocity = new Vector2(OtherBody.velocity.x, 0);
            }
        }


    }



    private void OnCollisionExit2D(Collision2D collision)
    {

    }

    public void SetSpeed(int speed)
    {
        //Speed = speed;
    }
}
