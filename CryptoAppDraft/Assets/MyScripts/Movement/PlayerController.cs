﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Movement {


    Camera Cam;
    //internal CameraFollow FollowRef;

    internal float MoveY { get; private set; }
    internal float MoveX { get; private set; }
    internal float UpdateTimer = 0;

    //public int PlayerSpeed = 5;

    //Transform PlayerTransform;
    Rigidbody2D Body;
    PlayerEntity Playerentity;
    ContactFilter2D CFilter = new ContactFilter2D();

    void Start ()
    {
        //C.SetLayerMask(LayerMask.NameToLayer("Default"));
        foreach(Camera C in GetComponentsInChildren<Camera>())
        {
            if (C.tag == "MainCamera") Cam = C;
        }

        Cam.cameraType = CameraType.Game;
        Body = GetComponent<Rigidbody2D>();
        Playerentity = GetComponent<PlayerEntity>();

        //do i need to instantiate a new camera?
        //if (isLocalPlayer) PlayerTransform = this.transform;
        //PlayerTransform = this.transform;
        CFilter.SetLayerMask(27);
    }
	

	void Update ()
    {
        //if (!isLocalPlayer) return;

        UpdateTimer += Time.deltaTime;
        //commands?
        if (Input.GetMouseButtonDown(0) && Playerentity.PlayerWeapon != null && !Playerentity.CharHUD.HudUiOpen())
        {
            Debug.Log("attacking in update");
            Playerentity.BasicAttack();
        }

        if(Input.GetMouseButtonDown(1))
        {

            Ray ray = Cam.ScreenPointToRay(Input.mousePosition);

            //Debug.DrawRay(ray.origin, ray.direction, Color.red, 3f);
            Debug.DrawLine(ray.origin, ray.direction, Color.blue, 3f);
            //Debug.DrawRay(ray.origin, ray.direction, Color.yellow, 3f, false);

            RaycastHit2D Hit = Physics2D.Raycast(ray.origin, ray.direction, 200f, 1<<27);

            if (Hit)
            {
                Debug.Log("cast ray, hit: " + Hit.collider.name);
                Interactable interactable = Hit.collider.GetComponent<Interactable>();

                if (interactable != null && interactable.CanInteract(Playerentity))
                {
                    Debug.Log("hit an interactable");
                    interactable.Interact();
                    return;
                }
                else Debug.Log("interactable is null");

            }
            else if (Playerentity.OffHand != null && !Playerentity.CharHUD.HudUiOpen()) //maybe put below interact stuff so it takes precident
            {
                //the offhand ability needs to be inside the player controller instead of using the normal ability cooldown interface
                //the reason is so that it can be prioritized over or under other right mouse button actions such as the interact function
                Ability offhand = Playerentity.EntityClass.OffHandAbility;
                AbilityCoolDown offhandcd = Playerentity.CharHUD.AbilityBar.OffhandCooldownSlot;
                if(offhandcd != null && offhand != null && offhand.HasResource && offhand.IsAvaliable && offhandcd.CoolDownFinished) Playerentity.CharHUD.AbilityBar.OffhandCooldownSlot.OnKeyHit();
                //for animatation and cooldown
            }

        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Playerentity.CharHUD.ToggleDataSheet();
            Playerentity.CharHUD.ToolTipPanel.gameObject.SetActive(false);
            //toggle off tooltip
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            Playerentity.CharHUD.ToggleInventory();
            Playerentity.CharHUD.ToolTipPanel.gameObject.SetActive(false);
            //toggle off tooltip
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!Playerentity.CharHUD.CharSheet.gameObject.activeInHierarchy && !Playerentity.CharHUD.InvUI.gameObject.activeInHierarchy) Playerentity.CharHUD.ToggleOptions();

            if (Playerentity.CharHUD.CharSheet.gameObject.activeInHierarchy || Playerentity.CharHUD.InvUI.gameObject.activeInHierarchy)
            {
                Playerentity.CharHUD.ToggleAllOff();
            }
            
        }

        if(Input.GetKeyDown(KeyCode.E))
        {
            //enable the emblem tab
        }


        if(Input.GetKeyDown(KeyCode.M))
        {
            if (Playerentity.Minimapcam.TheMap.IsBigMode) Playerentity.Minimapcam.TheMap.DebiggenMap();
            else Playerentity.Minimapcam.TheMap.EmbiggenMap();
        }

        if (Playerentity.CanMove)
        {
            if (Input.GetAxisRaw("Vertical") > 0.1f || Input.GetAxisRaw("Vertical") < -0.1f)
            {
                float Y = Input.GetAxisRaw("Vertical");
                float Sign = Y;
                MoveY = Ysensitivity * Sign;
                PreviousFacing = LastMove;
                Playerentity.MoveMotor.LastMove = new Vector2(Input.GetAxisRaw("Horizontal")*10, Input.GetAxis("Vertical")*10); //need the x10 to help the anim blender make more concret decisions on which animation is playing

            }

            if (Input.GetAxisRaw("Horizontal") > 0.1f || Input.GetAxisRaw("Horizontal") < -0.1f)
            {
                float X = Input.GetAxisRaw("Horizontal");
                float Sign = X;
                MoveX = Xsensitivity * Sign;
                PreviousFacing = LastMove;
                Playerentity.MoveMotor.LastMove = new Vector2(Input.GetAxisRaw("Horizontal")*10, Input.GetAxisRaw("Vertical")*10);
            }
        }

        IsMoving = !(MoveY == 0 && MoveX == 0);

        //BasicMove(MoveX, MoveY, Body);
        Vector3 Dir = new Vector3(MoveX, MoveY, 0);
        BasicMove(Dir, Body, Playerentity.RunSpeed.GetValue());


        Vector2 V = LastMove;
        V.Normalize();
        Vector2 V2 = PreviousFacing;
        V2.Normalize();

        if (V2 != Vector2.zero && V != V2) 
        {
            //Debug.Log("Compared " + V + " to " + Playerentity.MoveMotor.GetNormCurrentDirection());
            if(Playerentity.MoveMotor.OnDirectionChanged != null) Playerentity.MoveMotor.OnDirectionChanged();
            //LastMove = GetNormCurrentDirection();
        }

        MoveX = MoveY = 0;
       
    }



}
