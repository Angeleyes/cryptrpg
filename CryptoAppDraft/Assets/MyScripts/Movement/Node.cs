﻿using UnityEngine;

public class Node : IHeapItem<Node> {

    public bool Walkable;
    public Vector3 WorldPosition;

    public int Gcost;
    public int Hcost;

    internal int NodeID;
    internal int GridX;
    internal int GridY;
    internal Vector2 GridCoordiantes;

    public int NodeHeapIndex;

    public Node Parent = null;

    public Node(bool walkable, Vector3 worldpos, int Gridx, int Gridy, int id)
    {
        Walkable = walkable;
        WorldPosition = worldpos;
        GridX = Gridx;
        GridY = Gridy;
        GridCoordiantes = new Vector2(Gridx, Gridy);
        NodeID = id;
    }

    public void SetWalkable(bool canwalk)
    {
        Walkable = canwalk;
    }

    public void InitNode(bool walkable, Vector3 worldpos, int Gridx, int Gridy, int id)
    {
        Walkable = walkable;
        WorldPosition = worldpos;
        GridX = Gridx;
        GridY = Gridy;
        GridCoordiantes = new Vector2(Gridx, Gridy);
        NodeID = id;
    }

    public int Fcost { get { return Gcost + Hcost; } }

    public int HeapIndex
    {
        get { return NodeHeapIndex; }
        set { NodeHeapIndex = value; }
    }

    public int CompareTo(Node N)
    {
        int Compare = Fcost.CompareTo(N.Fcost);
        if (Compare == 0) Compare = Hcost.CompareTo(N.Hcost);

        return -Compare;
    }


}
