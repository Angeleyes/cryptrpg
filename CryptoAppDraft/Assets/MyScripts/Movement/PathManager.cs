﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PathManager : MonoBehaviour {

    struct PathRequest
    {
        public Vector3 PathBegin;
        public Vector3 PathFinish;
        public Action<Vector3[], bool, MonsterAI> callback;

        public PathRequest(Vector3 pathbegin, Vector3 pathfinish, Action<Vector3[], bool, MonsterAI> _callback)
        {
            PathBegin = pathbegin;
            PathFinish = pathfinish;
            callback = _callback;
        }
    }

    internal bool IsProcessingPath;
    Queue<PathRequest> PathRequests = new Queue<PathRequest>();
    PathRequest CurrentPathRequest;

    static PathManager Instance;
    internal PathFinder PathFinding;

    private void Awake()
    {
        Instance = this;
        PathFinding = GetComponent<PathFinder>();
    }

    public static void RequestPath(Vector3 PathStart, Vector3 PathEnd, Action<Vector3[], bool, MonsterAI> Callback, MonsterAI AI)
    {
        //this is being called too much
        //should put an if path start and path end diff is < 0.5 or something, too close
        Debug.Log("pathmanager:  Path start is: " + PathStart + " and path end is: " + PathEnd);
        PathRequest NewRequest = new PathRequest(PathStart, PathEnd, Callback);

        Instance.PathRequests.Enqueue(NewRequest);
        Instance.TryProcessNext(AI);
    }

    void TryProcessNext(MonsterAI AI)
    {
        if(!IsProcessingPath && PathRequests.Count > 0)
        {
            CurrentPathRequest = PathRequests.Dequeue();
            IsProcessingPath = true;
            Debug.Log("trying proccess path to: " + CurrentPathRequest.PathFinish);
            PathFinding.StartFindPath(CurrentPathRequest.PathBegin, CurrentPathRequest.PathFinish, AI);
        }
    }


    public void FinishedProcessingPath(Vector3[] Path, bool Success, MonsterAI AI)
    {

        CurrentPathRequest.callback(Path, Success, AI);
        IsProcessingPath = false;
        TryProcessNext(AI);
    }

}
