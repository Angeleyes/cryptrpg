﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;

public class PathFinder : MonoBehaviour {

    public Vector3 StartPosition;
    public Vector3 TargetPosition;

    GroundGrid TheGrid;

    internal PathManager pathmanager;

	void Start ()
    {
        TheGrid = GetComponent<GroundGrid>();
        pathmanager = GetComponent<PathManager>();
	}

    public void StartFindPath(Vector3 start, Vector3 target, MonsterAI AI)
    {
        UnityEngine.Debug.Log("started find path to: " + target);
        StartCoroutine(FindPath(start, target, AI));
    }

    IEnumerator FindPath(Vector3 Start, Vector3 Target, MonsterAI AI)
    {
        //UnityEngine.Debug.Log("inside find path coroutine to path: " + Target);
        Stopwatch Timer = new Stopwatch();
        Timer.Start();

        Vector3[] Waypoints = new Vector3[0];
        bool PathFound = false;

        Node StartNode = TheGrid.GetNodeFromWorldPoint(Start);
        Node TargetNode = TheGrid.GetNodeFromWorldPoint(Target);

        //UnityEngine.Debug.Log("after getnodefromworld, start is: " + StartNode.WorldPosition + " with end node: " + TargetNode.WorldPosition);

        MinHeap<Node> OpenSet = new MinHeap<Node>(TheGrid.MaxSize);

        HashSet<Node> ClosedSet = new HashSet<Node>();

        OpenSet.Add(StartNode);

        while (OpenSet.Count > 0)
        {
            Node CurrentNode = OpenSet.RemoveFirst();
            ClosedSet.Add(CurrentNode);
            UnityEngine.Debug.Log("at top of while open set");
            if(CurrentNode == TargetNode) 
            {
                RetracePath(StartNode, TargetNode);
                PathFound = true;
                Timer.Stop();
                UnityEngine.Debug.Log("found in: " + Timer.ElapsedMilliseconds + "ms");
                //yield return new WaitForEndOfFrame();
                UnityEngine.Debug.Log("breaking");
                break;
            }

            foreach(Node Neighbor in TheGrid.GetNeighbors(CurrentNode))
            {

                if (!Neighbor.Walkable || ClosedSet.Contains(Neighbor)) continue;

                int NewNeighborCost = CurrentNode.Gcost + GetDistance(CurrentNode, Neighbor);

                if(NewNeighborCost < Neighbor.Gcost || !OpenSet.Contains(Neighbor))
                {
                    Neighbor.Gcost = NewNeighborCost;
                    Neighbor.Hcost = GetDistance(Neighbor, TargetNode);
                    Neighbor.Parent = CurrentNode;

                    if (!OpenSet.Contains(Neighbor))
                    {
                        OpenSet.Add(Neighbor);
                    }
                    else OpenSet.UpdateItem(Neighbor);
             
                }
            }


        } //end of while
        
        UnityEngine.Debug.Log("Path found?: " + PathFound);
        if (PathFound)
        {
            UnityEngine.Debug.Log("found path in path finder");
            Waypoints = RetracePath(StartNode, TargetNode);
            pathmanager.FinishedProcessingPath(Waypoints, PathFound, AI);
        }
        UnityEngine.Debug.Log("after path found, should break here");
        yield return null;

    }


    public int GetDistance(Node A, Node B)
    {
        int DistX = Mathf.Abs(A.GridX - B.GridX);
        int DistY = Mathf.Abs(A.GridY - B.GridY);

        if (DistX > DistY) return 14 * DistY + 10 * (DistX - DistY);
        else return 14 * DistX + 10 * (DistY - DistX);
    }

    public Vector3[] RetracePath(Node Start, Node End)
    {
        List<Node> Path = new List<Node>();

        Node current = End;

        //Debug.Log("same?:" + (current == Start));
        while(current != Start)
        {
            Path.Add(current);
            if (Path.Count > 3) return new Vector3[0];
            current = current.Parent;
        }

        Vector3[] V = SimplifyPath(Path);
        Array.Reverse(V);
        return V;
    }

    Vector3[] SimplifyPath(List<Node> NodePath)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 DirectionOld = Vector2.zero;

        for (int i = 1; i < NodePath.Count; i++)
        {
            Vector2 NewDirection = new Vector2(NodePath[i - 1].GridX - NodePath[i].GridX, NodePath[i - 1].GridY - NodePath[i].GridY);
            if (NewDirection != DirectionOld) waypoints.Add(NodePath[i].WorldPosition);

            DirectionOld = NewDirection;
        }


        return waypoints.ToArray();
    }

    IEnumerator SearchOpenSet(List<Node> OpenList)
    {
        while(OpenList.Count > 0)
        {
            Node CurrentNode = OpenList[0];

            for (int i = 0; i < OpenList.Count; i++)
            {
                if((OpenList[i].Fcost < CurrentNode.Fcost || OpenList[i].Fcost == CurrentNode.Fcost) && OpenList[i].Hcost < CurrentNode.Hcost)
                {
                    CurrentNode = OpenList[i];
                }
            }

            yield return null;
        }
        yield break;
    }
	

}
