﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomWind : MonoBehaviour
{
    public Vector3 Waypoint = new Vector3();
    public float DistanceBuffer = 5f;
    private float Speed = 0.0005f;
    private Vector3 Origin = new Vector3();

    private void Awake()
    {
        Origin = this.transform.position;
        StartCoroutine(Travel());
    }

    IEnumerator Travel()
    {
        while(true)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, Waypoint, Speed);
            if ((Waypoint - this.transform.position).sqrMagnitude <= DistanceBuffer * DistanceBuffer)
            {
                this.transform.position = Origin;
                Speed = Random.Range(0.0004f, 0.0007f);
            }

            yield return null;
        }
    }
}
