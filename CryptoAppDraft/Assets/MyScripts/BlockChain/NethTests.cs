﻿using System.Collections;
using System.Collections.Generic;
// Here we import the Netherum.JsonRpc methods and classes.
using Nethereum.JsonRpc.UnityClient;
using UnityEngine;
using UnityEngine.UI;
using Nethereum.Contracts;
using Nethereum.RPC.Eth.DTOs;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.Encoders;
using Nethereum.Signer;
using Nethereum.Hex.HexConvertors.Extensions;

public class NethTests : MonoBehaviour
{
    private string AddressFromMetaMask;
    private Text BalanceText;
    public static string ABI = @"[{""constant"":true,""inputs"":[],""name"":""pings"",""outputs"":[{""name"":"""",""type"":""uint256""}],""payable"":false,""stateMutability"":""view"",""type"":""function""},{""constant"":false,""inputs"":[{""name"":""value"",""type"":""uint256""}],""name"":""ping"",""outputs"":[],""payable"":false,""stateMutability"":""nonpayable"",""type"":""function""},{""anonymous"":false,""inputs"":[{""indexed"":false,""name"":""pong"",""type"":""uint256""}],""name"":""Pong"",""type"":""event""}]";
    private static string contractAddress = "0xc4b054A90676fea7E8CBb8e311fd1ed086A296e1";
    private Contract contract;

    // This is the secret key of the address, you should put yours.
    //get this data from the metamask data object
    private string accountPrivateKey = "541e20c8e1ff510f3accab1e461cf3f27bf7ebe61528fa9c470c3aa1e757118a";
    //private string accountPrivateKey = " ";

    private string _url = "https://kovan.infura.io";

    void Start()
    {

        AddressFromMetaMask = "0x63F5B016e0f197C353f992f968f321Ad11689149";
        BalanceText = GetComponentInChildren<Text>();
        this.contract = new Contract(null, ABI, contractAddress);

    }


    public void GetBalance()
    {
        StartCoroutine(getAccountBalance(AddressFromMetaMask, (balance) =>
        {
            // When the callback is called, we are just going print the balance of the account
            Debug.Log(balance);
            BalanceText.text = balance.ToString();
        }));
    }

    public void PingTest()
    {
        StartCoroutine(PingTransaction());
    }

    public IEnumerator PingTransaction()
    {
        // Create the transaction input with encoded values for the function
        // We will need the public key (accountAddress), the private key (accountPrivateKey),
        // the pingValue we are going to send to our contract (10000),
        // the gas amount (50000 in this case),
        // the gas price (25), (you can send a gas price of null to get the default value)
        // and the ammount of ethers you want to transfer, remember that this contract doesn't receive
        // ethereum transfers, so we set it to 0. You can modify it and see how it fails.
        var transactionInput = CreatePingTransactionInput(
            AddressFromMetaMask,
            accountPrivateKey,
            new HexBigInteger(10000),
            new HexBigInteger(50000),
            new HexBigInteger(25),
            new HexBigInteger(0)
        );

        // Here we create a new signed transaction Unity Request with the url, private key, and the user address we get before
        // (this will sign the transaction automatically :D )


        //var key = Sign

        var transactionSignedRequest = new TransactionSignedUnityRequest(_url, accountPrivateKey, AddressFromMetaMask);
     
        // Then we send it and wait
        Debug.Log("Sending Ping transaction...");
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if (transactionSignedRequest.Exception == null)
        {
            // If we don't have exceptions we just display the result, congrats!
            Debug.Log("Ping tx submitted: " + transactionSignedRequest.Result);
        }
        else
        {
            // if we had an error in the UnityRequest we just display the Exception error
            Debug.Log("Error submitting Ping tx: " + transactionSignedRequest.Exception.Message);
        }
    }

    public Function GetPingFunction()
    {
        return contract.GetFunction("ping");
    }

    public TransactionInput CreatePingTransactionInput(
        // For this transaction to the contract we are going to use
        // the address which is excecuting the transaction (addressFrom), 
        // the private key of that address (privateKey),
        // the ping value we are going to send to this contract (pingValue),
        // the maximum amount of gas to consume,
        // the price you are willing to pay per each unit of gas consumed, (higher the price, faster the tx will be included)
        // and the valueAmount in ETH to send to this contract.
        // IMPORTANT: the PingContract doesn't accept eth transfers so this must be 0 or it will throw an error.
        string addressFrom,
        string privateKey,
        BigInteger pingValue,
        HexBigInteger gas = null,
        HexBigInteger gasPrice = null,
        HexBigInteger valueAmount = null)
    {

        var function = GetPingFunction();
        return function.CreateTransactionInput(addressFrom, gas, gasPrice, valueAmount, pingValue);
    }

    public static IEnumerator getAccountBalance(string address, System.Action<decimal> callback)
    {
        // Now we define a new EthGetBalanceUnityRequest and send it the testnet url where we are going to
        // check the address, in this case "https://kovan.infura.io".
        // (we get EthGetBalanceUnityRequest from the Netherum lib imported at the start)
        //var getBalanceRequest = new EthGetBalanceUnityRequest("https://kovan.infura.io");
        var getBalanceRequest = new EthGetBalanceUnityRequest("https://kovan.infura.io");

        //https://kovan.infura.io/DMSw9TVeD780aErK2Pqv
        // Then we call the method SendRequest() from the getBalanceRequest we created
        // with the address and the newest created block.
        yield return getBalanceRequest.SendRequest(address, Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());

        // Now we check if the request has an exception
        if (getBalanceRequest.Exception == null)
        {
            // We define balance and assign the value that the getBalanceRequest gave us.
            var balance = getBalanceRequest.Result.Value;
            // Finally we execute the callback and we use the Netherum.Util.UnitConversion
            // to convert the balance from WEI to ETHER (that has 18 decimal places)
            callback(Nethereum.Util.UnitConversion.Convert.FromWei(balance, 18));
        }
        else
        {
            // If there was an error we just throw an exception.
            throw new System.InvalidOperationException("Get balance request failed");
        }
    }














}

