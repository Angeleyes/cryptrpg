﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvToBankXfer : MonoBehaviour {



    public Inventory Bank;
    public Inventory PlayerInventory;


    public void OnClickXfer()
    {
        if (Bank == null || PlayerInventory == null)
        {
            //find player inv game object
            //find bank
        }

        //need to create special bank inventory slots in which they call a transfer function insead of a use item function

        foreach (Item I in PlayerInventory.ListOfItems)
        {
            if (I is EquipableItem)
            {
                Bank.AddItem(I);
                PlayerInventory.RemoveItem(I);
            }
        }
    }

}
