﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAI : Movement {

    //public List<Abilities> AbilityList;
    //maybe put stat mods in this class


    internal AIStrategies AIMovement, AIAttack, AIideling;
    internal AttackPoint TargetAttackPoint;

    internal float MachineUpdateTime = 0.5f;
    public float AgroRadius = 5;
    public float DeAgroRange = 10;

    public float AttackRange;
    //public float AttackRangeYModifier;
    //public bool IsRanged = false;
    //public bool Patroller = false;
    internal List<Vector3> Waypoints = new List<Vector3>();


    internal MonsterEntity TheMonster;
    internal Rigidbody2D MonsterBody;
    internal List<PlayerEntity> Targets = new List<PlayerEntity>();
    internal delegate void MonsterFunction();
    internal MonsterFunction StateFunction;

    //Specific path variables
    internal bool IsTraversing = false;
    internal float TraverseCD = 0;
    internal Vector3[] AIPath;
    internal int AIPathIndex = 0;

    public MonsterState CurrentState = MonsterState.Idle;

    public MoveStrategy MovementAI;
    public AttackStrategy AttackAI;
    public IdleStrategy IdleAI;
    public LayerMask TargetMask;
    public AttackHitBox MonsterWeaponObject;
    public ProjectileShooter WeaponShooter;

    //I should turn all of these into some kind of static singleton or arrays
    internal Dictionary<MoveStrategy, AIStrategies> FSMMovement = new Dictionary<MoveStrategy, AIStrategies>();
    internal Dictionary<AttackStrategy, AIStrategies> FSMAttack = new Dictionary<AttackStrategy, AIStrategies>();
    internal Dictionary<IdleStrategy, AIStrategies> FSMIdle = new Dictionary<IdleStrategy, AIStrategies>();

    internal Coroutine CurrentMoveRoutine;
    internal Coroutine CurrentAttackRoutine;
    internal StateMachine<MonsterAI> TheStateMachine { get; set; }


    //ENUMS HERE LOOK
    #region Enums
    public enum MoveStrategy
    {
        Basic = 1, //just random movement
        Aggressive = 2, //a* charges you, good for melee
        Cautious = 3, //a* stays back, good for ranged
        Advanced = 4,      //a* uses tactics
        Efficient = 5,
    }


    public enum AttackStrategy
    {
        Basic = 1, //attacks first person seen
        Mean = 2, //always attacks lowest health player
        Switcher = 3, //randomly switches targets when attacking
        Ranged = 4,
        MeanRanged = 5,
    }

    public enum MonsterState
    {
        Idle = 1,
        Patroling = 2,
        Persuing = 3,
        Attacking = 4,
        Pausing = 5,
    }

    public enum IdleStrategy
    {
        Idle = 1,
        Patroling = 2,
        Random = 3,
    }

    #endregion


    public void SetupAI()
    {
        TheMonster = GetComponent<MonsterEntity>();
        MonsterBody = GetComponent<Rigidbody2D>();


        //I kind of want to move all of this into one static object that all the ai's reference to, just 1 dictionary class
        //THIS NEEDS TO BE IN A MONSTER AI STATIC SINGLETON INSTANCE, SERVICE
        FSMMovement.Add(MoveStrategy.Basic, new AIStrategies(BasicMonsterMovement.Instance));
        FSMMovement.Add(MoveStrategy.Advanced, new AIStrategies(AdvancedMonsterMovement.Instance));
        FSMMovement.Add(MoveStrategy.Efficient, new AIStrategies(EfficientMonsterMovement.Instance));
        //FSMMovement.Add(MoveStrategy.Cautious, new AIStrategies(new CautiousMonsterMovement())); //for ranged

        FSMAttack.Add(AttackStrategy.Basic, new AIStrategies(BasicMonsterAttack.Instance));
        FSMAttack.Add(AttackStrategy.Ranged, new AIStrategies(RangedAttack.Instance));
        //FSMAttack.Add(AttackStrategy.Mean, new AIStrategies(new MeanAttack()));

        FSMIdle.Add(IdleStrategy.Idle, null); //null for stand still and do nothing idle
        FSMIdle.Add(IdleStrategy.Patroling, new AIStrategies(PatrollingMonsterMovement.Instance));
        FSMIdle.Add(IdleStrategy.Random, new AIStrategies(RandomMonsterMovement.Instance));

        //FSMAttack.Add(AttackStrategy.Ranged, new AIStrategies());
        //FSMAttack.Add(AttackStrategy.MeanRanged, new AIStrategies());
        //Random switcher

        AIMovement = FSMMovement[MovementAI]; //picked in inspector
        AIAttack = FSMAttack[AttackAI]; //picked in inspector
        AIideling = FSMIdle[IdleAI];
        //setting this is how u set the owner.

        TheStateMachine = new StateMachine<MonsterAI>(this);

        if (TheMonster.RunSpeed.GetValue() <= 0) Debug.Log("WARNING, Monster speed is less than or equal to 0, monster will not move");
    }

    public void StartAI()
    {
        TheStateMachine.ChangeState(IdleState.Instance, null); //starts in idle state
        StartCoroutine(RunStateMachine());
        StartCoroutine(DrawRayCollision());
    }

    IEnumerator RunStateMachine()
    {
        yield return new WaitForSeconds(MachineUpdateTime);
        while(true)
        {
            TheStateMachine.UpdateMachine();
            yield return null;
        }

    }


    //public bool IsHostile(string EntityTag)
    //{
    //    return true;
    //}


    Collider2D WallHit = null;
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.GetComponent<PlayerEntity>() == null && collision.gameObject.GetComponent<MonsterEntity>() == null)
        {
            //Debug.Log("monster is colliding with " + collision.gameObject.name);
            WallHit = collision.collider as Collider2D;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.collider == WallHit)
        {
            //Debug.Log("exiting wall collision, going null");
            WallHit = null;
        }
    }

    public bool IsStuck()
    {
        if (WallHit != null) return true;
        else return false;
    }

    public void ResetWallCollision()
    {
        WallHit = null;
    }

    public Vector3 GetNextWaypoint()
    {
        return Waypoints[0];
    }

    void OnPlayerDied(PlayerEntity P)
    {
        if (Targets.Contains(P))
        {
            Targets.Remove(P);
            //FoundTarget = false; might need it, might not
        }
    }


    internal Vector3[] Path;
    internal int Index = 0;
    public void SetPath(Vector3[] path, int index)
    {
        Path = path;
        Index = index;
    }

    private void DrawCollisoinRay()
    {
        //RaycastHit2D ColRay;
        RaycastHit2D Hit;
        Hit = Physics2D.Raycast(TheMonster.GetEntityPosition(), GetNormCurrentDirection(), 2f, 0);

        if(Hit.collider != null)
        {
            if(Hit.collider.gameObject.GetComponent<MonsterEntity>() == null && Hit.collider.gameObject.GetComponent<PlayerEntity>() == null)
            {
                Debug.Log("RAYCAST HIT A WALL : " + Hit.collider.name);
                WallHit = Hit.collider;
            }
        }
    }

    internal void FaceTarget(Entity Target)
    {
        //Debug.Log("facing target : " + Target.transform.position);
        //Debug.Log("facing dif1: " + (Target.transform.position - GetCurrentDirection()));
        //Debug.Log("facing dif2: " + (GetCurrentDirection() - Target.transform.position));
        Vector3 FacingDir = Target.GetEntityPosition() - TheMonster.GetEntityPosition();

        FacingDir = FacingDir.normalized * 10;
        //Debug.Log("setting last facing to  : " + FacingDir);
        if (Mathf.Abs(FacingDir.x) > Mathf.Abs(FacingDir.y)) FacingDir.y = 0;
        else FacingDir.x = 0;

        LastMove = FacingDir;
        Direction = FacingDir;
        //Debug.Log("setting last facing to  : " + FacingDir);
        //Debug.Log("pos facing is: " + (TheMonster.GetEntityPosition() - Target.transform.position).normalized * 10);

        TheMonster.EntityAnimator.SetFloat("LastFacingX", FacingDir.x);
        TheMonster.EntityAnimator.SetFloat("LastFacingY", FacingDir.y);

    }

    IEnumerator DrawRayCollision()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.2f, 0.6f));
            DrawCollisoinRay();
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = (WallHit == null) ? Color.blue : Color.red;

        Gizmos.color = Color.white;

        if (Application.isEditor && Application.isPlaying)
        {
            Gizmos.DrawRay(TheMonster.GetEntityPosition(), GetNormCurrentDirection() * 2);
            Gizmos.DrawWireSphere(TheMonster.GetEntityPosition(), AgroRadius);
            Gizmos.DrawWireSphere(TheMonster.GetEntityPosition(), AttackRange);
            Gizmos.DrawWireSphere(TheMonster.GetEntityPosition(), DeAgroRange);
        }

        //UnityEditor.Handles.DrawWireDisc
        if(Path != null)
        {
            for (int i = Index; i < Path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(Path[i], Vector3.one);

                if (i == Index) Gizmos.DrawLine(transform.position, Path[i]);
                else Gizmos.DrawLine(Path[i - 1], Path[i]);

            }
        }
    }


}
