﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : State<MonsterAI>
{


    //this state goes unused for now, may delete later

    private static PatrolState _instance;

    private PatrolState()
    {
        if (_instance != null) return;

        _instance = this;
    }

    public static PatrolState Instance
    {
        get
        {
            if (_instance == null) new PatrolState();
            return _instance;
        }
    }


    public override void EnterState(MonsterAI owner, State<MonsterAI> Statecaller)
    {
        //start check agro radius maybe
        Debug.Log("entering patrol state");
        //if (owner.Patroller && owner.Waypoints.Count > 0)
        //{
        //    owner.StartCoroutine(PatrolWaypoints(owner, owner.Waypoints));
        //}

    }

    public override void ExitState(MonsterAI owner)
    {
        Debug.Log("exiting patrol state");
        owner.StopCoroutine(PatrolWaypoints(owner, new List<Vector3>()));
        //owner.StopCoroutine("PatrolWaypoints");
    }

    public override void UpdateState(MonsterAI owner)
    {
        //Debug.Log("in patrol");

        //owner.AIMovement.MoveStrategy.Move(owner, owner.transform.position);

        //timer += Time.deltaTime;
        //if (timer > 0.5f) //check every 1/2 second
        //{
        //    timer = 0;
        //    Debug.Log("checking for player");

        //    if (owner.Patroller && owner.Waypoints.Count == 0) //if ai has no waypoints, just do random patroling
        //    {
        //        owner.AIMovement.MoveStrategy.Move(owner, new Vector3(UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5)));
        //    }
        //    else
        //    {
        //        owner.AIMovement.MoveStrategy.Move(owner, owner.GetNextWaypoint());
        //    }

        //    bool found = Physics2D.OverlapCircle(owner.transform.position, owner.AgroRadius, owner.TargetMask);
        //    if (found)
        //    {
        //        Debug.Log("found, setting target");
        //        GameObject G = Physics2D.OverlapCircle(owner.transform.position, owner.AgroRadius, owner.TargetMask).gameObject;
        //        PlayerEntity P = G.GetComponent<PlayerEntity>();
        //        if (P != null) owner.Targets.Add(P);
        //        owner.TheStateMachine.ChangeState(PersueState.Instance, Instance);
        //        //switch state
        //    }

        //}



    }

    //delegate IEnumerator PatrolRoutine(MonsterAI ai, List<Vector3> wp);
    //PatrolRoutine PatrolWaypointRoutine = PatrolState.Instance.PatrolWaypoints;

    IEnumerator PatrolWaypoints(MonsterAI AI, List<Vector3> waypoints)
    {
        Debug.Log("the ai with id: " + AI.GetInstanceID() + " is running this");
        int index = 0;
        while(true)
        {
            if(Vector3.Distance(AI.transform.position, waypoints[index]) < 0.5f)
            {
                index += 1 % waypoints.Count;
            }

            AI.AIMovement.MoveStrategy.Move(AI, waypoints[index]);
            yield return null;
        }


    }

}
