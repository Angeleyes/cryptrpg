﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BasicMonsterClass")]
public class MonsterClass : ScriptableObject
{
    public OnStrikeDebuff TargetDebuffStrike;
    public OnStrikeBuff SelfBuffStrike;
    
}
