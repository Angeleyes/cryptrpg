﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : State<MonsterAI>
{

    private static AttackState _instance;

    private AttackState()
    {
        if (_instance != null) return;

        _instance = this;
    }

    public static AttackState Instance
    {
        get
        {
            if (_instance == null) new AttackState();
            return _instance;
        }
    }

    private float AttackRangeModifier = 0.5f;

    public override void EnterState(MonsterAI owner, State<MonsterAI> Statecaller)
    {
        Debug.Log("entering attack state");
        owner.CurrentState = MonsterAI.MonsterState.Attacking;
        if(owner.CurrentMoveRoutine != null) owner.StopCoroutine(owner.CurrentMoveRoutine);
        owner.IsMoving = false;
        //make movement ai the original ai in the case that its behavior changed based on some circumstance defined in the original ai strat
        owner.AIMovement = owner.FSMMovement[owner.MovementAI];
    }

    public override void ExitState(MonsterAI owner)
    {
        Debug.Log("exiting attack state");
        //if (owner.CurrentAttackRoutine != null) owner.StopCoroutine(owner.CurrentAttackRoutine);
        owner.TheMonster.IsAttacking = false;
        if (owner.TargetAttackPoint != null)
        {
            //Debug.Log("releasing attack point");
            owner.TargetAttackPoint.Release();
            owner.TargetAttackPoint = null;
        }

    }

    public override void UpdateState(MonsterAI owner)
    {
        if (owner.Targets.Count <= 0)
        {
            Debug.Log("targets count is zero, going straight to idle");
            owner.TheStateMachine.ChangeState(IdleState.Instance, Instance);
        }
        PlayerEntity CurrentTarget = owner.Targets[0]; //this could be a problem if target moves out of radius quickly after entering it

        //if the target is still in attack range, attack them
        if (InAttackRange(CurrentTarget, owner))
        {
            //Debug.Log("in attack range: target pos is: " + CurrentTarget.transform.position);
            if(!owner.TheMonster.IsAttacking) owner.AIAttack.AttackStrategy.Attack(CurrentTarget, owner.TheMonster);
        }
        else
        {
            Debug.Log("out of range....pausing....then going back to persue");
            //might need a global target variable to have entity targe data persist through this state change....
            owner.TheStateMachine.ChangeState(PausingState.Instance, PersueState.Instance);
        }

    }

    private bool InAttackRange(PlayerEntity CurrentTarget, MonsterAI owner)
    {       
        //modify this to be a range, if within a range return true otherwise false

        return ((CurrentTarget.GetEntityPosition() - owner.TheMonster.GetEntityPosition()).sqrMagnitude <= ((owner.AttackRange + AttackRangeModifier) * (owner.AttackRange + AttackRangeModifier)));
    }


}
