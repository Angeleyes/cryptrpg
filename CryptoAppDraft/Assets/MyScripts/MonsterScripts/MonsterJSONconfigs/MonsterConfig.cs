﻿using System;

[Serializable]
public class MonsterConfig 
{

    public MonsterConfig()
    {

    }

    public int Level = 1;

    public float HealthMax = 100f;
    public float Resource = 20f;
    public int ExpWorth = 20;
    //public int TotalExp = 0;
    //public int CurrentExp = 0;
    //public int ExpUntilNext = 200;

    //public int Intelligence = 0;
    //public int Strength = 0;
    //public int Agility = 0;

    public int AttackPower = 5;
    public int SpellPower = 5;

    public int Armor = 5;
    public float AttackSpeed = 1f;
    public float CritChane = 5f;
    public float LifeSteal = 0f;
    public float ResourceRegen = 1f;
    public float HealthRegen = 1f;

    public float RunSpeed = 15f;




    public int AuraFoeLayer = 30;



}
