﻿
using UnityEngine;

public class PersueState : State<MonsterAI>
{

    private static PersueState _instance;

    private PersueState()
    {
        if (_instance != null) return;

        _instance = this;
    }

    public static PersueState Instance
    {
        get
        {
            if (_instance == null) new PersueState();
            return _instance;
        }
    }


    public override void EnterState(MonsterAI owner, State<MonsterAI> Statecaller)
    {
        Debug.Log("entering persue state");
        owner.CurrentState = MonsterAI.MonsterState.Persuing;
        //if(owner.CurrentMoveRoutine != null) owner.StopCoroutine(owner.CurrentMoveRoutine);

        owner.ResetWallCollision();
        owner.AIMovement = owner.FSMMovement[owner.MovementAI]; //back to original move strat

        if (owner.TargetAttackPoint != null)
        {
            owner.TargetAttackPoint.Release();
            owner.TargetAttackPoint = null;
        }

    }

    public override void ExitState(MonsterAI owner)
    {
        Debug.Log("exting persue state");
        //if (owner.CurrentMoveRoutine != null) owner.StopCoroutine(owner.CurrentMoveRoutine);
        //owner.TargetAttackPoint.Release();
        //owner.TargetAttackPoint = null;
        //owner.AIMovement.MoveStrategy = BasicMonsterMovement.Instance;
        owner.AIMovement = owner.FSMMovement[owner.MovementAI];
    }

    float timer = 0;
    public override void UpdateState(MonsterAI owner)
    {
        timer += Time.deltaTime;
        //if there are no targets, go back to calm state
        if (owner.Targets.Count <= 0)
        {
            //Debug.Log("no target to persue");
            owner.TheStateMachine.ChangeState(IdleState.Instance, Instance);         
        }

        //get first target
        PlayerEntity CurrentTarget = null;
        if (owner.Targets.Count > 0) CurrentTarget = owner.Targets[0]; 
        else owner.TheStateMachine.ChangeState(IdleState.Instance, Instance);
        //if the target is in attack range, attack

        //if owner does not have an attack point, try to get one
        if (owner.TargetAttackPoint == null) owner.TargetAttackPoint = CurrentTarget.PlayerAttackGrid.GetNearestAvaliablePoint(owner.TheMonster.GetEntityPosition());

        if(owner.TargetAttackPoint == null) //if there is no point avaliable
        {
            //think about a possible pausing timer or something becuase this will be called a lot
            //for possibly 10 seconds plus
            //Debug.Log("inside persue state, could not find open attack point, waiting for open spot....");
            owner.TheStateMachine.ChangeState(IdleState.Instance, Instance);

            return;
        }

        //Debug.Log("checking if player is in attack range, dif : " + Vector3.Distance(CurrentTarget.TheMonster.GetEntityPosition(), owner.TheMonster.GetEntityPosition()) + " with agro radius " + owner.AttackRange);
        if (InAttackRange(CurrentTarget, owner))
        {
            owner.TheStateMachine.ChangeState(AttackState.Instance, Instance);
        }
        else if(owner.TheMonster.GetEntityPosition() == CurrentTarget.PlayerAttackGrid.GetWorldPositionByPoint(owner.TargetAttackPoint) && !InAttackRange(CurrentTarget, owner))
        {
            Debug.Log("WARNING: The monster is at the current target point location but its NOT in attack range, difference is: " + Vector3.Distance(CurrentTarget.transform.position, owner.transform.position));
            Debug.Log("player pos: " + CurrentTarget.GetEntityPosition() + " and ai: " + owner.TheMonster.GetEntityPosition());

        }
        else //otherwise, check other stuff
        {
            //if the target runs out of agro range, remove current target and try for next target
            if (InDeagroRange(CurrentTarget, owner))
            {
                //ai gets stuck here after a path find
                //Debug.Log("target out of agro range");
                //owner.Targets.Remove(CurrentTarget);
                owner.TheStateMachine.ChangeState(IdleState.Instance, Instance);
                CurrentTarget = null;
            }
            else //otherwise, current target is still in range, so go move towards them after each second
            {
                //need a wait here..... maybe
                //continue to chase target, in this case the nearest attack point on the character grid
                //Debug.Log("still in range, keep on chasing");
                //Debug.Log("persueing target attack point: " + CurrentTarget.PlayerAttackGrid.GetWorldPositionByPoint(owner.TargetAttackPoint));
                owner.AIMovement.MoveStrategy.Move(owner, CurrentTarget.PlayerAttackGrid.GetWorldPositionByPoint(owner.TargetAttackPoint));

            }
        }


    }


    private bool InAttackRange(PlayerEntity CurrentTarget, MonsterAI owner)
    {
        return ((CurrentTarget.GetEntityPosition() - owner.TheMonster.GetEntityPosition()).sqrMagnitude <= (owner.AttackRange*owner.AttackRange));
        //return InModifiedAttackRange(CurrentTarget, owner);
    }

    private bool InDeagroRange(PlayerEntity Target, MonsterAI owner)
    {
        return ((Target.GetEntityPosition() - owner.TheMonster.GetEntityPosition()).sqrMagnitude >= (owner.DeAgroRange*owner.DeAgroRange));
    }

}
