﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMoveStrategy
{
    void Move(MonsterAI TheAI, Vector3 TargetLocation);
    //IEnumerator MoveRoutine(MonsterAI TheAI, Vector3 TargetLocation, float delay);
    //void RandomMove(MonsterAI TheAI);
      
}

public interface IAttackStrategy
{
    void Attack(Entity TargetToAttack, MonsterEntity Attacker);

}

public class AIStrategies
{

    public IMoveStrategy MoveStrategy;
    public IAttackStrategy AttackStrategy;

    public AIStrategies(IMoveStrategy Strat)
    {
        MoveStrategy = Strat;
    }

    public AIStrategies(IAttackStrategy Strat)
    {
        AttackStrategy = Strat;
    }

}

//think about singleton pattern for these strategies
//making a static instance object used instead of new instances....interesting

public class RandomMonsterMovement : IMoveStrategy
{

    private static RandomMonsterMovement _Instance;

    private RandomMonsterMovement()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static RandomMonsterMovement Instance
    {
        get
        {
            if (_Instance == null) new RandomMonsterMovement();
            return _Instance;
        }
    }

    public void Move(MonsterAI TheAI, Vector3 TargetPosition)
    {
        if (!TheAI.IsTraversing)
        {
            float randomDelay = Random.Range(1.5f, 8f);
            //TheAI.StartCoroutine(MoveRoutine(TheAI, TargetPosition, randomDelay));
            TheAI.CurrentMoveRoutine = TheAI.StartCoroutine(MoveRoutine(TheAI, TargetPosition, randomDelay));
        }
    }

    public IEnumerator MoveRoutine(MonsterAI TheAI, Vector3 TargetPosition, float delay)
    {
        //need to stop coroutine when transitioning into attack state

        TheAI.IsTraversing = true;
        int MoveX = Random.Range(-5, 6);
        int MoveY = Random.Range(-5, 6);
        TargetPosition = new Vector3(TheAI.TheMonster.GetEntityPosition().x + MoveX, TheAI.TheMonster.GetEntityPosition().y + MoveY, 0);
        //Debug.Log("starting traverse in random move to target: " + TargetPosition);
        float timer = 0;

        while (timer < delay)
        {
            if (TheAI.CurrentState != MonsterAI.MonsterState.Idle)
            {
                TheAI.IsTraversing = false;
                yield break;
            }

            timer += Time.deltaTime;
            if (TheAI.TheMonster.GetEntityPosition() != TargetPosition) TheAI.BasicMove(TheAI.TheMonster.GetEntityPosition(), TargetPosition, TheAI.TheMonster.RunSpeed.GetValue() / 3);
            else TheAI.IsMoving = false;

            yield return null;
        }

        TheAI.IsTraversing = false;
        yield break;

    }

}

public class PatrollingMonsterMovement : IMoveStrategy
{

    private static PatrollingMonsterMovement _Instance;

    private PatrollingMonsterMovement()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static PatrollingMonsterMovement Instance
    {
        get
        {
            if (_Instance == null) new PatrollingMonsterMovement();
            return _Instance;
        }
    }



    public void Move(MonsterAI TheAI, Vector3 TargetLocation)
    {
        //TheAI.CurrentMoveRoutine = MoveRoutine(TheAI, TargetLocation);
    }


    IEnumerator MoveRoutine(MonsterAI TheAI, Vector3 Target)
    {
        yield return null;
    }
}


public class AdvancedMonsterMovement : IMoveStrategy
{

    private static AdvancedMonsterMovement _Instance;

    private AdvancedMonsterMovement()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static AdvancedMonsterMovement Instance
    {
        get
        {
            if (_Instance == null) new AdvancedMonsterMovement();
            return _Instance;
        }
    }

    public void Move(MonsterAI TheAI, Vector3 TargetPosition)
    {
        //TheAI.CurrentMoveRoutine = TraversePath(TheAI);

        //start some kind of countdown where the ai is switched back to efficient move
        //TheAI.StartCoroutine(AISwitchCountdown(TheAI));
        if (TheAI.TraverseCD <= 0 && !TheAI.IsTraversing && TheAI.TheMonster.CanMove)
        {
            Debug.Log(TheAI.name + " is requesting path to: " + TargetPosition + " with movecd: " + TheAI.TraverseCD + " traversing: " + TheAI.IsTraversing);
            PathManager.RequestPath(TheAI.TheMonster.GetEntityPosition(), new Vector3(TargetPosition.x, TargetPosition.y, 0) , PathFound, TheAI);
            TheAI.TraverseCD = 0.2f;
        }
        else TheAI.TraverseCD -= Time.deltaTime;
         
    }

    IEnumerator AISwitchCountdown(MonsterAI TheAI)
    {
        yield return new WaitForSeconds(3f);
        TheAI.AIMovement = TheAI.FSMMovement[TheAI.MovementAI];
    }


    //IEnumerator move = AdvancedMonsterMovement.Instance.TraversePath();
    public void PathFound(Vector3[] NewPath, bool PathSuccess, MonsterAI AI)
    {
        if (NewPath.Length > 3) Debug.Log("new path is greater than 3 inside path found !!!!!!! too long");
        if(PathSuccess)
        {
            //Debug.Log("has new path: " + HasNewPath + "..trying start coroutine to pos: " + NewPath[NewPath.Length-1]);
            AI.AIPath = NewPath;
            Debug.Log("staring pathfinding coroutine, istraversing: " + AI.IsTraversing + " with moveing" + AI.IsMoving + " and last move: " + AI.LastMove);
            //monster gets stuck at is traversing = true in here....somehow
            //IEnumerator move = TraversePath(AI);
            //AI.StopCoroutine(move);
            if(!AI.IsTraversing) AI.CurrentMoveRoutine = AI.StartCoroutine(TraversePath(AI));
            //if (AI.IsTraversing && AI.LastMove.magnitude == 0) AI.IsTraversing = false;
        }
        else
        {
            Debug.Log("path unsuccessful");
        }
    }
    
    public IEnumerator TraversePath(MonsterAI AI)
    {
        AI.IsTraversing = true;
        Debug.Log("traversing..");
        if(AI.AIPath.Length <= 0)
        {
            Debug.Log("path is empty..");
            AI.IsTraversing = false;
            yield break;
        }
        Vector3 CurrentWaypoint = AI.AIPath[0]; //throwing null index out of range

        //Debug.Log("got current wp: " + CurrentWaypoint);
        //Debug.Log("targetindex equals path length: " + (AI.AIPathIndex == AI.AIPath.Length));
        //the stall bug is caused by this while check, it thinks the skelly is at the target location/index
        //while(TargetIndex != PathToTarget.Length)
        Debug.Log("checking if in range of current pathing node");
        while((AI.AIPath.Length > 0) && (AI.TheMonster.GetEntityPosition() - AI.AIPath[AI.AIPath.Length-1]).sqrMagnitude > 0.01f*0.01f) //index out of range
        {
            Debug.Log("in while targetindex != pathtarget.length");
            if(AI.TheMonster.GetEntityPosition() == CurrentWaypoint || (AI.TheMonster.GetEntityPosition() - CurrentWaypoint).sqrMagnitude < 0.01f*0.01f) //added 1/1/2019
            {
                Debug.Log("current waypoint reached, moving to next waypoint");
                //TargetIndex++;
                AI.AIPathIndex++;
                
                //dont think i need this if statement
                if (AI.AIPathIndex >= AI.AIPath.Length)
                {
                    Debug.Log("REACHED PATH");
                    AI.AIPathIndex = 0;
                    AI.IsTraversing = false;
                    yield break;
                }

                CurrentWaypoint = AI.AIPath[AI.AIPathIndex];

            }

            //Debug.Log("calling basic move with targetindex:" + AI.AIPathIndex + " with path length: " + AI.AIPath.Length);
            AI.BasicMove(AI.TheMonster.GetEntityPosition(), CurrentWaypoint, AI.TheMonster.RunSpeed.GetValue());
            //AI.IsMoving = !(CurrentWaypoint.x == 0 && CurrentWaypoint.x == 0);
            //AI.LastMove = new Vector2(AI.transform.position.x, AI.transform.position.y);
            AI.SetPath(AI.AIPath, AI.AIPathIndex);
            yield return null;

        } //end of while

        //Debug.Log("ai.paith length zero?: " + AI.AIPath.Length);
        AI.AIPathIndex = 0;
        AI.IsTraversing = false;
        yield break;

    }

}


public class EfficientMonsterMovement : IMoveStrategy
{

    private static EfficientMonsterMovement _Instance;

    private EfficientMonsterMovement()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static EfficientMonsterMovement Instance
    {
        get
        {
            if (_Instance == null) new EfficientMonsterMovement();
            return _Instance;
        }
    }

    public void Move(MonsterAI TheAi, Vector3 TargetPosition)
    {
        //Debug.Log("inside efficient move");
        if (TheAi.IsStuck() && TheAi.AIMovement.MoveStrategy != AdvancedMonsterMovement.Instance)
        {
            Debug.Log("ai got stuck on something, changing to smart monster movement");
            TheAi.AIMovement.MoveStrategy = AdvancedMonsterMovement.Instance;
        }
        else if (TheAi.AIMovement.MoveStrategy != EfficientMonsterMovement.Instance)
        {
            Debug.Log("ai movement is not efficient, setting to efficient");
            TheAi.AIMovement.MoveStrategy = EfficientMonsterMovement.Instance;
            TheAi.BasicMove(TheAi.TheMonster.GetEntityPosition(), TargetPosition, TheAi.TheMonster.RunSpeed.GetValue());
        }
        else TheAi.BasicMove(TheAi.TheMonster.GetEntityPosition(), TargetPosition, TheAi.TheMonster.RunSpeed.GetValue());
    }




}

public class BasicMonsterMovement : IMoveStrategy
{
    private static BasicMonsterMovement _Instance;

    private BasicMonsterMovement()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static BasicMonsterMovement Instance
    {
        get
        {
            if (_Instance == null) new BasicMonsterMovement();
            return _Instance;
        }
    }

    public void Move(MonsterAI TheAi, Vector3 TargetPosition)
    {
        TheAi.BasicMove(TheAi.TheMonster.GetEntityPosition(), TargetPosition, TheAi.TheMonster.RunSpeed.GetValue());
    }


}

public class CautiousMonsterMovement : IMoveStrategy
{
    public void Move(MonsterAI TheAI, Vector3 TargetPosition)
    {

    }

    public IEnumerator MoveRoutine(MonsterAI TheAI, Vector3 TargetLocation, float delay)
    {
        yield return null;
    }

 
}




//MONSTER ATTACK STRATS


public class BasicMonsterAttack : IAttackStrategy
{

    private static BasicMonsterAttack _Instance;
    private BasicMonsterAttack()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static BasicMonsterAttack Instance
    {
        get
        {
            if (_Instance == null) new BasicMonsterAttack();
            return _Instance;
        }
    }


    public void Attack(Entity TargetToAttack, MonsterEntity Attacker)
    {
        Attacker.AI.CurrentAttackRoutine = Attacker.AI.StartCoroutine(DoAttack(TargetToAttack, Attacker));        
    }

    IEnumerator DoAttack(Entity Target, MonsterEntity Attacker)
    {
        Debug.Log("doing attack");

        Attacker.IsAttacking = true;
        Attacker.AI.FaceTarget(Target);
        yield return new WaitForSeconds(Attacker.AttackSpeed.GetValue());
        Attacker.IsAttacking = false;

        yield break;
    }


}



public class RangedAttack : IAttackStrategy
{

    private static RangedAttack _Instance;
    private RangedAttack()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static RangedAttack Instance
    {
        get
        {
            if (_Instance == null) new RangedAttack();
            return _Instance;
        }
    }

    public void Attack(Entity TargetToAttack, MonsterEntity Attacker)
    {
        Attacker.StartCoroutine(DoRangeAttack(TargetToAttack, Attacker));
    }


    IEnumerator DoRangeAttack(Entity Target, MonsterEntity Attacker)
    {
        //Debug.Log("doing ranged attack");

        Attacker.IsAttacking = true;
        Attacker.AI.FaceTarget(Target);

        if (Attacker.AI.WeaponShooter.TheProjectile != null)
        {
            //Vector3 ProjectileSpawnPoint = Attacker.AI.MonsterWeaponObject.transform.position;
            Attacker.AI.WeaponShooter.SetTarget(Target.GetEntityPosition());
        }
        else Debug.Log("Error in ranged attack: There is no assigned projectile object in the attack hit box object");

        yield return new WaitForSeconds(Attacker.AttackSpeed.GetValue());
        Attacker.IsAttacking = false;

        yield break;
    }
}

public class MeanAttack : IAttackStrategy //attack lowest hp
{

    Queue<Entity> PossibleTargets = new Queue<Entity>();

    public void Attack(Entity TargetToAttack, MonsterEntity Attacker)
    {
        //int Damage = Attacker.AttackPower.GetValue() + (int)(Attacker.Strength.GetValue() * 0.05);
        if (!PossibleTargets.Contains(TargetToAttack)) PossibleTargets.Enqueue(TargetToAttack);

        Entity CurrentTarget = PossibleTargets.Dequeue();

        BasicMonsterAttack.Instance.Attack(CurrentTarget, Attacker);


    }
}


