﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public interface IStateAction
//{
//    void DoStateAction();
//}

public class MachineState : State<MonsterAI>
{

    //public IStateAction StateAction;
    //could just use move and attack strats here too i guess

    private static MachineState _instance;

    private MachineState()
    {
        if (_instance != null) return;

        _instance = this;

    }

    public static MachineState Instance
    {
        get
        {
            if (_instance == null) new MachineState();
            return _instance;
        }
    }


    public override void EnterState(MonsterAI owner, State<MonsterAI> Statecaller)
    {
        //something needs to happen 1 time on enter
        Debug.Log("entering state");
    }

    public override void ExitState(MonsterAI owner)
    {
        //something needs to happen 1 time on exit
        Debug.Log("exting state");
    }

    public override void UpdateState(MonsterAI owner)
    {
        Debug.Log("updating state");
        //if (Monsterstate.switchstate){ owner.statemachine.changestate(switchstate);}



    }
}
