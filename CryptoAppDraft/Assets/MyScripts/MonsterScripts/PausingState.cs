﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausingState : State<MonsterAI>
{

    private static PausingState _instance;

    private PausingState()
    {
        if (_instance != null) return;

        _instance = this;
    }

    public static PausingState Instance
    {
        get
        {
            if (_instance == null) new PausingState();
            return _instance;
        }
    }

    bool IsPaused = true;
    State<MonsterAI> StateCaller;

    public override void EnterState(MonsterAI owner, State<MonsterAI> Statecaller)
    {
        Debug.Log("entering pause state");
        IsPaused = true;
        owner.CurrentState = MonsterAI.MonsterState.Pausing;
        //if(owner.CurrentAttackRoutine != null) owner.StopCoroutine(owner.CurrentMoveRoutine);
        //if(owner.CurrentAttackRoutine != null) owner.StopCoroutine(owner.CurrentAttackRoutine);
        owner.TheMonster.CanMove = false;
        StateCaller = Statecaller;
        float delay = Random.Range(0.8f, 1f);
        if (delay < owner.TheMonster.AttackSpeed.GetValue()) delay = owner.TheMonster.AttackSpeed.GetValue();
        delay -= 0.5f;
        owner.StartCoroutine(Pause(delay));
        //need to tie pausing to attack speed
    }

    IEnumerator Pause(float TimeInSeconds)
    {
        yield return new WaitForSeconds(TimeInSeconds);
        IsPaused = false;
        
    }

    public override void ExitState(MonsterAI owner)
    {
        //Debug.Log("exiting pause state");
        owner.TheMonster.CanMove = true;
    }


    public override void UpdateState(MonsterAI owner)
    {
        if (IsPaused)
        {
            //do nothing
            //when attacking....he is doing a yield time return
            Debug.Log("paused.....");
        }
        else owner.TheStateMachine.ChangeState(StateCaller, Instance);
    }


}
