﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MonsterSpawner : MonoBehaviour {

    public ScriptMonsterList MonsterList;
    public GameObject MonsterParent;
    //public List<MonsterEntity> MonstersToSpawn;
    public MonsterEntity StrictMonsterSpawn;
    internal List<MonsterEntity> SpawnedMonsters = new List<MonsterEntity>();
    internal CircleCollider2D ActivateRadius;
    internal List<PlayerEntity> KnownTargets = new List<PlayerEntity>();
    internal RoomPiece ParentRoom;
    public bool DebugSingleSpawn = false;
    public bool SpawnSingle = false;
    internal bool MonstersActivated = false;
    public float DefaultRadius = 50f;
    public List<Vector3> SpawnPoints;

    public float ChanceToSpawnSpecialMonster;

    public void Awake()
    {
        ParentRoom = GetComponentInParent<RoomPiece>();
        if (ParentRoom == null) Debug.Log("parent room in null for me " + this.transform.position);
        if (ParentRoom != null)
        {
            ParentRoom.SpawnRoomMonsters += SpawnInactiveMonsters;
            ParentRoom.LoadMonsterConfigs += LoadMonsterData;
        }
        ActivateRadius = GetComponent<CircleCollider2D>();
        if(MonsterParent == null) MonsterParent = GameObject.FindGameObjectWithTag("MonsterParent");
        if(!DebugSingleSpawn) ActivateRadius.radius = DefaultRadius;

        //Debug.Log("this spawneres positoin is : " + this.transform.position + " with a local pos : " + this.transform.localPosition);

    }

    public void SpawnInactiveMonsters()
    {
        MonsterEntity RandomMonster = GetRandomMonster();

        if (!DebugSingleSpawn)
        {
            foreach (Vector3 V in SpawnPoints)
            {
                if (StrictMonsterSpawn != null)
                {
                    SpawnMonster(StrictMonsterSpawn, V);
                }
                else
                {
                    SpawnMonster(RandomMonster, V);
                }
            }
        }
        else SpawnMonster(StrictMonsterSpawn, SpawnPoints[0]);
    }

    public void LoadMonsterData(MonsterConfig config)
    {
        foreach(MonsterEntity M in SpawnedMonsters)
        {
            M.InitMonsterStats(config);
        }
    }

    private MonsterEntity GetRandomMonster()
    {

        int RandomIndex = Random.Range(0, MonsterList.Monsters.Count);

        MonsterEntity Monster = MonsterList.Monsters[RandomIndex];
        return Monster;
    }

    private void ToggleMonstersActive(bool toggle, PlayerEntity P)
    {
        //if(P != null) Debug.Log("toggeling monsters active and adding target : " + P.name);
        foreach (MonsterEntity M in SpawnedMonsters)
        {
            if(!M.WasKilled) M.gameObject.SetActive(toggle);
            if(!M.AI.Targets.Contains(P)) M.AI.Targets.Add(P);
        }
        //UpdatePlayerList(KnownTargets);
        if(!KnownTargets.Contains(P))
        {
            KnownTargets.Add(P);
            
        }

        //StartCoroutine(StartPlayerDistCheck());
    }

    public void SpawnMonster(MonsterEntity MonsterToSpawn, Vector3 Pos)
    {
        Vector3 NextPoint = this.transform.position + Pos;
        NextPoint.z = 0;

        MonsterEntity MonsterClone = Instantiate(MonsterToSpawn, NextPoint, Quaternion.identity);
        //MonsterToSpawn = Instantiate(MonsterToSpawn, NextPoint, Quaternion.identity);

        if (Random.Range(0, 101) <= ChanceToSpawnSpecialMonster)
        {
            AssignRandomAttribute(MonsterClone);
        }

        SpawnedMonsters.Add(MonsterClone);
        MonsterClone.transform.SetParent(MonsterParent.transform);

        MonsterClone.gameObject.SetActive(false);
    }

    //place holder function, needs more interesting options and stuff
    private void AssignRandomAttribute(MonsterEntity M)
    {
        List<Color> ColorList = new List<Color>() { Color.green, Color.yellow, Color.red };
        string S = "Strength";
        int rand = Random.Range(0, 3);
        if (rand == 0) S = "Health";
        else if (rand == 1) S = "RunSpeed";
        else if (rand == 2) S = "AttackPower";

        M.Rend.color = ColorList[rand];

        M.IncreaseStat(S, 0.5f);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 22 && collision.gameObject.GetComponent<PlayerEntity>() != null && !MonstersActivated)
        {
            Debug.Log("setting mosnter go to true");
            PlayerEntity P = collision.gameObject.GetComponent<PlayerEntity>();
            MonstersActivated = true;
            ToggleMonstersActive(true, P);
            //ActivateRadius.enabled = false;
        }
        //else if(collision.gameObject.layer == 22 && MonstersActivated)
        //{
        //    //if the monsters are already activeated and an unkown other player enters the trigger area
        //    //add that new player to the list of potential targets
        //    PlayerEntity P = collision.gameObject.GetComponent<PlayerEntity>();
        //    foreach (MonsterEntity M in SpawnedMonsters)
        //    {
        //        if (!M.AI.Targets.Contains(P)) M.AI.Targets.Add(P);
        //    }
        //}
    }

    //broken, needs fixin
    IEnumerator StartPlayerDistCheck()
    {
        bool TriggerFalseFlag = false;
        while(KnownTargets.Count > 0)
        {
            yield return new WaitForSeconds(3f);
            foreach (PlayerEntity P in KnownTargets)
            {               
                Debug.Log("checking player dist for unspawn");
                //if just ONE of the players is still near the spawner, keep the monsters active
                if ((P.GetEntityPosition() - this.transform.position).sqrMagnitude <= DefaultRadius + 10 * DefaultRadius + 10)
                {
                    Debug.Log("at least one player is in range: keeping monsters active");
                    TriggerFalseFlag = false;
                    break;
                }
                else
                {
                    Debug.Log("NO PLAYER NEARBY, SETTING FALSE FLAG TRUE");
                    TriggerFalseFlag = true;
                    continue;
                }
            }

            if(TriggerFalseFlag)
            {
                Debug.Log("no nearby players, turning monsters off for " + this.transform.position);
                foreach(MonsterEntity M in SpawnedMonsters)
                {
                    M.gameObject.SetActive(false);
                    MonstersActivated = false;
                    yield break;
                }
            }
            yield return null;
        }

        yield break;
    }

    private void OnDrawGizmos()
    {
        Color C = new Color(1, 0.92f, 0.016f, 0.4f);
        Gizmos.color = C;
        Gizmos.DrawSphere(this.transform.position, 0.5f);

        foreach(Vector3 V in SpawnPoints)
        {
            Gizmos.DrawWireSphere(V+this.transform.position, 0.3f);
            
        }

    }



}
