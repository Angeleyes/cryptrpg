﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterClosetSpawner : MonoBehaviour
{
    public MonsterEntity TheMonster;
    public int NumberOfMonsters = 1;
    private GameObject MonsterParent;

    public event Action TriggerSpawn;
    private List<MonsterEntity> SpawnedMonsters = new List<MonsterEntity>();
    private List<PlayerEntity> KnownTargets = new List<PlayerEntity>();
    public Vector2 SpawnPosOffset = new Vector2();
    private Vector2 SpawnPosition = new Vector2();

    //observer?

    private void Awake()
    {
        if (MonsterParent == null) MonsterParent = GameObject.FindGameObjectWithTag("MonsterParent");
        SpawnPosition = this.transform.position + new Vector3(SpawnPosOffset.x, SpawnPosOffset.y, 0);
        MonsterEntity Mon = null;
        for (int i = 0; i < NumberOfMonsters; i++)
        {
            Mon = Instantiate(TheMonster, SpawnPosition, Quaternion.identity, MonsterParent.transform);
            Mon.gameObject.SetActive(false);
            SpawnedMonsters.Add(Mon);
        }

        //maybe check if this object has an interactable script on it? and subscribe to its interact function
        if(this.GetComponent<Interactable>() != null)
        {
            GetComponent<Interactable>().TriggerEvent += TriggerMonseterSpawn;
        }
    }

    public void TriggerMonseterSpawn()
    {
        //get surrounding players
        Collider2D Collision = Physics2D.OverlapCircle(this.transform.position, 25f, 1<<22);
        if (Collision != null)
        {
            Debug.Log("collision isnt null, but player?");
            PlayerEntity P = Collision.GetComponent<PlayerEntity>();
            if (P != null)
            {
                Debug.Log("found player: " + P.name);
                KnownTargets.Add(P);
            }
        }
        else Debug.Log("collisoin is null");

        //give monsters the player data and activate the monster
        if(KnownTargets.Count >= 0)
        {
            foreach(MonsterEntity M in SpawnedMonsters)
            {
                M.AI.Targets = KnownTargets;
                M.gameObject.SetActive(true);
            }
        }
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(SpawnPosition, 0.5f);

        //Gizmos.DrawWireSphere(this.transform.position, 25f);
    }
}
