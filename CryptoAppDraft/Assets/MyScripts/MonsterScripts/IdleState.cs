﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State<MonsterAI>
{
    private static IdleState _instance;

    private IdleState()
    {
        if (_instance != null) return;

        _instance = this;

    }

    public static IdleState Instance
    {
        get
        {
            if (_instance == null) new IdleState();
            return _instance;
        }
    }

    public override void EnterState(MonsterAI owner, State<MonsterAI> Statecaller)
    {
        Debug.Log("entering idle state");
        owner.CurrentState = MonsterAI.MonsterState.Idle;
        //owner.IsMoving = false;
        owner.ResetWallCollision();

        if(owner.TargetAttackPoint != null)
        {
            owner.TargetAttackPoint.Release();
            owner.TargetAttackPoint = null;
        }
       
    }

    public override void ExitState(MonsterAI owner)
    {
        Debug.Log("exiting idle state");
    }

    float timer = 0;
    public override void UpdateState(MonsterAI owner)
    {
        //owner.IsMoving = false;
        DistanceCheck(owner);
        //OverlapCircleCheck(owner);
    }



    private void DistanceCheck(MonsterAI owner)
    {
        if (owner.AIideling != null) owner.AIideling.MoveStrategy.Move(owner, new Vector3()); //new vec3 for testing but put in the waypoints as args later

        timer += Time.deltaTime;
        if (timer > 0.1f && owner.Targets.Count > 0)
        {
            timer = 0;
            foreach (Entity E in owner.Targets)
            {
                if (E == null) Debug.Log("Target suddenly went null");
                if ((E.GetEntityPosition() - owner.TheMonster.GetEntityPosition()).sqrMagnitude <= owner.AgroRadius*owner.AgroRadius)
                {
                    owner.TheStateMachine.ChangeState(PersueState.Instance, Instance);
                }
            }
        }

    }

    //going unused for now, too expensive
    //private void OverlapCircleCheck(MonsterAI owner)
    //{
    //    //Debug.Log("started check agro");
    //    timer += Time.deltaTime;
    //    if (timer > 0.5f) //check every 1/2 second
    //    {
    //        timer = 0;
    //        Debug.Log("checking for player");
    //        bool found = Physics2D.OverlapCircle(owner.transform.position, owner.AgroRadius, owner.TargetMask);
    //        if (found)
    //        {
    //            //this code sux, change it later 
    //            Debug.Log("found, setting target");
    //            GameObject G = Physics2D.OverlapCircle(owner.transform.position, owner.AgroRadius, owner.TargetMask).gameObject;
    //            PlayerEntity P = G.GetComponent<PlayerEntity>();
    //            if (P != null) owner.Targets.Add(P);
    //            owner.TheStateMachine.ChangeState(PersueState.Instance, Instance);
    //        }

    //        //owner.AIMovement.MoveStrategy.Move(owner, )

    //    }
    //}



}
