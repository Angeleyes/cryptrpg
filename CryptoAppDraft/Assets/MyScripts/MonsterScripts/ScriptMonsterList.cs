﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MonsterList")]
public class ScriptMonsterList : ScriptableObject
{
    public List<MonsterEntity> Monsters = new List<MonsterEntity>();
}
