﻿
[System.Serializable]

public abstract class State<T>
{
    public abstract void EnterState(T owner, State<T> StateCaller);
    public abstract void ExitState(T owner); //good place to do something once like die or something
    public abstract void UpdateState(T owner);

}

public class StateMachine<T> {

    public State<T> CurrentState { get; private set; }
    public T Owner;

    public StateMachine(T owner)
    {
        Owner = owner;
        CurrentState = null;
    }

    public void ChangeOwner(T owner)
    {
        Owner = owner;
    }

    public void ChangeState(State<T> NewState, State<T> PreviousState)
    {
        if(CurrentState != null) CurrentState.ExitState(Owner);
        CurrentState = NewState;
        CurrentState.EnterState(Owner, PreviousState);
    }

    public void UpdateMachine()
    {
        if (CurrentState != null) CurrentState.UpdateState(Owner);
    }

}
