﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterHUD : MonoBehaviour {


    public Slider HealthBar;
    public Text FloatingText;
    public Text HealthTextDisplay;
    internal MonsterEntity ParentMonster;

	void Start ()
    {
        //rather than instantiant, just have it already parented, text too
        //make a default monsterhud prefab to make it easier and less tedious in the inspector
        //the prefab should have like 5 hit effects and 10 texts, also the text should be connected


        ParentMonster = GetComponentInParent<MonsterEntity>();

        for (int i = 0; i < 5; i++) // create 5 hit effects to cycle through
        {
            ParentMonster.HitEffects.Enqueue(Instantiate(ParentMonster.HitEffect, this.transform.position, Quaternion.identity, this.transform));
        }

        HealthBar.maxValue = ParentMonster.HealthMax.GetValue();
        HealthBar.value = ParentMonster.HealthMax.GetValue();
        FloatingText.gameObject.SetActive(false);
    }


    public void UpdateHUD()
    {
        if(HealthBar.gameObject.activeInHierarchy == false) HealthBar.gameObject.SetActive(true);
        HealthBar.value = ParentMonster.Health.GetValue();
        HealthTextDisplay.text = ParentMonster.Health.GetValue() + "/" + ParentMonster.HealthMax.GetValue();
    }

    public IEnumerator ShowDamage(int damage)
    {
        float timer = 0;
        FloatingText.gameObject.SetActive(true);
        FloatingText.text = damage.ToString();
        Vector2 pos = FloatingText.transform.position;
        while (timer < 1.5f)
        {
            timer += Time.deltaTime;
            FloatingText.transform.position = Vector2.Lerp(FloatingText.transform.position, new Vector2(FloatingText.transform.position.x, FloatingText.transform.position.y + 1), Time.deltaTime);
            yield return null;
        }
        FloatingText.gameObject.SetActive(false);
        FloatingText.transform.position = pos;
    }


}
