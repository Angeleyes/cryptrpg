﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MonsterEntity : Entity, ILootable {

    internal MonsterAI AI;

    public delegate void MonsterState();

    private readonly MonsterState CurrentState;

    public MonsterClass TheMonsterClass;
    public Vector2 SpawnerStartPosition;

    internal Queue<OnHitEffect> HitEffects = new Queue<OnHitEffect>(2);
    internal MonsterHUD Monsterhud;
    internal MonsterAuraHandler MonsterAuraHand;
    internal bool WasKilled = false;

    internal AttackHitBox MonsterAttackHitBox;
    internal CircleCollider2D Box;

    internal int AuraFoeLayer = 0;

    [SerializeField]
    public LootRequest TheMobLoot = new LootRequest();

    public override void Awake()
    {
        base.Awake();
        AI = GetComponent<MonsterAI>(); //maybe require component?
        MoveMotor = AI;
        this.gameObject.layer = 23; //Magic number

        Monsterhud = GetComponentInChildren<MonsterHUD>();
        if (MonsterAuraHand == null) MonsterAuraHand = GetComponentInChildren<MonsterAuraHandler>();


        if(MonsterAttackHitBox == null) MonsterAttackHitBox = GetComponentInChildren<AttackHitBox>();

        if (TheMonsterClass != null)
        {
            if (TheMonsterClass.SelfBuffStrike != null) MonsterAttackHitBox.SuccessSelf += TheMonsterClass.SelfBuffStrike.OnStrike;
            if (TheMonsterClass.TargetDebuffStrike != null) MonsterAttackHitBox.SuccessHitTarget += TheMonsterClass.TargetDebuffStrike.OnStrike;
        }


        if (MonsterAttackHitBox.gameObject.activeInHierarchy) MonsterAttackHitBox.gameObject.SetActive(false);


        AI.SetupAI();


    }


    //i dont like this at all.....loading from a file every awake...nooo
    //do something else
    public void InitMonsterStats(MonsterConfig config)
    {

        HealthMax.AddToBase(config.HealthMax);
        Health.AddToBase(config.HealthMax);
        Armor.AddToBase(config.Armor);
        AttackPower.AddToBase(config.AttackPower);
        SpellPower.AddToBase(config.SpellPower);
        AttackSpeed.AddToBase(config.AttackSpeed);
        ExpWorth.AddToBase(config.ExpWorth);
        CriticalHitChance.AddToBase(config.CritChane);
        //Resource.AddToBase(config.Resource);
        //ResourceMax.AddToBase(config.Resource);
        //HealthRegen.AddToBase(config.HealthRegen);
        LifeSteal.AddToBase(config.LifeSteal);
        //ResourceRegen.AddToBase(config.ResourceRegen);
        RunSpeed.AddToBase(config.RunSpeed);

        Level = config.Level;
        AuraFoeLayer = config.AuraFoeLayer;
        MonsterAuraHand.SetEntityOwner(this, AuraFoeLayer);

    }

    public override void Start()
    {
        base.Start();


        AI.StartAI();
    }

    public override void TakeDamage(float damage, Entity Attacker, AttackHitBox.DamageSpec DamageType)
    {
        Monsterhud.StartCoroutine("ShowDamage", damage); //make sure this works
        if (HitEffects.Count > 0)
        {
            OnHitEffect E = HitEffects.Dequeue();
            E.Activate();
            HitEffects.Enqueue(E);
        }
        base.TakeDamage(damage, Attacker, DamageType);
        Monsterhud.UpdateHUD();


        //knockback

        Vector3 Dir = Attacker.GetEntityPosition() - this.GetEntityPosition();
        Dir.Normalize();
        Dir *= 0.1f; //0.1 is the place holder magic numbrer knockback amount
        Debug.Log("trying to knockback " + this.name + " by " + Dir);
        this.SetEntityPosition(this.GetEntityPosition() - Dir); // or minus....i dunno
    }

    public override void Die()
    {
        //play death animation
        //drop loot

        DropLoot();

        StopAllCoroutines();
        if(AI.TargetAttackPoint!= null) AI.TargetAttackPoint.Release();
        AI.TargetAttackPoint = null;
        base.Die();
        //do object pooling for monster spawner
        transform.position = SpawnerStartPosition; //go back to where u spawned
        WasKilled = true;

    }

    public bool DropLoot()
    {
        bool dropped = false;
        float delta = Random.Range(-1f, 1f);
        Vector3 DropSpot = this.transform.position + new Vector3(delta, delta, 0);
        LootRequest MonsterLootRequest = new LootRequest(new List<int>() { 0 }, 0, this.transform.position, null);

        dropped = LootTableManagerLocator.GetLootManagerService().RequestLoot(MonsterLootRequest);
        return dropped;
    }

    //should be the job of the monster spawner
    //public void RespawnMonster()
    //{
    //    //must reset and reload all health stat before doing this
    //    this.gameObject.SetActive(true); //spawn in

    //}

    public void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerEntity>().TakeDamage(AttackPower.GetValue(), this, AttackHitBox.DamageSpec.PhysicalDamage);
        }
    }

    public override void IncreaseStat(string StatString, float value)
    {
        base.IncreaseStat(StatString, value);

        this.transform.localScale *= Random.Range(0.7f, 1.3f);
    }

    public override void OnCollisionEnter2D(Collision2D collision) //maybe on collision stay
    {
        //Debug.Log("monster has been hit by: " + collision.gameObject.tag + "..." + collision.gameObject.name);
        //if (collision.gameObject.tag == "Weapon" && collision.gameObject.GetComponent<EquipableItem>().Weilder == "Player") //if you are hit by a weapon with and the hitter has a class
        //{
        //    Debug.Log("player has hit this monster..hp before: " + this.Health);
        //    TakeDamage(collision.gameObject.GetComponent<PlayerEntity>().GetAttackPower());
        //    Debug.Log("health after attack: " + this.Health);
        //}

        //collision.contacts


    }

    //public void OnTriggerEnter2D(Collider2D collision) //maybe should do on exit
    //{

    //    //maybe to typeof()
    //    //makes weapons hit from too far cuz of the agro collider
    //    if (collision.gameObject.tag == "Weapon" && collision.gameObject.GetComponent<EquipableItem>().Owner is PlayerEntity) //if you are hit by a weapon with and the hitter has a class
    //    {
    //        if (HitEffects.Count > 0)
    //        {
    //            OnHitEffect E = HitEffects.Dequeue();
    //            E.Activate();
    //            HitEffects.Enqueue(E);
    //        }
    //        if (collision.gameObject.GetComponent<EquipableItem>().Owner == null) Debug.Log("owner is null");
    //        TakeDamage(collision.gameObject.GetComponent<EquipableItem>().Owner.AttackPower.GetValue(), collision.gameObject.GetComponent<EquipableItem>().Owner);
    //        //Debug.Log("health after attack: " + this.Health);
    //    }
    //}

}
