﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider2D))]
public class AttackHitBox : MonoBehaviour {


    internal Collider2D HitBox;
    internal Entity Holder;
    public int LayerInt;
    //public bool UsePolygonCollider = false;
    internal delegate float GetDamageType();
    internal GetDamageType DamagePowerFunction;

    public event Action OnHitBoxEnabled;
    internal event Action<Entity> SuccessHitTarget;
    internal event Action<Entity> SuccessSelf;
    internal event Action OnHit;
    internal bool IsHitting = false;
    public DamageSpec DamageType;

    public enum DamageSpec
    {
        PhysicalDamage = 0,
        SpellDamage = 1,
    }

    private void Awake()
    {
        Holder = GetComponentInParent<Entity>();
        //Holder.EntityAttackHitBox = this;

        //if (UsePolygonCollider) HitBox = GetComponent<PolygonCollider2D>();
        HitBox = GetComponent<BoxCollider2D>();
        if (!HitBox.isTrigger) HitBox.isTrigger = true;

        if (DamageType == DamageSpec.PhysicalDamage) DamagePowerFunction = Holder.GetPhysicalAttackDamage;
        else DamagePowerFunction = Holder.GetSpellAttackDamage;

    }

    //this is used for projectiles
    private void OnEnable()
    {

        if(OnHitBoxEnabled != null) OnHitBoxEnabled();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log(collision.name + " has hit my " + collision.gameObject.name + " attack box with layer: " + collision.gameObject.layer.ToString());
        if (collision.gameObject.layer == LayerInt && collision.gameObject.GetComponent<Entity>() != null)
        {
            Debug.Log("hitting target: " + collision.name);
            Entity Target = collision.gameObject.GetComponent<Entity>();
            Target.TakeDamage(DamagePowerFunction(), Holder, DamageType);
            if (SuccessHitTarget != null) SuccessHitTarget(Target);
            else Debug.Log("succeshittarget is null");

            if(SuccessSelf != null) SuccessSelf(Holder);
            if (OnHit != null) OnHit();
        }
    }



    //public void ToggleHitBox(bool Toggle)
    //{
    //    HitBox.enabled = Toggle;
    //}


}
