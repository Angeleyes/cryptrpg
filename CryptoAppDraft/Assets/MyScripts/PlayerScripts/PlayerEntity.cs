﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Barebones.MasterServer;
//using UnityEngine.Networking;
using System;
using System.Text;


public class PlayerEntity : Entity {

    internal string PlayerName;

    internal SpriteRenderer PlayerSpriteRend;
    internal Weapon PlayerWeapon;
    internal Offhand OffHand;

    public SpriteMask FeetMask;
    public CameraEffects PlayerMainCam;

    public Transform WeaponPosition;
    public Transform OffhandPosition;

    public AttackGrid PlayerAttackGrid;
    public MiniMapCamera Minimapcam;
    public SpriteRenderer ClassIconObject;
    internal Inventory PlayerInventory;
    internal PlayerAuraHandler PlayerAuraHand;
    //internal List<EquipableItem> EquipOnStart = new List<EquipableItem>();

    //private ObservableProfile CharacterProfile;
    public CharacterHUD CharHUD;
    private GameObject HUDParent;
    private AttackHitBox WeaponHitbox;

    public event Action<int> OnExpChange;
    public event Action OnDefeatedBoss;
    public event Action<int> OnLevelUp;
    //public event Action OnPlayerDeath;


    public event Action<Item> OnPickedUpItem;

    internal List<AnimationClip> AttackAnimations = new List<AnimationClip>();

    internal EquipmentManager GearManager;

    StringBuilder Sbuilder = new StringBuilder();

    public override void Start()
    {

        base.Start();

        PlayerSpriteRend = Rend;
        GearManager = GetComponent<EquipmentManager>();

        InitModStats();
        UpdateStatsByLevel();

        //if I feel like chaning this find go part, i can assign it from the gamemanager when spawning players
        HUDParent = GameObject.FindGameObjectWithTag("HUDParent");
        CharHUD = Instantiate(CharHUD, CharHUD.transform.position, Quaternion.identity, HUDParent.transform);


        CharHUD.SetWatchMe(this);
        Stat.StatChanged += () => CharHUD.CharSheet.UpdateDataSheet(CharDataAsString());
        Stat.StatChanged += () => CharHUD.UpdateHUD(Health.GetValue(), HealthMax.GetValue(), Resource.GetValue());

        CharHUD.InitBarValues(Health.GetValue(), HealthMax.GetValue(), Resource.GetValue());
        CharHUD.CharSheet.UpdateDataSheet(CharDataAsString());

        CharHUD.CharSheet.EquipmentChanged += GearManager.UpdateEquipmentStats;
        CharHUD.BuffUI.BuffExpired += RemoveBuff;
        if (PlayerAuraHand == null) PlayerAuraHand = GetComponentInChildren<PlayerAuraHandler>();
        PlayerAuraHand.SetEntityOwner(this, 31); //31 magic number

        InitInventory();

        CharHUD.CharSheet.WeaponEquipped += GearManager.ManageWeapon;
        CharHUD.CharSheet.OffhandEquipped += GearManager.ManageOffhand;
        CharHUD.CharSheet.ArmorEquipped += GearManager.ManageArmorPiece;
        CharHUD.ResetExpBar(ExpUntilNext);

        WeaponHitbox = WeaponPosition.GetComponent<AttackHitBox>();
 
        EntityClass.SetPlayer(this);
        //CharHUD.InitalizeClassAbilites();
        SpriteRenderer PlayerIcon = Instantiate(ClassIconObject, Vector3.zero, Quaternion.identity);
        PlayerIcon.sprite = EntityClass.ClassIcon;
        PlayerIcon.transform.SetParent(this.transform);
        PlayerIcon.transform.localPosition = Vector3.zero;

        PlayerAttackGrid = GetComponent<AttackGrid>();
        Minimapcam.CreateMiniMap(HUDParent, PlayerIcon, CharHUD);

        CameraLocator.Provide(PlayerMainCam); //this should be moved to gamemanager and assign provider only on client

        WeaponHitbox.OnHit += CameraLocator.GetCamEffects().ShakeCamera;
        //Debug.Log(this.gameObject.layer + " layer shit : " + this.gameObject.layer.ToString() + " also " + LayerMask.LayerToName(this.gameObject.layer));

    }



    public string CharDataAsString() //write a function in stat to give this data and put this function into charhud
    {
        //need a better way to do this
        //oh hey there is, appendline oh well, fix later
        Sbuilder.Length = 0;
        Sbuilder.Append("Class: ");
        Sbuilder.Append(EntityClass.ClassName);
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Health: ");
        Sbuilder.Append(Health.GetValue());
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Level: ");
        Sbuilder.Append(Level);
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Experience: ");
        Sbuilder.Append(TotalExperience.GetValue());
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Exp to next: ");
        Sbuilder.Append(ExpUntilNext);
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Strength: ");
        Sbuilder.Append(Strength.GetValue());
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Intelligence: ");
        Sbuilder.Append(Intelligence.GetValue());
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Agility: ");
        Sbuilder.Append(Agility.GetValue());
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Attack Power: ");
        Sbuilder.Append(AttackPower.GetValue());
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Armor: ");
        Sbuilder.Append(Armor.GetValue());
        Sbuilder.Append(Environment.NewLine);
        Sbuilder.Append("Resource Regen: ");
        Sbuilder.Append(ResourceRegen.GetValue());

        return Sbuilder.ToString();
    }

    public void SetUpPlayerProfile(ScriptableClass Class, int level, int Exp, string playername)
    {
        //if (!isLocalPlayer) return;
        EntityClass = Class;
        Level = level;
        TotalExperience.AddToBase(Exp);
        PlayerSpriteRend = GetComponent<SpriteRenderer>();
        PlayerSpriteRend.material.color = EntityClass.ClassColorTheme;
        PlayerName = playername;

        //might merge these two functions or call 1 in the other
        InitModStats();
        UpdateStatsByLevel();
       
        //public static event PickedUpItem <---this needs to be written VERY carefully to avoid hacking and exploiting and cheating
    }



    public void InitInventory()
    {
        PlayerInventory = GetComponent<Inventory>();
        PlayerInventory.SetUI(CharHUD.InvUI);
    }

    public override void LevelUp()
    {
        //if (!isLocalPlayer) return;
        base.LevelUp();

        if (OnLevelUp != null) OnLevelUp.Invoke(Level);
        CharHUD.ResetExpBar(ExpUntilNext);
    }

    public override void GrantExp(float ExpAmount)
    {
        //if (!isLocalPlayer) return;
        base.GrantExp(ExpAmount);
        CharHUD.UpdateExp(CurrentExperience.GetValue(), ExpUntilNext);
    }


    public void PickUpTheItem(Pickup TheItem)
    {
        //if (!isLocalPlayer) return;
        //need very very sctrict rules here to avoid exploints and stuff....hmmmm
        if (OnPickedUpItem != null) OnPickedUpItem.Invoke(TheItem.TheItem);
    }

    /// <summary>
    // change this attack system with scriptable class delegate attack function 
    /// </summary>
    //#############################################################
    //bool attacking = false; //dont like this
    public void BasicAttack()
    {
        //if (!isLocalPlayer) return;
        if (!IsAttacking) StartCoroutine(Attack());
    }

    //this should be in the scriptable class file
    IEnumerator Attack()
    {
        IsAttacking = true;
        //WeaponHitbox.ToggleHitBox(true);
        //Vector3 SmallJump = MoveMotor.GetNormCurrentDirection() * 0.1f;
        //SetEntityPosition(this.transform.position + SmallJump);
        PlayAudioClip(AttackSounds);
        //EntityAnimator.speed = AttackSpeed.GetValue();
        CanMove = false;
        EntityAnimator.SetInteger("AttackID", UnityEngine.Random.Range(1,4));
        Debug.Log("using weapon speed: " + PlayerWeapon.WeaponSpeed);
        yield return new WaitForSeconds((PlayerWeapon.WeaponSpeed - AttackSpeed.GetValue())); 
        EntityAnimator.SetInteger("AttackID", 0);
        IsAttacking = false;
        CanMove = true;    

    }

    //###############################################################

    public override void TakeDamage(float damage, Entity Attacker, AttackHitBox.DamageSpec DamageType)
    {
        if (OffHand != null && OffHand.IsActive)
        {
            Debug.Log("in takedamage, doing offhand logic");
            EntityClass.DoOffhandAbility(damage, Attacker);
            return;
        }
        //if (!isLocalPlayer) return;
        if (HasDamageImmunity) return;
        base.TakeDamage(damage, Attacker, DamageType);
        CharHUD.UpdateHP(Health.GetValue());
        StartCoroutine(TriggerImmune());

    }

    public void OnHPChanged(float NewHP)
    {
        //Debug.Log("sync var on hp changed on player entity to: " + NewHP);
        UpdateHpBar(NewHP);
    }

    public void UpdateHpBar(float newhp)
    {
        CharHUD.UpdateHP(newhp);
    }
    

    IEnumerator TriggerImmune()
    {
        HasDamageImmunity = true;
        StartCoroutine(FlashWhite());
        yield return new WaitForSeconds(0.5f); //should get rid of this magic number
        HasDamageImmunity = false;
    }

    IEnumerator FlashWhite()
    {
        while (HasDamageImmunity)
        {
            PlayerSpriteRend.color = Color.gray;
            yield return new WaitForSeconds(0.03f);
            PlayerSpriteRend.color = Color.white;
            yield return new WaitForSeconds(0.03f);
        }
    }

    public override void Die()
    {
        //if (!isLocalPlayer) return;
        //base.Die();
        Debug.Log("Player died");
    }

    public override void ApplyBuff(Buff B)
    {
        base.ApplyBuff(B);
        CharHUD.BuffUI.AddBuff(B);
    }

    public override void RemoveBuff(Buff B)
    {
        base.RemoveBuff(B);
        //gets signaled by buff ui
    }


    internal override void ToggleLiquidMask(bool toggle)
    {
        bool prevtoggle = FeetMask.gameObject.activeInHierarchy;
        base.ToggleLiquidMask(toggle);
        if (prevtoggle) FeetMask.gameObject.SetActive(false);
        else FeetMask.gameObject.SetActive(prevtoggle);
        
    }

    //public override void OnCollisionEnter2D(Collision2D collision)
    //{
    //    //if (collision.gameObject.tag == "Weapon" && collision.gameObject.GetComponent<EquipableItem>().Weilder == "Player") //if you are hit by a weapon with and the hitter has a class
    //    //{
    //    //    Debug.Log("player has hit this monster..hp before: " + this.Health);
    //    //    TakeDamage(collision.gameObject.GetComponent<PlayerEntity>().GetAttackPower());
    //    //    Debug.Log("health after attack: " + this.Health);
    //    //}
    //}


    ////maybe this should be handled on the item script
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    //if(collision.gameObject.tag == "PickUpItem")
    //    //{
    //    //    Debug.Log(this.EntityClass.ClassName + " is picking up " + collision.gameObject.name);
    //    //    Pickup TheItem = collision.gameObject.GetComponent<Pickup>();
    //    //    PickUpTheItem(TheItem); //the item is set to destroy itselt after 1 second, this should avoid null races conditions but might be a bug anyway
    //    //}
    //}

}
