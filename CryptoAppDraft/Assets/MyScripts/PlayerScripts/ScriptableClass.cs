﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Classes/New Class")]
public class ScriptableClass : ScriptableObject {

    public string ClassName;
    public string ClassDescription;

    public float StrModPercent;
    public float IntModPercent;
    public float AgiModPercent;

    public float HpModPercent;
    public float ResoruceModPercent;

    internal PlayerEntity Playerentity;
    public Color ClassColorTheme;

    public Sprite ClassIcon;

    public Ability OffHandAbility;
    public List<Ability> Abilities = new List<Ability>(3);
    //public List<EquipableItem> StartingGear;

    public enum ClassID
    {
        Barbarian = 0,
        Witch = 1,
        Archer = 2,
    }

    public ClassID ClassType;

    public void SetPlayer(PlayerEntity P)
    {
        Playerentity = P;
    }

    public virtual void DoOffhandAbility(float value, Entity entity)
    {
        OffHandAbility.TriggerAbility();
    }

    public virtual void DoAttack()
    {

    }

}
