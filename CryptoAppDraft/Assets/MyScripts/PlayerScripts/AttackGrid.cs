﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackGrid : MonoBehaviour {


    public float AttackPointPaddingMultiplyer = 1; //min of 1

    private float AttackPointPaddingX;
    private float AttackPointPaddingY;

    private Color UnTakenPoint = Color.yellow;
    public float GizmosRadius = 0.1f;

    private Dictionary<int, AttackPoint> AttackPointTable = new Dictionary<int, AttackPoint>();

    private List<Vector2> AttackPoints = new List<Vector2> { new Vector2(-0.6f,0.6f), new Vector2(0,1.2f), new Vector2(0.6f,0.6f), new Vector2(-0.8f,0), new Vector2(0,0),
    new Vector2(0.8f,0), new Vector2(-0.6f,-0.6f), new Vector2(0,-1.0f), new Vector2(0.6f,-0.6f) };

    private const bool Avaliable = true;
    private const bool Unavaliable = false;


    private float Yoffset = 0.8f;
    // X,Y grid: -1,1   0,1  1,1
    //           -1,0   0,0  1,0
    //           -1,-1  0,-1 1,-1

    // x patter is -1 0 1 for each
    // y pattern is 1x3  0x3   -1x3 in a row

    public void Start()
    {
        Gizmos.color = UnTakenPoint;
        int Pointid = 100;
        //AttackPoints.Add(this.transform.position)

        for (int i = 0; i < AttackPoints.Count; i++)
        {
            AttackPoints[i] = new Vector2(AttackPoints[i].x, AttackPoints[i].y + Yoffset);
        }

        foreach(Vector2 V in AttackPoints)
        {
            AttackPoint NewPoint = new AttackPoint();
            NewPoint.AssignID(Pointid);
            NewPoint.SetPointCoordinates(V);
            NewPoint.Release();
            AttackPointTable.Add(NewPoint.PointID, NewPoint);
            Pointid++;
        }
    }


    private AttackPoint GetNextAvaliablePoint()
    {
        AttackPoint Point = null;

        foreach (KeyValuePair<int,AttackPoint> K in AttackPointTable)
        {
            if (K.Value.IsOpen)
            {
                Point = K.Value;
            }
        }

        //Debug.Log("returning avaliable point: " + Point.PointID + " and isopen?: " + Point.IsOpen);
        return Point;
    }


    public AttackPoint GetNearestAvaliablePoint(Vector3 AttackerPosition)
    {
        AttackPoint Point1 = GetNextAvaliablePoint();
        if (Point1 == null) return Point1;

        Point1.Release(); // need to release here becuase it gets occupied at the end and if I swap, then 2 occupy for 1 point request

        foreach(KeyValuePair<int, AttackPoint> K in AttackPointTable)
        {
            AttackPoint Point2 = K.Value; // 2nd point
            //Debug.Log("evaluating distance of point " + Point2.PointID + "...is open?: " + Point2.IsOpen);
            //Debug.Log("point 1: " + Point1.PointID + "...is open?:" + Point1.IsOpen);

            if (!Point2.IsOpen || (Point1.PointID == Point2.PointID)) continue; //if the point is closed or is the same point we are trying to compare, then skip


            //Debug.Log("checking point distance between p1 and p2,   p2 distance: " + Vector3.Distance(AttackerPosition, GetWorldPositionByPoint(Point2)) + "::: p1 distance: " + Vector3.Distance(AttackerPosition, GetWorldPositionByPoint(Point1)));
            if (Vector3.Distance(AttackerPosition, GetWorldPositionByPoint(Point2)) < Vector3.Distance(AttackerPosition, GetWorldPositionByPoint(Point1)))
            {
                //Debug.Log("closer point found");
                Point1 = Point2;
            }
        }

        //Debug.Log("occupying point" + Point1.AttackPointCoordinates);
        Point1.Occupy();
        //Debug.Log("occupying point " + Point1.PointID);
        return Point1;

    }

    //private AttackPoint Get

    public Vector3 GetWorldPositionByPoint(AttackPoint P)
    {
        if (P == null) Debug.LogError("point is null in world position function....weird");
        return new Vector3(this.transform.position.x + P.AttackPointCoordinates.x, this.transform.position.y + P.AttackPointCoordinates.y, 0);
    }

    private void OnDrawGizmos() //relies on other lists that get filled in the start to be used....can't preview in 
    {
        if (AttackPointTable.Count > 0)
        {

            foreach(KeyValuePair<int,AttackPoint> K in AttackPointTable)
            {

                Gizmos.color = (K.Value.IsOpen) ? Color.yellow : Color.red;

                Gizmos.DrawSphere(GetWorldPositionByPoint(K.Value), GizmosRadius);
            }
        }

    }


}
