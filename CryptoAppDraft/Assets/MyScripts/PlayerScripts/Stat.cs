﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Stat {

    [SerializeField]
    public float BaseValue;
    public static event Action StatChanged;


    public List<float> Modifiers = new List<float>();
    public List<float> StatBuffs = new List<float>();

    Stat()
    {
        BaseValue = 0;
    }

    public Stat(float value)
    {
        BaseValue = value;
    }

    public float GetValue()
    {
        float FinalValue = BaseValue;

        Modifiers.ForEach(value => FinalValue += value);
        StatBuffs.ForEach(value => FinalValue += value);

        if (FinalValue < 0) FinalValue = 0; //stats cant go negative
        return FinalValue;
    }

    public float GetBaseValue()
    {
        return BaseValue;
    }

    public void AddToBase(float value)
    {
        BaseValue += value;
        //might want to add the non negative thing to base aswell....
        if(StatChanged != null) StatChanged.Invoke();
    }

    public void AddModifier(float value)
    {
        if (value != 0) Modifiers.Add(value);
        if (StatChanged != null) StatChanged.Invoke();
    }

    public void BuffStat(Buff B)
    {
        if (B.AmountToAffect != 0)
        {
            if (B.AsPercent) StatBuffs.Add(Mathf.Round((B.AmountToAffect * 0.01f) * GetBaseValue()));
            else StatBuffs.Add(B.AmountToAffect);
            //Debug.Log("buff " + B.name + " added ");
            if (StatChanged != null) StatChanged.Invoke();
        }
    }

    public void RemoveBuffStat(Buff B)
    {
        if (B.AmountToAffect != 0)
        {
            if (B.AsPercent)
            {
                //float v = (B.AmountToAffect * 0.01f) * GetBaseValue();
                //Debug.Log("trying to remove buff: " + v + " with roun: " + Mathf.Round(v));
                StatBuffs.Remove(Mathf.Round((B.AmountToAffect * 0.01f) * GetBaseValue()));
                //Debug.Log("after removed buff count: " + StatBuffs.Count);

            }
            else StatBuffs.Remove(B.AmountToAffect);
            if (StatChanged != null) StatChanged.Invoke();
        }
    }

    public void RemoveAllModifiers()
    {
        Modifiers.Clear();
    }

    public void RemoveModifier(float value)
    {
        if (value != 0) Modifiers.Remove(value);
        if (StatChanged != null) StatChanged.Invoke();
    }

}

