﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Offhand : EquipableItem {


    public bool IsActive { get; private set; }
    public Action OffhandFunction { get; private set; }
    public Action<int> OffhandFunction2 { get; private set; }
    public float OffhandCooldown = 0.5f;

    public AudioClip OffhandSoundEffect;

    internal PlayerEntity Holder;
    internal float OffhandTimer = 0;

    public void SetUpOffhand(PlayerEntity P)
    {
        Holder = P;
        this.gameObject.SetActive(true);
        Debug.Log("setup offhand done");
        //this.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void Active()
    {
        if (Holder != null && !IsActive && Holder.OffHand != null) StartCoroutine(StartCooldown()); //the !isactive is a doulbe check...
        else Debug.Log("no holder attached to offhand");
    }


    /// <summary>
    // should tie this into the ability cooldown
    /// </summary>
    /// This should be worked into the shield ability scriptable class
    IEnumerator StartCooldown()
    {
        Debug.Log("starting off hand cooldown anims");
        IsActive = true;
        Holder.ActiveOffhandAbility = true;
        Holder.EntityAnimator.SetBool("Ability1Trigger", true);
        Holder.EntityAnimator.SetBool("OffhandTrigger", true);
        Holder.CanMove = false;
        
        while (OffhandTimer < OffhandCooldown)
        {
            OffhandTimer += Time.deltaTime;
            if (OffhandTimer >= OffhandCooldown)
            {
                Holder.CanMove = true;
                Debug.Log("offhand anim is done..ending anim");
                OffhandTimer = 0;
                Holder.EntityAnimator.SetBool("OffhandTrigger", false);
                Holder.EntityAnimator.SetBool("Ability1Trigger", false);
                IsActive = false;
                Holder.ActiveOffhandAbility = false;
                yield break;

            }
            yield return null;
        }
        yield break;
    }

}
