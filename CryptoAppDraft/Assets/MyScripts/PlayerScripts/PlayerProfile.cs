﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Barebones.MasterServer;
//using Barebones.Networking;
//: ServerModuleBehaviour
public class PlayerProfile {

    public const int InvKey = 1000;
    public const int CharListKey = 1001;
    public const int ExpKey = 1002;
    public const int CryptLvlKey = 1003;


    internal static Dictionary<string, int> Inventory; //string is the hash key (owner) from the blockchain, int is the id that corresponds to the item in the item table

    internal static Dictionary<string, int> CharacterList; //string is the class name, int is the class level

    internal static Dictionary<string, int> ExpTable; //string is the class name, int is the exp amount


    private void Start()
    {

        CharacterList = new Dictionary<string, int>();
        ExpTable = new Dictionary<string, int>();
        Inventory = new Dictionary<string, int>();
        
    }

    void Awake()
    {
        // Request for profiles module
        //OUT
        //AddOptionalDependency<ProfilesModule>();
        //AddOptionalDependency<ProfilesDbMysql>();

        Debug.Log("awaking profile");



            // We can still allow players to play with default profile ^_^

            // Let's spawn the player character
            //var playerObject = MiniNetworkManager.SpawnPlayer(player.Connection, player.Username, "carrot");

            // Set coins value from profile
            //playerObject.Coins = coinsProperty.Value;

            //playerObject.CoinsChanged += () =>
            //{
            //    // Every time player coins change, update the profile with new value
            //    coinsProperty.Set(playerObject.Coins);
            //};
        
    }


    //public IProfilesDatabase ProfileDatabase()
    //{
    //    Debug.Log("inside profile call");
    //    if(Database != null) return Database;
    //    else
    //    {
    //        IProfilesDatabase D = null;
    //        Debug.Log("WARNING, DB IS NULL IN PROFILERS");
    //        return D;
    //    }
    //}

        //OUT
    //public override void Initialize(IServer server)
    //{
    //    base.Initialize(server);
    //    //Database = Msf.Server.DbAccessors.GetAccessor<IProfilesDatabase>();
    //    Debug.Log("inizilazlied");
    //    var profilesModule = server.GetModule<ProfilesModule>();

    //    if (profilesModule == null)
    //    {
    //        Debug.Log("profile moduel is null");
    //        return;
    //    }

    //    // Set the new factory
    //    profilesModule.ProfileFactory = CreatePlayerProfileInServer;
    //    Logs.Warn("Created Demo profile factory");
    //}


        //OUT
    //public static ObservableServerProfile CreatePlayerProfileInServer(string username, IPeer peer) //this is a new custom factory
    //{
    //    return new ObservableServerProfile(username, peer)
    //    {
    //        new ObservableDictStringInt(InvKey, Inventory),
    //        new ObservableDictStringInt(CharListKey, CharacterList), //check for class choice empty perhaps
    //        new ObservableDictStringInt(ExpKey, ExpTable),
    //        new ObservableInt(CryptLvlKey, 1)
                    
    //    };
    //}

    //OUT
    //public static ObservableServerProfile CreatePlayerProfileInServer(string username)
    //{
    //    return CreatePlayerProfileInServer(username, null);
    //}


}
