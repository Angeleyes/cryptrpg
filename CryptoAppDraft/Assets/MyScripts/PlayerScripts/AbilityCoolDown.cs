﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System;

public class AbilityCoolDown : UIElement {

    public KeyCode KeyDownTrigger;
    public bool UsesMouseInput = false;
    public Image DarkMask;

    private Image ButtonImage;

    public Text CoolDownText;

    [SerializeField] internal Ability TheAbility; //serialized for testing
    //[SerializeField] private GameObject Weapon; //needed for shooting data and logic.....wont do it this way

    private AudioSource AudioPlayer;

    private float CoolDownDuration;
    private float NextReadyTime;
    private float CoolDownRemainingTime;

    internal bool CoolDownFinished = true;

    internal delegate bool InputChoice();
    internal InputChoice InputType;


    void Start()
    {
        //at the moment the mouseinput is not working for ability, but for using the shield ability it has to be
        //inside player controller becuase it has a lower priority than interacting which also uses right mouse button
        //ill solve this mystery not working later, for now it doesnt matter
        InputType = KeyDownTrue;
    }

    private void Update()
    {

        if (TheAbility != null && InputType() && CoolDownFinished && TheAbility.IsAvaliable && TheAbility.HasResource)
        {
            OnKeyHit();
        }
    }


    internal void CheckResource(int CurrentResourceValue)
    {
        if (TheAbility.IsAvaliable)
        {
            if (CurrentResourceValue >= TheAbility.ResourceCost)
            {
                TheAbility.HasResource = true;
                ButtonImage.color = Color.white;
            }
            else
            {
                TheAbility.HasResource = false;
                ButtonImage.color = Color.red;
            }
        }

        //finish writing this with event on resource changed
    }

    private void ProccessedDesc()
    {
        //Debug.Log("processing desc for : " + TheAbility.name);
        if (TheAbility.AbilityDescription == "") return;
        StringBuilder NewString = new StringBuilder();
        string[] P = TheAbility.AbilityDescription.Split(' ');

        for (int i = 0; i < P.Length; i++)
        {
            NewString.Append(P[i]);
            NewString.Append(" ");
            if ((i + 1) % 5 == 0) NewString.Append(Environment.NewLine);
        }

        TheAbility.TooltipDesc = NewString.ToString();
    }

    //private bool MouseInputTrue()
    //{
    //    Debug.Log("abiliyty cooldown update on key hit mouse ");
    //    return Input.GetMouseButtonDown(1);
    //}

    private bool KeyDownTrue()
    {
        return Input.GetKeyDown(KeyDownTrigger);
    }

    public void InitalizeAbility(Ability SelectedAbility)
    {
        //Debug.Log("initalizing ability : " + SelectedAbility.name + " in ability cool down");
        TheAbility = SelectedAbility;

        ButtonImage = GetComponent<Image>();
        ButtonImage.sprite = SelectedAbility.AbilityIcon;

        AudioPlayer = GetComponent<AudioSource>();
        AudioPlayer.clip = SelectedAbility.AbilitySoundEffect;

        CoolDownDuration = SelectedAbility.AbilityBaseCoolDown;
        ProccessedDesc();
        AbilityReady();

    }

    public void AbilityReady()
    {
        //Debug.Log("setting ability ready");
        CoolDownText.gameObject.SetActive(false);
        DarkMask.gameObject.SetActive(false);
    }


    IEnumerator Cooldown()
    {
        CoolDownFinished = false;       

        while(CoolDownRemainingTime > 0)
        {
            CoolDownRemainingTime -= Time.deltaTime;
            DarkMask.fillAmount = (CoolDownRemainingTime / CoolDownDuration);
            CoolDownText.text = Mathf.Round(CoolDownRemainingTime).ToString();
            yield return null;
        }


        CoolDownFinished = true;
        CoolDownRemainingTime = NextReadyTime;
        AbilityReady();

        yield break;
    }

    internal void OnKeyHit()
    {
        NextReadyTime = CoolDownDuration + Time.time; //whats time.time
        CoolDownRemainingTime = CoolDownDuration;

        DarkMask.gameObject.SetActive(true);
        CoolDownText.gameObject.SetActive(true);

        if(AudioPlayer.clip != null) AudioPlayer.Play();

        TheAbility.TriggerAbility();
        TheAbility.OnResourceUsed(TheAbility.ResourceCost);

        StartCoroutine(Cooldown());
    }

    public override string GetTooltipData(out UIElement U)
    {
        U = this;
        string Data = "";
        if (TheAbility != null) Data += TheAbility.GetAbilityData();

        return Data;
    }
}
