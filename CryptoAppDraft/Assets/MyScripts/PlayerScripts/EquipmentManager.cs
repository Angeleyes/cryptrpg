﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour {


    public Transform GearAnimated;

    PlayerEntity ThePlayer;

    private const bool Equip = true;
    private const bool Unequip = false;

    private SpriteRenderer WeaponSpriteRend;
    private SpriteRenderer PlayerOffhandRend;
    private Transform BootParent;


    private Transform HelmParent;
    private Transform ChestParent;
    private Transform PantsParent;

    private GameObject SpecialGear;

    private Dictionary<SlotType, List<SpriteRenderer>> GearFramesTable = new Dictionary<SlotType, List<SpriteRenderer>>();
    private Dictionary<SlotType, Armor> CurrentEquippedArmor = new Dictionary<SlotType, Armor>();
    private Dictionary<SlotType, GameObject> ArmorRenderLookUp = new Dictionary<SlotType, GameObject>();

    Entity.AnimationEnum CurrentAnimation;

    private void Start()
    {
        ThePlayer = GetComponent<PlayerEntity>();
        WeaponSpriteRend = ThePlayer.WeaponPosition.gameObject.GetComponent<SpriteRenderer>();
        PlayerOffhandRend = ThePlayer.OffhandPosition.gameObject.GetComponent<SpriteRenderer>();
        //ThePlayer.MoveMotor.OnDirectionChanged += ChangeSprites;
        //ThePlayer.OnAnimationChanged += OnAnimationChange;

        //ThePlayer.WeaponPosition.gameObject.SetActive(false);
        SetUpGear();
    }

    //toggle is true if equipped, false if unequipped
    void ToggleItem(EquipableItem Item, bool Toggle)
    {

        if (Item is Armor)
        {
            if (Toggle) CurrentEquippedArmor.Add(Item.ItemType, Item as Armor);
            else CurrentEquippedArmor.Remove(Item.ItemType);
        }

        if (Item.ItemType == SlotType.Weapon)
        {
            ThePlayer.WeaponPosition.gameObject.SetActive(Toggle);
            if (!ThePlayer.WeaponPosition.gameObject.activeInHierarchy)
            {
                ThePlayer.WeaponPosition.gameObject.GetComponent<SpriteRenderer>().sprite = null;
                ThePlayer.PlayerWeapon = null;
            }
            return;
        }
        else if (Item.ItemType == SlotType.Ring || Item.ItemType == SlotType.Ring2) return;

        //manage armor
        GameObject Garmor;
        ArmorRenderLookUp.TryGetValue(Item.ItemType, out Garmor);

        if (Garmor != null) Garmor.SetActive(Toggle);

    }

    public void ManageWeapon(EquipableItem Weapon)
    {
        //Weapon = CharHUD.CharSheet.GetWeapon();
        //Weapon.Owner = this;
        ThePlayer.PlayerWeapon = Weapon as Weapon;
        Debug.Log("managing weapon...");
        //ThePlayer.WeaponPosition.gameObject.SetActive(true);

        Weapon.transform.SetParent(ThePlayer.WeaponPosition);
        Weapon.transform.position = ThePlayer.WeaponPosition.position;

        if (Weapon != null)
        {
            //all gear must be done this way
            WeaponSpriteRend.sprite = Weapon.ItemSprite;
            WeaponSpriteRend.color = Weapon.SpriteColorTint;
            //ThePlayer.WeaponPosition.transform.localScale = Weapon.transform.localScale;
            Debug.Log("setting weapon sprite");
        }
        else
        {
            Debug.Log("weapon null in manage weapon");
            WeaponSpriteRend.sprite = null;
        }
    }

    public void ManageOffhand(Offhand TheOffhand)
    {
        if (TheOffhand == null)
        {
            ThePlayer.OffHand = null;
            ThePlayer.EntityClass.OffHandAbility.IsAvaliable = false;
            return;

        }
        ThePlayer.OffHand = TheOffhand;
        ThePlayer.EntityClass.OffHandAbility.Initalize(ThePlayer);

        TheOffhand.transform.SetParent(ThePlayer.OffhandPosition);
        TheOffhand.transform.position = ThePlayer.OffhandPosition.position;

        if (TheOffhand != null)
        {
            PlayerOffhandRend.sprite = TheOffhand.ItemSprite;
            PlayerOffhandRend.color = TheOffhand.SpriteColorTint;
            TheOffhand.SetUpOffhand(ThePlayer); //sets the holder and sets object active
            Debug.Log("setting offhand avaliable");
            ThePlayer.EntityClass.OffHandAbility.IsAvaliable = true;
            //offhandposition.sprite = theoffhand.backsprite;
        }
        else
        {
            Debug.Log("offhand is null, doing null stuff");
            PlayerOffhandRend.sprite = null;
            ThePlayer.OffHand = null;
            ThePlayer.EntityClass.OffHandAbility.IsAvaliable = false;
        }
    }


    //wow it works
    public void ChangeSprites(Entity.AnimationEnum AnimType)
    {
        //call by event on animation
        int AnimIndex = (int)AnimType;
        //Debug.Log("using animtype: " + AnimIndex + " with counts: " + CurrentEquippedArmor.Count);
        foreach(KeyValuePair<SlotType, Armor> Arm in CurrentEquippedArmor)
        {
            if (Arm.Value.ItemType == SlotType.Helm || Arm.Value.ItemType == SlotType.ChestArmor || Arm.Value.ItemType == SlotType.Pants)
            {
                //Debug.Log("changing sprites in armor: " + Arm.Value.ItemType + " with gear frames count: " + GearFramesTable[Arm.Key].Count);
                for (int i = 0; i < GearFramesTable[Arm.Key].Count; i++) //4
                {
                    //if (ArmorRenderLookUp[Arm.Key].activeInHierarchy)
                    //{
                    //Debug.Log("changing sprite of " + GearFramesTable[Arm.Key][i].name + " to :" + Arm.Value.GearSprites.GearSpriteFrameList[AnimIndex][i].name);
                    //i is the frame
                    GearFramesTable[Arm.Key][i].sprite = Arm.Value.GearSprites.GearSpriteFrameList[AnimIndex][i];
                    //the gear frame table is the list of gear objects (helm, chest pants) and the arm.key tells the table
                    //which armor piece object to get, the i is the frame slots (frame0,1,2,3...) and its accesses the sprite
                    // and sets it...the Arm is the armor piece that is equipped and it has its own gearsprite table
                    //and AnimIndex is the animation type that it needs to set its sprites to and which sprite frame it neees (the i)

                    //SPECIAL NOTE: if the animation has more than 4 frames, it should spill over the the 2 extra frames that go unused in a 4 frame anim
                    //if it has 4 frames, then only the 4 will matter, but in six i just need to add frame5 and 6 to turn on
                    //the extra 2 frames in the gearparent can just have repeated sprites that will never be turned on in the animation

                    //GearFramesTable[Arm.Key][i].color = //gota get the color from somewhere
                    //}
                }
            }
        }

    }

    public void ManageArmorPiece(Armor ArmorPiece)
    {
        Debug.Log("managing armor " + ArmorPiece.name);
        if(ArmorPiece.GearSprites != null) ArmorPiece.GearSprites.SetupGearList();
    }




    public void UpdateEquipmentStats(EquipableItem NewItem, EquipableItem OldItem)
    {
        //is called when equipped or unequipped


        if (OldItem != null)
        {
            foreach (KeyValuePair<string, Stat> S in OldItem.EquipmentStats)
            {
                if (S.Value != null)
                {
                    if (S.Value.GetValue() > 0) ThePlayer.EntityStats[S.Key].RemoveModifier(S.Value.GetValue());
                }
                //remove buffs, auras, and effects, on hit effect watever
            }

            //if (NewItem.AuraEffect == null) Debug.Log("aura is null...but i knew that");
            //else if (ThePlayer.EntityAuraHandler == null) Debug.Log("aura handler null here....weird");


            if (OldItem.AuraEffect != null) ThePlayer.PlayerAuraHand.UnEquipAura(OldItem.AuraEffect);

            ToggleItem(OldItem, Unequip);
        }


        if (NewItem != null)
        {
            //foreach stat on the new piece of gear, add them all to the player stats by modification
            foreach (KeyValuePair<string, Stat> S in NewItem.EquipmentStats) //make equipmentstats readonly
            {
                //if the stat is greater than zero, but could change to != 0 for negative stats
                if (S.Value != null)
                {
                    Debug.Log("trying to find key: " + S.Key);
                    if (S.Value.GetValue() > 0) ThePlayer.EntityStats[S.Key].AddModifier(S.Value.GetValue());
                }

            }

            if (NewItem.AuraEffect != null) ThePlayer.PlayerAuraHand.EquipAura(NewItem.AuraEffect);

            ToggleItem(NewItem, Equip);
        }

    }



    //Sorts the gear parent objects and frame objects
    private void SetUpGear()
    {

        Transform[] Gear = GearAnimated.GetComponentsInChildren<Transform>();
        //Debug.Log("gear length: " + Gear.Length);
        //int count = 0;
        //foreach(Transform T in Gear)
        //{
        //    Debug.Log(count + " " + T.name);
        //    count++;
        //}

        HelmParent = Gear[1];
        ChestParent = Gear[2];
        PantsParent = Gear[7];

        BootParent = Gear[12];
        SpecialGear = Gear[13].gameObject;


        GetFrames(HelmParent, SlotType.Helm);
        GetFrames(ChestParent, SlotType.ChestArmor);
        GetFrames(PantsParent, SlotType.Pants);

        ArmorRenderLookUp.Add(SlotType.Helm, HelmParent.gameObject);
        ArmorRenderLookUp.Add(SlotType.ChestArmor, ChestParent.gameObject);
        ArmorRenderLookUp.Add(SlotType.Pants, PantsParent.gameObject);

        ArmorRenderLookUp.Add(SlotType.Special, SpecialGear.gameObject);
        ArmorRenderLookUp.Add(SlotType.Boots, BootParent.gameObject);
        //starts off initially and is turn on later when you have that item equipped



        HelmParent.gameObject.SetActive(false);
        ChestParent.gameObject.SetActive(false);
        PantsParent.gameObject.SetActive(false);
        BootParent.gameObject.SetActive(false);

    }


    private void GetFrames(Transform T, SlotType Type)
    {
        List<SpriteRenderer> SpriteFrames = new List<SpriteRenderer>();
        foreach(SpriteRenderer S in T.GetComponentsInChildren<SpriteRenderer>()) // the 4 frame objects
        {
            SpriteFrames.Add(S);
        }

        GearFramesTable.Add(Type, SpriteFrames);

    }


    //This script looks at a special gear gameobject that has all the gear objects as its children
    //each gear child has 4 frame objects as children
    //if the player equips a piece of gear (like a helm) that gear object turns on
    //if they gear object is on, the animation controls which different sprite frames turn on and off





    //Go into the gear parent
    //Grab all of the specific armor parents
    //Grab all of the armor parents 4 frame objects

    //When equipping an armor piece, grab all of its gear sprites frames from the scriptablegearlist

    //Depending on the characters direction and animation, change all 4 frame sprites to the correct sprites
    //The anim will turn on and off each frame accordingly and set its position






}
