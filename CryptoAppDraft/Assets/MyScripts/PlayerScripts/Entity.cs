﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
//using UnityEngine.Networking.NetworkSystem;
using System;
using UnityEngine.UI;

public abstract class Entity : MonoBehaviour {

    public ScriptableClass EntityClass;
    public Vector3 TransformOffset = new Vector3();
    internal float ArmorPercent = 0.1f;

    //[SyncVar(hook = "OnHPChanged")]
    internal BoxCollider2D EntityCollider;

    public Stat Health;

    public Stat HealthMax;
    public Stat Resource;
    public Stat ResourceMax;
    public Stat ExpWorth;
    public Stat TotalExperience;
    public Stat CurrentExperience;
    public float ExpUntilNext = 200f;

    public Stat Strength;
    public Stat Intelligence;
    public Stat Agility;

    public Stat AttackPower;
    public Stat SpellPower;
    public Stat Armor;
    public Stat AttackSpeed;
    public Stat CriticalHitChance;
    public Stat LifeSteal;
    public Stat ResourceRegen;
    public Stat HealthRegen;
    public Stat RunSpeed;

    //public event Action OnLevelUp;

    internal Dictionary<string, Stat> EntityStats;

    internal List<Buff> CurrentBuffs = new List<Buff>();

    internal float StatIncreasePerLevel = 5; //percentage
    public int Level;

    public OnHitEffect HitEffect;
    public OnHitEffect DeathEffect;
    public EffectHandler EntityEffectHandler;

    internal bool HasDamageImmunity = false;

    internal Movement MoveMotor;
    public bool CanMove = true;

    internal Rigidbody2D EntityBody;
    internal Animator EntityAnimator;

    internal bool IsAttacking = false;
    internal bool ActiveOffhandAbility = false;

    public List<AudioClip> AttackSounds = new List<AudioClip>();
    public List<AudioClip> AbilitySounds = new List<AudioClip>();
    public List<AudioClip> HitTakenSounds = new List<AudioClip>();
    public SpriteMask LiquidMask;

    internal Shadow2DAnimated EntityShadow;
    internal AudioSource AudioPlayer;
    internal SpriteRenderer Rend;
    internal Color StartColor;

    internal event Action<Entity> OnEntityDeath;
    internal event Action<Entity> OnHitTaken;

    private const float ExpIncrease = 200f;

    public enum AnimationEnum
    {
        IdleDown = 0,
        IdleUp = 1,
        IdleLeft = 2,
        IdleRight = 3,

        WalkingDown = 4,
        WalkingUp = 5,
        WalkingLeft = 6,
        WalkingRight = 7,

        AttackingDown = 8,
        AttackingUp = 9,
        AttackingLeft = 10,
        AttackingRight = 11,

        SpecailDown = 12,
        SpecialUp = 13,
        SpecialLeft = 14,
        SpecialRight = 15,

        Special2Down = 16,
        Special2Up = 17,
        Special2Left = 18,
        Special2Right = 19,

        Attacking2Down = 20,
        Attacking2Up = 21,
        Attacking2Left = 22,
        Attacking2Right = 23,

        Attacking3Down = 24,
        Attacking3Up = 25,
        Attacking3Left = 26,
        Attacking3Right = 27,
    }

    public AnimationEnum AnimationType;

    public virtual void Start () //maybe changet to awake
    {
        StartCoroutine(Animate());

    }

    public virtual void Awake()
    {

        EntityCollider = GetComponent<BoxCollider2D>();
        EntityEffectHandler = GetComponentInChildren<EffectHandler>();

        EntityShadow = GetComponent<Shadow2DAnimated>();



        if (CanMove)
        {
            MoveMotor = GetComponent<Movement>();
        }
        AudioPlayer = GetComponent<AudioSource>();
        EntityBody = GetComponent<Rigidbody2D>();
        EntityAnimator = GetComponent<Animator>();
        Rend = GetComponent<SpriteRenderer>();
        StartColor = Rend.color;
        //EntityAnimator.clip
        //EntityAnimator.speed = MoveMotor.Speed;

        EntityStats = new Dictionary<string, Stat>();

        EntityStats.Add("Health", Health);
        EntityStats.Add("HealthMax", HealthMax);
        EntityStats.Add("Resource", Resource);
        EntityStats.Add("ResourceMax", ResourceMax);
        EntityStats.Add("ExpWorth", ExpWorth);
        EntityStats.Add("Experience", TotalExperience);
        EntityStats.Add("Strength", Strength);
        EntityStats.Add("Intelligence", Intelligence);
        EntityStats.Add("Agility", Agility);
        EntityStats.Add("AttackPower", AttackPower);
        EntityStats.Add("SpellPower", SpellPower);
        EntityStats.Add("AttackSpeed", AttackSpeed);
        EntityStats.Add("Armor", Armor);
        EntityStats.Add("CriticalHitChance", CriticalHitChance);
        EntityStats.Add("LifeSteal", LifeSteal);
        EntityStats.Add("ResourceRegen", ResourceRegen);
        EntityStats.Add("RunSpeed", RunSpeed);
    }

    //doing anim.stuff, need an update function

    IEnumerator Animate() //could put this inside of the move functions
    {
        yield return new WaitForSeconds(0.5f);

        if (MoveMotor == null) Debug.LogError("motor is null ");

        float LastMoveX = 0;
        float LastMoveY = 0;

        while (true)
        {
            EntityAnimator.SetFloat("MoveX", MoveMotor.GetCurrentDirection().x);
            EntityAnimator.SetFloat("MoveY", MoveMotor.GetCurrentDirection().y);
            EntityAnimator.SetBool("EntityMoving", MoveMotor.IsMoving);
            EntityAnimator.SetBool("IsAttacking", IsAttacking);

            //if (MoveMotor.LastMove.x == MoveMotor.LastMove.y && MoveMotor.LastMove.y > 0.1f) LastMoveY = 0;
            //else LastMoveX = 0;
            LastMoveX = MoveMotor.LastMove.x;
            LastMoveY = MoveMotor.LastMove.y;

            if (Mathf.Abs(MoveMotor.LastMove.x) > Mathf.Abs(MoveMotor.LastMove.y)) LastMoveY = 0;
            else LastMoveX = 0;

            
            // if y is pos and x == y, go with x
            //if y is neg and x == y go with y...or x

            EntityAnimator.SetFloat("LastMoveY", LastMoveY);
            EntityAnimator.SetFloat("LastMoveX", LastMoveX);

            //set animated shadow here
            //S.AnimatedShadowData.Shadow.ShadowRenderer.sprite = S.AnimatedShadowData.TheEntity.Rend.sprite;
            if(EntityShadow != null) EntityShadow.ShadowRenderer.sprite = this.Rend.sprite;

            yield return null;
        }

    }


    public Vector3 GetEntityPosition()
    {
        return new Vector3(this.transform.position.x, this.transform.position.y, 0) + TransformOffset;
    }

    public void SetEntityPosition(Vector3 pos)
    {
        this.transform.position = (pos - TransformOffset);
    }

    public virtual void IncreaseStat(string statname, float value)
    {
        Stat S = null;
        EntityStats.TryGetValue(statname, out S);
        if (S != null)
        {
            //Debug.Log("increasing stat: " + S.ToString() + " by : " + value + " for " + this.name);
            S.AddModifier(value);
        }
        else Debug.Log("could not find stat : " + statname);
       
    }

    //at the moment, monster buffs are perm, they don't decay
    public virtual void ApplyBuff(Buff B)
    {
        //Debug.Log("applying buff : " + B.name + " to entity: " + this.name);
        if (CurrentBuffs.Contains(B))
        {
            //Debug.Log("already have this buff");
            return;
        }
        else
        {
            CurrentBuffs.Add(B);

            Stat S = null;
            EntityStats.TryGetValue(B.StatToAffect, out S);
            if (S != null)
            {
                //Debug.Log("applying buff " + B.name);
                S.BuffStat(B);
            }
        }
        //IncreaseStat(B.StatToAffect, B.AmountToAffect);

        if (B.ColorTint.a != 0 && B.ColorTint != Color.black) Rend.color = B.ColorTint;
    }

    public virtual void RemoveBuff(Buff B)
    {
       // Debug.Log("removing buff " + B.name + " from entity : " + this.name + " with value : " + B.AmountToAffect);
        if (CurrentBuffs.Contains(B))
        {
            CurrentBuffs.Remove(B);

            Stat S = null;
            EntityStats.TryGetValue(B.StatToAffect, out S);
            if (S != null)
            {
                //Debug.Log("removing the buff ");
                S.RemoveBuffStat(B);
            }
            else Debug.Log("S is null in stat table");

        }
        else Debug.Log("cant find buff to remove from current buffs");

        if (B.ColorTint != Color.black) Rend.color = StartColor;
    }

    public virtual void RemoveStatIncrease(string Statname, float value)
    {
        Stat S = null;
        EntityStats.TryGetValue(Statname, out S);
        if (S != null)
        {
            Debug.Log("removing stat inc: " + value);
            S.RemoveModifier(value);
        }
    }

    public virtual void TakeDamage(float damage, Entity Attacker, AttackHitBox.DamageSpec DamageType) //have argument for modifiers later
    {
        PlayAudioClip(HitTakenSounds);


        if (DamageType == AttackHitBox.DamageSpec.PhysicalDamage)
        {
            int Ar = Mathf.RoundToInt(Armor.GetValue() * ArmorPercent); //apply armor reductions    
            Ar = Mathf.Clamp(Ar, 0, int.MaxValue);
            damage -= Ar;
        }
        else
        {
            Debug.Log("taking spell damage and reducing it damage: " + damage + " by " + ((Strength.GetValue() * 0.01f) + (Intelligence.GetValue() * 0.01f)));
            damage -= ((Strength.GetValue() * 0.01f) + (Intelligence.GetValue() * 0.1f));
        }

        //Health.AddToBase(damage * -1); //multiplay by negative 1 to subtract hp
        Health.AddToBase(damage * -1);
        if (Health.GetValue() <= 0)
        {
            Die();
            if (Attacker == null)
            {
                Debug.Log("attacker is null");
                return;
            }
            Attacker.GrantExp(this.ExpWorth.GetValue());
        }


    }


    public virtual float GetPhysicalAttackDamage()
    {
        float Damage = AttackPower.GetValue() + (Strength.GetValue() * 0.05f); //5% of strength
        return Damage;
    }

    public virtual float GetSpellAttackDamage()
    {
        float Damage = SpellPower.GetValue() + (Intelligence.GetValue() * 0.05f); //5% of int
        return Damage;
    }

    public virtual void HealHP(float amount)
    {
        Health.AddModifier(amount);
    }

    public virtual void GrantExp(float ExpAmount)
    {
        TotalExperience.AddToBase(ExpAmount);
        CurrentExperience.AddModifier(ExpAmount);
        if (CurrentExperience.GetValue() >= ExpUntilNext) LevelUp();
    }

    public virtual void LevelUp()
    {
        ExpUntilNext += ExpIncrease;
        Level += 1;
        CurrentExperience.RemoveAllModifiers();
        //OnLevelUp.Invoke();
        UpdateStatsByLevel();
    }

    //public virtual void OnHPChanged(int NewHp)
    //{

    //}

    //this function tallys up the total stat increases based on your level that you should recieve
    public virtual void UpdateStatsByLevel() //fix and adjust these equations later
    {
        float Factor = StatIncreasePerLevel / 100;
        Factor += (Level/100f);


        int hpfactor = (int)(Health.GetValue() * Factor); //the dividsion by 100 turns the int into a percentage
        int strfactor = (int)(Strength.GetValue() * Factor);
        int agifactor = (int)(Agility.GetValue() * Factor);
        int intfactor = (int)(Intelligence.GetValue() * Factor);
        int resfactor = (int)(Resource.GetValue() * Factor);

        Health.AddToBase(hpfactor);
        HealthMax.AddToBase(hpfactor);

        Strength.AddToBase(strfactor);      
        Agility.AddToBase(agifactor);
        Intelligence.AddToBase(intfactor);
        Resource.AddToBase(resfactor);
        ResourceMax.AddToBase(resfactor);
    }

    //this fucntion totals up all the stat increases that you should recieve based on what class you are
    public virtual void InitModStats()
    {
        int hpfactor = (int)(Health.GetValue() * (EntityClass.HpModPercent / 100f));
        int strfactor = (int)(Strength.GetValue() * (EntityClass.StrModPercent / 100f));
        int agifactor = (int)(Agility.GetValue() * (EntityClass.AgiModPercent / 100f));
        int intfactor = (int)(Intelligence.GetValue() * (EntityClass.IntModPercent / 100f));
        int resfactor = (int)(Resource.GetValue() * (EntityClass.ResoruceModPercent / 100f));

        Health.AddToBase(hpfactor);
        HealthMax.AddToBase(hpfactor);

        Strength.AddToBase(strfactor);
        Agility.AddToBase(agifactor);
        Intelligence.AddToBase(intfactor);
        Resource.AddToBase(resfactor);
        ResourceMax.AddToBase(resfactor);

        AttackPower.AddModifier(Strength.GetValue());
        SpellPower.AddModifier(Intelligence.GetValue());
    }

    public void PlayAudioClip(List<AudioClip> Clips)
    {
        AudioClip C = GetRandomClip(Clips);
        if(C != null && !AudioPlayer.isPlaying)
        {
            AudioPlayer.clip = C;
            //float Pitch = AudioPlayer.pitch;
            AudioPlayer.pitch = UnityEngine.Random.Range(1f, 1.2f);
            AudioPlayer.Play();
            //AudioPlayer.pitch = 1;
        }
    }

    private AudioClip GetRandomClip(List<AudioClip> Clips)
    {
        if (Clips.Count > 0)
        {
            int randomindex = UnityEngine.Random.Range(0, Clips.Count);
            //Debug.Log("got random index: " + randomindex + " with clip count: " + Clips.Count);
            return Clips[randomindex];
        }
        else return null;
    }

    public virtual void Die() //do a callback??
    {
        //play a lil animation thing and a sound
        //Destroy(this);
        Instantiate(DeathEffect, transform.position, Quaternion.identity);

        this.gameObject.SetActive(false);

        //maybe drop some loot
        OnEntityDeath(this);
    }

    public virtual void OnCollisionEnter2D(Collision2D collision){ }

    internal virtual void ToggleLiquidMask(bool toggle)
    {
        if (LiquidMask == null) Debug.Log("mask is null in toggle in entity");
        LiquidMask.gameObject.SetActive(toggle);

    }
}
