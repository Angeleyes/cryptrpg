﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPoint {


    public bool IsOpen { get; private set; }
    public int PointID { get; private set; }

    public Vector2 AttackPointCoordinates { get; private set; }

    public void Occupy()
    {
        IsOpen = false;
    }

    public void Release()
    {
        IsOpen = true;
    }

    public void AssignID(int id)
    {
        PointID = id;
    }

    public void SetPointCoordinates(Vector2 V)
    {
        AttackPointCoordinates = V;
    }

}
