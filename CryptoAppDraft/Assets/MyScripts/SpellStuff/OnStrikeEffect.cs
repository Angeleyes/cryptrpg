﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(menuName = "SpellEffects/OnStrike")]
public abstract class OnStrikeEffect : SpellEffect
{

    public abstract void OnStrike(Entity Target);

    public override void DoEffect()
    {
        
    }
}
