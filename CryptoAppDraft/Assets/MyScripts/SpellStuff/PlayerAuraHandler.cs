﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAuraHandler : AuraHandler
{
    PlayerEntity PlayerOwner;


    //this whole aura system can be written better with an observer design pattern
    //monsteraura observer
    //playeraura observer
    //refactor later

    public void EquipAura(Aura A)
    {
        SelfAuras.Add(A);
        AppliedAuras.Add(A);
        if (A.FriendlyAuraBuff != null)
        {
            PlayerOwner.ApplyBuff(A.FriendlyAuraBuff);
            FriendlyAuras.Add(A);
        }

        //notify observers
        if (A.FoeAuraBuff != null) OffensiveAuraBuffs.Add(A);

        if (AppliedAuras.Count > 0 && !IsRunning) StartCoroutine(RenderAuras());
    }


    public void UnEquipAura(Aura A)
    {
        if (A.FriendlyAuraBuff != null)
        {
            FriendlyAuras.Remove(A);
            PlayerOwner.CharHUD.BuffUI.RemoveBuff(A.FriendlyAuraBuff);
            PlayerOwner.RemoveBuff(A.FriendlyAuraBuff);

        }
        if (A.FoeAuraBuff != null) OffensiveAuraBuffs.Remove(A);
        SelfAuras.Remove(A);
        AppliedAuras.Remove(A);
        //notify observers

        Debug.Log("unequipping aura " + A.name);
        //tell the affected entties to remove buffs
        //in case player unequips while in range
        //Debug.Log("unequipping aura from player");
        foreach(Entity E in AuraAffectedEntites)
        {
            if (A.FriendlyAuraBuff != null && E is PlayerEntity) (E as PlayerEntity).PlayerAuraHand.UnRecieveFriendlyAuraBuff(A);
            else if (A.FoeAuraBuff != null && E is MonsterEntity)
            {
                Debug.Log("found a debuff arua, signalling enemies to remove debuff for entity: " + E.name);

                (E as MonsterEntity).MonsterAuraHand.UnRecieveOffensiveAuraBuff(A);
            }
        }
    }

    public override void RecieveFriendlyAuraBuff(Aura RecievedAura)
    {
        //PlayerOwner.CharHUD.BuffUI.AddBuff(RecievedAura.FriendlyAuraBuff);
        PlayerOwner.ApplyBuff(RecievedAura.FriendlyAuraBuff);
        AppliedAuras.Add(RecievedAura);
    }

    public override void UnRecieveFriendlyAuraBuff(Aura RecievedAura)
    {
        //when a player is unrecieving a buff you can just use the buffui, but when reciveing you use applybuff
        //PlayerOwner.RemoveBuff(RecievedAura.FriendlyAuraBuff);
        PlayerOwner.CharHUD.BuffUI.RemoveBuff(RecievedAura.FriendlyAuraBuff);
        AppliedAuras.Remove(RecievedAura);
    }

    public override void RecieveOffensiveAuraBuff(Aura A)
    {
        Debug.Log("playere is recieving offensive aura from a monster");
        PlayerOwner.ApplyBuff(A.FoeAuraBuff);
        AppliedAuras.Add(A);
    }

    public override void UnRecieveOffensiveAuraBuff(Aura A)
    {
        PlayerOwner.CharHUD.BuffUI.RemoveBuff(A.FoeAuraBuff);
        AppliedAuras.Remove(A);
    }

    public override void SetEntityOwner(Entity E, int foelayer)
    {
        PlayerOwner = E as PlayerEntity;
        FriendLayer = E.gameObject.layer;
        FoeLayer = foelayer;
        //maybe set an on death event
    }



    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 1 << FriendLayer && collision.gameObject.GetComponent<PlayerEntity>() != null)
        {
            //at this point, i know that its a friendly player entity
            Debug.Log("found player entity friend " + collision.gameObject.name);
            //PlayerAuraHandler Ah = collision.gameObject.GetComponent<PlayerEntity>().PlayerAuraHand;
            PlayerAuraHandler Ah = collision.gameObject.GetComponent<PlayerAuraHandler>();
            foreach (Aura A in FriendlyAuras)
            {
                //tell the Ah to observe this aura or aura handler
                Ah.RecieveFriendlyAuraBuff(A);
                AuraAffectedEntites.Add(collision.gameObject.GetComponent<PlayerEntity>());
            }

        }
        else if (collision.gameObject.layer == 1 << FoeLayer && collision.gameObject.GetComponentInParent<MonsterEntity>() != null)
        {
            //at this point i know its an enenmy monster entity
            Debug.Log("found a monster " + collision.gameObject.name);
            //MonsterAuraHandler Ah = collision.gameObject.GetComponentInParent<MonsterEntity>().MonsterAuraHand;
            MonsterAuraHandler Ah = collision.gameObject.GetComponent<MonsterAuraHandler>();
            foreach (Aura B in OffensiveAuraBuffs)
            {
                Debug.Log("applying debuffs to monster");
                Ah.RecieveOffensiveAuraBuff(B);
                AuraAffectedEntites.Add(collision.gameObject.GetComponentInParent<MonsterEntity>());
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 1 << FriendLayer && collision.gameObject.GetComponent<PlayerEntity>() != null)
        {
            //at this point, i know that its a friendly player entity
            //PlayerAuraHandler Ah = collision.gameObject.GetComponent<PlayerEntity>().PlayerAuraHand;
            PlayerAuraHandler Ah = collision.gameObject.GetComponent<PlayerAuraHandler>();
            foreach (Aura A in FriendlyAuras)
            {
                Ah.UnRecieveFriendlyAuraBuff(A);
                AuraAffectedEntites.Remove(collision.gameObject.GetComponent<PlayerEntity>());
            }

        }
        else if (collision.gameObject.layer == 1 << FoeLayer && collision.gameObject.GetComponentInParent<MonsterEntity>() != null)
        {
            //at this point i know its an enenmy monster entity

            //MonsterAuraHandler Ah = collision.gameObject.GetComponentInParent<MonsterEntity>().MonsterAuraHand;
            MonsterAuraHandler Ah = collision.gameObject.GetComponent<MonsterAuraHandler>();
            foreach (Aura B in OffensiveAuraBuffs)
            {
                Debug.Log("unrecieving offensive buff ");
                Ah.UnRecieveOffensiveAuraBuff(B);
                AuraAffectedEntites.Remove(collision.gameObject.GetComponentInParent<MonsterEntity>());
            }
        }
    }

}
