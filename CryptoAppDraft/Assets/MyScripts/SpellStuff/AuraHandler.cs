﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//emmits auras from entity
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public abstract class AuraHandler : MonoBehaviour
{
    public float AuraRadius = 10f;
    //public bool IsOffensiveAura = false;
    internal List<Aura> AppliedAuras = new List<Aura>();

    internal List<Aura> SelfAuras = new List<Aura>();
    //internal List<Aura> RecievedAuras = new List<Aura>();
    //internal List<Aura> NegativeRecievedAuras = new List<Aura>();
    internal List<Aura> OffensiveAuraBuffs = new List<Aura>();
    internal List<Aura> FriendlyAuras = new List<Aura>();
    internal List<Entity> AuraAffectedEntites = new List<Entity>();

    private float AuraDisplayTime = 3f;
    private SpriteRenderer AuraRenderer;
    internal CircleCollider2D AuraTriggerArea;
    internal Entity EntityOwner;
    internal int FriendLayer;
    internal int FoeLayer;
    internal bool IsRunning = false;

    ////NOTE: need to think about when a monster or player dies, 
    ////what happens to the aura, does the ontriggerexit work when its just turned off


        //changed to awake from start 5/24/19
    private void Awake()
    {
        //apply self auras....?
        //StartCoroutine(DisplayAuras());
        AuraTriggerArea = GetComponent<CircleCollider2D>();
        AuraTriggerArea.radius = AuraRadius;
        AuraTriggerArea.enabled = false;
        AuraRenderer = GetComponent<SpriteRenderer>();

    }

    public abstract void SetEntityOwner(Entity E, int foelayer);

    public abstract void RecieveFriendlyAuraBuff(Aura A);
    public abstract void RecieveOffensiveAuraBuff(Aura A);

    public abstract void UnRecieveFriendlyAuraBuff(Aura A);
    public abstract void UnRecieveOffensiveAuraBuff(Aura A);



    internal IEnumerator RenderAuras()
    {
        int count = -1;
        IsRunning = true;
        AuraTriggerArea.enabled = true;
        AuraTriggerArea.radius = AuraRadius;
        Coroutine AnimRoutine;


        while (AppliedAuras.Count > 0)
        {
            count++;
            count %= AppliedAuras.Count;
            Aura CurrentlyRenderedAura = AppliedAuras[count];
            if (CurrentlyRenderedAura == null) yield break;

            //NEED to check affected entites and check if aura is still on 
            //in just a monster dies or player unequips an aura
            AnimRoutine = StartCoroutine(CurrentlyRenderedAura.AuraAnimator.PlayAnimation(AuraRenderer));
            yield return new WaitForSeconds(AuraDisplayTime);
            StopCoroutine(AnimRoutine);


            yield return null;
        }

        IsRunning = false;
        AuraRenderer.sprite = null;
        AuraTriggerArea.enabled = false;
        yield break;
    }


    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    //if the entity is on the same layer and the owner entity, then apply positive buffs
    //    //if (collision.gameObject.layer == 22) Debug.Log("collding iwht layer 22");
    //    //Debug.Log("collidign with " + collision.gameObject.layer + " with layer name : " + (1 << collision.gameObject.layer) + " looking for layer : " + FoeLayer.value + " or " + FoeLayer);

    //    if (collision.gameObject.layer == FriendLayer || collision.gameObject.layer == FoeLayer)
    //    {
    //        Entity E = null;
    //        if (collision.gameObject.layer == FriendLayer) E = collision.gameObject.GetComponent<Entity>();
    //        else if (collision.gameObject.layer == FoeLayer) E = collision.gameObject.GetComponentInParent<Entity>();

    //        Debug.Log("colliding with : " + collision.gameObject.name);
    //        if (E != null)
    //        {
    //            Debug.Log("found aura handler for layer : " + collision.gameObject.layer);
    //            AuraHandler Ah = E.EntityAuraHandler;
    //            if (Ah != null) ApplyAura(Ah, collision.gameObject.layer);

    //        }
    //        else Debug.Log("...entity is null");
    //    }
    //}

    //private void OnTriggerExit2D(Collider2D collision)
    //{
    //    if (collision.gameObject.layer == FriendLayer || collision.gameObject.layer == FoeLayer)
    //    {

    //        Entity E = null;
    //        if (collision.gameObject.layer == FriendLayer) E = collision.gameObject.GetComponent<Entity>();
    //        else if (collision.gameObject.layer == FoeLayer) E = collision.gameObject.GetComponentInParent<Entity>();

    //        Debug.Log("exiting colliding with : " + collision.gameObject.name);
    //        if (collision.gameObject.GetComponent<Entity>() != null)
    //        {
    //            Debug.Log("found aura handler for layer on exit: " + collision.gameObject.layer);
    //            AuraHandler Ah = collision.GetComponent<Entity>().EntityAuraHandler;
    //            if (Ah != null) UnApplyAura(Ah, collision.gameObject.layer);

    //        }
    //    }
    //}
}
