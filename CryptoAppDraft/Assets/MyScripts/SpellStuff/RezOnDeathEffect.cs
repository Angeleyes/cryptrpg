﻿using UnityEngine;

public class RezOnDeathEffect : OnDeathEffect
{
    public float ChanceToRez;
    public override void OnDeath(Entity DeadEntity)
    {
        if(Random.Range(0,101) <= ChanceToRez)
        {
            DeadEntity.HealHP(DeadEntity.HealthMax.GetValue());
        }
    }


    public override void DoEffect()
    {

    }


}
