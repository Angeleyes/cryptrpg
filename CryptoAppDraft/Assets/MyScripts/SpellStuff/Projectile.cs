﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float Lifetime = 2f;
    public float Speed = 5f;
    public SimpleSpriteAnimator SpriteAnim;
    public OnHitEffect HitEffect;
    public bool Animates = false;
    private SpriteRenderer Renderer;
    internal AttackHitBox AttackBox;
    internal Vector3 TargetAim;
    //internal ProjectileShooter ShooterMananger;
    //internal ParticleSystem ParticleObject;

    internal Vector3 OriginScale;
    internal Color OriginColor;

    internal bool HitSomething = false;

    private void Awake()
    {
        Renderer = GetComponent<SpriteRenderer>();
        AttackBox = GetComponent<AttackHitBox>();
        OriginScale = this.transform.localScale;
        OriginColor = this.Renderer.color;
    }

    internal void ResetProjectileStats()
    {
        this.transform.localScale = OriginScale;
        this.Renderer.color = OriginColor;
    }

    private void OnEnable()
    {
        if(Animates) StartCoroutine(SpriteAnim.PlayAnimation(Renderer));
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.isTrigger == false && collision.gameObject.layer != 23)
        {
            //spawn on hit effect or decal
            Debug.Log("projectile hit something: " + collision.name);
            HitSomething = true;
            //ShooterMananger.RepoolProjectile(this);
        }
    }
}
