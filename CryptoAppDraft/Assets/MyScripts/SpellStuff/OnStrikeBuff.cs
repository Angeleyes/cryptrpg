﻿
using UnityEngine;

[CreateAssetMenu(menuName = "OnStrike/BuffSelf")]
public class OnStrikeBuff : OnStrikeEffect
{
    public float SelfBuffChance;
    public Buff SelfBuff;

    public override void OnStrike(Entity Target)
    {
        if(Random.Range(0,101) <= SelfBuffChance)
        {
            Target.ApplyBuff(SelfBuff);
        }
    }
}
