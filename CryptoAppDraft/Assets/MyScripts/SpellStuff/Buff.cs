﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewBuff")]
public class Buff : ScriptableObject
{
    public Sprite BuffIcon;
    public float BuffDuration;
    public bool NoDuration = false;

    public string StatToAffect;
    public float AmountToAffect;
    public bool AsPercent = false;
    public Color ColorTint = new Color(1, 1, 1, 1);
    [TextArea]
    public string BuffToolTip;

    public int BuffID;
    internal float BuffTimer;

    internal bool FlagForRemoval { get; set; }

    //wonder how to add things like scale/sprite color tint/
}
