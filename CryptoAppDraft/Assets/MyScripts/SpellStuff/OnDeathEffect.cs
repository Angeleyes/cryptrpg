﻿
using UnityEngine;

[CreateAssetMenu(menuName = "SpellEffects/OnDeath")]
public abstract class OnDeathEffect : SpellEffect
{

    //ressurect once 
    //spawn babi monsters

    public abstract void OnDeath(Entity DeadEntity);
}
