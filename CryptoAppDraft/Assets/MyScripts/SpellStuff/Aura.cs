﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAura")]
public class Aura : ScriptableObject
{
    public Buff FriendlyAuraBuff;
    public Buff FoeAuraBuff;
    public SimpleSpriteAnimator AuraAnimator;
    [TextArea]
    public string AuraTipData;

    internal bool FlagForRemoval { get; set; }
    
}
