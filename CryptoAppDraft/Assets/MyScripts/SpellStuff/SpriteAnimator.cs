﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteAnimator : MonoBehaviour
{
    private SpriteRenderer Renderer;
    public SpriteRenderer CustomRenderer;
    public List<Sprite> SpriteList = new List<Sprite>();
    public float Duration;
    public bool Loop = false;
    public bool PlayOnAwake = false;
    public bool NullEndSprite = false;
    internal IEnumerator AnimationRoutine;

    //public SimpleSpriteAnimator CustomAnimation;


    public void Awake()
    {
        Renderer = GetComponent<SpriteRenderer>();
        if (CustomRenderer != null) Renderer = CustomRenderer;
        if (PlayOnAwake) StartCoroutine(PlayAnimation());
        AnimationRoutine = PlayAnimation();
    }

    public void Play()
    {
        //StopCoroutine(AnimationRoutine);
        StartCoroutine(PlayAnimation());
    }

    public IEnumerator PlayAnimation()
    {
        if (SpriteList.Count <= 0)
        {
            Debug.Log("ERROR, sprite list is empty in simple animator scriptable object");
            yield break;
        }

        int FrameCount = SpriteList.Count;
        float TimeWait = Duration / FrameCount;
        int count = 0;
        //Debug.Log("using frame count : " + FrameCount + " with time for each frame: " + TimeWait);
        //Renderer.sprite = null;
        while (count < FrameCount || Loop)
        {
            Renderer.sprite = SpriteList[count%SpriteList.Count];
            count++;
            yield return new WaitForSeconds(TimeWait);

        }

        if (NullEndSprite) Renderer.sprite = null;

        yield break;

    }
}
