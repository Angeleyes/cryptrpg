﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{
    internal int NumberOfProjectiles = 3;
    public Projectile TheProjectile;
    public float ProjectileSize = 1;
    public List<ProjectileStrategies> ShootStrategies = new List<ProjectileStrategies>();
    public bool RandomShootStrats = false;
    private Queue<Projectile> ProjectilePool = new Queue<Projectile>();
    private Vector3 Target = new Vector3();
    private float TargetYoffset = 0.65f;
    public AttackHitBox AttackBox;

    internal Light ProjectileLight;


    private delegate void AdditiveShootFunction(Projectile P, Vector3 Target, float time, ProjectileShooter Shooter);
    private AdditiveShootFunction ShootFunctions;

    private void Awake()
    {
        if (AttackBox == null)
        {
            //AttackBox = GetComponentInParent<AttackHitBox>();
            Debug.Log("attack box is null for proj shooter, shooting may not function");
        }
        //need to assign this manually
        if(AttackBox != null) AttackBox.OnHitBoxEnabled += Shoot;

        if(TheProjectile.GetComponentInChildren<Light>() != null)
        {
            ProjectileLight = TheProjectile.GetComponentInChildren<Light>();
        }

        foreach (ProjectileStrategies Ps in ShootStrategies)
        {

            if (RandomShootStrats)
            {
                int rand = Random.Range(0, 2);
                if (rand == 1) ShootFunctions += Ps.ShootFunction;
                else continue;
            }
            else
            {
                ShootFunctions += Ps.ShootFunction;
                NumberOfProjectiles += Ps.AdditionalProjectiles;
            }
        }

        if (ShootFunctions == null) Debug.Log("error in shooter, no shoot functions assigned");
        if (TheProjectile != null && AttackBox != null)
        {
            TheProjectile.transform.localScale = new Vector3(ProjectileSize, ProjectileSize, 1);
            for (int i = 0; i < NumberOfProjectiles; i++)
            {
                Projectile P = Instantiate(TheProjectile, this.transform.parent);
                P.name += i.ToString();
                P.transform.position = Vector3.zero;
                ProjectilePool.Enqueue(P);
                P.gameObject.SetActive(false);
            }
        }

    }

    public void SetTarget(Vector3 T)
    {
        Debug.Log("setting target pos in shooter as: " + T);
        Target = new Vector3(T.x, T.y + TargetYoffset, 0);
    }

    internal void Shoot()
    {
        //Debug.Log("in shoot, count is: " + ProjectilePool.Count);
        this.transform.position = AttackBox.transform.position;
        if(ProjectilePool.Count > 0)
        {
            StartCoroutine(ShootProjectile());
        }
    }

    IEnumerator ShootProjectile()
    {
        if (ProjectilePool.Count <= 0 || Target == Vector3.zero) yield break;
        Projectile P = ProjectilePool.Dequeue();
        Debug.Log("shooting " + P.name + " at the pos: " + Target);
        //maybe some kinda get target position callback listener thing here
        P.gameObject.SetActive(true);
        P.transform.position = AttackBox.transform.position;
        ProjectilePool.Enqueue(P);
        float timer = 0;

        Vector3 TargetPosDifference = Target - this.transform.position;
        Debug.Log("new target pos is : " + TargetPosDifference + " with p pos: " + P.transform.position);

        //float angle = Mathf.Atan2(TargetPosDifference.y - 1, TargetPosDifference.x) * Mathf.Rad2Deg;
        //P.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        P.transform.right = TargetPosDifference;
        P.TargetAim = TargetPosDifference; //each projectile needs its own aim, otherwise it gets retroactively updated and messing with trajectory     
        
        while (timer < P.Lifetime)
        {
            //Debug.Log("using target pos: " + TargetPos);
            if (P.HitSomething) break;
            timer += 0.017f;
            if (timer > P.Lifetime) Debug.Log("life time is exceeded, killing proj " + P.name);
            ShootFunctions(P, TargetPosDifference, timer, this);
            yield return new WaitForEndOfFrame();
        }

        Debug.Log(P.name + " is being re-pooled");
        RepoolProjectile(P);

        yield break;
    }

    internal void RepoolProjectile(Projectile P)
    {

        P.transform.position = this.transform.position;
        P.ResetProjectileStats();
        //ProjectilePool.Enqueue(T); //projectile is enqued in the routine...
        P.HitSomething = false;
        P.gameObject.SetActive(false);
    }

    public void AddProjectileModifiers()
    {
        //add things like noise to particle system
        //Projectile.noise.f
    }



}
