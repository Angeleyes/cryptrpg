﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BuffHandler : MonoBehaviour
{
    //takes a buff and displays its timer and sprite
    private List<BuffSlot> BuffSlots = new List<BuffSlot>(5);
    private List<Buff> BuffList = new List<Buff>(5);
    private bool IsRunning = false;
    public event Action<Buff> BuffExpired;

    //IEnumerator BuffRoutine;

    private void Awake()
    {
        foreach(BuffSlot Bs in GetComponentsInChildren<BuffSlot>())
        {
            BuffSlots.Add(Bs);
            Bs.gameObject.SetActive(false);
            Bs.BuffImage = Bs.GetComponent<Image>();
            Bs.CustomTipOffset = new Vector2(60, -130);
        }

        //BuffRoutine = Dec
        //StartCoroutine(DeacyActiveBuffs());

    }

    private void OrderBuffs()
    {
        BuffList.Sort(delegate (Buff b1, Buff b2) { return b1.BuffDuration.CompareTo(b2.BuffDuration); });

        foreach(BuffSlot Bs in BuffSlots)
        {
            Bs.gameObject.SetActive(false);
        }

        for (int i = 0; i < BuffList.Count; i++)
        {
            if (i >= BuffSlots.Count) return;

            BuffSlots[i].gameObject.SetActive(true);
            BuffSlots[i].SetBuff(BuffList[i].BuffIcon, BuffList[i].BuffTimer, BuffList[i].BuffToolTip);
        }
        
    }

    public void AddBuff(Buff B)
    {
        B.BuffTimer = B.BuffDuration;
        if(BuffList.Count == BuffList.Capacity)
        {
            Debug.Log("too many buffs, cant add " + B.name);
            return;
        }
        else if(BuffList.Contains(B))
        {
            Debug.Log("already have this buff " + B.name);
            return;
        }



        BuffList.Add(B);
        OrderBuffs();

        if(!IsRunning && BuffList.Count > 0)
        {
            StartCoroutine(DecayActiveBuffs());
            IsRunning = true;
        }
    }

    public void RemoveBuff(Buff B)
    {
        Debug.Log("removing buff " + B.name);
        int index = BuffList.IndexOf(B);
        Debug.Log("removing index of " + B.name + " index: " + index);
        BuffList.RemoveAt(index);
        BuffExpired(B);
        OrderBuffs();

    }

    IEnumerator DecayActiveBuffs()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            if (BuffList.Count <= 0)
            {
                Debug.Log("no more buffs, stopping coroutine");
                IsRunning = false;
                yield break;
            }
            foreach (Buff B in BuffList)
            {
                if (B.NoDuration) continue;
                else if(B != null)
                {
                    B.BuffTimer -= 1f;
                    int index = BuffList.IndexOf(B);
                    BuffSlots[index].SetText(B.BuffTimer);
                    if (B.BuffTimer <= 0)
                    {
                        RemoveBuff(B);
                        break;
                    }
                    
                }

            }

            yield return null;
        }
    }
}
