﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(SpriteRenderer))]

public class EffectHandler : MonoBehaviour {

    internal SpriteRenderer AnimRenderer;
    internal List<ParticleSystem> ParticleSystems = new List<ParticleSystem>();
    internal AudioSource AudioEffect;

    //internal Camera PlayerCamera; //can be used for camera shake effects

    private void Awake()
    {
        ParticleSystems = new List<ParticleSystem>() { null, null, null };
        AnimRenderer = GetComponent<SpriteRenderer>();
        AudioEffect = GetComponent<AudioSource>();
    }

    public void SetEffectAnimation(SimpleSpriteAnimator anim) // need a new custom sprite list data object anim thing, maybe scriptable class
    {
        if(this != null) StartCoroutine(PlayAnimation(anim));
    }

    public void PlayAudioEffect(AudioClip Clip)
    {
        if (this != null)
        {
            AudioEffect.clip = Clip;
            AudioEffect.Play();
        }
    }

    public void ShakeCamera(Camera cam)
    {

    }

    public void FlashCamera(Camera cam, Sprite S)
    {

    }

    public void AddParticle(ParticleSystem particle, int abilityID)
    {

        if (!AlreadyInSystem(abilityID))
        {
            ParticleSystem Child = Instantiate(particle, new Vector3(0, 0, 0), Quaternion.identity, this.transform);
            Child.transform.localPosition = Vector3.zero;
            Child.gameObject.SetActive(false);
            ParticleSystems.Insert(abilityID, Child);
            StartCoroutine(PlayParticleEffect(abilityID));

        }
        else StartCoroutine(PlayParticleEffect(abilityID));

    }

    private bool AlreadyInSystem(int index)
    {
        return ParticleSystems[index] == null ? false : true;
    }

    IEnumerator PlayParticleEffect(int index)
    {
        if(ParticleSystems[index].transform.localPosition != Vector3.zero) ParticleSystems[index].transform.localPosition = Vector3.zero;
        float timer = 0;
        ParticleSystems[index].gameObject.SetActive(true);
        while (timer < ParticleSystems[index].main.duration)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        ParticleSystems[index].gameObject.SetActive(false);
        yield break;
    }


    IEnumerator PlayAnimation(SimpleSpriteAnimator Anim)
    {
        int FrameCount = Anim.Sprites.Count;
        float TimeWait = Anim.Duration / FrameCount;
        int count = 0;

        while(count < FrameCount)
        {
            AnimRenderer.sprite = Anim.Sprites[count];
            count++;
            yield return new WaitForSeconds(TimeWait);

        }
        AnimRenderer.sprite = null;
        yield break;

    }


}
