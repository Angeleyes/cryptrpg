﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAuraHandler : AuraHandler
{
    public Aura MonsterAura;

    MonsterEntity MonsterOwner;

    public void Start()
    {
        //might need to do something ondeath of monster to unequip auras or something
        //is triggerexit called when a monster dies? 
        if (MonsterAura != null)
        {
            SelfAuras.Add(MonsterAura);
            AppliedAuras.Add(MonsterAura);
            AuraTriggerArea.enabled = true;

            if (MonsterAura.FriendlyAuraBuff != null)
            {
                FriendlyAuras.Add(MonsterAura);
                MonsterOwner.ApplyBuff(MonsterAura.FriendlyAuraBuff);
            }
            if (MonsterAura.FoeAuraBuff != null) OffensiveAuraBuffs.Add(MonsterAura);
        }

        if (!IsRunning && AppliedAuras.Count > 0) StartCoroutine(RenderAuras());
    }

    public override void SetEntityOwner(Entity E, int foelayer)
    {
        MonsterOwner = E as MonsterEntity;
        FriendLayer = MonsterOwner.gameObject.layer;
        Debug.Log("setting monster foe layer to: " + foelayer);
        FoeLayer = foelayer;
        //on death event maybe
        MonsterOwner.OnEntityDeath += OnDeathAuraNotify;
    }

    private void OnDeathAuraNotify(Entity DeadEntity)
    {
        MonsterEntity Dead = DeadEntity as MonsterEntity;

        foreach (Aura A in Dead.MonsterAuraHand.SelfAuras)
        {
            foreach (Entity E in Dead.MonsterAuraHand.AuraAffectedEntites)
            {
                if (A.FoeAuraBuff != null && E is PlayerEntity) (E as PlayerEntity).PlayerAuraHand.UnRecieveOffensiveAuraBuff(A);

                if (A.FriendlyAuraBuff != null && E is MonsterEntity) (E as MonsterEntity).MonsterAuraHand.UnRecieveFriendlyAuraBuff(A);
            }
        }
    }

    public override void RecieveFriendlyAuraBuff(Aura A)
    {
        MonsterOwner.ApplyBuff(A.FriendlyAuraBuff);
        AppliedAuras.Add(A);
    }

    public override void UnRecieveFriendlyAuraBuff(Aura A)
    {
        MonsterOwner.RemoveBuff(A.FriendlyAuraBuff);
        AppliedAuras.Remove(A);
    }

    public override void RecieveOffensiveAuraBuff(Aura A)
    {
        MonsterOwner.ApplyBuff(A.FoeAuraBuff);
        AppliedAuras.Add(A);       
    }

    public override void UnRecieveOffensiveAuraBuff(Aura A)
    {
        Debug.Log("removing monster buff from unrecoffbuff");
        MonsterOwner.RemoveBuff(A.FoeAuraBuff);
        AppliedAuras.Remove(A);
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 1 << FriendLayer && collision.gameObject.GetComponentInParent<MonsterEntity>() != null)
        {
            //at this point, i know that its a friendly monster
            //MonsterAuraHandler Ah = collision.gameObject.GetComponentInParent<MonsterEntity>().MonsterAuraHand;
            MonsterAuraHandler Ah = collision.gameObject.GetComponent<MonsterAuraHandler>();
            foreach (Aura A in FriendlyAuras)
            {
                Ah.RecieveFriendlyAuraBuff(A);
                AuraAffectedEntites.Add(collision.gameObject.GetComponentInParent<MonsterEntity>());
            }

        }
        else if(collision.gameObject.layer == 1 << FoeLayer && collision.gameObject.GetComponent<PlayerEntity>() != null)
        {
            //at this point i know its an enenmy player entity
            Debug.Log("player enetered enemy aura");
            //PlayerAuraHandler Ah = collision.gameObject.GetComponent<PlayerEntity>().PlayerAuraHand;
            PlayerAuraHandler Ah = collision.gameObject.GetComponent<PlayerAuraHandler>();
            foreach (Aura B in OffensiveAuraBuffs)
            {
                Ah.RecieveOffensiveAuraBuff(B);
                AuraAffectedEntites.Add(collision.gameObject.GetComponent<PlayerEntity>());
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 1 << FriendLayer && collision.gameObject.GetComponentInParent<MonsterEntity>() != null)
        {
            //at this point, i know that its a friendly monster
            //MonsterAuraHandler Ah = collision.gameObject.GetComponentInParent<MonsterEntity>().MonsterAuraHand;
            MonsterAuraHandler Ah = collision.gameObject.GetComponent<MonsterAuraHandler>();
            foreach (Aura A in FriendlyAuras)
            {
                Ah.UnRecieveFriendlyAuraBuff(A);
                AuraAffectedEntites.Remove(collision.gameObject.GetComponentInParent<MonsterEntity>());
            }

        }
        else if (collision.gameObject.layer == 1 << FoeLayer && collision.gameObject.GetComponent<PlayerEntity>() != null)
        {
            //at this point i know its an enenmy player entity
            //PlayerAuraHandler Ah = collision.gameObject.GetComponent<PlayerEntity>().PlayerAuraHand;
            PlayerAuraHandler Ah = collision.gameObject.GetComponent<PlayerAuraHandler>();
            foreach (Aura B in OffensiveAuraBuffs)
            {
                Ah.UnRecieveOffensiveAuraBuff(B);
                AuraAffectedEntites.Remove(collision.gameObject.GetComponent<PlayerEntity>());
            }
        }
    }

}
