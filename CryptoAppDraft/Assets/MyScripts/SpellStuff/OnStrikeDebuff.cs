﻿
using UnityEngine;

[CreateAssetMenu(menuName = "OnStrike/DebuffTarget")]
public class OnStrikeDebuff : OnStrikeEffect
{
    public float DebuffChance;
    public Buff Debuff;

    public override void OnStrike(Entity Target)
    {

        if(Random.Range(0,101) <= DebuffChance)
        {

            Target.ApplyBuff(Debuff);
        }
    }
}
