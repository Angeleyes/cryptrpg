﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

using System.IO;

public class GameManager : MonoBehaviour {

    public Vector3 StartSpawn = new Vector3();

    public Light MainLight;
    public Canvas LoadScreen;
    public DungeonGenerator TheDungeonGenerator;
    public GroundGrid PathGrid;
    public GameObject MonsterParentObj;
    Action GameGridCallback;
    public bool RunGame;

    //public Staircase StaircaseRef;
    public WorldTooltipHandler WorldTipHandler;
    public LootTableManager TheLootTableManager;
    //public AnimatedShadow2DService AnimatedShadowService;
    //internal RPGNetworkManager Nm;
    internal Vector3 Map1Position = new Vector3(0, 0, 0);
    internal Vector3 Map2Position = new Vector3(-500, 0, 0);
    internal Vector3 PlayerHoldingPosition = new Vector3(500, 500, 0);
    internal List<DungeonMap> MapList = new List<DungeonMap>();
    internal int MapNumber = 0;
    internal DungeonMap CurrentLoadedMap;
    internal DungeonMap NextMap;
    internal TextReader Reader;

    public Canvas TempStartCanvas;

    internal List<PlayerEntity> PlayerList = new List<PlayerEntity>();

    internal string LoadingString = "Loading...";

    internal MonsterConfig MonsterConfig = new MonsterConfig();

    private void Awake()
    {
        MainLight = Instantiate(MainLight);
        MainLight.intensity = UnityEngine.Random.Range(0f, 0.5f);
        WorldTipHandler = Instantiate(WorldTipHandler, new Vector3(0, 0, 0), Quaternion.identity);

        TheLootTableManager = Instantiate(TheLootTableManager, new Vector3(0, 0, 0), Quaternion.identity);
        LootTableManagerLocator.Provide(TheLootTableManager);

        string MConfig = File.ReadAllText(Application.dataPath + "/MonsterConfig1.json");
        MonsterConfig = JsonUtility.FromJson<MonsterConfig>(MConfig);
        //MonsterConfig Mconfig = new MonsterConfig();
        //string jsonstring = JsonUtility.ToJson(Mconfig);


        //File.WriteAllText(Application.dataPath + "/MonsterConfig1.json", jsonstring);

        //Debug.Log("writing file to " + Application.dataPath);

        //AnimatedShadowService = Instantiate(AnimatedShadowService, new Vector3(0, 0, 0), Quaternion.identity);

        if (RunGame)
        {
            //Nm = GetComponent<RPGNetworkManager>();
            GameGridCallback += PathGrid.CreateGrid;
            GameGridCallback += GenerateNextMap;
            GameGridCallback += FadeLoadScreenOut;
            //GameGridCallback += Nm.SetClientsReady; //sets clients ready when the game is done loading for the first time

            FadeLoadScreen(LoadingString, 1f);

            DungeonMap Map = TheDungeonGenerator.GenerateDungeon(GameGridCallback, Map1Position); //generate map 1
            MapList.Add(Map);
            Map.SetMapID(MapNumber);
            CurrentLoadedMap = Map;

        }

    }

    //temp code
    public void Turnoffcanvas()
    {
        TempStartCanvas.gameObject.SetActive(false);
    }

    public void ChooseChar(PlayerEntity P)
    {
        Instantiate(P, StartSpawn, Quaternion.identity);
        PlayerList.Add(P);
        Turnoffcanvas();
    }
    //end temp code

    IEnumerator OnTriggerStaircase()
    {
        FadeLoadScreen(LoadingString, 1f); //fade load screen in
        
        Debug.Log("staircase triggered");

        CurrentLoadedMap.DestroyMap();

        NextMap.transform.position = Map1Position;
        CurrentLoadedMap = NextMap;

        //need to setup monster spawns here, after the map has been moved
        CurrentLoadedMap.SpawnDungeonMonsters(); // or could do this on the on triggere enter thing but nah, too many instantiate calls potentially

        //after generatenextmap is called, the mapnumber is updated to current map
        if(MapNumber < 5) GenerateNextMap();
        else
        {
            //spawn boss room
            Debug.Log("mapnumber is : " + MapNumber + " time to spawn boss room");
            yield break;
        }

        foreach (PlayerEntity P in PlayerList)
        {
            Debug.Log("moving player: " + P.name + " to start pos: " + StartSpawn);
            P.SetEntityPosition(StartSpawn);
        }

        foreach (MonsterEntity M in MonsterParentObj.GetComponentsInChildren<MonsterEntity>())
        {
            Destroy(M.gameObject);
        }


        PathGrid.gameObject.SetActive(false);
        ActivateMap();
        yield return new WaitForSeconds(5f);
        //the other new map should be already generated at this point
        PathGrid.gameObject.SetActive(true);

        PathGrid.CreateGrid();

        yield return new WaitForSeconds(5f);

        FadeLoadScreenOut();

        yield break;
    }

    private Vector3 GetNextMapLocation()
    {
        if (MapNumber+1 % 2 == 0) return Map1Position;
        else return Map2Position;
    }

    private Vector3 GetCurrentMapLocation()
    {
        if (MapNumber % 2 != 0) return Map2Position;
        return Map1Position;
    }

    public void GenerateNextMap()
    {

        FinalizeMapSetup(); //finalizes the previous map

        Debug.Log("generating next map at " + Map2Position);
        Vector3 Pos = Map2Position;

        NextMap = TheDungeonGenerator.GenerateDungeon(DeactivateMap, Pos);
        MapList.Add(NextMap);
        MapNumber++;
        NextMap.SetMapID(MapNumber);
        //spawn staircase for current map
    }

    public void DeactivateMap()
    {
        MapList[MapNumber].gameObject.SetActive(false);
    }

    public void ActivateMap()
    {
        MapList[MapNumber].gameObject.SetActive(true);
    }

    public void FinalizeMapSetup()
    {
        if (CurrentLoadedMap != null)
        {
            CurrentLoadedMap.ActivateDungeonMap(PlayerList, MonsterConfig); //activates current map
            LoadWorldTipHandler();
            Debug.Log("initing staircase with player count: " + PlayerList.Count);
            CurrentLoadedMap.MapStaircase.SetupStaircase(OnTriggerStaircase(), PlayerList.Count);
        }


        //List<RoomPosition> CurrentMapRoomPos = TheMapGenerator.GetAllRoomPositions(MapNumber); //to make it more readable


        //spawn locked doors, make locked doors active
        //init key
    }


    private void LoadWorldTipHandler()
    {
        //WorldTipHandler = Instantiate(WorldTipHandler, new Vector3(0, 0, 0), Quaternion.identity);
        
        foreach(WorldTooltip W in CurrentLoadedMap.GetAllWorldTips())
        {
            //Debug.Log("registering world tip to " + W.name);
            W.RegisterHandler(WorldTipHandler);
        }
        //register worldtips stuff
    }

    public void FadeLoadScreenOut()
    {
        FadeLoadScreen("", 0f);
    }

    public void FadeLoadScreen(string LoadingMessage, float TargetAlpha)
    {
        LoadScreen.GetComponentInChildren<Text>().text = LoadingMessage;
        LoadScreen.GetComponentInChildren<RawImage>().CrossFadeAlpha(TargetAlpha, 1f, true);
    }

}
