﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour {


    IEnumerator LightOperation;

    public float TimeFlickerMin;
    public float TimeFlickerMax;
    public float RangeMin;
    public float RangeMax;
    public float IntensityMax;
    public float IntensityMin;
    public float FlickTime; //maybe make this a min max thing too

    public float LerpFactor = 0.1f;
    public float PulsePuase;

    public bool UseFlicker1;
    public bool UseFlicker2;
    public bool Pulser;


    internal Light TheLight;


    private void Start()
    {
        TheLight = GetComponent<Light>();

        if (TheLight != null)
        {
            if (UseFlicker1) LightOperation = Flicker();
            else if (UseFlicker2) LightOperation = Flicker2();
            else if (Pulser) LightOperation = Pulse();
        }

        StartCoroutine(LightOperation);
    }


    IEnumerator Flicker()
    {
        while(true)
        {
            float PrevIntensity = TheLight.intensity;
            TheLight.intensity = Random.Range(IntensityMin, IntensityMax);
            yield return new WaitForSeconds(FlickTime);
            TheLight.intensity = PrevIntensity;
            yield return new WaitForSeconds(Random.Range(TimeFlickerMin, TimeFlickerMax));
            
        }
    }

    IEnumerator Flicker2()
    {
        while (true)
        {
            float PrevRange = TheLight.range;
            TheLight.range = Random.Range(RangeMin, RangeMax);
            yield return new WaitForSeconds(FlickTime);
            TheLight.range = PrevRange;
            yield return new WaitForSeconds(Random.Range(TimeFlickerMin, TimeFlickerMax));

        }
    }

    IEnumerator Pulse()
    {
        TheLight.intensity = IntensityMax;
        float NewIntensity = TheLight.intensity;
        while (TheLight.intensity > IntensityMin)
        {
            NewIntensity -= LerpFactor;
            TheLight.intensity = NewIntensity;
            yield return null;
        }

        NewIntensity = IntensityMin;
        yield return new WaitForSeconds(PulsePuase);

        while (TheLight.intensity < IntensityMax)
        {
            NewIntensity += LerpFactor;
            TheLight.intensity = NewIntensity;
            yield return null;
        }

        yield return new WaitForSeconds(PulsePuase);
        StartCoroutine(Pulse());
        yield break;
    }

}
