﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class OnHitEffect : MonoBehaviour {


    //AudioSource Audio;
    //AudioClip Clip;
    public ParticleSystem ParticleEffect;



	void Start ()
    {
        //at the moment its just play on awake so....
        //Audio = GetComponent<AudioSource>();
        //Clip = Audio.clip;
        ParticleEffect = GetComponent<ParticleSystem>();

	}



    private void Awake()
    {
        if (transform.parent == null) Invoke("EndLife", 2f);
    }

    private void EndLife()
    {
        Destroy(this.gameObject);
    }

    public void Activate()
    {
        this.gameObject.SetActive(true);
        try //bug here in which it calls coroutine when the monster dies so its null
        {
            StartCoroutine(PlayEffect());
        }
        catch(Exception e) { Debug.Log("caught double die exception, probably caused by double hit " + e.Message); }
    }

    public IEnumerator PlayEffect()
    {
        yield return new WaitForSeconds(0.4f);
        if(gameObject != null || transform.parent != null) ParticleEffect.gameObject.SetActive(false);
    }


}
