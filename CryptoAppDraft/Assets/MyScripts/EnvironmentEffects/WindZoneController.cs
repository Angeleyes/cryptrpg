﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindZoneController : MonoBehaviour
{
    [SerializeField]
    internal WindZone Wind;

    private void Awake()
    {
        Wind = GetComponent<WindZone>();
        Wind.windMain = 2;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("some thing hit the windzone: " + collision.name + " with layer num: " + collision.gameObject.layer);
        if(collision.gameObject.layer == 25)
        {
            StartCoroutine(WindGust());
        }
    }


    IEnumerator WindGust()
    {
        Wind.windMain = 350;
        yield return new WaitForSeconds(2);
        Wind.windMain = 2;
        yield break;
    }
}
