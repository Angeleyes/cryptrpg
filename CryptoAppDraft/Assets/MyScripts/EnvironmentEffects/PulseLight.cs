﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseLight : MonoBehaviour {


    public float IntensityMin;
    public float IntensityMax;

    public float LerpFactor = 0.1f;
    public float PulsePuase;

    internal Light TheLight;

    private void Start()
    {
        TheLight = GetComponent<Light>();
        StartCoroutine(Pulse());
    }

    IEnumerator Pulse()
    {
        TheLight.intensity = IntensityMax;
        float NewIntensity = TheLight.intensity;
        while(TheLight.intensity > IntensityMin)
        {
            NewIntensity -= LerpFactor;
            TheLight.intensity = NewIntensity;
            yield return null;
        }

        NewIntensity = IntensityMin;
        yield return new WaitForSeconds(PulsePuase);
        
        while(TheLight.intensity < IntensityMax)
        {
            NewIntensity += LerpFactor;
            TheLight.intensity = NewIntensity;
            yield return null;
        }

        StartCoroutine(Pulse());
        yield break;
    }


}
