﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Shadow2D : MonoBehaviour
{
    //public GameObject EmptyObject;
    public Sprite CustomShadowSprite;
    public Vector2 CustomOffset = new Vector2();
    public Material ShadowMat;
    public Sprite ShadowMask;
    internal SpriteRenderer ObjectRenderer;
    internal SpriteRenderer ShadowRenderer;
    internal GameObject ShadowObject;
    internal SpriteMask Mask;
    //public float LightAngle = 0;
    public Vector2 CustomLightPos = new Vector2();
    internal Vector2 XyLightDirComps = new Vector2(-1,-9);
    public bool UsesSecondMask = true;
    public bool UsesMainSpriteAsMask = true;
    internal Vector2 LightPosition = new Vector2();
    internal float ShadowAlpha = 0.3f;

    private void Awake()
    {
        ObjectRenderer = GetComponent<SpriteRenderer>();
        ShadowObject = new GameObject();
        ShadowObject.transform.SetParent(this.transform);
        ShadowObject.transform.localPosition = Vector3.zero;
        ShadowObject.transform.localScale = Vector3.one;

        ShadowRenderer = ShadowObject.AddComponent<SpriteRenderer>();
        ShadowRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        Mask = null;
        if (UsesSecondMask) Mask = ShadowObject.AddComponent<SpriteMask>();
        ShadowRenderer.sortingLayerID = SortingLayer.NameToID("Shadow");
        if (UsesMainSpriteAsMask)
        {
            SpriteMask MainSpriteMask = this.gameObject.AddComponent<SpriteMask>();

            if (ShadowMask != null) MainSpriteMask.sprite = ShadowMask;
            else MainSpriteMask.sprite = ObjectRenderer.sprite;
        }
        //MainSpriteMask.frontSortingLayerID = SortingLayer.NameToID("Shadow");
        //MainSpriteMask.backSortingLayerID = SortingLayer.NameToID("UI");

        if (ShadowMask != null && Mask != null)
        {
            Mask.sprite = ShadowMask;
            Mask.isCustomRangeActive = true;
            Mask.frontSortingLayerID = SortingLayer.NameToID("Shadow");
            Mask.backSortingLayerID = SortingLayer.NameToID("Default");
        }

        ShadowRenderer.sortingOrder = 0;
        ShadowObject.name = "Shadow2D";
        ShadowObject.transform.localPosition += new Vector3(CustomOffset.x, CustomOffset.y, 0);
        //ShadowRenderer.sprite = ObjectRenderer.sprite;
        if (CustomShadowSprite != null) ShadowRenderer.sprite = CustomShadowSprite;
        else ShadowRenderer.sprite = Sprite.Create(ObjectRenderer.sprite.texture, ObjectRenderer.sprite.rect, new Vector2(0.5f, 0.1f), ObjectRenderer.sprite.pixelsPerUnit);
        ShadowRenderer.color = new Color(Color.black.r, Color.black.g, Color.black.b, ShadowAlpha);
        ShadowObject.transform.localPosition += new Vector3(0, 0, 0.1f);
        ShadowRenderer.material = ShadowMat;

        if (CustomLightPos != Vector2.zero) SetShadow(CustomLightPos);
        else SetShadow(XyLightDirComps);


        if(this.GetComponent<Interactable>() != null)
        {
            GetComponent<Interactable>().TriggerEvent += UpdateSprite;
        }
    }

    //this is a temp update for in game testing, comment out/remove when finished
    //public void Update()
    //{
    //    ShadowObject.transform.localPosition = new Vector3(CustomOffset.x, CustomOffset.y, 0);
    //}

    public void UpdateSprite()
    {
        ShadowRenderer.sprite = Sprite.Create(ObjectRenderer.sprite.texture, ObjectRenderer.sprite.rect, new Vector2(0.5f, 0.1f), ObjectRenderer.sprite.pixelsPerUnit);
    }
    public virtual void SetShadow(Vector3 LightPos)
    {
        Vector2 Direction = this.transform.position - LightPos;
        LightPosition = LightPos;
        XyLightDirComps = Direction;
        float dist = (this.transform.position - LightPos).sqrMagnitude;
        float ypercent = Mathf.Abs(dist);
        ypercent /= 200f;
        float xpercent = Mathf.Abs(dist);
        xpercent /= 200f;
        ypercent += 0.7f;
        xpercent += 0.75f;
        xpercent = Mathf.Clamp(xpercent, 0.7f, 0.75f);
        ypercent = Mathf.Clamp(ypercent, 0.7f, 1.8f);

        ShadowObject.transform.localScale = new Vector3(xpercent, ypercent, 1);
        ShadowObject.transform.up = Direction;
        //Debug.Log("setting light pos data for" + this.name + " using dir: " + Direction);

    }


    //public virtual void SetZRot(float zrot, Vector2 Direction, float yoffset, float xoffset, Vector3 LightPos)
    //{

    //    float dist = (this.transform.position - LightPos).sqrMagnitude;
    //    //float dec = dist % 1;
    //    //dist = Mathf.Clamp(dist, 0, 1);
    //    //dist += dec;

    //    //float ypercent = Mathf.Abs((this.transform.position.y - LightPos.y));
    //    float ypercent = Mathf.Abs(dist);
    //    ypercent /= 200f;

    //    //float xpercent = Mathf.Abs((this.transform.position.x - LightPos.x));
    //    float xpercent = Mathf.Abs(dist);
    //    xpercent /= 200f;

    //    ypercent += 0.7f;

    //    xpercent += 0.75f;
    //    xpercent = Mathf.Clamp(xpercent, 0.7f, 0.75f);
    //    ypercent = Mathf.Clamp(ypercent, 0.7f, 1.8f);
    //    ShadowObject.transform.localScale = new Vector3(xpercent, ypercent, 1);
    //    ShadowObject.transform.up = Direction;
    //}
}
