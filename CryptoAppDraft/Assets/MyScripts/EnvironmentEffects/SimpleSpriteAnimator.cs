﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpriteAnimator", menuName = "CustomAnimators", order = 1)]
public class SimpleSpriteAnimator : ScriptableObject {


    public List<Sprite> Sprites = new List<Sprite>();
    public float Duration = 1f;
    public bool Loop = false;


    public IEnumerator PlayAnimation(SpriteRenderer Renderer)
    {
        if(Sprites.Count <= 0)
        {
            Debug.Log("ERROR, sprite list is empty in simple animator scriptable object");
            yield break;
        }
        int FrameCount = Sprites.Count;
        float TimeWait = Duration / FrameCount;
        int count = 0;

        while (count < FrameCount || Loop)
        {
            Renderer.sprite = Sprites[count%Sprites.Count];
            count++;
            yield return new WaitForSeconds(TimeWait);

        }
        Renderer.sprite = null;
        yield break;

    }


}
