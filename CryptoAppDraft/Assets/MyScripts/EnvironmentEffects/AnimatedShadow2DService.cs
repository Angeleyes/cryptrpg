﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class AnimatedShadow2DService : MonoBehaviour
//{

//    public static AnimatedShadow2DService Instance;
//    public Dictionary<int, ShadowData> ShadowEntites = new Dictionary<int, ShadowData>();


//    internal int Id = 0;

//    private void Awake()
//    {
//        if (Instance == null) Instance = this;
//        else
//        {
//            Destroy(Instance);
//            Instance = this;
//        }
//    }

//    private void LateUpdate()
//    {
//        foreach(KeyValuePair<int, ShadowData> K in ShadowEntites)
//        {
//            if (K.Value.TheEntity == null)
//            {
//                ShadowEntites.Remove(K.Key);
//                continue;
//            }
//            K.Value.Shadow.ShadowRenderer.sprite = K.Value.TheEntity.Rend.sprite;
//            if(K.Value.LightPos != Vector3.zero)
//            {
//                K.Value.Shadow.SetShadow(K.Value.LightPos);                  
//            }
//        }
//    }


//    //IEnumerator LerpShadow()
//    //{


//    //    yield break;
//    //}

//    public int GetShadowID()
//    {
//        Id++;
//        return Id;
//    }

//    public void SetShadowData(Shadow2DAnimated S)
//    {
//        //Debug.Log("chaning light data from: " + ShadowEntites[S.ShadowID].LightPos + " to : " + S.AnimatedShadowData.LightPos);
//        S.DefaultLightComp = S.AnimatedShadowData.LightPos;
//        ShadowEntites[S.ShadowID] = S.AnimatedShadowData;

//    }

//    public void RegisterShadow(ShadowData Sdata)
//    {
//        if (!ShadowEntites.ContainsKey(Sdata.Shadow.ShadowID))
//        {
//            Debug.Log("registering shadow: " + Sdata.TheEntity.name);
//            ShadowEntites.Add(Sdata.Shadow.ShadowID, Sdata);
//        }
//    }

//    public void UnRegisterShadow(int id)
//    {
//        Debug.Log("unregistering shadow data");
//        ShadowEntites.Remove(id);
//    }
//}


//public struct ShadowData
//{
//    public ShadowData(Vector3 V, Entity E, Shadow2DAnimated S)
//    {
//        LightPos = V;
//        TheEntity = E;
//        Shadow = S;
//    }

//    public Vector3 LightPos;
//    public Entity TheEntity;
//    public Shadow2DAnimated Shadow;
//}

