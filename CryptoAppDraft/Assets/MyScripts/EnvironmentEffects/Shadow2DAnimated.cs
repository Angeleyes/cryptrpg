﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow2DAnimated : Shadow2D
{
    public ShadowData AnimatedShadowData { get; set; }
    internal Vector2 DefaultLightComp = new Vector2(-1,-9);
    //internal int ShadowID = 0;
    internal bool IsLerped = false;

    public struct ShadowData
    {
        public ShadowData(Vector3 V, Entity E, Shadow2DAnimated S)
        {
            LightPos = V;
            TheEntity = E;
            Shadow = S;
        }

        public Vector3 LightPos;
        public Entity TheEntity;
        public Shadow2DAnimated Shadow;
    }

    private void Start()
    {
        //ShadowData S = new ShadowData(new Vector3(0,0,0), GetComponent<Entity>(), this);
        ShadowData S = new ShadowData(DefaultLightComp, GetComponent<Entity>(), this);
        //ShadowID = AnimatedShadow2DService.Instance.GetShadowID();
        //AnimatedShadow2DService.Instance.RegisterShadow(S);

        SetShadow(this.transform.position + new Vector3(DefaultLightComp.x, DefaultLightComp.y, this.transform.position.z));
        ShadowRenderer.color = new Color(ShadowRenderer.color.r, ShadowRenderer.color.g, ShadowRenderer.color.b);
        ShadowRenderer.sortingLayerID = SortingLayer.NameToID("Ground");
        ShadowRenderer.sortingOrder = 1;
        ShadowObject.transform.position = new Vector3(ShadowObject.transform.position.x, ShadowObject.transform.position.y, 1);

        if (GetComponent<SpriteMask>() != null) Destroy(GetComponent<SpriteMask>());
        else
        {
            Mask.frontSortingLayerID = SortingLayer.NameToID("Shadow");
            Mask.frontSortingOrder = 1;
            Mask.backSortingLayerID = SortingLayer.NameToID("Shadow");
            Mask.backSortingOrder = 0;
        }

        //ResetShadow(this.transform.position + new Vector3(DefaultLightComp.x, DefaultLightComp.y, this.transform.position.z));
        //maybe start reset shadow here
    }

    public override void SetShadow(Vector3 LightPos)
    {
        base.SetShadow(LightPos);

    }

    IEnumerator LerpShadow(Vector3 LightPos)
    {
        Vector3 LerpedLight = Vector3.Lerp(LightPos, DefaultLightComp, 0.1f);
        while ((LightPos - LerpedLight).sqrMagnitude >= 0.1f*0.1f)
        {
            LerpedLight = Vector3.Lerp(LerpedLight, DefaultLightComp, 0.1f);
            Debug.Log("lerping shadow back with lerped light interval: " + LerpedLight + " with dif: " + (LightPos - LerpedLight).sqrMagnitude);
            SetShadow(LerpedLight);

            yield return null;
        }
    }

    public void ResetShadow(Vector3 LastKnownLightPos)
    {
        //need light position relative to the entity
        Vector3 LightPosRelative = LastKnownLightPos - AnimatedShadowData.TheEntity.GetEntityPosition();
        StartCoroutine(LerpShadow(LightPosRelative));
    }


}
