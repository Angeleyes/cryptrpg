﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonMap : MonoBehaviour {


    List<RoomPiece> MapRooms = new List<RoomPiece>();
    private List<PlayerEntity> PlayerList = new List<PlayerEntity>();

    public float RoomRenderDistance = 100f;
    private float DistanceCheckTime = 1f;
    public Material RoomMat;
    internal int MapID;
    internal Staircase MapStaircase { get; set; }

    public void SetRooms(List<RoomPiece> L)
    {
        MapRooms = L;
    }

    public void SetMapID(int id)
    {
        MapID = id;
        this.name += id;
    }

    public List<RoomPosition> GetAllRoomPositions()
    {
        return null;
    }

    public List<WorldTooltip> GetAllWorldTips()
    {
        List<WorldTooltip> Tips = new List<WorldTooltip>();
        foreach(RoomPiece R in MapRooms)
        {
            foreach(WorldTooltip W in R.GetRoomTips())
            {
                Tips.Add(W);
            }
        }

        return Tips;
    }

    public void DestroyMap()
    {
        StopAllCoroutines();
        Destroy(this.gameObject);
    }

    public void SpawnDungeonMonsters()
    {
        foreach(RoomPiece R in MapRooms)
        {
            if(R.SpawnRoomMonsters != null) R.SpawnRoomMonsters();
        }
    }

    public void ActivateDungeonMap(List<PlayerEntity> playerList, MonsterConfig config)
    {
        Debug.Log("activating dungeon" + MapID);

        this.gameObject.SetActive(true);
        PlayerList = playerList;

        foreach(RoomPiece R in MapRooms)
        {
            if(R.LoadMonsterConfigs != null) R.LoadMonsterConfigs(config);
        }
        //StartCoroutine(PlayerDistanceCheck());
        //SetAllRoomsMat();
    }

    public void OnNewPlayerJoined(PlayerEntity NewPlayer)
    {
        PlayerList.Add(NewPlayer);
    }



}
