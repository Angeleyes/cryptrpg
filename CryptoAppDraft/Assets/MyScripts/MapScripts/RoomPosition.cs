﻿using System.Collections.Generic;
using UnityEngine;

public class RoomPosition : MonoBehaviour {


    //public bool UseAsDestructablesSpawner = false;
    public bool UseAsWaypoint = false;
    //public Vector3 Point1 = new Vector3(2, 0, 0);
    //public Vector3 Point2 = new Vector3(-2, 0, 0);
    public List<Vector3> ExtraPoints = new List<Vector3>();
    public float WaypointRadius = 5f;

    public List<BasicDestructable> Destructables = new List<BasicDestructable>();
    public List<ChestInteractable> Chests = new List<ChestInteractable>();

    public float ChestSpawnChance = 5f;
    public float DestructableSpawnChance = 25f;

    internal bool InUse = false;
    internal bool IsUsedAsWaypoint = false;
    public bool StaircasePoint = false;
    //internal GameObject RoomObject { get; private set; }

    //internal Interactable InteractableObject;
    internal Transform RoomParent;


    public void Start()
    {
        RoomParent = GetComponentInParent<Transform>();

        if (!StaircasePoint)
        {
            if (Random.Range(0, 101) <= DestructableSpawnChance)
            {
                //Debug.Log("spawning dest");
                int rand = Random.Range(2, ExtraPoints.Count);
                BasicDestructable B = Destructables[Random.Range(0, Destructables.Count)];
                SpawnObjects(B, rand);

            }
            else if (Random.Range(0, 101) <= ChestSpawnChance)
            {
                SpawnObjects(Chests[0]);
            }
        }


    }

    internal void SpawnObjects(BasicDestructable B, int count)
    {
        for (int i = 0; i < count; i++)
        {
            BasicDestructable B1 = Instantiate(B, ExtraPointWorldPosition(ExtraPoints[i]), Quaternion.identity, RoomParent);
        }
    }

    internal Vector3 ExtraPointWorldPosition(Vector3 Point)
    {
        return this.transform.position + Point;
    }

    internal void SpawnObjects(ChestInteractable C)
    {
        Instantiate(C, this.transform.position, Quaternion.identity, RoomParent);
    }

    private void OnDrawGizmos()
    {
        Color C = new Color(0, 1, 0, 0.4f);
        Gizmos.color = C;
        Gizmos.DrawSphere(this.transform.position, 0.4f);
        Gizmos.color = new Color(0, 0.5f, 0.4f);
        //Gizmos.DrawWireSphere(this.transform.position + Point1, 0.2f);
        //Gizmos.DrawWireSphere(this.transform.position + Point2, 0.2f);

        if(ExtraPoints.Count > 0)
        {
            foreach(Vector3 V in ExtraPoints)
            {
                Gizmos.DrawWireSphere(this.transform.position + V, 0.2f);
            }
        }


        Gizmos.DrawWireSphere(this.transform.position, WaypointRadius);
    }


}
