﻿
public class MapDataLocator
{
    static MapData Service;

    public static MapData GetMapData() { return Service; }
    public static void Provide(MapData Md) { Service = Md; }

}
