﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData
{
    internal string MapName = " ";
    public int DungeonLayer { get; internal set; }


    MapData(string mapname, int dungeonlayer)
    {
        MapName = mapname;
        DungeonLayer = dungeonlayer;
    }
}
