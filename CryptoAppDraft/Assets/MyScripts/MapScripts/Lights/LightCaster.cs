﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCaster : MonoBehaviour
{

    public Material ShadowMaterial;
    public float LightRadius = 15f;

    List<Shadow2D> ShadowObjects = new List<Shadow2D>();
    List<Shadow2DAnimated> AnimatedObjects = new List<Shadow2DAnimated>();
    private bool ShadowServiceRunning = false;

    //private void CastRays()
    //{
    //    Vector3 Direction = new Vector3();
    //    foreach (Shadow2D S1 in ShadowObjects)
    //    {
    //        Direction = S1.transform.position - this.transform.position;
    //    }

    //    if(ShadowObjects.Count >= 4)
    //    {

    //        for (int i = 0; i < ShadowObjects.Count; i++)
    //        {
    //            Direction = ShadowObjects[i].transform.position - this.transform.position;
    //            //Debug.DrawRay(this.transform.position, (ShadowObjects[i].transform.position - this.transform.position), Color.blue, 10);
    //            //Debug.DrawRay(ShadowObjects[i].transform.position, ShadowObjects[i].transform.up * 2f, Color.red, 10);
    //            ShadowObjects[i].XyLightDirComps = new Vector2(Direction.x, Direction.y);
    //            ShadowObjects[i].LightAngle = Vector2.Angle(Direction, ShadowObjects[i].transform.up);
    //            ShadowObjects[i].SetZRot(Vector2.Angle(Direction, ShadowObjects[i].transform.up), Direction, 0, 0, this.transform.position);
    //            //Debug.Log("angle: " + ShadowObjects[i].name + " : " + Vector2.Angle(Direction, ShadowObjects[i].transform.up));
    //        }
    //    }
    //}


    IEnumerator UpdateShadows()
    {

        ShadowServiceRunning = true;
        List<Shadow2DAnimated> AnimShadowCopy = new List<Shadow2DAnimated>();

        while (true)
        {

            yield return new WaitForEndOfFrame();
            AnimShadowCopy = AnimatedObjects;

            if(AnimShadowCopy.Count == 0)
            {
                ShadowServiceRunning = false;
                //Debug.Log("no more shadows nearby, stopping routine");
                yield break;
            }

            foreach(Shadow2DAnimated S in AnimShadowCopy)
            {
                if (S.AnimatedShadowData.LightPos != Vector3.zero) S.AnimatedShadowData.Shadow.SetShadow(S.AnimatedShadowData.LightPos); //set the shadow angle
            }

            yield return new WaitForSeconds(0.05f);
            yield return null;
        }

        yield return null;
    }
    public void PollLightData(out GameObject LightData)
    {
        LightData = new GameObject();
        //not implementing yet
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Shadow2D S = collision.gameObject.GetComponent<Shadow2D>();
        if(S != null)
        {

            if (S is Shadow2DAnimated)
            {
                Shadow2DAnimated.ShadowData Sdata = new Shadow2DAnimated.ShadowData(this.transform.position, S.gameObject.GetComponent<Entity>(), S as Shadow2DAnimated);
                (S as Shadow2DAnimated).AnimatedShadowData = Sdata;
                AnimatedObjects.Add(S as Shadow2DAnimated);
                if (!ShadowServiceRunning) StartCoroutine(UpdateShadows());
                //AnimatedShadow2DService.Instance.RegisterShadow(Sdata);
                //AnimatedShadow2DService.Instance.SetShadowData((S as Shadow2DAnimated));

            }
            else
            {
                ShadowObjects.Add(S);
                S.SetShadow(this.transform.position);
            }


        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Shadow2D S = collision.gameObject.GetComponent<Shadow2D>();
        if(S != null)
        {
            if (S is Shadow2DAnimated)
            {
                Shadow2DAnimated Sa = (S as Shadow2DAnimated);
                Sa.AnimatedShadowData = new Shadow2DAnimated.ShadowData(Sa.DefaultLightComp, Sa.AnimatedShadowData.TheEntity, Sa);
                AnimatedObjects.Remove(Sa);
                //reset animated shadow here
                Sa.ResetShadow(this.transform.position);
                //AnimatedShadow2DService.Instance.SetShadowData(Sa);
    
            }
            else ShadowObjects.Remove(S);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(this.transform.position, LightRadius);

        //foreach (Shadow2D S in ShadowObjects)
        //{
        //    //Gizmos.DrawLine(this.transform.position, S.transform.position);
        //    //Gizmos.color = Color.red;
        //    //Gizmos.DrawLine(S.transform.position, new Vector3(0,S.transform.position.y, 0));
        //    //Gizmos.color = Color.green;
        //    //Vector3 Dir = S.transform.position - this.transform.position;
        //    //Gizmos.DrawLine(this.transform.position, Dir);
        //    //Gizmos.color = Color.black;
        //    //Gizmos.DrawLine(S.transform.position, S.transform.position.normalized);
        //    //Gizmos.DrawLine(S.transform.position, new Vector3(S.transform.position.x, 0, 0));
        //    //Gizmos.color = Color.cyan;
        //    //Gizmos.DrawLine(S.transform.position, Vector3.right);
        //    //Debug.Log(Vector3.Angle(Dir, new Vector3(0, transform.position.y, 0)));

        //}


    }
}
