﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joint : MonoBehaviour {


    //public BoxCollider2D AttatchedDoor;
    public bool IsAttached = false;
    public Vector3 JointPosition;
    //public Vector3 JointParentLocal;
    //public SpriteRenderer Blocker;
    public Vector3Int DoorPart1;
    public Vector3Int DoorPart2;
    public bool IsFractleJoint = false;
    public Vector3Int DoorCoords = new Vector3Int();
    //for blockers
    private Vector4 TopPoints = new Vector4(2.0325f, -1.1025f, -2.0325f, -1.1025f);
    private Vector4 BotPoints = new Vector4(2.0325f, -1.075f, -2.0325f, -1.075f);
    private Vector4 LeftPoints = new Vector4(2.1f, -2.1f, 2.1f, 2.1f);
    private Vector4 RightPoints = new Vector4(-2.1f, -2.1f, -2.1f, 2.1f);

    private Vector2 LeftSideDoorEdge = new Vector2(0, 2.1f);
    private Vector2 LeftSideDoorEdge2 = new Vector2(1.85f, 2.1f);
    [SerializeField]
    public Joint CloseJoint { get; private set; }

    public Vector2[] BlockerPoints = new Vector2[2];

    public EdgeCollider2D BlockerEdge;
    public EdgeCollider2D DoorEdge1;
    public EdgeCollider2D DoorEdge2;

    public enum OrientationEnum
    {
        Up,
        Down,
        Left,
        Right
    }

    internal void SetClosestsJoint(Joint J)
    {
        Debug.Log("FOUND CLOSE JOINT ");
        CloseJoint = J;
        Vector3 direction = CloseJoint.transform.position - this.transform.position;
        float dist = Vector3.Distance(CloseJoint.transform.position, this.transform.position);
        RaycastHit2D Hit = Physics2D.Raycast(this.transform.position, direction, dist);
        if (Hit.collider == null)
        {
            Debug.Log("OMG, FOUND A LOOP, comps are: " + direction.x + "," + direction.y);
        }
        else
        {
            Debug.Log("close joint ray hit a collider : " + Hit.collider + "   NO LINK");
            CloseJoint = null;
        }

    }

    public OrientationEnum JointOrientation;

    public Transform JParent;

    public void Start()
    {
        CalculateBlockerCollision();
        ToggleBlockerCollision(false);
        SetupSideDoorCollision();
    }


    private void CalculateBlockerCollision()
    {

        BlockerPoints = new Vector2[2];

        if (JointOrientation == OrientationEnum.Up)
        {
            BlockerPoints[0] = new Vector2(TopPoints.x, TopPoints.y);
            BlockerPoints[1] = new Vector2(TopPoints.z, TopPoints.w);

        }
        else if (JointOrientation == OrientationEnum.Down)
        {
            BlockerPoints[0] = new Vector2(BotPoints.x, BotPoints.y);
            BlockerPoints[1] = new Vector2(BotPoints.z, BotPoints.w);
        }
        else if (JointOrientation == OrientationEnum.Left)
        {
            BlockerPoints[0] = new Vector2(LeftPoints.x, LeftPoints.y);
            BlockerPoints[1] = new Vector2(LeftPoints.z, LeftPoints.w);
        }
        else if (JointOrientation == OrientationEnum.Right)
        {
            BlockerPoints[0] = new Vector2(RightPoints.x, RightPoints.y);
            BlockerPoints[1] = new Vector2(RightPoints.z, RightPoints.w);
        }

        if (BlockerEdge == null)
        {
            Debug.Log(JointPosition + "blocker edge on joint its null");
        }
        else BlockerEdge.points = BlockerPoints;
    }

    public void ToggleSideDoorCollision(bool toggle)
    {
        if (this.JointOrientation == OrientationEnum.Up || this.JointOrientation == OrientationEnum.Down) return;
        DoorEdge1.enabled = toggle;
        DoorEdge2.enabled = toggle;
    }

    public void SetupSideDoorCollision()
    {
        if (JointOrientation == OrientationEnum.Down || JointOrientation == OrientationEnum.Up)
        {
            foreach(EdgeCollider2D E in GetComponents<EdgeCollider2D>())
            {
                E.enabled = false;
            }
            return;
        }
        else if (JointOrientation == OrientationEnum.Left)
        {
            //top edge of left side
            Vector2[] P1 = new Vector2[2];
            P1[0] = LeftSideDoorEdge;
            P1[1] = LeftSideDoorEdge2;

            DoorEdge1.points = P1;
            //bottom side
            P1[0] = new Vector2(LeftSideDoorEdge.x, (LeftSideDoorEdge.y * -1));
            P1[1] = new Vector2(LeftSideDoorEdge2.x, (LeftSideDoorEdge2.y * -1));

            DoorEdge2.points = P1;
        }
        else
        {
            Vector2[] P1 = new Vector2[2];

            P1[0] = new Vector2((LeftSideDoorEdge.x * -1), LeftSideDoorEdge.y);
            P1[1] = new Vector2((LeftSideDoorEdge2.x * -1), LeftSideDoorEdge2.y);
            DoorEdge1.points = P1;

            P1[0] = new Vector2(P1[0].x, (LeftSideDoorEdge.y * -1));
            P1[1] = new Vector2((LeftSideDoorEdge2.x * -1), (LeftSideDoorEdge2.y * -1));

            DoorEdge2.points = P1;
        }
    }

    public void InitJoint(Vector3 Pos, OrientationEnum O, Transform parent, Vector3Int doorCoords)
    {
        JointPosition = Pos;
        JointOrientation = O;
        JParent = parent;
        IsAttached = false;

        DoorCoords = doorCoords;
        //Debug.Log("doorcoords in init: "+ doorCoords);
    }

    private void SpawnLockedDoor()
    {

    }

    public void CalculateDoorCoords()
    {

        //the actual position in space of the chunk
        Vector3Int v2 = new Vector3Int();
        Vector3Int v1 = new Vector3Int();

        //doorcoords is the cell position in the grid
        //so it must be converted to world space


        //Debug.Log("initial coords: " + DoorCoords);
        v1 = DoorCoords*2;
        //Debug.Log("after: " + v1);
        if (JointOrientation == OrientationEnum.Up)
        {
            v2 = new Vector3Int(v1.x + 2, v1.y, 0);
        }
        else if (JointOrientation == OrientationEnum.Down)
        {
            v2 = new Vector3Int(v1.x + 2, v1.y, 0);
        }
        else if (JointOrientation == OrientationEnum.Left)
        {
            v2 = new Vector3Int(v1.x, v1.y + 2, 0);
        }
        else if (JointOrientation == OrientationEnum.Right)
        {
            v2 = new Vector3Int(v1.x, v1.y + 2, 0);
        }

        DoorPart1 = v1;
        DoorPart2 = v2;
    }


    public void CalculateFractleDoorCoords()
    {
        //the actual position in space of the chunk
        Vector3Int v2 = new Vector3Int();
        Vector3Int v1 = new Vector3Int();

        //doorcoords is the cell position in the grid
        //so it must be converted to world space


        //Debug.Log("initial coords: " + DoorCoords);
        v1 = DoorCoords;
        //Debug.Log("after: " + v1);
        if (JointOrientation == OrientationEnum.Up)
        {
            v2 = new Vector3Int(DoorCoords.x + 2, DoorCoords.y, 0);
        }
        else if (JointOrientation == OrientationEnum.Down)
        {
            v2 = new Vector3Int(DoorCoords.x + 2, DoorCoords.y, 0);
        }
        else if (JointOrientation == OrientationEnum.Left)
        {
            v2 = new Vector3Int(DoorCoords.x, DoorCoords.y + 2, 0);
        }
        else if (JointOrientation == OrientationEnum.Right)
        {
            v2 = new Vector3Int(DoorCoords.x, DoorCoords.y + 2, 0);
        }

        DoorPart1 = v1;
        DoorPart2 = v2;
    }

    public void ToggleBlockerCollision(bool toggle)
    {
        BlockerEdge.enabled = toggle;
    }

    public void InitParent()
    {
        this.transform.SetParent(JParent);
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawSphere(new Vector3(this.transform.position.x, this.transform.position.y, -1), 0.1f);
        Gizmos.DrawWireSphere(new Vector3(this.transform.position.x, this.transform.position.y, -1), 0.2f);
        Gizmos.DrawWireSphere(new Vector3(this.transform.position.x, this.transform.position.y, -1), 50f);
        Gizmos.color = Color.green;
        if (CloseJoint != null) Gizmos.DrawLine(this.transform.position, CloseJoint.transform.position);
        Gizmos.color = Color.blue;
        if (CloseJoint != null)
        {
            Vector3 dir = CloseJoint.transform.position = this.transform.position;
            Gizmos.DrawLine(this.transform.position, new Vector3(0, dir.y, 0));
            Gizmos.DrawLine(this.transform.position, new Vector3(dir.x, 0, 0));
        }
    }
}
