﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FractleRoomPiece : RoomPiece {

    BoxCollider2D FractleBox;
    public Tilemap FractleFloor;


    private Dictionary<Joint.OrientationEnum, List<Joint>> FractleJointLookUp = new Dictionary<Joint.OrientationEnum, List<Joint>>();

    public override void Start()
    {
        base.Start();

        Tilemap[] Maps = GetComponentsInChildren<Tilemap>();
        FloorLayerMap = Maps[0];
        DoorLayerMap = Maps[1];

        FractleFloor = Maps[2];
        FractleBox = FractleFloor.GetComponent<BoxCollider2D>();
    }


    private void InitDict()
    {
        FractleJointLookUp.Clear();
        FractleJointLookUp.Add(Joint.OrientationEnum.Up, new List<Joint>());
        FractleJointLookUp.Add(Joint.OrientationEnum.Down, new List<Joint>());
        FractleJointLookUp.Add(Joint.OrientationEnum.Left, new List<Joint>());
        FractleJointLookUp.Add(Joint.OrientationEnum.Right, new List<Joint>());
    }
    public override void FillJointLookUp()
    {
        InitDict();

        //if(FractleJointLookUp.Count > 0)
        //{
        //    Debug.Log("joint lookup already filled in fractle");
        //    return;
        //}

        foreach (Joint J in AllJoints)
        {
            FractleJointLookUp[J.JointOrientation].Add(J);
        }
    }

    public override void TurnOffOverlapBox()
    {
        FractleBox.enabled = false;
    }

    public override Joint GetJointByOppositeOrientation(Joint.OrientationEnum O)
    {

        List<Joint> JList = new List<Joint>();

        FractleJointLookUp.TryGetValue(GetOppositeOrientation(O), out JList);

        if (JList.Count > 0) return JList[Random.Range(0, JList.Count)];
        else
        {
            Debug.Log("returning null in fractle get joint by opp orientation");
            return null;
        }

    }

    public override Joint GetJointByOrientation(Joint.OrientationEnum O)
    {
        //Debug.Log("calling fractle getjointbyorientation");
        List<Joint> JList = new List<Joint>();

        FractleJointLookUp.TryGetValue(O, out JList);

        if (JList.Count > 0) return JList[Random.Range(0, JList.Count)];
        else
        {
            //Debug.Log("returning null in fractle get joint by orientation");
            return null;
        }


    }


    public void FractleInit(BoxCollider2D Box1, BoxCollider2D Box2, ScriptableTileMap map, RoomType Rtype)
    {
        InitRoom(Box1, map, Rtype);
        FractleBox = Box2;
    }


    public override void BlockOpenJoints()
    {

        base.BlockOpenJoints();

        Tile T = new Tile();

        //SetBlockerSprites();
        if(FractleJoints.Count <= 0)
        {
            foreach(Joint J in AllJoints)
            {
                if (J.IsFractleJoint) FractleJoints.Add(J);
            }
        }

        foreach (Joint J in FractleJoints)
        {
            //Debug.Log("unattachted joint found: " + J.gameObject.name + " pos: " + J.transform.position);
            if (!J.IsAttached)
            {
                J.ToggleBlockerCollision(true);

                //calculate the door coords if they are not zero
                //if they are zero, that means they were manually set
                if (J.DoorCoords != Vector3Int.zero) J.CalculateFractleDoorCoords();

                if (J.JointOrientation == Joint.OrientationEnum.Down || J.JointOrientation == Joint.OrientationEnum.Up)
                {
                    T = DoorBlockerT;
                    //Debug.Log("setting tile at " + J.DoorPart1 + " and " + J.DoorPart2);
                    DoorLayerMap.SetTile(J.DoorPart1, null);
                    DoorLayerMap.SetTile(J.DoorPart2, null);

                    Vector3Int ZFix = new Vector3Int(J.DoorPart1.x, J.DoorPart1.y, -1);
                    Vector3Int ZFix2 = new Vector3Int(J.DoorPart2.x, J.DoorPart2.y, -1);

                    DoorLayerMap.SetTile(ZFix, null);
                    DoorLayerMap.SetTile(ZFix2, null);

                    DoorLayerMap.SetTile(J.DoorPart1, T);
                    DoorLayerMap.SetTile(J.DoorPart2, T);
                    T = DoorBlockerB;
                    FractleFloor.SetTile(J.DoorPart1, T);
                    FractleFloor.SetTile(J.DoorPart2, T);
                }
                else
                {
                    if (J.JointOrientation == Joint.OrientationEnum.Left)
                    {
                        T = DoorBlockerSideR;
                        DoorLayerMap.SetTile(J.DoorPart1, null);
                        DoorLayerMap.SetTile(J.DoorPart2, null);
                        FractleFloor.SetTile(J.DoorPart1, null);
                        FractleFloor.SetTile(J.DoorPart2, null);

                        Vector3Int ZFix = new Vector3Int(J.DoorPart1.x, J.DoorPart1.y, -1);
                        Vector3Int ZFix2 = new Vector3Int(J.DoorPart2.x, J.DoorPart2.y, -1);

                        DoorLayerMap.SetTile(ZFix, null);
                        DoorLayerMap.SetTile(ZFix2, null);
                        FractleFloor.SetTile(ZFix, null);
                        FractleFloor.SetTile(ZFix2, null);
                        DoorLayerMap.SetTile(J.DoorPart1, T);
                        DoorLayerMap.SetTile(J.DoorPart2, T);
                        J.ToggleSideDoorCollision(false);
                    }
                    else if (J.JointOrientation == Joint.OrientationEnum.Right)
                    {
                        T = DoorBlockerSideL;
                        DoorLayerMap.SetTile(J.DoorPart1, null);
                        DoorLayerMap.SetTile(J.DoorPart2, null);
                        FractleFloor.SetTile(J.DoorPart1, null);
                        FractleFloor.SetTile(J.DoorPart2, null);

                        Vector3Int ZFix = new Vector3Int(J.DoorPart1.x, J.DoorPart1.y, -1);
                        Vector3Int ZFix2 = new Vector3Int(J.DoorPart2.x, J.DoorPart2.y, -1);

                        DoorLayerMap.SetTile(ZFix, null);
                        DoorLayerMap.SetTile(ZFix2, null);
                        FractleFloor.SetTile(ZFix, null);
                        FractleFloor.SetTile(ZFix2, null);



                        DoorLayerMap.SetTile(J.DoorPart1, T);
                        DoorLayerMap.SetTile(J.DoorPart2, T);
                        J.ToggleSideDoorCollision(false);
                    }
                }
            }
        }

    }

}
