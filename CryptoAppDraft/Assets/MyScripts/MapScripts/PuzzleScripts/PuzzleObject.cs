﻿using UnityEngine;
using System.Collections.Generic;

public class PuzzleObject : MonoBehaviour
{
    public List<Vector2> SpawnPoints = new List<Vector2>() { new Vector2(0, 3), new Vector2(2, 3), new Vector2(-2, 3) };
    public Interactable TriggerObject;
    public ChestInteractable RewardChest;
    public bool ReversePuzzle = false;
    public PuzzleType PuzzleMode;
    public PhraseTable CluePhraseTable;
    [SerializeField]
    internal List<Interactable> TriggerSubjects = new List<Interactable>();
    [SerializeField]
    internal bool[] TriggerFlagList;
    [SerializeField]
    internal bool[] BitSolution;
    internal Queue<int> OrderSolution = new Queue<int>();
    internal int[] TriggerOrderList;

    internal PuzzleStrategies PuzzleStrats;
    internal IPuzzleSolution PuzzleSol;
    public delegate bool PuzzleSolutionCheck(PuzzleObject P);
    public PuzzleSolutionCheck SolutionCheck;

    private bool PuzzleSolved = false;

    [TextArea]
    public string PlaqueToolTip;

    public enum PuzzleType
    {
        AND = 0,
        ORDER = 1,
    }

    internal WorldTooltip PuzzleToolTip;

    public void Start()
    {
        TriggerFlagList = new bool[SpawnPoints.Count];

        TriggerOrderList = new int[SpawnPoints.Count];

        BitSolution = new bool[SpawnPoints.Count];

        PuzzleToolTip = GetComponent<WorldTooltip>();

        foreach(Vector2 V in SpawnPoints)
        {
            if (TriggerObject is IPuzzleObservable)
            {
                Interactable I = Instantiate(TriggerObject, PointWorldPosition(V), Quaternion.identity);
                I.transform.SetParent(this.transform);
                (I as IPuzzleObservable).AddObserver(this);
                if (I.GetComponent<AudioSource>() != null) Destroy(I.GetComponent<AudioSource>());
                TriggerSubjects.Add(I); // need to sort list by x value, higher x values are on the left
            }
        }

        //from left to right
        TriggerSubjects.Sort(delegate (Interactable I1, Interactable I2) { return I1.transform.position.x.CompareTo(I2.transform.position.x); });
        if(ReversePuzzle) TriggerSubjects.Reverse();

        switch (PuzzleMode)
        {
            case PuzzleType.AND: PuzzleStrats = new PuzzleStrategies(ANDPuzzle.Instance);
                break;
            case PuzzleType.ORDER: PuzzleStrats = new PuzzleStrategies(ORDERPuzzle.Instance);
                break;
            default:Debug.Log("Error in puzzle mode, cannot identify mode");
                break;
        }

        SolutionCheck = PuzzleStrats.ThePuzzle.CheckSolution;
        PuzzleStrats.ThePuzzle.GeneratePuzzle(this);
        PlaqueToolTip = PuzzleStrats.ThePuzzle.GenerateClue(this);
        PuzzleToolTip.SetTooltipText(PlaqueToolTip);
    }

    public void Notify()
    {
        for (int i = 0; i < TriggerSubjects.Count; i++)
        {
            BitSolution[i] = (TriggerSubjects[i] as IPuzzleObservable).IsTriggered();
            if ((TriggerSubjects[i] as IPuzzleObservable).IsTriggered() && !OrderSolution.Contains(i))
            {
                OrderSolution.Enqueue(i);
                //Debug.Log("enquiing " + i);
            }
        }



        if (!PuzzleSolved && SolutionCheck(this)) Reward();
    }

    protected void Reward()
    {
        PuzzleSolved = true;
        Instantiate(RewardChest, new Vector3(this.transform.position.x + 5, this.transform.position.y, 0), Quaternion.identity);
    }


    private Vector2 PointWorldPosition(Vector2 Point)
    {
        return this.transform.position + new Vector3(Point.x, Point.y, 0);
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        foreach(Vector2 V in SpawnPoints)
        {
            Gizmos.DrawWireSphere(this.transform.position + new Vector3(V.x, V.y, 0), 0.3f);
        }

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(this.transform.position, 0.6f);

        if (PuzzleMode == PuzzleType.AND)
        {
            if (TriggerSubjects.Count > 0)
            {
                for (int i = 0; i < TriggerSubjects.Count; i++)
                {
                    if (TriggerFlagList[i]) Gizmos.DrawSphere(TriggerSubjects[i].transform.position, 0.5f);
                }
            }
        }
        else if(PuzzleMode == PuzzleType.ORDER)
        {
            Gizmos.color = Color.green;
            if(TriggerSubjects.Count > 0)
            {
                float size = 0.2f;
                for (int i = 0; i < TriggerSubjects.Count; i++)
                {
                    int slotnumber = TriggerOrderList[i];

                    Gizmos.DrawSphere(new Vector3(TriggerSubjects[slotnumber].transform.position.x, TriggerSubjects[slotnumber].transform.position.y + 2, 0), size);
                    size += 0.05f;
                }
            }
        }
    }

}
