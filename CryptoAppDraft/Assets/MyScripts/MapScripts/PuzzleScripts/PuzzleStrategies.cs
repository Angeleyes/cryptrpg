﻿
using UnityEngine;
using System.Collections.Generic;
using System.Text;

public interface IPuzzleSolution
{
    bool CheckSolution(PuzzleObject owner);
    void GeneratePuzzle(PuzzleObject owner);
    string GenerateClue(PuzzleObject owner);
}

public class PuzzleStrategies
{
    public IPuzzleSolution ThePuzzle;

    public PuzzleStrategies(IPuzzleSolution puzzle)
    {
        ThePuzzle = puzzle;
    }
}



public class ANDPuzzle : IPuzzleSolution
{

    private static ANDPuzzle _Instance;

    private ANDPuzzle()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static ANDPuzzle Instance
    {
        get
        {
            if (_Instance == null) new ANDPuzzle();
            return _Instance;
        }
    }

    public bool CheckSolution(PuzzleObject owner)
    {
        bool[] PossibleSolution = new bool[owner.TriggerFlagList.Length];

        for (int i = 0; i < owner.TriggerFlagList.Length; i++)
        {
            if (owner.TriggerFlagList[i] == owner.BitSolution[i]) PossibleSolution[i] = true;
            else PossibleSolution[i] = false;
        }

        for (int i = 0; i < PossibleSolution.Length; i++)
        {
            if (PossibleSolution[i] != true) return false;
        }

        return true;
    }

    public void GeneratePuzzle(PuzzleObject owner)
    {
        int NumTriggers = Random.Range(1, owner.TriggerFlagList.Length+1); //at least 2 triggers must be part of the solution
        int index = 0;
        int num = 0;
        //Debug.Log("picked num triggers: " + NumTriggers);
        List<int> RandomIndexs = new List<int>();


        for (int i = 0; i < owner.TriggerFlagList.Length; i ++)
        {
 
            RandomIndexs.Add(i);
        }

        for (int i = 0; i < NumTriggers; i++)
        {
            index = Random.Range(0, RandomIndexs.Count);
            //Debug.Log("index : " + i + " with random index : " + index + "number taken out: " + RandomIndexs[index]);
            num = RandomIndexs[index];
            //keep on eye on this...either remove index or num....
            RandomIndexs.Remove(num);
 
            owner.TriggerFlagList[num] = true;
        }


    }


    public string GenerateClue(PuzzleObject owner)
    {
        List<int> TrueIndexs = new List<int>();
        List<int> FalseIndexs = new List<int>();

        List<string> Phrases = new List<string>();

        for (int i = 0; i < owner.TriggerFlagList.Length; i++)
        {
            if (owner.TriggerFlagList[i]) TrueIndexs.Add(i);
            else FalseIndexs.Add(i);
        }


        //direct phrase e.g: "On the 'number'th day."   <====tells you the slot number to hit
        //negative direct phrase e.g: "'number' have no vision." <=====tells you how many should NOT be activated

        //algo: for every true trigger flag, have a direct phrase
        // maybe take away one of the direct phrases for difficulty and flip a coin if you have a negative direct phrase or not
        //depending on difficulty, randomize phrases order in the paragraph

        Phrases.Add(owner.CluePhraseTable.GetRandomANDPhrase(owner.ReversePuzzle));
        foreach(int j in TrueIndexs)
        {
            Phrases.Add(owner.CluePhraseTable.GenerateDirectPhrase(j));
        }

        int NumOffTriggers = owner.TriggerSubjects.Count - TrueIndexs.Count;

        Phrases.Add(owner.CluePhraseTable.GenerateNeagtivePhrase(NumOffTriggers-1));

        StringBuilder Sb = new StringBuilder();
        foreach(string s in Phrases)
        {
            Sb.AppendLine(s);
            //Sb.Append(System.Environment.NewLine);
        }

        return Sb.ToString();
        //Phrases.Add(PickNegativePhrase(NumOffTriggers));
    }
}


public class ORDERPuzzle : IPuzzleSolution
{


    private static ORDERPuzzle _Instance;

    private ORDERPuzzle()
    {
        if (_Instance != null) return;
        else _Instance = this;
    }

    public static ORDERPuzzle Instance
    {
        get
        {
            if (_Instance == null) new ORDERPuzzle();
            return _Instance;
        }
    }

    public bool CheckSolution(PuzzleObject owner)
    {

        if(owner.OrderSolution.Count == owner.TriggerOrderList.Length)
        {
            Debug.Log("checking order solution");
            for (int i = 0; i < owner.OrderSolution.Count; i++)
            {
                if (owner.OrderSolution.Dequeue() != owner.TriggerOrderList[i])
                {
                    owner.OrderSolution.Clear();
                    foreach(Interactable I in owner.TriggerSubjects)
                    {
                        (I as KnightInteract).ResetInteractable();
                    }
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    public void GeneratePuzzle(PuzzleObject owner)
    {
        //int NumTriggers = Random.Range(2, owner.TriggerFlagList.Length + 1); //at least 2

        int index = 0;
        int num = 0;

        List<int> RandomIndexs = new List<int>();


        for (int i = 0; i < owner.TriggerSubjects.Count; i++)
        {
            RandomIndexs.Add(i);
        }

        for (int i = 0; i < owner.TriggerSubjects.Count; i++)
        {
            index = Random.Range(0, RandomIndexs.Count);
            //Debug.Log("index : " + i + " with random index : " + index + "number taken out: " + RandomIndexs[index] + " with randomindex count: " + RandomIndexs.Count);
            num = RandomIndexs[index];
            RandomIndexs.Remove(num);

            owner.TriggerOrderList[i] = num;
        }

        //for (int i = 0; i < owner.TriggerOrderList.Length; i++)
        //{
        //    //Debug.Log("order trigger list is: " + i + " : " + owner.TriggerOrderList[i]);
        //}


    }



    public string GenerateClue(PuzzleObject owner)
    {

        StringBuilder Sb = new StringBuilder();
        Sb.AppendLine(owner.CluePhraseTable.GetRandomANDPhrase(false));
        Sb.AppendLine(owner.CluePhraseTable.GetRandomORDERPhrase());

        for (int i = 0; i < owner.TriggerOrderList.Length; i++)
        {
            Sb.AppendLine(owner.CluePhraseTable.GenerateDirectPhrase(owner.TriggerOrderList[i]));
        }

        return Sb.ToString();
    }




}