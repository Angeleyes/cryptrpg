﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FloorChunk : TileChunk {


    public FloorChunk(int x, int y, Tile tile)
    {
        ChunkCoordinates = new Vector3Int(x, y, 0);
        ChunkSprite = tile.sprite;
        ChunkTile = tile;

    }

    public FloorChunk(int x, int y, int z, Sprite sprte)
    {
        ChunkCoordinates = new Vector3Int(x, y, z);
        ChunkSprite = sprte;
    }
    public void SetToDeviateFloorTile(Sprite NewTile)
    {
        ChunkSprite = NewTile;
    }

    //public float x;
    //public float y;

    //public float xx;

    //public BoxCollider2D Box;

    //public Vector2 V = new Vector2();

    //private void Start()
    //{
    //    x = Box.size.x;
    //    y = Box.size.y;

    //    xx = Box.bounds.size.magnitude;

    //    float yy = Box.bounds.max.y;
    //    float xxx = Box.bounds.max.x;



    //    //V = new Vector2(xxx, yy);

    //    //Debug.Log(V);

    //    float x3 = xxx;

    //    xxx -= x3;
    //    //xxx -= x3;

    //    V = new Vector3(xxx, yy, 0);

    //    Debug.Log(Vector3.Distance(V, Box.bounds.min));
    //}

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.green;
    //    Gizmos.DrawSphere(Box.bounds.min, 0.5f); //bottom left corner yay 
    //    Gizmos.DrawSphere(Box.bounds.max, 0.5f);
    //    Gizmos.color = Color.red;

    //    Gizmos.DrawSphere(new Vector3(V.x, V.y, 0), 0.6f);


    //    Debug.Log(Box.bounds.min);

    //    //Gizmos.DrawSphere(Box.size.m)
    //}

}
