﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class DungeonGenerator : MonoBehaviour {


    public DungeonMap MapRef;
    public ScriptableList NormalRoomList;
    public ScriptableList CorridorList;
    public ScriptableList EndRoomList;
    public ScriptableList BossRoomList;

    public int Waves = 1;
    //public Transform ParentMover;

    //private bool RelistFlag = false;
    private int FailFitCounter = 0;
    //private int StepCounter = 0;
    internal DungeonMap TheMap;
    List<RoomPiece> PreviousRooms = new List<RoomPiece>();
    List<ScriptableList> RoomLists = new List<ScriptableList>();
    Queue<ScriptableList> ListQue = new Queue<ScriptableList>();
    List<RoomPiece> AllRooms = new List<RoomPiece>();
    List<RoomPiece> FinalRooms = new List<RoomPiece>();
    public Staircase TheStaircase;
    //public int DestructableSpawnChance = 75;
    //public int ChestSpawnChance = 10;
    //public List<BasicDestructable> Destructables;
    //public List<ChestInteractable> ChestsList;
    //float timer = 0;

    RoomPiece StartingRoom;

    public DungeonMap GenerateDungeon(Action Callback, Vector3 StartPosition)
    {
        
        ResetGenerator();
        TheMap = Instantiate(MapRef, StartPosition, Quaternion.identity);
        TheMap.transform.position = new Vector3(TheMap.transform.position.x, TheMap.transform.position.y, 0);
        //spawn a start room

        RoomPiece StartRoom1 = NormalRoomList.GetStartRoom();
        RoomPiece StartRoom = Instantiate(StartRoom1, StartPosition, Quaternion.identity);
        StartingRoom = StartRoom;

       //Debug.Log("about to organize ::: " + NormalRoomList.Rooms.Count);
        NormalRoomList.OrganizeRooms();
        CorridorList.OrganizeRooms();
        EndRoomList.OrganizeRooms();
        //BossRoomList.OrganizeRooms();

        PreviousRooms.Add(StartRoom);
        AllRooms.Add(StartRoom);
        ListQue.Enqueue(CorridorList);
        ListQue.Enqueue(NormalRoomList);
        ListQue.Enqueue(CorridorList);
        ListQue.Enqueue(NormalRoomList);
        ListQue.Enqueue(CorridorList);
        //ListQue.Enqueue(EndRoomList);

        if (Waves > ListQue.Count) Waves = ListQue.Count;
        StartCoroutine(StepStart(Callback));

        return TheMap;

    }

    private void FinalizeDungeon()
    {
        Debug.Log("finalizing dungeon");
        StartingRoom.transform.SetParent(TheMap.gameObject.transform);
        TryConnectOpenJoints();
        foreach (RoomPiece R in AllRooms)
        {
            //R.RoomBody.Sleep();
            Destroy(R.RoomBody);
            
            R.transform.position = new Vector3(R.transform.position.x, R.transform.position.y, 0);
            //try to connect close joints
            R.TurnOffOverlapBox();
            R.BlockOpenJoints();

            R.SetStatic();
        }

        TheMap.MapStaircase = SpawnStairCase();
        //StopCoroutine(TimerRoutine());
        //Debug.Log("Total Time: " + timer);
        //in end rooms, get all staircase positions and randomly spawn a staircase

    }

    private void TryConnectOpenJoints()
    {
        Debug.Log("TRYING TO CONNECT OPEN JOINTS");
        List<Joint> RemainingOpenJoints = new List<Joint>();
        foreach(RoomPiece R in AllRooms)
        {
            foreach(Joint J in R.GetAllOpenJoints())
            {
                RemainingOpenJoints.Add(J);
            }
        }

        Queue<Joint> JointQue = new Queue<Joint>(RemainingOpenJoints);

        //Joint TestingJoint = JointQue.Dequeue();
        float SmallestDistance = 50f;

        foreach (Joint TestingJoint in JointQue)
        {
            foreach (Joint CompareJoint in RemainingOpenJoints)
            {
                if (CompareJoint.transform.position == TestingJoint.transform.position) continue;
                else if ((CompareJoint.transform.position - TestingJoint.transform.position).sqrMagnitude < SmallestDistance * SmallestDistance)
                {

                    SmallestDistance = (TestingJoint.transform.position - CompareJoint.transform.position).magnitude;
                    Debug.Log("found new smallest dist : " + SmallestDistance);
                    TestingJoint.SetClosestsJoint(CompareJoint);
                }
            }
        }



    } //end try connect

    public Staircase SpawnStairCase()
    {
        int Rand = UnityEngine.Random.Range(0, FinalRooms.Count);
        Debug.Log("using final room count: " + FinalRooms.Count);
        RoomPosition P = FinalRooms[Rand].GetStaircasePoint();
        if (P != null)
        {
            Staircase S = Instantiate(TheStaircase, P.transform.position, Quaternion.identity);
            S.transform.SetParent(TheMap.transform);
            return S;
        }
        Debug.Log("error, cannot find stair pos, returning null staircase");
        return null;
        
    }

    private void ResetGenerator()
    {
        AllRooms.Clear();
        FinalRooms.Clear();
        PreviousRooms.Clear();
        RoomLists.Clear();
        ListQue.Clear();
    }


    IEnumerator StepStart(Action callback)
    {
        for (int i = 0; i < Waves; i++)
        {
            //Debug.Log("starting spawn wave: " + i +  " with wave count : " + Waves);
            yield return StartCoroutine(SpawnRoomAtDoor(ListQue.Dequeue()));

        }

        //Debug.Log("starting end rooms");
        yield return StartCoroutine(SpawnRoomAtDoor(EndRoomList));

        TheMap.SetRooms(AllRooms);


        FinalizeDungeon();

        if(callback != null) callback();

        yield break;
    }

    IEnumerator SpawnRoomAtDoor(ScriptableList RoomList)
    {
        //Debug.Log("STARTING, copying prev rooms" + PreviousRooms.Count);
    
        List<Joint> JointsFromPrevious = new List<Joint>();
        List<RoomPiece> PreviousRoomsCopy = new List<RoomPiece>(PreviousRooms);

        //i clear this so that at the end, the previousrooms will be correct
        PreviousRooms.Clear();

        //for each room that was placed, get all of their open joints and put them into a list
        foreach (RoomPiece R in PreviousRoomsCopy) //creates a list of all unattatched joints from the previous rooms
        {
            foreach (Joint J in R.GetAllOpenJoints())
            {
                if (J == null) continue; //corridors only have 2 joints so skip the null ones
                JointsFromPrevious.Add(J);
                J.name = JointsFromPrevious.Count.ToString();
            }
        }
      
        foreach(Joint Jnt in JointsFromPrevious)
        {
            //RoomPiece R1 = RoomList.GetRandomRoom();
            //Debug.Log("starting step spawn with a list: " + RoomList.GetRoomListByOppositeOrientation(Jnt.JointOrientation).Count + " and with joints from prev: " + JointsFromPrevious.Count);
            yield return StartCoroutine(StepSpawn(RoomList.GetRoomListByOppositeOrientation(Jnt.JointOrientation), Jnt, RoomList));
            //need a random list that i can remove from
        }

        //Debug.Log("finishing spawnroomatdoor");
        FinalRooms = new List<RoomPiece>(PreviousRooms);
        yield return null;
    }

    
    IEnumerator StepSpawn(List<RoomPiece> RoomList, Joint JointTarget, ScriptableList TheRoomList)
    {
 
        List<RoomPiece> List = new List<RoomPiece>(RoomList);
        if(List.Count <= 0)
        {
            Debug.Log("OUT OF ROOMS could not find a room for this joint " + JointTarget.JointOrientation+ " "  + JointTarget.transform.position);

            yield break;
        }
        int RoomIndex = UnityEngine.Random.Range(0, List.Count);
        RoomPiece SpawningRoom1 = List[RoomIndex]; //get a random room

        RoomPiece SpawningRoom = Instantiate(SpawningRoom1, new Vector3(-500, -500, 0), Quaternion.identity);
        SpawningRoom.FillJointLookUp();
        yield return new WaitForEndOfFrame();
        //yield return new WaitForSeconds(1);
        //now that we have a room we need to get a joint from it
        //we need a joint that has the opposite orientation from the target joint
        Joint RoomJoint = SpawningRoom.GetJointByOppositeOrientation(JointTarget.JointOrientation);

        if (RoomJoint == null) Debug.LogError("cannot find joint in step spwan");

        GameObject ParentMover1 = Instantiate(gameObject, new Vector3(0, 0, 0), Quaternion.identity);

        yield return new WaitForEndOfFrame();
        ParentMover1.name = "P1";

        //Debug.Log("moving room");

        yield return MoveRoom(SpawningRoom, ParentMover1.transform, RoomJoint, JointTarget);

        //check for overlap
        //Debug.Log("overlap checking");
        yield return new WaitForEndOfFrame();
        SpawningRoom.ResetOverlap();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(0.15f);
        SpawningRoom.ActivateRoom();
        //yield return new WaitForSeconds(1);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(0.15f);
        //Debug.Log("checking room for admitance");
        //if there is overlap (aka, room doesnt fit) and there are still rooms in the room list
        if (SpawningRoom.Overlap() && List.Count > 0)
        {
            //try a new room after removing 
            FailFitCounter++;
            //Debug.Log("Rejecting room: " + SpawningRoom.name + " trying new room, " + (List.Count -1) + " left to try");
            Destroy(SpawningRoom.gameObject);
            Destroy(ParentMover1.gameObject);

            //if the failed attempts is like 3 or something and the room is <some size>, then skip those kinds of rooms

            List.RemoveAt(RoomIndex);

            //if there are too many non fitting rooms, replace the list with small rooms and only try small rooms
            if (FailFitCounter > 10)
            {
                Debug.Log("FAIL FIT TRIGGERED");
                yield return new WaitForEndOfFrame();
                yield break;
                //yield return new WaitForSeconds(2);
                //FailFitCounter = 0;
                //RelistFlag = true;
                //List = TheRoomList.GetSmallRooms(RoomJoint.JointOrientation);
            }

            yield return new WaitForEndOfFrame();
            if (List.Count <= 0) yield break; //out of rooms to try, skip joint

            yield return StepSpawn(List, JointTarget, TheRoomList); //call this coroutine again but with the unfit room removed from the list
            yield break;
        }
        //else no overlap, it fits

        //Debug.Log("accepting room " + SpawningRoom.name);

        FailFitCounter = 0;
        //RelistFlag = false;
        JointTarget.IsAttached = true;
        RoomJoint.IsAttached = true;
        AllRooms.Add(SpawningRoom);
        SpawningRoom.transform.SetParent(TheMap.gameObject.transform);
        PreviousRooms.Add(SpawningRoom);
        Destroy(ParentMover1.gameObject);    

        yield break;
    }



    IEnumerator MoveRoom(RoomPiece RoomToMove, Transform Mover, Joint JointForMover, Joint JointToConnectTo)
    {
        Mover.transform.DetachChildren();
        yield return new WaitForEndOfFrame();
        RoomToMove.transform.parent = null;
        //yield return new WaitForSeconds(0.1f);
        yield return new WaitForEndOfFrame();
        Mover.transform.position = JointForMover.transform.position;
        yield return new WaitForEndOfFrame();
        //RoomToMove.transform.parent = Mover;
        RoomToMove.transform.SetParent(Mover);
        yield return new WaitForEndOfFrame();
        Mover.transform.position = JointToConnectTo.transform.position;
        yield return new WaitForEndOfFrame();
        RoomToMove.transform.parent = null;
        yield return new WaitForEndOfFrame();
        //Debug.Log("finished moving");
        yield break;
    }

}
