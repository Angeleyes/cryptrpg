﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RoomList")]
public class ScriptableList : ScriptableObject {



    public List<RoomPiece> Rooms = new List<RoomPiece>();
    public List<RoomPiece> StartRooms = new List<RoomPiece>();


    internal List<RoomPiece> TopJointRooms = new List<RoomPiece>();
    internal List<RoomPiece> BottomJointRooms = new List<RoomPiece>();
    internal List<RoomPiece> LeftJointRooms = new List<RoomPiece>();
    internal List<RoomPiece> RightJointRooms = new List<RoomPiece>();

    public List<RoomPiece> GetRoomListByOppositeOrientation(Joint.OrientationEnum O)
    {
        //Debug.Log("returing opposite list of: " + O);

        if (O == Joint.OrientationEnum.Down) return TopJointRooms;
        else if (O == Joint.OrientationEnum.Up) return BottomJointRooms;
        else if (O == Joint.OrientationEnum.Left)
        {
            //Debug.Log("returning right list");
            return RightJointRooms;
        }
        else if (O == Joint.OrientationEnum.Right) return LeftJointRooms;

        else
        {
            //Debug.LogError("could not find room list for giving orietation in scriptable list");
            return new List<RoomPiece>();
        }
    }


    public RoomPiece GetStartRoom()
    {
        if (StartRooms.Count <= 0)
        {
            Debug.LogError("start rooms list is empty");
            return null;
        }
        return StartRooms[Random.Range(0, StartRooms.Count)];
    }


    public void OrganizeRooms()
    {
        TopJointRooms.Clear();
        BottomJointRooms.Clear();
        LeftJointRooms.Clear();
        RightJointRooms.Clear();

        //Debug.Log("organizing rooms with count: " + Rooms.Count);

        if(StartRooms.Count > 0)
        {
            foreach(RoomPiece R in StartRooms)
            {
                R.FillJointLookUp();

                if (R.GetJointByOrientation(Joint.OrientationEnum.Up) != null) TopJointRooms.Add(R);

                if (R.GetJointByOrientation(Joint.OrientationEnum.Down) != null) BottomJointRooms.Add(R);

                if (R.GetJointByOrientation(Joint.OrientationEnum.Left) != null) LeftJointRooms.Add(R);

                if (R.GetJointByOrientation(Joint.OrientationEnum.Right) != null) RightJointRooms.Add(R);

            }
        }

        foreach(RoomPiece R in Rooms)
        {

            R.FillJointLookUp();

            if (R.GetJointByOrientation(Joint.OrientationEnum.Up) != null) TopJointRooms.Add(R);

            if (R.GetJointByOrientation(Joint.OrientationEnum.Down) != null) BottomJointRooms.Add(R);

            if (R.GetJointByOrientation(Joint.OrientationEnum.Left) != null) LeftJointRooms.Add(R);

            if (R.GetJointByOrientation(Joint.OrientationEnum.Right) != null) RightJointRooms.Add(R);
        }

        //Debug.Log("finishg organizing room lists with start room count: " + StartRooms.Count);
    }


    public List<RoomPiece> GetSmallRooms(Joint.OrientationEnum O)
    {
        List<RoomPiece> NewSmallList = new List<RoomPiece>();
        List<RoomPiece> SmallList = new List<RoomPiece>();

        switch (O)
        {
            case Joint.OrientationEnum.Down: NewSmallList = BottomJointRooms;
                break;
            case Joint.OrientationEnum.Up: NewSmallList = TopJointRooms;
                break;
            case Joint.OrientationEnum.Right: NewSmallList = RightJointRooms;
                break;
            case Joint.OrientationEnum.Left: NewSmallList = LeftJointRooms;
                break;
            default: Debug.Log("Cannot find room to match with joint orientation in getsmallrooms");
                break;
        }
        

        foreach(RoomPiece R in NewSmallList)
        {
            if (R.RoomSize == RoomPiece.RoomType.Small) SmallList.Add(R);

        }


        return SmallList;
    }



}
