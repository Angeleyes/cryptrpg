﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Staircase : MonoBehaviour {

    //maybe all of this countdown canvas stuff should be handled by gamemanager???
    public Canvas CountdownCanvas;
    internal int PlayerCount = 0;
    internal Text CountdownText;
    internal float CountdownTime = 30;
    internal int TotalPlayerCount; //needs to retroactivly update this....
    internal Action LevelTransition;
    internal List<int> CharIds = new List<int>();
    internal int TriggerCount = 0;

    //public delegate int PlayerCountUpdate();
    //internal PlayerCountUpdate GetPlayerCount;

    internal IEnumerator Counter;
    internal IEnumerator TriggerStaircase;

    private void Start()
    {
        //Debug.Log("staircase has been spawned at " + transform.position);
        CountdownCanvas = GetComponentInChildren<Canvas>();
        CountdownText = CountdownCanvas.GetComponentInChildren<Text>();
        Counter = DisplayCountdown();
        CountdownCanvas.gameObject.SetActive(false);

    }

    public void SetupStaircase(IEnumerator Transition, int Playercount)
    {
        TriggerStaircase = Transition;
        Debug.Log("staircase has been setup with player count: " + Playercount);
        PlayerCount = Playercount;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<PlayerEntity>() != null)
        {
            if (!CharIds.Contains(collision.gameObject.GetInstanceID()))
            {
                CharIds.Add(collision.gameObject.GetInstanceID());
                TriggerCount++;

                //TotalPlayerCount = GetPlayerCount();
                Debug.Log("adding player with trigger count: " + TriggerCount + " and player count " + PlayerCount);
                if (TriggerCount >= PlayerCount)
                {
                    StopCoroutine(Counter);
                    StartCoroutine(TriggerStaircase);
                    //LevelTransition();
                }
                else if (PlayerCount > 0) StartCoroutine(Counter);
            }
            else Debug.Log("already counted this player");
            //Debug.Log("player counts: " + PlayerCount + " with nm count: " + GetPlayerCount());
        }
    }



    IEnumerator DisplayCountdown()
    {
        CountdownCanvas.gameObject.SetActive(true);
        string Message = " ";
        Debug.Log("countdown begun");
        while(true)
        {
            if(CountdownTime <= 0)
            {
                if (TriggerStaircase != null) StartCoroutine(TriggerStaircase);
                CountdownCanvas.gameObject.SetActive(false);
                CountdownTime = 30;
                yield break;
            }
            CountdownTime -= Time.deltaTime;
            Message = "Staircase has been found, waiting for other players: " + "(" + TriggerCount + "/" + PlayerCount + ")" + "\n" + (int)CountdownTime + " seconds remain...";
            CountdownText.text = Message;
            yield return null;
        }
    }
}
