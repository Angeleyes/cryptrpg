﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System;

public class RoomPiece : MonoBehaviour {


    //formatted to be left right top bot
    //public Joint JointRef;

    public enum RoomType
    {
        Large = 1,
        Medium = 2,
        Small = 3,
        HorizontalCorridor = 4,
        VerticalCorridor = 5,
        FractleCorridor = 6,
    }

    public RoomType RoomSize;

    public ScriptableTileMap TileSprites;
    private BoxCollider2D OverlapBox;
    public Tilemap DoorLayerMap;
    public Tilemap FloorLayerMap;

    public List<Joint> OpenRoomJoints = new List<Joint>();
    public List<Joint> AllJoints = new List<Joint>();
    private Dictionary<Joint.OrientationEnum, Joint> JointLookUp = new Dictionary<Joint.OrientationEnum, Joint>();
    private bool IsOverlapping = false;
    internal Rigidbody2D RoomBody;

    protected Tile DoorBlockerB;
    protected Tile DoorBlockerT;
    protected Tile DoorBlockerSideL;
    protected Tile DoorBlockerSideR;

    public List<Joint> NormalJoints = new List<Joint>();
    public List<Joint> FractleJoints = new List<Joint>();


    private List<PlayerEntity> Players = new List<PlayerEntity>();
    internal List<RoomPosition> RoomPositionList = new List<RoomPosition>();
    internal List<WorldTooltip> RoomTips = new List<WorldTooltip>();

    public Action SpawnRoomMonsters;
    public Action<MonsterConfig>LoadMonsterConfigs;

    public virtual void Start()
    {
        //Debug.Log("starting room piece");
        OverlapBox = GetComponentInChildren<BoxCollider2D>();

        RoomBody = GetComponent<Rigidbody2D>();

        if (TileSprites == null) Debug.Log("TILE MAP IS NULL, YOU MUST MANUALLY ASSIGN");


        if (DoorLayerMap == null || FloorLayerMap == null)
        {
            Tilemap[] Maps = GetComponentsInChildren<Tilemap>();
            FloorLayerMap = Maps[0];
            DoorLayerMap = Maps[1];
        }


        if (RoomBody == null)
        {
            Debug.Log("ROOM BODY IS NULL");
            RoomBody = this.gameObject.AddComponent<Rigidbody2D>();
            RoomBody.freezeRotation = true;
            RoomBody.constraints = RigidbodyConstraints2D.FreezeAll;
        }

        RoomBody.simulated = true;

        foreach(RoomPosition Rp in GetComponentsInChildren<RoomPosition>())
        {
            RoomPositionList.Add(Rp);
        }

        foreach(WorldTooltip W in GetComponentsInChildren<WorldTooltip>())
        {

            RoomTips.Add(W);
        }

    } //end of start

    public void SetRoomMaterial(Material M)
    {
        foreach (TilemapRenderer T in this.GetComponentsInChildren<TilemapRenderer>())
        {
            if (T.name == "DoodadLayer" || T.name == "TopLayerMap") continue;
            else T.material = M;
        }
    }

    public void SetStatic()
    {
        this.gameObject.isStatic = true;
    
    }

    public List<WorldTooltip> GetRoomTips()
    {
        return RoomTips;
    }


    public RoomPosition GetStaircasePoint()
    {
        foreach(RoomPosition P in RoomPositionList)
        {
            if (P.StaircasePoint) return P;
        }

        return null;
    }

    private RoomPosition GetUnusedPosition()
    {
        foreach(RoomPosition P in RoomPositionList)
        {
            if (!P.InUse) return P;
        }
        return null;
    }

    public int GetNumJoints()
    {
        return AllJoints.Count;
    }

    public virtual Joint GetJointByOrientation(Joint.OrientationEnum O)
    {
        Joint J;
        JointLookUp.TryGetValue(O, out J);
        //Debug.Log("found joint? " + JointLookUp.ContainsKey(O));
        //if (J == null) Debug.Log(this.name + " returning null joint from getjointbyorientation, could not find: " + O);
        return J;
    }

    public virtual void FillJointLookUp()
    {
        //if a room is in the list twice it breaks
        if (JointLookUp.Count > 0)
        {
            //Debug.Log("This room: " + this.name + " already has its lookup filled, is the room in a room list twice?  Or is this room a starter AND a normal room");
            return;
        }

        foreach(Joint J in AllJoints)
        {
            //doesnt work for L rooms cuz they can have multiple of the same joints
            JointLookUp.Add(J.JointOrientation, J);
        }

        //Debug.Log("lookup count: " + JointLookUp.Count);
    }

    public Joint GetRandomJoint()
    {
        Joint RandomJoint = null;
        foreach(Joint J in OpenRoomJoints)
        {
            int Rand = UnityEngine.Random.Range(0, OpenRoomJoints.Count);
            RandomJoint = OpenRoomJoints[Rand];
            if (RandomJoint.IsAttached)
            {
                OpenRoomJoints.RemoveAt(Rand);
                continue;
            }
            else break;
        }
        if (RandomJoint == null) Debug.Log("returning null ranomd joint");
        return RandomJoint;
    }

    public virtual void TurnOffOverlapBox()
    {
        OverlapBox.enabled = false;
    }

    public List<Joint> GetAllOpenJoints()
    {
        List<Joint> List = new List<Joint>();
        for (int i = 0; i < AllJoints.Count; i++)
        {
            if (AllJoints[i].IsAttached) continue;
            else List.Add(AllJoints[i]);
        }
        return List;
    }

    public virtual Joint GetJointByOppositeOrientation(Joint.OrientationEnum O)
    {

        return JointLookUp[GetOppositeOrientation(O)];
    }

    protected Joint.OrientationEnum GetOppositeOrientation(Joint.OrientationEnum O)
    {
        switch (O)
        {
            case Joint.OrientationEnum.Down: return Joint.OrientationEnum.Up;
            case Joint.OrientationEnum.Up: return Joint.OrientationEnum.Down;
            case Joint.OrientationEnum.Left: return Joint.OrientationEnum.Right;
            case Joint.OrientationEnum.Right: return Joint.OrientationEnum.Left;
            default: Debug.Log("cannot find opposite orientation");
                break;
        }

        return Joint.OrientationEnum.Up;

    }

    public List<Joint> AllOpenJoints()
    {
        List<Joint> OpenJoints = new List<Joint>();
        foreach(Joint J in OpenRoomJoints)
        {
            if (!J.IsAttached) OpenJoints.Add(J);
        }

        return OpenJoints;
    }

    public int GetLookUpCount()
    {
        return JointLookUp.Count;
    }

    public void ResetOverlap()
    {
        this.gameObject.SetActive(false);
        IsOverlapping = false;
    }

    public void ActivateRoom()
    {
        this.gameObject.SetActive(true);
    }

    public bool Overlap()
    {
        return IsOverlapping;
    }

    public void SpawnJoints(Joint JointReference, List<Joint> Jlist, List<Joint> fractleJoints)
    {
        //List<Joint> Jlist2 = new List<Joint>(Jlist);
        Joint K = new Joint();

        AllJoints.Clear();

        foreach(Joint J in Jlist)
        {
            K = Instantiate(JointReference, J.JointPosition, Quaternion.identity);
            K.InitParent();
            K.JointOrientation = J.JointOrientation;
            K.DoorCoords = J.DoorCoords;
            K.JointPosition = J.JointPosition;
            K.transform.SetParent(J.JParent);
            K.IsFractleJoint = false;
            NormalJoints.Add(K);
            AllJoints.Add(K);
            OpenRoomJoints.Add(K);
        }

        foreach(Joint J in fractleJoints)
        {
            K = Instantiate(JointReference, J.JointPosition, Quaternion.identity);
            K.InitParent();
            K.JointOrientation = J.JointOrientation;
            K.DoorCoords = J.DoorCoords;
            K.JointPosition = J.JointPosition;
            K.transform.SetParent(J.JParent);
            K.IsFractleJoint = true;
            AllJoints.Add(K);
            OpenRoomJoints.Add(K);
            FractleJoints.Add(K);
        }

    }



    //public int x;
    //public int y;
    //public int z;
    public void SetDumbTile()
    {
        //Tile T = new Tile();
        //T.sprite = TileSprites.EmptyTile;
        //DoorLayerMap.SetTile(new Vector3Int(x, y, z), null);
        Debug.Log("refreshing tiles");
        foreach(Tilemap M in GetComponentsInChildren<Tilemap>())
        {
            Debug.Log("refreshing " + M.name);
            M.RefreshAllTiles();
        }
    }


    public void InitRoom(BoxCollider2D Box, ScriptableTileMap Map, RoomType Rtype)
    {
        TileSprites = Map;
        OverlapBox = Box;
        OverlapBox.size = new Vector2(OverlapBox.size.x - 5.5f, OverlapBox.size.y - 5.5f);
        OverlapBox.isTrigger = true;
        RoomSize = Rtype;
    }

    protected void SetBlockerSprites()
    {
        DoorBlockerT = TileSprites.TopWallPiece;
        DoorBlockerB = TileSprites.BotWallPiece;
        DoorBlockerSideL = TileSprites.SideWallTileL;
        DoorBlockerSideR = TileSprites.SideWallTileR;
    }

    public virtual void BlockOpenJoints()
    {
        Debug.Log("starting to block joints");
        Tile T = new Tile();

        SetBlockerSprites();

        if(NormalJoints.Count <= 0)
        {
            foreach(Joint J in AllJoints)
            {
                if (!J.IsFractleJoint) NormalJoints.Add(J);
            }
        }

        foreach(Joint J in NormalJoints)
        {
            
            if(!J.IsAttached)
            {
                //Debug.Log("unattachted joint found: " + J.gameObject.name + " pos: " + J.transform.position);
                J.ToggleBlockerCollision(true);

                //calculate the door coords if they are not zero
                //if they are zero, that means they were manually set
                if(J.DoorCoords != Vector3Int.zero) J.CalculateDoorCoords();

                if(J.JointOrientation == Joint.OrientationEnum.Down || J.JointOrientation == Joint.OrientationEnum.Up)
                {
                    T = DoorBlockerT;
                    //Debug.Log("setting normal tile at " + J.DoorPart1 + " and " + J.DoorPart2);
                    DoorLayerMap.SetTile(J.DoorPart1, null);
                    DoorLayerMap.SetTile(J.DoorPart2, null);

                    Vector3Int ZFix = new Vector3Int(J.DoorPart1.x, J.DoorPart1.y, -1);
                    Vector3Int ZFix2 = new Vector3Int(J.DoorPart2.x, J.DoorPart2.y, -1);

                    DoorLayerMap.SetTile(J.DoorPart1, T);
                    DoorLayerMap.SetTile(J.DoorPart2, T);

                    T = DoorBlockerB;

                    FloorLayerMap.SetTile(J.DoorPart1, T);
                    FloorLayerMap.SetTile(J.DoorPart2, T);

                    FloorLayerMap.SetTile(ZFix, T);
                    FloorLayerMap.SetTile(ZFix2, T);
                }
                else
                {
                    if(J.JointOrientation == Joint.OrientationEnum.Left)
                    {
                        T = DoorBlockerSideR;
                        DoorLayerMap.SetTile(J.DoorPart1, null);
                        DoorLayerMap.SetTile(J.DoorPart2, null);
                        FloorLayerMap.SetTile(J.DoorPart1, null);
                        FloorLayerMap.SetTile(J.DoorPart2, null);
                        DoorLayerMap.SetTile(J.DoorPart1, T);
                        DoorLayerMap.SetTile(J.DoorPart2, T);
                        J.ToggleSideDoorCollision(false);
                    }
                    else if(J.JointOrientation == Joint.OrientationEnum.Right)
                    {
                        T = DoorBlockerSideL;
                        DoorLayerMap.SetTile(J.DoorPart1, null);
                        DoorLayerMap.SetTile(J.DoorPart2, null);
                        FloorLayerMap.SetTile(J.DoorPart1, null);
                        FloorLayerMap.SetTile(J.DoorPart2, null);
                        DoorLayerMap.SetTile(J.DoorPart1, T);
                        DoorLayerMap.SetTile(J.DoorPart2, T);
                        J.ToggleSideDoorCollision(false);
                    }
                }
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "GroundLevel" && collision.gameObject.layer == 9)
        {
            //Debug.Log(this.name + " is overlapping with " + collision.name);
            //collision.Distance(OverlapBox);
            IsOverlapping = true;
        }
    }

}
