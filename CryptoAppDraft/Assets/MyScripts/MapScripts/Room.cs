﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class Room : MonoBehaviour {


//    public enum RoomEnum
//    {
//        BigRoom,
//        MediumRoom,
//        SmallRoom,
//        SmallCorridor,
//        LongCorridor,
//        LCorridor
//    }

//    public RoomEnum RoomType;

//    public Room RoomCounterpart;
//    internal List<Vector3> PositionList = new List<Vector3>();
//    internal Queue<Joint> JointQue = new Queue<Joint>();
//    //public Dictionary<Transform, bool> Joints; //for checking if already connected
//    internal int RotateCount = 0;

//    internal List<Joint> JointsList = new List<Joint>();
//    public Joint JointRef;

//    public bool IsOverlapping = false;

//    //public int MinNumJoints = 1;

//    internal int JointCounter = 0;
//    internal int JointCounter2 = 0;

//    public Joint CenterJoint;
//    private Vector3 CenterPosition;
//    internal GameObject WallsLayer;

//    public bool RoomInitialize = false;
//    internal BoxCollider2D OverlapBox;
//    internal PolygonCollider2D LOverlapBox;
//    internal List<Tiled2Unity.Layer> RoomLayers = new List<Tiled2Unity.Layer>();
//    internal List<Tiled2Unity.Layer> Arches = new List<Tiled2Unity.Layer>() { null, null, null, null };
//    internal List<PolygonCollider2D> WallCollisionList = new List<PolygonCollider2D>();
//    internal List<Joint> OpenJoints = new List<Joint>();
//    internal BoxCollider2D GroundBox;

//    public List<RoomPosition> RoomPositions = new List<RoomPosition>();

//    public Tiled2Unity.Layer TopWalls;
//    public Tiled2Unity.Layer BottomWalls;

//    public void Awake()
//    {

//        if (RoomInitialize) SetUpRoom();
//        else
//        {
//            //testing just using normal collision as overlap
//            //else OverlapBox = GetComponent<BoxCollider2D>();

//            FillArchesList();

//            CheckJointList();
//            //the joint blocker and box collider get wiped
//            FillWallCollisionList();
//            SetLayerOrder();

//            ToggleWallCollision(false);
//        }

//    }


//    public void FillArchesList()
//    {
//        foreach (Tiled2Unity.Layer L in GetComponentsInChildren<Tiled2Unity.Layer>())
//        {
//            if (L.tag == "Doors")
//            {
//                int index = 0;
//                if (L.GetComponentInChildren<BoxCollider2D>().gameObject.tag == "DoorTop") index = 0;
//                else if (L.GetComponentInChildren<BoxCollider2D>().gameObject.tag == "DoorRight") index = 1;
//                else if (L.GetComponentInChildren<BoxCollider2D>().gameObject.tag == "DoorBottom") index = 2;
//                else if (L.GetComponentInChildren<BoxCollider2D>().gameObject.tag == "DoorLeft") index = 3;

//                L.gameObject.transform.position = new Vector3(L.gameObject.transform.position.x, L.gameObject.transform.position.y, -1);
//                Arches.RemoveAt(index);
//                Arches.Insert(index, L);

//                continue;
//            }
//            else if (L.tag == "GroundLevel") //this is for getting center joint
//            {
//                //this code is not needed but will fix later on refactor
//                GroundBox = L.GetComponentInChildren<BoxCollider2D>();
//                CenterPosition = GroundBox.bounds.center;
//                GroundBox.isTrigger = true;
//                if (OverlapBox == null) OverlapBox = GroundBox.gameObject.GetComponent<BoxCollider2D>();

//                if(this.RoomType == RoomEnum.LCorridor)
//                {
//                    LOverlapBox = L.GetComponentInChildren<PolygonCollider2D>();
//                    LOverlapBox.isTrigger = true;
//                }

//                //if (GroundBox == null) Debug.Log("ground box is null in line 87");
//            }
//            else RoomLayers.Add(L);
//        }
//    }

//    private void SpawnCenterJoint()
//    {
//        CenterJoint = Instantiate(JointRef, CenterPosition, Quaternion.identity);
//        CenterJoint.transform.parent = this.transform;
//        CenterJoint.name = "Center";
//    }

//    public void CheckJointList()
//    {

//        if (JointsList.Count > 0) return;
//        else
//        {
//            JointQue.Clear();
//            foreach (Joint J in GetComponentsInChildren<Joint>())
//            {
//                if (J.gameObject.name == "Center") continue;
//                JointsList.Add(J);
//                JointQue.Enqueue(J);

//                //J.AssignJointBlockers(GetBlockedArch(ArchName));
//            }
//        }
//    }

//    public RequirementsInteractable GetRandomOpenDoor()
//    {
//        RequirementsInteractable Door = null;
//        Joint K = null;
//        foreach(Joint J in JointsList)
//        {
//            K = JointsList[Random.Range(0, JointsList.Count)];
//            if (!K.IsAttached) continue;
//            else break;
//        }

//        Door = K.AttatchedDoor.GetComponentInChildren<RequirementsInteractable>();

//        return Door;
//    }

//    public void ResetRoom()
//    {
//        if (JointsList.Count > 0)
//        {
//            foreach (Joint J in JointsList)
//            {
//                Destroy(J.gameObject);
//            }
//        }

//        JointsList.Clear();
//        OpenJoints.Clear();
//        //Arches.Clear();
//        //WallCollisionList.Clear();
//        //RoomLayers.Clear();
//        JointQue.Clear();
//        //Waypoints.Clear();
//    }

//    public Joint GetJointByDoor(string doortag)
//    {
//        foreach (Joint J in JointsList)
//        {
//            if (J.AttatchedDoor.tag == doortag) return J;
//        }

//        return null;
//    }

//    public Joint GetOppositeJoint(Joint J)
//    {

//        if (J.AttatchedDoor.tag == "DoorTop") return GetJointByDoor("DoorBottom");
//        else if (J.AttatchedDoor.tag == "DoorBottom") return GetJointByDoor("DoorTop");
//        else if (J.AttatchedDoor.tag == "DoorLeft") return GetJointByDoor("DoorRight");
//        else if (J.AttatchedDoor.tag == "DoorRight") return GetJointByDoor("DoorLeft");
//        else return null;

//    }

//    public int CountAllJoints()
//    {
//        int counter = 0;
//        foreach(Joint J in GetComponentsInChildren<Joint>())
//        {
//            counter++;
//            J.name = J.name + "Counted";
//        }

//        return counter;
//    }

//    private void KillExtraJoints()
//    {
//        foreach(Joint J in GetComponentsInChildren<Joint>())
//        {
//            if (J.name == "Center") continue;
//            else Destroy(J.gameObject);
//        }
//    }


//    //requires Arches list to be full and 
//    private void SpawnJoints()
//    {

//        foreach (Tiled2Unity.Layer L in Arches)
//        {
//            if (L == null) continue;

//            Joint I = PositionJoint(L);

//            //Debug.Log(Box.gameObject.tag + " " + Pos); //new BUG 4/6/2018, spawning joints a 2nd time puts them in the totally wrong position
//            //if(!PositionList.Contains(Pos)) PositionList.Add(Pos);
//            if (!PositionList.Contains(I.JointPosition)) PositionList.Add(I.JointPosition);
//            else
//            {
//                //the issue here is that even if the joint has already been spawned, it doesnt check if it already contains the joint properly
//                //it has a new instance id
//                Debug.Log("already contains this position" + I.JointPosition);
//            }

//            Joint J = Instantiate(JointRef, I.JointPosition, Quaternion.identity);
//            J.transform.parent = this.transform;

//            //check if this works for init
//            J.JointOrientation = I.JointOrientation;
//            J.AttatchedDoor = I.AttatchedDoor;
//            //J.AssignJointBlockers(GetBlockedArch(L.gameObject.name)); //this is kind of clunky, but works for now

//            if (J.transform.position != I.JointPosition)
//            {
//                Debug.Log("joint pos not equal..");
//                J.transform.position = I.JointPosition;
//            }

//            JointsList.Add(J);
//            JointQue.Enqueue(J);
//            //end check
            

//        } // end of foreach

//        SpawnCenterJoint();
//    }//end of spawn joints

//    private void SetLayerOrder()
//    {
//        foreach (PolygonCollider2D P in WallCollisionList)
//        {
//            if (P.enabled == true) P.gameObject.layer = 21; //sets to unwalkable
//        }

//        foreach (Tiled2Unity.Layer L in Arches)
//        {
//            if (L == null) continue;
//            L.gameObject.transform.position = new Vector3(L.gameObject.transform.position.x, L.gameObject.transform.position.y, -1.1f);
//        }

//        foreach (Tiled2Unity.Layer L in RoomLayers)
//        {
//            if (L.name == "CornerSideWalls") L.gameObject.transform.position = new Vector3(L.gameObject.transform.position.x, L.gameObject.transform.position.y, -1f);
//            //else if (L.name == "TopWalls") L.gameObject.layer = 1;
//            //else if (L.name == "BottomWalls") L.gameObject.layer = 1;
//        }

//        if(this.RoomType == RoomEnum.LCorridor || this.RoomType == RoomEnum.LongCorridor || this.RoomType == RoomEnum.SmallCorridor)
//        {
//            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0.5f);
//        }

//    }

//    private Joint PositionJoint(Tiled2Unity.Layer L)
//    {
//        BoxCollider2D Box;
//        Joint J = new Joint();

//        //do a continue check in the foreach for the layers
//        Box = L.GetComponentInChildren<BoxCollider2D>();
//        Vector3 Pos = Box.bounds.center;
//        Pos.z = 0;

//        Joint.OrientationEnum O = Joint.OrientationEnum.Up;

//        switch (Box.gameObject.tag)
//        {
//            case "DoorTop":
//                {
//                    Pos.y = Box.bounds.max.y;
//                    O = Joint.OrientationEnum.Up;
//                }
//                break;
//            case "DoorBottom":
//                {
//                    Pos.y = Box.bounds.max.y; //changed to max y instead of min y for this new method/new rooms
//                    O = Joint.OrientationEnum.Down;
//                }
//                break;
//            case "DoorLeft":
//                {
//                    Pos.x = Box.bounds.min.x;
//                    O = Joint.OrientationEnum.Left;
//                }
//                break;
//            case "DoorRight":
//                {
//                    Pos.x = Box.bounds.max.x;
//                    O = Joint.OrientationEnum.Right;
//                }
//                break;
//            default:
//                Debug.Log("could not identify door type, make sure you tag the door");
//                break;
//        }

//        //Debug.Log("fixing joint pos to:" + Pos);

//        J.JointPosition = Pos;
//        J.JointOrientation = O;
//        J.AttatchedDoor = Box;

//        return J;
//    }

//    public Joint.OrientationEnum ReOrientJoint(Joint J)
//    {
//        Joint.OrientationEnum O = Joint.OrientationEnum.Up;
//        if (J.AttatchedDoor.gameObject.tag == "DoorTop") O = Joint.OrientationEnum.Up;
//        else if (J.AttatchedDoor.gameObject.tag == "DoorBottom") O = Joint.OrientationEnum.Down;
//        else if (J.AttatchedDoor.gameObject.tag == "DoorRight") O = Joint.OrientationEnum.Right;
//        else if (J.AttatchedDoor.gameObject.tag == "DoorLeft") O = Joint.OrientationEnum.Left;


//        return O;
//    }



//    public void LabelDoors()
//    {
//        //top, right, bottom, left at 0
//        //bottom, left, top, right at 180 and -180 // shift 2 right
//        //left, top, right, bottom at 90 //shift 1 right
//        //right, bottom, left, top at -90 //shfit 1 left 

//        //shift value is negative to go right, pos to go left

//        //int index = (index - shiftvalue)%List.count

//        //need to say to each room, if room.z is this or that, then each doors joints is this or that orientation

//        List<string> Orientations0 = new List<string>() { "DoorTop", "DoorRight", "DoorBottom", "DoorLeft" };
//        List<string> Orientations180 = new List<string>() { "DoorBottom", "DoorLeft", "DoorTop", "DoorRight" };
//        List<string> Orientations90 = new List<string>() { "DoorLeft", "DoorTop", "DoorRight", "DoorBottom" };
//        List<string> Orientationsn90 = new List<string>() { "DoorRight", "DoorBottom", "DoorLeft", "DoorTop" };
//        Dictionary<float, List<string>> NewOrientations = new Dictionary<float, List<string>>();


//        NewOrientations.Add(180f, Orientations180);
//        NewOrientations.Add(90f, Orientations90);
//        NewOrientations.Add(270f, Orientationsn90); //this is actually 270, changed from -90
//        NewOrientations.Add(0f, Orientations0);

//        float Zrot = transform.eulerAngles.z;

//        BoxCollider2D Box;
//        for (int i = 0; i < Arches.Count; i++)
//        {
//            if (Arches[i] == null) continue;
//            Box = Arches[i].GetComponentInChildren<BoxCollider2D>();
//            try
//            {
//                Box.gameObject.tag = NewOrientations[Zrot][i];
//            }
//            catch (System.Exception e) { Debug.Log("tried to look up rot: " + Zrot + " but..." + e.Message); }

//        }

//    }


//    public void FixJointOffset()
//    {
//        foreach (Tiled2Unity.Layer L in Arches)
//        {
//            if (L == null) continue;
//            OpenJoints.Add(PositionJoint(L));
//        }

//    }


//    private void FillWallCollisionList()
//    {
//        if (WallCollisionList.Count > 0) return;
//        foreach (Tiled2Unity.Layer L in RoomLayers)
//        {
//            if (L == null) continue;
//            PolygonCollider2D P = L.GetComponentInChildren<PolygonCollider2D>();
//            if (P != null) WallCollisionList.Add(P);
//        }

//        foreach (Tiled2Unity.Layer A in Arches)
//        {
//            if (A == null) continue;
//            PolygonCollider2D P = A.GetComponentInChildren<PolygonCollider2D>();
//            if (P != null) WallCollisionList.Add(P);
//        }


//        //if (this.RoomType != RoomEnum.LCorridor) OverlapBox = GroundBox;
//        //else Debug.Log("L CORRIDOR NEEDS CUSTOM BOX MADE");


//        if (this.RoomType == RoomEnum.LongCorridor || this.RoomType == RoomEnum.SmallCorridor) OverlapBox.size = new Vector2(OverlapBox.size.x, OverlapBox.size.y - 70);
//        else if(OverlapBox != null) OverlapBox.size = new Vector2(OverlapBox.size.x, OverlapBox.size.y - 90);
//        //corridor -40
//        // -90 on big room, medium 
//        //
//    }

//    public void ToggleWallCollision(bool toggle)
//    {
//        foreach (PolygonCollider2D P in WallCollisionList)
//        {
//            P.enabled = toggle;
//        }

//        if(this.RoomType == RoomEnum.LCorridor && OverlapBox != null && CenterJoint != null)
//        {
//            OverlapBox.gameObject.GetComponent<BoxCollider2D>().enabled = false;
//            if (LOverlapBox == null) Debug.Log("GROUND BOX FOR L CORRIDOR IS OFF BUT HAS NO LOVERLAPBOX");
//        }
//        //if (GroundBox != null) GroundBox.enabled = false;
//        //else Debug.Log("ground box is null");
//    }

//    public void TurnOffArchBox()
//    {
//        foreach (Tiled2Unity.Layer L in Arches)
//        {
//            if (L == null) continue;
//            L.GetComponentInChildren<BoxCollider2D>().enabled = false;
//        }
//    }

//    public Joint GetNextJoint() //should never return null, also should always be called from a room in question
//    {
//        return JointQue.Dequeue();
//    }

//    public void ResetOverlapAndDeactivate()
//    {

//        this.gameObject.SetActive(false);
//        IsOverlapping = false;
//        //Debug.Log("set false and overlap set false");
//    }

//    public List<Joint> UnAttatchedJoints()
//    {
//        List<Joint> JList = new List<Joint>() { null, null, null, null };
//        foreach (Joint J in JointsList)
//        {
//            if (!J.IsAttached)
//            {
//                int index = 0;
//                if (J.JointOrientation == Joint.OrientationEnum.Up) index = 0;
//                else if (J.JointOrientation == Joint.OrientationEnum.Right) index = 1;
//                else if (J.JointOrientation == Joint.OrientationEnum.Down) index = 2;
//                else if (J.JointOrientation == Joint.OrientationEnum.Left) index = 3;

//                JList.RemoveAt(index);
//                JList.Insert(index, J);
//            }

//        }

//        //Debug.Log(JList[0].Orientation + " " + JList[1].Orientation + " " + JList[2].Orientation + " " + JList[3].Orientation);

//        return JList;
//    }

//    public void RotateJoint(Transform T)
//    {
//        T.Rotate(Vector3.forward, 90);
//        RotateCount++;
//    }

//    public Joint GetRandomJoint()
//    {
//        if (JointsList.Count <= 0)
//        {
//            Debug.LogError("joints list count is zero somehow");
//            return null;
//        }
//        return JointsList[Random.Range(0, (JointsList.Count - 1) % JointsList.Count)]; //added -1 5/9/18
//    }

//    public void ToggleOffOverlapBox()
//    {
//        if (this.RoomType == RoomEnum.LCorridor && LOverlapBox != null) LOverlapBox.enabled = false;
//        else OverlapBox.enabled = false;
//    }

//    public Vector3 GetRandomWaypoint()
//    {
//        if (RoomPositions.Count > 0)
//        {
//            foreach (RoomPosition P in RoomPositions)
//            {
//                RoomPosition R = RoomPositions[Random.Range(0, RoomPositions.Count)];
//                if (R.InUse) continue;
//                else
//                {
//                    R.InUse = true;
//                    return R.transform.position;
//                }
//            }
//            return new Vector3(0, 0, 0);
//        }
//        else
//        {
//            Debug.Log("COULD NOT FIND RANDOM WAYPOINT");
//            return new Vector3(0, 0, 0);
//        }
//    }


//    //sets the room scale to 1/16 size
//    //adds rigibody2d to the room
//    //adds box collider for overlap checking and for finding center joints
//    //tags the ground floor as groundlevel so it can add boxcollider for finding center joint
//    //tags arches as doors and adds colliders to find joint positions
//    private void SetUpRoom()
//    {
//        Debug.Log("SETTING UP ROOM");
//        this.gameObject.transform.localScale *= 0.0625f;
//        this.gameObject.transform.localScale += new Vector3(0, 0, (1 - 0.0625f));

//        Rigidbody2D Body = this.gameObject.AddComponent<Rigidbody2D>();
//        //Body.simulated = false;
//        Body.constraints = RigidbodyConstraints2D.FreezeAll;
//        //this.gameObject.GetComponent<Rigidbody2D>().simulated = false;
//        //this.gameObject.AddComponent<BoxCollider2D>(); //overlap box
//        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
//        //if (this.RoomType == RoomEnum.LCorridor)
//        //{
//        //    LOverlapBox = this.gameObject.AddComponent<PolygonCollider2D>();
//        //}

//        foreach(Tiled2Unity.Layer G in GetComponentsInChildren<Tiled2Unity.Layer>())
//        {
//            if (G.gameObject.name == "GroundFloor")
//            {
//                G.gameObject.tag = "GroundLevel";
//                Debug.Log("MAKE SURE ROOM TYPE IS PROPERLY SET");
//                //this is for the l corridor and the box must be manually shaped to fit the corridor
//                //also need to set it to trigger
//                G.gameObject.GetComponentInChildren<Tiled2Unity.SortingLayerExposed>().gameObject.AddComponent<BoxCollider2D>();
//                if (this.RoomType == RoomEnum.LCorridor) G.gameObject.GetComponentInChildren<Tiled2Unity.SortingLayerExposed>().gameObject.AddComponent<PolygonCollider2D>();
//            }
//            else if (G.gameObject.name == "ArchTop")
//            {
//                G.gameObject.tag = "Doors";
//                G.gameObject.layer = 8;
//                Tiled2Unity.SortingLayerExposed J = G.GetComponentInChildren<Tiled2Unity.SortingLayerExposed>();
//                J.gameObject.tag = "DoorTop";
//                J.gameObject.AddComponent<BoxCollider2D>();
//            }
//            else if (G.gameObject.name == "ArchBottom")
//            {
//                G.gameObject.tag = "Doors";
//                G.gameObject.layer = 8;
//                Tiled2Unity.SortingLayerExposed J = G.GetComponentInChildren<Tiled2Unity.SortingLayerExposed>();
//                J.gameObject.tag = "DoorBottom";
//                J.gameObject.AddComponent<BoxCollider2D>();
//            }
//            else if (G.gameObject.name == "ArchLeft")
//            {
//                G.gameObject.tag = "Doors";
//                G.gameObject.layer = 8;
//                Tiled2Unity.SortingLayerExposed J = G.GetComponentInChildren<Tiled2Unity.SortingLayerExposed>();
//                J.gameObject.tag = "DoorLeft";
//                J.gameObject.AddComponent<BoxCollider2D>();
//            }
//            else if (G.gameObject.name == "ArchRight")
//            {
//                G.gameObject.tag = "Doors";
//                G.gameObject.layer = 8;
//                Tiled2Unity.SortingLayerExposed J = G.GetComponentInChildren<Tiled2Unity.SortingLayerExposed>();
//                J.gameObject.tag = "DoorRight";
//                J.gameObject.AddComponent<BoxCollider2D>();

//                //SetupDoor(G.gameObject);

//            }
//            else if (G.gameObject.name == "CornerWalls")
//            {
//                if (G.GetComponentInChildren<PolygonCollider2D>() != null) Destroy(G.GetComponentInChildren<PolygonCollider2D>());
//                else Debug.Log("couuld not find corner collision");

//            }
//            else if (G.gameObject.name == "TopWalls")
//            {
//                TopWalls = G;
//            }
//            else if (G.gameObject.name == "BottomWalls")
//            {
//                BottomWalls = G;
//            }

//            //should write a debug function here to check for null exceptions

//        }


//        FillArchesList();
//        SpawnJoints();
//        RoomInitialize = false;


//    }//end setuproom()


//    private void SetupDoor(GameObject G, string name)
//    {
//        string DoorTag = " ";

//        if (name == "ArchTop") DoorTag = "DoorTop";
//        else if (name == "ArchRight") DoorTag = "DoorRight";
//        else if (name == "ArchLeft") DoorTag = "DoorLeft";
//        else if (name == "ArchBottom") DoorTag = "DoorBottom";
//        else DoorTag = "UNDEFINED";

//        G.gameObject.tag = "Doors";
//        G.gameObject.layer = 8;
//        Tiled2Unity.SortingLayerExposed J = G.GetComponentInChildren<Tiled2Unity.SortingLayerExposed>();
//        if (DoorTag == "UNDEFINED") Debug.Log("ERROR, DOORTAG IS NOT DEFINED, CHECK NAME");
//        J.gameObject.tag = DoorTag;
//        J.gameObject.AddComponent<BoxCollider2D>();
//    }


//    //this function is called in spawn joints
//    private Tiled2Unity.Layer GetBlockedArch(string ArchName)
//    {
//        foreach(Tiled2Unity.Layer G in this.gameObject.GetComponentsInChildren<Tiled2Unity.Layer>()) //searches entire room children layers
//        {
//            //Debug.Log("searching for tag " + ArchTag + "Blocked....." + " current: " + G.gameObject.name);
//            if(G.gameObject.name == (ArchName + "Blocked"))
//            {
//                //Debug.Log("found object: " + G.gameObject.name);
//                G.gameObject.SetActive(false);
//                return G;
//            }
//        }
//        return null;
//    }





//    private void OnTriggerEnter2D(Collider2D collision)
//    {
//        if (collision.transform.parent != null && collision.transform.parent.gameObject.name == "GroundFloor")
//        {
//            //Debug.Log("overlapping");
//            IsOverlapping = true;
//            //if (collision.gameObject.GetComponent<Room>().RoomType == RoomEnum.LCorridor) Debug.Log("colliding with an L corridor");
//        }
//    }
//}
