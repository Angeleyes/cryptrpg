﻿using UnityEngine;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(Collider2D))]
public abstract class Interactable : MonoBehaviour, IInteractable {

    public float InteractionDistance;
    //internal CircleCollider2D InteractionRadius;
    //public float ChanceToDropLoot = 0f;
    //public bool IsDestructable = false;
    //public bool UsesPixelExploder = false;
    public bool IgnoreInteractionDistance = false;
    public bool OneTimeUse = false;
    public bool Interacted = false;
    public Sprite PostInteractSprite;
    internal Sprite InitialSprite;
    internal SpriteRenderer Renderer;
    internal AudioSource Audioplayer;
    internal Collider2D ColliderBox;

    internal Queue<Sprite> SpriteQue = new Queue<Sprite>();


    public AudioClip InteractSound;
    //public int SpawnChancePercent = 0;

    internal RoomPosition InteractableRoomPosition;

    public event Action TriggerEvent;

    public virtual void Awake()
    {
        ColliderBox = GetComponent<Collider2D>();
        Renderer = GetComponent<SpriteRenderer>();
        Audioplayer = GetComponent<AudioSource>();
        InitialSprite = Renderer.sprite;
        if(PostInteractSprite!= null) SpriteQue.Enqueue(PostInteractSprite);
        SpriteQue.Enqueue(InitialSprite);

        //if (InteractSound != null) Audioplayer.clip = InteractSound;
    }

    //interact is only called on the player controller if the caninteract returns true
    public virtual void Interact()
    {
        //think about making a 1 second delay before being able to call interact again

        if (Interacted && OneTimeUse) return;
        Debug.Log("interacting with " + this.name);
        if (Audioplayer != null && Audioplayer.clip != null) Audioplayer.Play();
        Interacted = true;

        AfterInteract();
    }


    public virtual void ResetInteractable()
    {
        Interacted = false;
        Renderer.sprite = InitialSprite;
        //AfterInteract();
    }

    public bool CanInteract(Entity Actor)
    {
        Debug.Log("trying to interact with: " + this.name + " is it in distance? : " + (Vector3.Distance(Actor.GetEntityPosition(), this.transform.position).ToString()));
        if (IgnoreInteractionDistance || ((Actor.GetEntityPosition() - this.transform.position).sqrMagnitude < InteractionDistance*InteractionDistance))
        {
            Debug.Log("returning true in can interact");
            return true;
        }

        else return false;
    }

    public virtual void AfterInteract()
    {
        Debug.Log("inside base after interact");
        if (PostInteractSprite != null)
        {
            Sprite S = SpriteQue.Dequeue();
            Renderer.sprite = S;
            SpriteQue.Enqueue(S);
        }

        if (TriggerEvent != null) TriggerEvent();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        //Gizmos.DrawCube(this.transform.position, new Vector3(0.5f, 0.5f, 0.5f);
        Gizmos.DrawWireSphere(this.transform.position, InteractionDistance);
    }



    public virtual void OnCollisionEnter2D(Collision2D collision)
    {

    }

}
