﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WalkableTileBehaviour : MonoBehaviour  {

    protected Color LiquidEffectColor = new Color();
    public Vector2 SinkFactors;
    public float SinkSpeed = 12;

    internal abstract void OnWalkOver(Entity Actor);
    internal abstract void OnWalkOff(Entity Actor);



    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Entity>() != null)
        {
            OnWalkOver(collision.gameObject.GetComponent<Entity>());
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Entity>() != null)
        {
            OnWalkOff(collision.gameObject.GetComponent<Entity>());
        }
    }

    protected void SinkActor(Entity Actor)
    {

        Actor.LiquidMask.GetComponentInChildren<SpriteRenderer>().color = LiquidEffectColor;
        Vector2 Direction = Actor.MoveMotor.GetNormCurrentDirection();

        float sinkfactorY = 0;
        float sinkfactorX = 0;


        if (Direction.y == 0) sinkfactorY = 0;
        else if (Direction.y > 0) sinkfactorY = SinkFactors.y;
        else sinkfactorY = SinkFactors.y*-1;


        if (Direction.x == 0) sinkfactorX = 0;
        else if (Direction.x > 0) sinkfactorX = SinkFactors.x;
        else sinkfactorX = SinkFactors.x*-1;


        Actor.transform.position = Vector3.Lerp(Actor.transform.position , new Vector3(Actor.transform.position.x + sinkfactorX, Actor.transform.position.y + sinkfactorY, Actor.transform.position.z), Time.deltaTime*SinkSpeed);

    }

}
