﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


[RequireComponent(typeof(Collider2D))]
public class WorldTooltip : MonoBehaviour
{
    [TextArea]
    public string TooltipText;

    [SerializeField]
    private RectTransform Tooltipbox;
    [SerializeField]
    private Text UIText;
    //[SerializeField]
    //private WorldTooltipHandler TipHandlerRef;

    public bool UseInteract = false;
    internal KnightInteract InteractObj;
    internal Collider2D TriggerBox;

    //this gets called in the setup of a room piece, a room piece takes all of its worldtooltip objects and inits them with the
    //gamemanagers worldtooltip handler and gives these references

    public void Awake()
    {
        if (UseInteract)
        {
            InteractObj = GetComponent<KnightInteract>();
            InteractObj.TriggerEvent += ShowWorldTip;
        }
    }

    public void RegisterHandler(WorldTooltipHandler Wh)
    {
        //TipHandlerRef = Wh;
        UIText = Wh.GetTipText();
        Tooltipbox = Wh.GetTipPanel();
    }

    public void OnMouseEnter()
    {
        if (UseInteract)
        {
            if (InteractObj.IsTriggered())
            {
                ShowWorldTip();
            }
            else { return; }
 
        }
        else ShowWorldTip();
    }

    public void SetTooltipText(string tiptext)
    {
        TooltipText = tiptext;
    }


    public void ShowWorldTip()
    {
        Tooltipbox.gameObject.SetActive(true);

        UIText.text = TooltipText;

        if (UIText.text == "")
        {
            Tooltipbox.gameObject.SetActive(false);
            return;
        }

        //Background.transform.position = new Vector3(UiObject.transform.position.x, UiObject.transform.position.y + 3f, UiObject.transform.position.z);
        Tooltipbox.position = Input.mousePosition;
        Tooltipbox.position += (Vector3.up * 70f); //goes a little bit above mouse position

        float Padding = 10f;
        Vector2 BackgroundSize = new Vector2(UIText.preferredWidth + Padding, UIText.preferredHeight + Padding/2);
        UIText.rectTransform.sizeDelta = BackgroundSize;
        //Background.sizeDelta = BackgroundSize;
        Tooltipbox.sizeDelta = new Vector2(BackgroundSize.x + Padding * 2, BackgroundSize.y + Padding * 2);
        //Background.localScale = BackgroundSize;
        if (UseInteract)
        {
            InteractObj.TriggerEvent -= ShowWorldTip;
            InteractObj.TriggerEvent += HideToolTip;
        }
    }

    public void HideToolTip()
    {
        Tooltipbox.gameObject.SetActive(false);
        if(UseInteract)
        {
            InteractObj.TriggerEvent -= HideToolTip;
            InteractObj.TriggerEvent += ShowWorldTip;
        }
    }

    public void OnMouseExit()
    {
        HideToolTip();
    }

    
}
