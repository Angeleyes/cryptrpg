﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTileBehaviour : WalkableTileBehaviour
{

    public void Start()
    {
        LiquidEffectColor = new Color(31, 162, 214, 130);
    }

    internal override void OnWalkOver(Entity Actor)
    {
        Actor.Rend.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        Actor.ToggleLiquidMask(true);
        if(Actor.EntityShadow != null)
        {
            Debug.Log("turning off shadow for water");
            Actor.EntityShadow.ShadowObject.SetActive(false);
        }
        SinkActor(Actor);
    }

    internal override void OnWalkOff(Entity Actor)
    {
        Actor.Rend.maskInteraction = SpriteMaskInteraction.None;
        Actor.ToggleLiquidMask(false);
        if (Actor.EntityShadow != null)
        {
            Debug.Log("turning back on shadow from water");
            Actor.EntityShadow.ShadowObject.SetActive(true);
        }
        SinkActor(Actor);
    }


}
