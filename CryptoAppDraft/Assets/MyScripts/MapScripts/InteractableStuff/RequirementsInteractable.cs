﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequirementsInteractable : Interactable {


    internal bool RequirementsMet = false;
    internal SpriteRenderer DoorColor;
    public Color ColorCode;
    internal Key TheKey;
    public Vector3 KeyStartPos;


    //when turned active
    public override void Awake()
    {
        base.Awake(); // dont need this??? CHECK THIS
        DoorColor = GetComponent<SpriteRenderer>();
        DoorColor.color = ColorCode;
        TheKey = Instantiate(TheKey, KeyStartPos, Quaternion.identity);

    }

    //after gameobject turned on, need to set key, maybe turn all of this into an init function
    public void SetKey(List<RoomPosition> Positions)
    {
        TheKey.InitKey(Positions, ColorCode, OnKeyPickUp);
    }

    public override void Interact()
    {
        if (RequirementsMet)
        {
            base.Interact();
        }
    }

    public override void AfterInteract()
    {
        Debug.Log("calling requiremenst after interact");
        base.AfterInteract();
        this.gameObject.SetActive(false);
    }


    public void OnKeyPickUp()
    {
        RequirementsMet = true;
    }



}
