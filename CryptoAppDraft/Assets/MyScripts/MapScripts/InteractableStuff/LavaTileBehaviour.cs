﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class LavaTileBehaviour : WalkableTileBehaviour
{
    
    public float DamageInterval = 1;
    public float LavaDamagePercent = 20;

    public bool IsAcid = false;


    private Color AcidColor = new Color(38, 123, 21, 130);
    private Color LavaColor = new Color(250, 21, 18, 130);

    public void Start()
    {
        if (IsAcid) LiquidEffectColor = AcidColor;
        else LiquidEffectColor = LavaColor;
        if (this.gameObject.GetComponent<Collider2D>() != null)
        {
            this.gameObject.GetComponent<Collider2D>().isTrigger = true;
        }

    }

    internal override void OnWalkOver(Entity Actor)
    {
        Actor.Rend.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        Actor.ToggleLiquidMask(true);
        SinkActor(Actor);
        if (Actor.EntityShadow != null)
        {
            Debug.Log("turning off shadow for lava");
            Actor.EntityShadow.ShadowObject.SetActive(false);
        }
        StartCoroutine(DoLavaDamage(Actor));
    }

    internal override void OnWalkOff(Entity Actor)
    {
        Actor.Rend.maskInteraction = SpriteMaskInteraction.None;
        Actor.ToggleLiquidMask(false);
        SinkActor(Actor);
        if (Actor.EntityShadow != null)
        {
            Debug.Log("turning off shadow for lava");
            Actor.EntityShadow.ShadowObject.SetActive(true);
        }
        StopAllCoroutines();
    }


    IEnumerator DoLavaDamage(Entity Actor)
    {

        while(true)
        {
            yield return new WaitForSeconds(DamageInterval);
            float hp = Actor.Health.GetValue();
            Actor.TakeDamage((int)(hp * (LavaDamagePercent / 100)), null, AttackHitBox.DamageSpec.SpellDamage);

            yield return null;
        }


    }
}
