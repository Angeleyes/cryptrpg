﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChestInteractable : Interactable, ILootable {


    public LootTable CustomLootTable;
    //internal Collider2D CollisionTrigger;

    private LootRequest ChestLoot = new LootRequest();
    private Vector3 DropPos;

    public override void Awake()
    {
        base.Awake();
        //CollisionTrigger = GetComponent<Collider2D>();
        DropPos = this.transform.position + new Vector3(0.5f, -0.5f, 0);
        ChestLoot = new LootRequest(new List<int>(), 1, DropPos, CustomLootTable);
    }

    public override void Interact()
    {
        base.Interact();
        DropLoot();
    }

    public override void AfterInteract()
    {
        base.AfterInteract();
        //CollisionTrigger.enabled = false;
    }

    public bool DropLoot()
    {

        bool result = false;
        result = LootTableManagerLocator.GetLootManagerService().RequestLoot(ChestLoot);
        return result;

    }
}
