﻿public interface IPuzzleObservable
{
    void NotifyObserver();
    void AddObserver(PuzzleObject Observer);

    bool IsTriggered();
}