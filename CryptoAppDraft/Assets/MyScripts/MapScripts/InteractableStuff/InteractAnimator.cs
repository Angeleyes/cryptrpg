﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractAnimator : Interactable {

    public SimpleSpriteAnimator SpriteAnim;
    //public SpriteRenderer CustomRenderer;
        
    public override void Awake()
    {
        base.Awake();
        Renderer = GetComponent<SpriteRenderer>();
    }

    public override void Interact()
    {
        if (Interacted && OneTimeUse) return;
        //base.Interact();
        if (Audioplayer != null && Audioplayer.clip != null) Audioplayer.Play();
        Interacted = true;

    }



}
