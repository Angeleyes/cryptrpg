﻿
public interface TileBehavior {

    void OnWalkOver(Entity Actor);

    void OnWalkOff(Entity Actor);
}
