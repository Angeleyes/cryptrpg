﻿
using UnityEngine;

public class KnightInteract : Interactable, IPuzzleObservable {

    internal bool Triggered = false;
    PuzzleObject PuzzleObserver;
    public SpriteRenderer CustomRenderer;

    public override void Awake()
    {
        base.Awake();
        if (CustomRenderer == null) Renderer = GetComponentsInChildren<SpriteRenderer>()[1];
        else Renderer = CustomRenderer;
        InitialSprite = Renderer.sprite;
        SpriteQue.Clear();
        SpriteQue.Enqueue(PostInteractSprite);
        SpriteQue.Enqueue(InitialSprite);
        Triggered = false;

        if (this.gameObject.layer != 27) this.gameObject.layer = 27;
    }

    public override void Interact()
    {
        base.Interact();
        Triggered = !Triggered;
        NotifyObserver();

    }

    public override void ResetInteractable()
    {
        base.ResetInteractable();
        Triggered = false;
    }

    public bool IsTriggered()
    {
        return Triggered;
    }

    public void NotifyObserver()
    {
        if(PuzzleObserver != null) PuzzleObserver.Notify();
    }

    public void AddObserver(PuzzleObject Observer)
    {
        PuzzleObserver = Observer;
    }

}
