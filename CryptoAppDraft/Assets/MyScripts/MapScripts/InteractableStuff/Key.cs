﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Key : Pickup {

    List<RoomPosition> TargetPositions;
    internal Vector3[] KeyPath;
    int PreviousIndex = 0;
    Action KeyPickedUp;

    public override void OnDrop()
    {
        if (TargetPositions.Count > 0) FindPoint(KeyPath, false, null);
    }

    public void FindPoint(Vector3[] NewPath, bool PathFound, MonsterAI Ai)
    {
        if(PathFound)
        {
            //move to it
            this.transform.position = NewPath[NewPath.Length - 1];
            return;
        }
        else PathManager.RequestPath(this.transform.position, GetNewRoomPosition(PreviousIndex), FindPoint, null); //request new path

    }

    public virtual void InitKey(List<RoomPosition> Pos, Color C, Action KeyPickup)
    {
        TargetPositions = Pos;
        this.GetComponent<SpriteRenderer>().material.color = C;
        KeyPickedUp = KeyPickup;
        OnDrop();
    }

    public override void OnPickUp(PlayerEntity PlayerThatisPickingUp)
    {
        if(KeyPickedUp != null) KeyPickedUp();
        base.OnPickUp(PlayerThatisPickingUp);

    }

    Vector3 GetNewRoomPosition(int PrevIndex)
    {
        TargetPositions.RemoveAt(PreviousIndex);
        int NewIndex = UnityEngine.Random.Range(0, TargetPositions.Count);
        PreviousIndex = NewIndex;
        return TargetPositions[NewIndex].transform.position;
    }

}
