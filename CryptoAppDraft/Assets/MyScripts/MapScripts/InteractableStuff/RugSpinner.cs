﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RugSpinner : MonoBehaviour {

    SpriteRenderer Rend;
    public int SpinDelay = 1;
    bool SpinToggle = true;

    private void Start()
    {
        Rend = GetComponent<SpriteRenderer>();
        StartCoroutine(Spin());
    }


    IEnumerator Spin()
    {
        while (true)
        {
            SpinToggle = !SpinToggle;
            yield return new WaitForSeconds(SpinDelay);
            Rend.flipX = SpinToggle;
            yield return new WaitForSeconds(SpinDelay);
            Rend.flipY = SpinToggle;

            yield return null;
        }

        //yield return null;

        
    }

}
