﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldTooltipHandler : MonoBehaviour
{
    public Text TipText;
    public RectTransform TipPanel;

    public void Awake()
    {
        TipPanel.gameObject.SetActive(false);
    }

    public Text GetTipText()
    {
        return TipText;
    }

    public RectTransform GetTipPanel()
    {
        return TipPanel;
    }
}
