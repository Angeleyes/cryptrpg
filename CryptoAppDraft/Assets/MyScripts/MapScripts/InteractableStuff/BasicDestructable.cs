﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class BasicDestructable : Interactable {

    internal Collider2D TriggerBox;

    public override void Awake()
    {
        base.Awake();
        TriggerBox = GetComponent<Collider2D>();
        //TriggerCapsule = GetComponent<CapsuleCollider2D>();
    }

    public override void Interact()
    {
        base.Interact();
        Destroy(this);
        
        //drop loot or watever
        //KillThis();
        Debug.Log("destroied this: " + this.name);
    }

    public override void AfterInteract()
    {
        base.AfterInteract();

        if (ColliderBox != null) ColliderBox.enabled = false;
        if (TriggerBox != null) TriggerBox.enabled = false;

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<AttackHitBox>() != null)
        {
            Debug.Log("interacting with basic destructable");
            Interact();
        }
    }


}
