﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PhraseTableList")]
public class PhraseTable : ScriptableObject
{
    //think about splitting these phrases specific lists into other scriptable classes, 1 for and 1 for order
    //for now this is fine
    public List<string> ANDStartPhrases = new List<string>();
    public List<string> ORDERStartPhrases = new List<string>();
    public List<string> DirectPhrases = new List<string>();
    public List<string> NegativePhrases = new List<string>();


    public string GenerateDirectPhrase(int num)
    {
        string Phrase = GetRandomPhrase(1);
        Phrase = ProcessPhrase(Phrase, num);


        return Phrase;
    }


    public string GenerateNeagtivePhrase(int num)
    {
        string Phrase = GetRandomPhrase(2);

        Phrase = ProcessPhrase(Phrase, num);

        return Phrase;
    }

    private string ProcessPhrase(string str, int num)
    {
        string phrase = str;
        //phrase.Replace("%", num.ToString());
        //string number = num.ToString();
        phrase = phrase.Replace("%", StringifyInt(num));


        return phrase;
    }

    public string GetRandomANDPhrase(bool isReverse)
    {
        //if reverse, then swap the index from......to.......
        int rand = Random.Range(0, ANDStartPhrases.Count);
        return ANDStartPhrases[rand];
    }

    public string GetRandomORDERPhrase()
    {
        int rand = Random.Range(0, ORDERStartPhrases.Count);
        return ORDERStartPhrases[rand];
    }

    private string StringifyInt(int Num)
    {
        Num++;
        string StringNum = "";

        switch(Num)
        {
            case 1: StringNum = "One";
                break;
            case 2: StringNum = "Two";
                break;
            case 3: StringNum = "Three";
                break;
            case 4: StringNum = "Four";
                break;
            case 5: StringNum = "Five";
                break;
            case 6: StringNum = "Six";
                break;
            case 7: StringNum = "Seven";
                break;
            case 8:StringNum = "Eight";
                break;
            case 9: StringNum = "Nine";
                break;
            case 10: StringNum = "Ten";
                break;
            default: StringNum = "ERR";
                break;
        }


        return StringNum;
    }

    private string GetRandomPhrase(int type)
    {
        int rand = 0;
        if(type == 1)
        {
            rand = Random.Range(0, DirectPhrases.Count);
            return DirectPhrases[rand];
        }
        else
        {
            rand = Random.Range(0, NegativePhrases.Count);
            return NegativePhrases[rand];
        }
    }
}
