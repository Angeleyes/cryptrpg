﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadTree : MonoBehaviour
{
    public List<Sprite> DeadTrunks = new List<Sprite>();

    Color[] ColorList = new Color[] {
        new Color(255f/255f,190f/255f,121f/255f), new Color(141f/255f,124f/255f,255f/255f),
        new Color(255f/255f,157f/255f,134f/255f), new Color(124f/255f,144f/255f,255f/255f),
        new Color(135f/255f,135f/255f,135f/255f), new Color(215f,215f,215f)
    };

    public void Awake()
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = DeadTrunks[Random.Range(0, DeadTrunks.Count)];
        this.gameObject.GetComponent<SpriteRenderer>().color = ColorList[Random.Range(0, ColorList.Length)];
        this.gameObject.transform.localScale *= Random.Range(0.8f, 1.3f);
        Destroy(this);
    }
}
