﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestBush : MonoBehaviour
{
    public List<Sprite> RandomBushes = new List<Sprite>();

    Color[] ColorList = new Color[] {
        new Color(255f/255f,190f/255f,121f/255f), new Color(141f/255f,124f/255f,255f/255f),
        new Color(255f/255f,157f/255f,134f/255f), new Color(124f/255f,144f/255f,255f/255f),
        new Color(135f/255f,135f/255f,135f/255f), new Color(255f,255f,255f)
    };

    public void Awake()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = ColorList[Random.Range(0, ColorList.Length)];
        this.gameObject.GetComponent<SpriteRenderer>().sprite = RandomBushes[Random.Range(0, RandomBushes.Count)];
        this.gameObject.transform.localScale *= Random.Range(1.5f, 2.7f);
        Destroy(this);
    }
}
