﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class ForestTree : MonoBehaviour
{
    public bool UseRandomColor = false;
    public bool UseRandomTrunk = false;
    public bool UseRandomSize = false;

    [SerializeField]
    internal List<Sprite> TrunkList = new List<Sprite>();
    private ParticleSystem FallingLeaves;
    private SpriteAnimator SpriteAnim2;
    internal bool CoolingDownAnimation = false;
    internal SpriteRenderer Rend;
    internal SpriteRenderer TrunkRend;

    #region coolcolors
    //7DAD53 grayscale

    //private Color BlueHue = 7C90FF;
    //8D7CFF
    //FFBE79

    //878787 same as above with non gray
    //FF9D86
    //7C90FF
    //FFFFFF
    #endregion

    Color[] ColorList = new Color[] {
        new Color(255f/255f,190f/255f,121f/255f), new Color(141f/255f,124f/255f,255f/255f),
        new Color(255f/255f,157f/255f,134f/255f), new Color(124f/255f,144f/255f,255f/255f),
        new Color(135f/255f,135f/255f,135f/255f), new Color(215f,215f,215f)
    };

    public void Awake()
    {
        FallingLeaves = GetComponent<ParticleSystem>();
        FallingLeaves.Stop();
        SpriteAnim2 = GetComponent<SpriteAnimator>();
        SpriteRenderer[] R = GetComponentsInChildren<SpriteRenderer>();
        Rend = R[0];
        TrunkRend = R[1];
        if (UseRandomColor) Rend.color = GetRandomListColor();
        if (UseRandomTrunk) TrunkRend.sprite = GetRandomTrunk();
        if (UseRandomSize) this.transform.localScale *= Random.Range(0.8f, 1.15f);

        this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, 1);


        //FallingLeaves.main.startColor = Rend.color;
        TrunkRend.color = new Color(Rend.color.r, Rend.color.g-(50f/255f), Rend.color.b-(50f/255f));
        ParticleSystem.MainModule M = FallingLeaves.main;
        M.startColor = Rend.color;
    }

    private Color GetRandomListColor()
    {
        int rand = Random.Range(0, ColorList.Length);
        return ColorList[rand];
    }

    private Sprite GetRandomTrunk()
    {
        int rand = Random.Range(0, TrunkList.Count);
        return TrunkList[rand];
    }

    IEnumerator TriggerFallingLeaves()
    {
        yield return new WaitForSeconds(0.5f);
        SpriteAnim2.Play();
        FallingLeaves.Play();
        yield return new WaitForSeconds(2f);
        FallingLeaves.Stop();

        yield break;
    }

    public void FallLeaves()
    {
        StartCoroutine(TriggerFallingLeaves());
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!CoolingDownAnimation && collision.gameObject.layer == 25)
        {
            FallLeaves();
        }
    }

}
