﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicCamMove : MonoBehaviour {


    public float Ysensitivity = 0;
    public float Xsensitivity = 0;
    public float speed = 5;

    public void FixedUpdate()
    {
        if (Input.GetAxisRaw("Vertical") > 0.1f || Input.GetAxisRaw("Vertical") < -0.1f)
        {
            float Y = Input.GetAxisRaw("Vertical");
            float Sign = Y;
            float MoveY = Ysensitivity * Sign;
            this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(transform.position.x, MoveY, transform.position.z), (speed*2) * Time.deltaTime);
        }

        if (Input.GetAxisRaw("Horizontal") > 0.1f || Input.GetAxisRaw("Horizontal") < -0.1f)
        {
            float X = Input.GetAxisRaw("Horizontal");
            float Sign = X;
            float MoveX = Xsensitivity * Sign;
            this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(MoveX, transform.position.y, transform.position.z), speed * Time.deltaTime);
        }



        if(Input.mouseScrollDelta.y > 0 || Input.mouseScrollDelta.y < 0)
        {
            Camera.main.orthographicSize -= Input.mouseScrollDelta.y;
        }
    }
}
