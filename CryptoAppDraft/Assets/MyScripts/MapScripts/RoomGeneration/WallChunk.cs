﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class WallChunk : TileChunk {



    List<Tile> WallDeviations = new List<Tile>();


    public bool IsSideWall { get; private set; }
    public bool IsTopWall { get; private set; }


    public WallChunk(int x, int y, int z, Tile tile, bool SideWall, bool TopWall, bool isFractle)
    {
        ChunkCoordinates = new Vector3Int(x, y, z);
        IsSideWall = SideWall;
        ChunkSprite = tile.sprite;
        ChunkTile = tile;
        if (!isFractle && IsSideWall) ChunkCollider = Tile.ColliderType.Sprite;
        else ChunkCollider = Tile.ColliderType.None;
        IsTopWall = TopWall;
    }

    public WallChunk(int x, int y, int z, Tile tile, bool SideWall, bool TopWall)
    {
        ChunkCoordinates = new Vector3Int(x, y, z);
        IsSideWall = SideWall;
        ChunkSprite = tile.sprite;
        ChunkTile = tile;
        if (IsSideWall) ChunkCollider = Tile.ColliderType.Sprite; //this may cuase a collider bug when testing, remember to remove this is not so
        else ChunkCollider = Tile.ColliderType.None;
        IsTopWall = TopWall;
        //WallDeviation1 = walldeviation1;
        //WallDeviation2 = walldeviation2;
        //WallDeviations = DeviationList;
        //SetToDeviationSprite();
    }

    public void SetDeviationList(List<Tile> DeviationList)
    {
        WallDeviations = DeviationList;
    }

    public void SetToDeviationSprite()
    {
        int i = Random.Range(0, 11);
        int j = Random.Range(0, WallDeviations.Count);
        if (i == 10) SetChunkTile(WallDeviations[j]);
        else return;
    }

}
