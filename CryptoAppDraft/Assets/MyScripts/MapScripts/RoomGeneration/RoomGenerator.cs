﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RoomGenerator : MonoBehaviour {


    //public Transform MainRoomParent;
    public ScriptableTileMap ScriptableTileMap;
    private Tilemap MainTileMap;
    private Tilemap TopLayerMap;
    private Tilemap FractleMap;
    private Tilemap FLeftWall;
    private Tilemap FRightWall;
    private Tilemap FTopWall;
    private Tilemap FBotWall;
    private Tilemap DoodadLayer;

    //private Tilemap WaterLayer;
    //private Tilemap PlaceableLayer1;
    //private Tilemap PlaceableLayer2;
    //private Tilemap PlaceableColLayer;
    //private Tilemap LavaLayer;
    //private Tilemap SpriteColLayer;


    private RoomPiece.RoomType RoomTypeSize;

    public Grid MainGrid;
    public Tile TestTile;
    public string RoomName = "Room";
    private string RoomSize = " ";
    public Joint JointRef;

    Grid TheGrid;
    Tilemap TheTileMap;

    TileChunk[,] RoomGrid;
    TileChunk[,] FractleGrid;

    [Range(16, 60)]
    public int GridMaxX;
    int GridMinX = 20;
    [Range(16, 60)]
    public int GridMaxY;
    int GridMinY = 20;

    private float YDoorLimiter = 0.3f;
    private float XDoorLimiter = 0.3f;

    private delegate void RoomOperation(TileChunk[,] Grid);
    private RoomOperation DoFloorLayout;

    public bool UseRandomGridMax = false;

    public bool RandomizeFloor = false;
    public int RandomFloorChance = 25;

    [Range(0, 15)]
    public int FloorSpriteIndex1 = 0;
    //public int PlacableSpriteIndex = 0;
    [Space]
    [Space]
    public Color FloorSprite1Color;
    [PreviewSprite]
    public Sprite FloorSprite1;
    [Space(order = 2)]
    public Color FloorSprite2Color;
    [Range(0, 15)]
    public int FloorSpriteIndex2 = 0;
    [Space]
    [PreviewSprite]
    public Sprite FloorSprite2;
    [Space]
    [Space]


    public bool DoRoomOutline = false;
    public bool OutlineBottomRoom = false;

    public bool BlockTopDoor = false;
    public bool BlockBottomDoor = false;
    public bool BlockLeftDoor = false;
    public bool BlockRightDoor = false;

    public bool CenteredDoors = false;
    public bool DoorSymmetry = false;
    public bool SetVRug = false;
    public bool SetHRug = false;
    public bool CleanRug = false;


    public bool SpiralFloor = false;
    [Range(1, 3)]
    public int SpiralCount = 1;
    [Range(1, 3)]
    public int SpiralDeviation = 1;
    public bool CheckerBoardFloor = false;
    [Range(1,25)]
    public int CheckerStartX = 1;
    [Range(1,20)]
    public int CheckerStartY = 1;
    [Range(1, 25)]
    public int CheckerEndX = 1;
    [Range(1, 20)]
    public int CheckerEndY = 1;
    [Range(1, 15)]
    public int CheckerMod = 1;
    public bool UseCheckerMod2 = false;
    [Range(1,15)]
    public int CheckerMod2 = 1;
    [Range(0, 15)]
    public int CheckerMod2Variable = 0;

    public bool isFractle = false;
    public bool FBlockBotDoor = false;
    public bool FBlockTopDoor = false;
    public bool FBlockLeftDoor = false;
    public bool FBlockRightDoor = false;

    public bool PlaceFloorClutter = false;

    //public int WallClutterChance = 20;
    public int FloorClutterChance = 10;

    public bool SpecialWallClutter = false;
    public int SpecialClutterChance = 15;

    [Range(-64, 25)]
    public int FXoffset = -2;
    [Range(-45, 25)]
    public int FYoffset = 0;
    [Range(-25, 20)]
    public int ThiccnessFactor = 0;
    [Range(-25, 20)]
    public int FatnessFactor = 0;

    private int DoorRandomizer = 0;

    int XMax;
    int YMax;

    internal List<DoorChunk> DoorList = new List<DoorChunk>();
    internal List<DoorChunk> FractleDoorList = new List<DoorChunk>();
    internal Dictionary<DoorChunk.DoorTypeSet, Vector3> FractleDoors = new Dictionary<DoorChunk.DoorTypeSet, Vector3>();
    internal Dictionary<DoorChunk.DoorTypeSet, Vector3> FractleDoorEdges = new Dictionary<DoorChunk.DoorTypeSet, Vector3>();
    internal Dictionary<DoorChunk.DoorTypeSet, Vector3> RoomDoors = new Dictionary<DoorChunk.DoorTypeSet, Vector3>();
    internal Dictionary<DoorChunk.DoorTypeSet, Vector3> DoorEdgePoints = new Dictionary<DoorChunk.DoorTypeSet, Vector3>();
    internal List<Joint> AllDoorJoints = new List<Joint>();
    internal List<Joint> FractleDoorJoints = new List<Joint>();

    internal Dictionary<DoorChunk.DoorTypeSet, Vector3Int> FractleDoorJointBlockers = new Dictionary<DoorChunk.DoorTypeSet, Vector3Int>();

    internal List<Vector2Int> OutlineLeft = new List<Vector2Int>();
    internal List<Vector2Int> OutlineRight = new List<Vector2Int>();
    internal List<Vector2Int> OutlineTop = new List<Vector2Int>();
    internal List<Vector2Int> OutlineBottom = new List<Vector2Int>();

    private Tilemap TileMapSelected;
    //private bool IsRunning = false;

    public void Start()
    {
        YMax = 33;
        FXoffset = -2;
        GenerateRoom();
        //IsRunning = true;
    }

    public void OnValidate()
    {
        FloorSprite1 = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1).sprite;
        FloorSprite2 = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex2).sprite;
        //if (IsRunning) GenerateRoom();
    }

    //public void FixedUpdate()
    //{
        //PlaceableSprite = ScriptableTileMap.SFloorSprites.GetSprite(PlacableSpriteIndex);
        //PlaceableAsset = ScriptableTileMap.AllFloorSprites.GetSprite(PlaceableAssetIndex);
        //PlaceableAsset2 = ScriptableTileMap.TopAssetsThin.GetSprite(PlaceableAsset2Index);
        //PlaceableAsset2Wide = ScriptableTileMap.TopAssetsWide.GetSprite(PlaceableAsset2WideIndex);
        //MatAsset = ScriptableTileMap.MatAssets.GetSprite(LiquidAssetIndex);

        //TestTile.colliderType = Tile.ColliderType.None;



        //switch (TileMapSelect)
        //{
        //    case TileMapType.Water: TileMapSelected = WaterLayer;
        //        break;
        //    case TileMapType.Placeable1: TileMapSelected = PlaceableLayer1;
        //        break;
        //    case TileMapType.Placeable2: TileMapSelected = PlaceableLayer2;
        //        break;
        //    case TileMapType.PlaceableCol: TileMapSelected = PlaceableColLayer;
        //        break;
        //    case TileMapType.Lava: TileMapSelected = LavaLayer;
        //        break;
        //    case TileMapType.SpriteCollision: TileMapSelected = SpriteColLayer;
        //        break;
        //    default: TileMapSelected = PlaceableLayer1;
        //        break;

        //}

        //if (Input.GetMouseButton(0))
        //{
        //    if (UseAssetBrushBottom)
        //    {
        //        TestTile.sprite = ScriptableTileMap.AllFloorSprites.GetSprite(PlaceableAssetIndex);
        //        TestTile.color = PlaceableAssetColor;
        //        TileMapSelected = PlaceableLayer1;
        //        TileMapSelect = TileMapType.Placeable1;
        //    }
        //    else if(UseAssetBrushTop)
        //    {
        //        TestTile.sprite = ScriptableTileMap.TopAssetsThin.GetSprite(PlaceableAsset2Index);
        //        TestTile.color = PlaceableAssetColor;
        //        TileMapSelected = PlaceableLayer2;
        //        TileMapSelect = TileMapType.Placeable2;
        //        UseThinCol = true;

        //    }
        //    else if(UseAssetBrushTopWide)
        //    {
        //        TestTile.sprite = ScriptableTileMap.TopAssetsWide.GetSprite(PlaceableAsset2WideIndex);
        //        TestTile.color = PlaceableAssetColor;
        //        TileMapSelected = PlaceableLayer2;
        //        TileMapSelect = TileMapType.Placeable2;
        //        UseThinCol = false;
        //    }
        //    else if(UseMatBrush)
        //    {
        //        TestTile.sprite = ScriptableTileMap.MatAssets.GetSprite(LiquidAssetIndex);
        //        TestTile.color = PlaceableAssetColor;
        //        TileMapSelected = WaterLayer;
        //        TileMapSelect = TileMapType.Water;
                
        //    }
        //    else
        //    {
        //        TestTile.sprite = ScriptableTileMap.SFloorSprites.GetSprite(PlacableSpriteIndex);
        //        TestTile.color = PlaceableSpriteColor;
        //    }

        //    //if (TileMapSelect == TileMapType.PlaceableCol || TileMapSelect == TileMapType.SpriteCollision) TestTile.colliderType = Tile.ColliderType.Sprite;
        //    //else TestTile.colliderType = Tile.ColliderType.None;

        //    Vector3 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //    Vector3Int Pos = TileMapSelected.WorldToCell(p);

        //    //Debug.Log("setting tile at: " + Pos + " converted from : " + p);
        //    if (Pos.x % 2 != 0) Pos = new Vector3Int(Pos.x + 1, Pos.y, 0);
        //    if (Pos.y % 2 != 0) Pos = new Vector3Int(Pos.x, Pos.y + 1, 0);

        //    TileMapSelected.SetTile(Pos, TestTile);
        //    if(UseAssetBrushTop || UseAssetBrushTopWide)
        //    {
        //        if (UseThinCol) TestTile.sprite = ScriptableTileMap.ThinCollisionSprite;
        //        else TestTile.sprite = ScriptableTileMap.WideCollisionSprite;

        //        TestTile.colliderType = Tile.ColliderType.Sprite;
        //        PlaceableColLayer.SetTile(Pos, TestTile);
        //    }

        //    TestTile.colliderType = Tile.ColliderType.None;
        //}

        //if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButton(1))
        //{
        //    Vector3 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //    Vector3Int Pos = TileMapSelected.WorldToCell(p);
        //    if (Pos.x % 2 != 0) Pos = new Vector3Int(Pos.x + 1, Pos.y, 0);
        //    if (Pos.y % 2 != 0) Pos = new Vector3Int(Pos.x, Pos.y + 1, 0);
        //    Sprite S = TileMapSelected.GetSprite(Pos);
        //    int index = 0;
        //    if (UseAssetBrushBottom)
        //    {
        //        index = ScriptableTileMap.AllFloorSprites.GetSpriteIndex(S);
        //        PlaceableAssetIndex = index;
                
        //    }
        //    else if(UseAssetBrushTop)
        //    {
        //        index = ScriptableTileMap.TopAssetsThin.GetSpriteIndex(S);
        //        PlaceableAsset2Index = index;
        //    }
        //    else if(UseAssetBrushTopWide)
        //    {
        //        index = ScriptableTileMap.TopAssetsWide.GetSpriteIndex(S);
        //        PlaceableAsset2WideIndex = index;
        //    }
        //    else if(UseMatBrush)
        //    {
        //        index = ScriptableTileMap.MatAssets.GetSpriteIndex(S);
        //        LiquidAssetIndex = index;
        //    }
        //    else
        //    {
        //        index = ScriptableTileMap.SFloorSprites.GetSpriteIndex(S);
        //        PlacableSpriteIndex = index;
        //    }
        //}
        //else if (Input.GetMouseButton(1))
        //{
        //    Vector3 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //    Vector3Int Pos = TileMapSelected.WorldToCell(p);
        //    if (Pos.x % 2 != 0) Pos = new Vector3Int(Pos.x + 1, Pos.y, 0);
        //    if (Pos.y % 2 != 0) Pos = new Vector3Int(Pos.x, Pos.y + 1, 0);
        //    TileMapSelected.SetTile(Pos, null);


        //    if (UseAssetBrushTop || UseAssetBrushTopWide)
        //    {
        //        PlaceableColLayer.SetTile(Pos, null);
        //    }
        //}

    //}


    //a better design for this script would be to have a decorator type pattern in which i made a interface class that had strategy patterns in it and
    // took the room generator as an arugment and then the fucntions would act upon it
    // push the strategies into a que and exectute based on the booleans

    //functions should just do work on a passed in grid with params

    public void GenerateRoom()
    {
        ClearAll();
        //RoomParent = Instantiate(MainRoomParent);
        TheGrid = Instantiate(MainGrid);
        Tilemap[] MapArray = TheGrid.GetComponentsInChildren<Tilemap>();
        TheTileMap = MapArray[0];
        TopLayerMap = MapArray[1];
        FractleMap = MapArray[2];
        FLeftWall = MapArray[3];
        FRightWall = MapArray[4];
        FTopWall = MapArray[5];
        FBotWall = MapArray[6];
        DoodadLayer = MapArray[7];

        //WaterLayer = MapArray[8];
        //PlaceableLayer1 = MapArray[9];
        //PlaceableLayer2 = MapArray[10];
        //PlaceableColLayer = MapArray[11];
        //LavaLayer = MapArray[12];
        //SpriteColLayer = MapArray[13];

        if (CheckerBoardFloor) DoFloorLayout += DoCheckerBoardFloor;
        if (SpiralFloor) DoFloorLayout += DoSpiralFloor;
        if(!SpiralFloor && !CheckerBoardFloor) DoFloorLayout = DoNormalFloor;

        if (UseRandomGridMax)
        {
            XMax = Random.Range(GridMinX, GridMaxX);
            YMax = Random.Range(GridMinY, GridMaxY);
            Debug.Log("using randomgrid: x" + XMax + " y" + YMax);
        }
        else
        {
            XMax = GridMaxX;
            YMax = GridMaxY;
        }

        if (isFractle)
        {
            RoomSize = "-LRoom";
            RoomTypeSize = RoomPiece.RoomType.FractleCorridor;
        }
        else
        {
            FRightWall.gameObject.SetActive(false);
            FLeftWall.gameObject.SetActive(false);
            FTopWall.gameObject.SetActive(false);
            FBotWall.gameObject.SetActive(false);

            if (XMax + YMax < 48)
            {
                RoomSize = "-Small";
                RoomTypeSize = RoomPiece.RoomType.Small;
            }
            else if ((XMax + YMax) >= 48 && (XMax + YMax) < 64)
            {
                RoomSize = "-Medium";
                RoomTypeSize = RoomPiece.RoomType.Medium;
            }
            else
            {
                RoomSize = "-Large";
                RoomTypeSize = RoomPiece.RoomType.Large;
            }
        }

        if (XMax <= 20 && YMax >= 25)
        {
            RoomSize = "-VerticalCorridor";
            RoomTypeSize = RoomPiece.RoomType.VerticalCorridor;
        }
        else if (YMax <= 20 && XMax >= 25)
        {
            RoomSize = "-HorizontalCorridor";
            RoomTypeSize = RoomPiece.RoomType.HorizontalCorridor;
        }

        TheGrid.name = RoomName + RoomSize;

        if (isFractle)
        {
            XMax = YMax / 2;
        }

        RoomGrid = new TileChunk[XMax+1, YMax+1];
        RoomGrid.Initialize();

        Debug.Log("room width: " + RoomGrid.GetLength(0) + "...with x max: " + XMax);
        Debug.Log("room height: " + RoomGrid.GetLength(1) + "...with y max: " + YMax);

        SetWalls(RoomGrid, YMax, XMax);

        OutlineLeft.RemoveAt(OutlineLeft.Count - 1);
        OutlineRight.RemoveAt(OutlineRight.Count - 1);
        OutlineBottom.RemoveAt(OutlineBottom.Count - 1);
        OutlineTop.RemoveAt(OutlineTop.Count - 1);
        OutlineLeft.RemoveAt(OutlineLeft.Count - 1);
        OutlineRight.RemoveAt(OutlineRight.Count - 1);
        OutlineBottom.RemoveAt(OutlineBottom.Count - 1);
        OutlineTop.RemoveAt(OutlineTop.Count - 1);

        DoorRandomizer = 0;
        DoorChunk Left = SetSideDoors(ScriptableTileMap.SideDoorLeftBottom, ScriptableTileMap.SideDoorLeftTop, 0, DoorChunk.DoorTypeSet.LeftDoor);
        DoorChunk Right = SetSideDoors(ScriptableTileMap.SideDoorRightBottom, ScriptableTileMap.SideDoorRightTop, XMax, DoorChunk.DoorTypeSet.RightDoor);
        DoorChunk Bot = SetTopBotDoors(ScriptableTileMap.TBDoorL1, ScriptableTileMap.TBDoorR2, 0, DoorChunk.DoorTypeSet.BottomDoor);
        DoorChunk Top = SetTopBotDoors(ScriptableTileMap.TBDoorL1, ScriptableTileMap.TBDoorR2, YMax, DoorChunk.DoorTypeSet.TopDoor);
        //Debug.Log("bottom door coords: " + Bot.GetChunkCoordinates() + " world pos: " + Bot.GetChunkWorldPosition(Bot.GetChunkCoordinates().x, Bot.GetChunkCoordinates().y));
        //left is the left side door bottom piece, so left2 is left.y+1
        //right is the right side door bottom piece so right2 is right.y+1
        //bot is bottom door left most piece
        //top left most piece 

        DoorList = new List<DoorChunk>() { Left, Right, Bot, Top };

        DoFloorLayout(RoomGrid);

        if (DoRoomOutline) DoRoomOutlining();

        

        //DoCornerDetail();
        if (PlaceFloorClutter)
        {
            DoRandomClutter(RoomGrid);            
        }

        FixDoorEntrence();
        FillTileMap();

        if (isFractle)
        {
            TryFractle();
        }

        //DrawEdgeCollision(0, 0);
        //DrawEdgeCollision(YMax, YMax);

        FinalizeRoomSetup();

    }

    void SetWalls(TileChunk[,] Grid, int ylimit, int xlimit)
    {

        for (int y = 0; y < ylimit + 1; y++)
        {
            //the side walls
            Grid[0, y] = new WallChunk(0, y, 0, ScriptableTileMap.SideWallTileR, true, false, isFractle);
            Grid[xlimit, y] = new WallChunk(xlimit, y, 0, ScriptableTileMap.SideWallTileL, true, false, isFractle);
            if (Grid[xlimit, y] == null)
            {
                Debug.LogError("null grid with x length: " + Grid.GetLength(0) + " and y length: " + Grid.GetLength(1) + " with x and y limits: " + xlimit + "," + ylimit + " and coords" + xlimit + "," + y);
            }
            OutlineLeft.Add(new Vector2Int(1, y + 1));
            OutlineRight.Add(new Vector2Int(xlimit - 1, y + 1));
        }

        for (int x = 1; x < xlimit; x++) //x is set to 1 to skip the origin cuz theres a sidewall there
        {
            //the bottom walls
            Grid[x, 0] = new WallChunk(x, 0, 0, ScriptableTileMap.BotWallPiece, false, false);

            Grid[x, ylimit] = new WallChunk(x, ylimit, 0, ScriptableTileMap.BotWallPiece, false, false); //bug? why is the ylimit in the x as an x coord....
            OutlineBottom.Add(new Vector2Int(x + 1, 1));
            OutlineTop.Add(new Vector2Int(x + 1, ylimit - 1));
        }
    }

    void DoNormalFloor(TileChunk[,] Grid)
    {
        //needs to start and 1 and end at max - 1 becuase the ends are sidewalls and top and bottom walls
        Tile T1 = new Tile();
        T1 = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1);
        for (int d1 = 1; d1 < Grid.GetLength(0)-1; d1++)
        {
            for (int d2 = 1; d2 < Grid.GetLength(1)-1; d2++)
            {

                if (RandomizeFloor)
                {
                    int rand = Random.Range(0, 101);
                    if (rand <= RandomFloorChance)
                    {
                        T1 = ScriptableTileMap.RandomizedFloorTiles.GetTile(Random.Range(0, ScriptableTileMap.RandomizedFloorTiles.TileList.Count));
                    }
                    else T1 = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1);
                }
                if (Grid[d1, d2] == null) Grid[d1, d2] = new FloorChunk(d1, d2, T1);
            }
        }
    }

    void DoCheckerBoardFloor(TileChunk[,] Grid)
    {

        //if the ymax is odd, you get stripes
        //if the ymax is even, you get checkerboard
        int counter = 0;
        DoNormalFloor(Grid);
        Queue<Tile> FloorQue = new Queue<Tile>();
        Tile Floor = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1);
        FloorQue.Enqueue(ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
        FloorQue.Enqueue(ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex2));
        //i can do x and y spacing by adding to the 1 and subtracting to the limit(xmax) 
        //maybe later

        for (int d1 = CheckerStartX; d1 < (Grid.GetLength(0)-CheckerEndX); d1++)
        {
            for (int d2 = CheckerStartY; d2 < (Grid.GetLength(1)-CheckerEndY); d2++) //also maybe start d2 at 2 instead of 1
            {
                if (d1 % CheckerMod2 == CheckerMod2Variable && UseCheckerMod2) continue;
                if (counter % CheckerMod == 0)
                {
                    Floor = FloorQue.Dequeue();
                    FloorQue.Enqueue(Floor);
                }
                ReplaceFloorChunk(Grid, d1, d2, Floor);
                counter++;
            }
        }
    }

    void DoSpiralFloor(TileChunk[,] Grid)
    {
        if(!CheckerBoardFloor) DoNormalFloor(Grid);

        Debug.Log("doing spiral floor");
        Tile Floor = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex2);

        List<Vector2Int> Left = new List<Vector2Int>();
        List<Vector2Int> Right = new List<Vector2Int>();
        List<Vector2Int> Top = new List<Vector2Int>();
        List<Vector2Int> Bottom = new List<Vector2Int>();

        Left = SpiralizeList(OutlineLeft, true, 1);
        Right = SpiralizeList(OutlineRight, true, -1);
        Top = SpiralizeList(OutlineTop, false, -1);
        Bottom = SpiralizeList(OutlineBottom, false, 1);

        for (int i = 0; i < SpiralCount; i++)
        {
            //need to pass in the changed lists

            foreach (Vector2Int V in Left)
            {
                ReplaceFloorChunk(V.x, V.y, Floor);
            }

            foreach (Vector2Int V in Right)
            {
                ReplaceFloorChunk(V.x, V.y, Floor);
            }

            foreach (Vector2Int V in Top)
            {
                ReplaceFloorChunk(V.x, V.y, Floor);
            }

            foreach (Vector2Int V in Bottom)
            {
                ReplaceFloorChunk(V.x, V.y, Floor);
            }

            Left = SpiralizeList(Left, true, 1);
            Right = SpiralizeList(Right, true, -1);
            Top = SpiralizeList(Top, false, -1);
            Bottom = SpiralizeList(Bottom, false, 1);
        }

    }


    private List<Vector2Int> SpiralizeList(List<Vector2Int> List, bool side, int sign)
    {
        List<Vector2Int> TheList = new List<Vector2Int>(List);

        for (int i = 0; i < SpiralDeviation; i++)
        {
            if (TheList.Count > 0)
            {
                TheList.RemoveAt(TheList.Count - 1);

                if (TheList.Count > 0) TheList.RemoveAt(0);
                else break;
            }
        }

        if(side)
        {
            for (int i = 0; i < TheList.Count; i++)
            {
                TheList[i] = new Vector2Int(TheList[i].x + (2 * sign), TheList[i].y);
                //TheList.Add(new Vector2Int(TheList[i].x + (2 * sign), TheList[i].y));
            }
        }
        else
        {
            for (int i = 0; i < TheList.Count; i++)
            {
                TheList[i] = new Vector2Int(TheList[i].x, TheList[i].y + (2*sign));
                //TheList.Add(new Vector2Int(TheList[i].x, TheList[i].y + (2 * sign)));
            }
        }

        return TheList;
    }


    public void TryFractle()
    {


        #region FractleNotes
        //bot left corner is new origin

        //-ymax,offset === 0,0
        //new ymax = ymax - xmax
        //new xmax = -ymax

        //a ccw90 and a cw90 rotation formula is Xprime = X1 + X2
        //and Yprime = Y1 + Y2

        //X1 and Y1 are the original points to convert and X2 and Y2 are the new origin
        //xprime and yprime are the new points after conversion

        // to change from ccw and cw just change the sign of x
        //to change vertical position of the matrix, just add or subtract to y


        //a bottom left origin is a cw90
        //a bottom right origin is a ccw90

        //new origin is -ymax, ymax - xmax for cw90
        //new origin for ccw90 is ymax, ymax - xmax

        //for the others it is 0, ymax - xmax and xmax, ymax - xmax
        #endregion

        int NewYorigin = (YMax - XMax) + FYoffset; //adding a positive widenfactor, makes it skinnier...
        int NewXorigin = ((YMax/2) + FXoffset) * -1; 

        int FXmax = (YMax / 2) + (FatnessFactor);
        int FYmax = (YMax / 2) + ThiccnessFactor;


        FractleGrid = new TileChunk[FXmax + 1, FYmax + 1]; //xmax is now y max
        FractleGrid.Initialize();

        int Xoffset = 0;
        int Yoffset = 0;

        //doesnt set the top part of the top/bot walls
        SetWalls(FractleGrid, FYmax, FXmax);

        DoFloorLayout(FractleGrid);


        //bot
        int Xoffset2 = FXmax / 2 + ((NewXorigin - (XMax)));
        int Yoffset2 = 0 + NewYorigin + ((YMax / 2));
        Vector3 Bot = FractleGrid[0, 0].GetChunkWorldPosition(FXmax / 2, 0, Xoffset2, Yoffset2);
        
        //left
        Xoffset2 = 0 + ((NewXorigin - (XMax)));
        Yoffset2 = (FYmax/2) + NewYorigin + ((YMax / 2));
        Vector3 Left = FractleGrid[0, 0].GetChunkWorldPosition(0, (FYmax / 2), Xoffset2, Yoffset2);

        //top
        Xoffset2 = (FXmax/2) + ((NewXorigin - (XMax)));
        Yoffset2 = FYmax + NewYorigin + ((YMax / 2));
        Vector3 Top = FractleGrid[0, 0].GetChunkWorldPosition((FXmax / 2), FYmax, Xoffset2, Yoffset2);
        Debug.Log("top door vector: " + Top); //part 1
        //right
        Xoffset2 = FXmax + ((NewXorigin - (XMax)));
        Yoffset2 = (FYmax/2) + NewYorigin + ((YMax / 2));
        Vector3 Right = FractleGrid[0, 0].GetChunkWorldPosition(FXmax, (FYmax / 2), Xoffset2, Yoffset2);

        FractleDoorJointBlockers.Add(DoorChunk.DoorTypeSet.BottomDoor, new Vector3Int((int)Bot.x, (int)Bot.y, 0));
        FractleDoorJointBlockers.Add(DoorChunk.DoorTypeSet.LeftDoor, new Vector3Int((int)Left.x, (int)Left.y, 0));
        FractleDoorJointBlockers.Add(DoorChunk.DoorTypeSet.TopDoor, new Vector3Int((int)Top.x, (int)Top.y, 0));
        FractleDoorJointBlockers.Add(DoorChunk.DoorTypeSet.RightDoor, new Vector3Int((int)Right.x, (int)Right.y,0));
        

        FractleDoorEdges.Add(DoorChunk.DoorTypeSet.BottomDoor, FixFractleDoorOffset(Bot, 0.875f, 2.15f));
        FractleDoorEdges.Add(DoorChunk.DoorTypeSet.TopDoor, FixFractleDoorOffset(Top, 0.875f, 2.15f));
        FractleDoorEdges.Add(DoorChunk.DoorTypeSet.LeftDoor, FixFractleDoorOffset(Left, 0.775f, 3.225f));
        FractleDoorEdges.Add(DoorChunk.DoorTypeSet.RightDoor, FixFractleDoorOffset(Right, -1.325f, 3.225f));

        if (FBlockBotDoor)
        {
            //do nothing
        }       
        else
        {
            FractleGrid[FXmax / 2, 0] = new DoorChunk(FXmax / 2, 0, DoorChunk.DoorTypeSet.BottomDoor, ScriptableTileMap.TBDoorL1); //left piece
            FractleGrid[(FXmax / 2) + 1, 0] = new DoorChunk((FXmax / 2) + 1, 0, DoorChunk.DoorTypeSet.BottomDoor, ScriptableTileMap.TBDoorR2); //right piece
            FractleDoorList.Add(FractleGrid[FXmax / 2, 0] as DoorChunk);
        }
        if(FBlockLeftDoor)
        {
            //do nothing
        }
        else
        {
            FractleGrid[0, FYmax / 2] = new DoorChunk(0, FYmax / 2, DoorChunk.DoorTypeSet.LeftDoor, ScriptableTileMap.SideDoorLeftBottom);
            FractleGrid[0, (FYmax / 2) + 1] = new DoorChunk(0, (FYmax / 2) + 1, DoorChunk.DoorTypeSet.LeftDoor, ScriptableTileMap.SideDoorLeftTop);
            FractleDoorList.Add(FractleGrid[0, FYmax / 2] as DoorChunk);
        }
        if(FBlockTopDoor)
        {
            //do nothing
        }
        else
        {
            FractleGrid[FXmax / 2, FYmax] = new DoorChunk(FXmax / 2, FYmax, DoorChunk.DoorTypeSet.TopDoor, ScriptableTileMap.TBDoorL1);
            FractleGrid[(FXmax / 2) + 1, FYmax] = new DoorChunk((FXmax / 2) + 1, FYmax, DoorChunk.DoorTypeSet.TopDoor, ScriptableTileMap.TBDoorR2);
            FractleDoorList.Add(FractleGrid[FXmax / 2, FYmax] as DoorChunk);
        }
        if(FBlockRightDoor)
        {
            //do nothing
        }
        else
        {
            FractleGrid[FXmax, FYmax / 2] = new DoorChunk(FXmax, FYmax / 2, DoorChunk.DoorTypeSet.RightDoor, ScriptableTileMap.SideDoorRightBottom);
            FractleGrid[FXmax, (FYmax / 2) + 1] = new DoorChunk(FXmax, (FYmax / 2) + 1, DoorChunk.DoorTypeSet.RightDoor, ScriptableTileMap.SideDoorRightTop);
            FractleDoorList.Add(FractleGrid[FXmax, FYmax / 2] as DoorChunk);
        }


        SetTopFractleWalls(FractleGrid, FYmax, FXmax, NewXorigin, NewYorigin);

        //sets the tiles and do clutter if selected
        int Rand = 0;

        for (int d1 = 0; d1 < FXmax + 1; d1++)
        {
            for (int d2 = 0; d2 < FYmax + 1; d2++)
            {
                if (FractleGrid[d1, d2] == null)
                {
                    Debug.Log("room grid is null at x: " + d1 + " and y: " + d2);
                    continue;
                }

                Xoffset = d1 + ((NewXorigin - (XMax))); 
                Yoffset = d2 + NewYorigin + ((YMax/2));

                TestTile = FractleGrid[d1, d2].GetChunkTile();
                TestTile.color = Color.white;

                //for the side doors
                if (FractleGrid[d1, d2] is DoorChunk)
                {
                    TestTile = FractleGrid[d1, d2].GetChunkTile();
                    Vector3Int V2 = FractleGrid[0, 0].GetChunkWorldPosition(d1, d2, Xoffset, Yoffset);

                    //always gets 2nd door chunk
                    if(FractleDoors.ContainsKey((FractleGrid[d1,d2] as DoorChunk).GetDoorType()))
                    {
                        FractleDoors.Remove((FractleGrid[d1, d2] as DoorChunk).GetDoorType());
                        FractleDoors.Add(((FractleGrid[d1, d2] as DoorChunk).GetDoorType()), V2);
                        Debug.Log("adding f door " + (FractleGrid[d1, d2] as DoorChunk).GetDoorType() + " with pos: " + V2 + " and coords: " + d1 + " , " + d2); //part 2
                    }
                    else FractleDoors.Add(((FractleGrid[d1, d2] as DoorChunk).GetDoorType()), V2);

                    //Debug.Log("setting a fractle door at: " + V2);
                    //TestTile.color = Color.white;
                    TopLayerMap.SetTile(V2, TestTile);

                    //CHANGE
                    TestTile = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1);
                } // end if door chunk


                if (FractleGrid[d1, d2] is WallChunk)
                {

                    WallChunk Wc = FractleGrid[d1, d2] as WallChunk;
                    if (Wc.IsSideWall)
                    {
                        if (d1 == 0) FLeftWall.SetTile(FractleGrid[d1, d2].GetChunkWorldPosition(d1, d2, Xoffset, Yoffset), TestTile);
                        else FRightWall.SetTile(FractleGrid[d1, d2].GetChunkWorldPosition(d1, d2, Xoffset, Yoffset), TestTile);
                    }
                    else if (!Wc.IsTopWall) // if its a bottom wall
                    {
                        if (d2 == 0) FBotWall.SetTile(FractleGrid[d1, d2].GetChunkWorldPosition(d1, d2, Xoffset, Yoffset), TestTile);
                        else FTopWall.SetTile(FractleGrid[d1, d2].GetChunkWorldPosition(d1, d2, Xoffset, Yoffset), TestTile);
                    }


                } // end if wall chunk
                else // otherwise just set a floor tile
                {
                    TestTile.color = FloorSprite1Color;
                    FractleMap.SetTile(FractleGrid[d1, d2].GetChunkWorldPosition(d1, d2, Xoffset, Yoffset), TestTile);
                    TestTile.color = Color.white;
                }

                if(PlaceFloorClutter)
                {
                    if (FractleGrid[d1, d2] is FloorChunk)
                    {
                        Rand = Random.Range(0, 101);
                        if (Rand <= FloorClutterChance)
                        {
                            TestTile = ScriptableTileMap.FloorClutter[Random.Range(0, ScriptableTileMap.FloorClutter.Count)];
                            DoodadLayer.SetTile(FractleGrid[d1, d2].GetChunkWorldPosition(d1, d2, Xoffset, Yoffset), TestTile);
                        }
                    }
                }


            } // end nested for
            
        }// end for


        #region bullshit
        //sets tiles on toplayer, top walls only
        //Vector3Int V = new Vector3Int();
        //int j = 0;
        //Tile t1 = new Tile();
        //Tile t2 = new Tile();

        //for (int i = 1; i < FXmax; i++)
        //{
        //    j = Random.Range(0, 101);

        //    if (DoSpecialClutter)
        //    {
        //        //if (j <= SpecialClutterChance) t1 = ScriptableTileMap.TopWallDeviations[Random.Range(0, ScriptableTileMap.TopWallDeviations.Count)];
        //        //else t1 = ScriptableTileMap.TopWallPiece;

        //        //j = Random.Range(0, 101);
        //        //if (j <= SpecialClutterChance) t2 = ScriptableTileMap.TopWallDeviations[Random.Range(0, ScriptableTileMap.TopWallDeviations.Count)];
        //        //else t2 = ScriptableTileMap.TopWallPiece;
        //        Debug.Log("special wall clutter for fractle not done yet");

        //    }
        //    else
        //    {
        //        t1 = ScriptableTileMap.TopWallPiece;
        //        t2 = ScriptableTileMap.TopWallPiece;
        //    }

        //    //if (FractleGrid[i, FYmax] is DoorChunk && !FBlockTopDoor)
        //    //{
        //    //    Xoffset = i + ((NewXorigin - (XMax)));
        //    //    Yoffset = NewYorigin + ((YMax / 2));
        //    //    V = FractleGrid[i, FYmax].GetChunkWorldPosition(i, 0, Xoffset, Yoffset);
        //    //    TopLayerMap.SetTile(V, FractleGrid[i,FYmax].GetChunkTile());
        //    //    Debug.Log("SETTING TOP F DOOR WITH SPRITE: " + TestTile.sprite.name);

        //    //    if (FractleGrid[i, 0] is DoorChunk && !FBlockBotDoor)
        //    //    {
        //    //        Xoffset = i + ((NewXorigin - (XMax)));
        //    //        Yoffset = FYmax + NewYorigin + ((YMax / 2));
        //    //        V = FractleGrid[i, FYmax].GetChunkWorldPosition(i, FYmax, Xoffset, Yoffset);
        //    //        TopLayerMap.SetTile(V, FractleGrid[i,0].GetChunkTile());
        //    //        Debug.Log("JUST SET BOTTOM F DOOR WITH SPRITE: " + TestTile.sprite.name);
        //    //        continue;
        //    //    }
        //    //    continue;
        //    //}

        //    ////Debug.Log("checking i: " + i + " oh and btw is door chunk ?: " + (FractleGrid[i,0] is DoorChunk));
        //    //if (FractleGrid[i, 0] is DoorChunk)
        //    //{
        //    //    Debug.Log("found door chunk on coord" + i + ",0" + " with type: " + (FractleGrid[i, 0] as DoorChunk).GetDoorType());
        //    //    if (!FBlockBotDoor) Debug.Log("also fbot door is false");
        //    //}


        //    Xoffset = i + ((NewXorigin - (XMax)));
        //    Yoffset = NewYorigin + ((YMax / 2));

        //    V = FractleGrid[i, 0].GetChunkWorldPosition(i, 0, Xoffset, Yoffset);
        //    TestTile = t1;
        //    TopLayerMap.SetTile(V, TestTile);
        //    Yoffset = FYmax + NewYorigin + ((YMax / 2));
        //    V = FractleGrid[i, FYmax].GetChunkWorldPosition(i, FYmax, Xoffset, Yoffset);
        //    TestTile = t2;
        //    TopLayerMap.SetTile(V, TestTile);
        //}

        //foreach(KeyValuePair<DoorChunk.DoorTypeSet, Vector3> K in FractleDoors)
        //{
        //    Debug.Log(K.Key + " " + K.Value);
        //}
        #endregion

    }


    private void SetTopFractleWalls(TileChunk[,] Grid, int ylimit, int xlimit, int NewXorigin, int NewYorigin)
    {
        Vector3Int V = new Vector3Int();
        int Xoffset = 0;
        int Yoffset = 0;

        for (int i = 1; i < xlimit; i++)
        {

            Xoffset = i + ((NewXorigin - (XMax)));
            Yoffset = NewYorigin + ((YMax / 2));
            V = FractleGrid[i, 0].GetChunkWorldPosition(i, 0, Xoffset, Yoffset);

            if (!(Grid[i, 0] is DoorChunk)) TopLayerMap.SetTile(V, ScriptableTileMap.TopWallPiece);

            Yoffset = ylimit + NewYorigin + ((YMax / 2));
            V = FractleGrid[i, 0].GetChunkWorldPosition(i, ylimit, Xoffset, Yoffset);

            if (!(Grid[i, ylimit] is DoorChunk)) TopLayerMap.SetTile(V, ScriptableTileMap.TopWallPiece);
        }
    }
    
    public DoorChunk SetSideDoors(Tile TopPartDoor, Tile BottomPartDoor, int Side, DoorChunk.DoorTypeSet Type)
    {
        int yupper = YMax - (int)(YDoorLimiter * YMax);
        int ylower = (int)(YDoorLimiter * YMax);
        yupper++;
        int y = 0;

        if (DoorRandomizer == 0)
        {
            y = Random.Range(ylower, yupper);
            DoorRandomizer = y;
        }

        if (DoorSymmetry) y = DoorRandomizer;
        else y = Random.Range(ylower, yupper);

        if (CenteredDoors) y = YMax / 2;
        //left side so side = 0, when called again below side = xmax
        RoomGrid[Side, y] = new DoorChunk(Side, y, Type, TopPartDoor);
        RoomGrid[Side, y + 1] = new DoorChunk(Side, y, Type, BottomPartDoor); // its y + 1 because the chunk above needs the 2nd part of the door

        return RoomGrid[Side, y] as DoorChunk;

    }

    public DoorChunk SetTopBotDoors(Tile DoorL, Tile DoorR, int Side, DoorChunk.DoorTypeSet Type)
    {
        int xupper = XMax - (int)(XDoorLimiter*XMax);
        int xlower = (int)(XDoorLimiter*XMax);
        xupper++;
        int x = 0;

        if (DoorRandomizer == 0)
        {
            x = Random.Range(xlower, xupper);
            DoorRandomizer = x;
        }
        else if (DoorSymmetry)
        {
            if (DoorRandomizer >= xupper || DoorRandomizer <= xlower) DoorRandomizer = Random.Range(xlower, xupper);
            x = DoorRandomizer;
        }
        else x = Random.Range(xlower, xupper);

        if (CenteredDoors) x = XMax / 2;

        //Debug.Log("using xupper: " + xupper + " with xlower: " + xlower + " and randomzier: " + DoorRandomizer + " and x: " + x + " with side: " + Side);
        if (RoomGrid[x, Side] == null) Debug.LogError("out of range with x: " + x + " and y: " + Side);
        RoomGrid[x, Side] = new DoorChunk(x, Side, Type, DoorL); //left most piece

        if (RoomGrid[x + 1, Side] == null) Debug.LogError("out of range with x: " + (x + 1) + " and y: " + Side);
        RoomGrid[x + 1, Side] = new DoorChunk(x, Side, Type, DoorR); //1 to the right

        return RoomGrid[x, Side] as DoorChunk;

    }

    public void FixDoorEntrence()
    {

        Vector2Int LeftDoor = new Vector2Int((DoorList[0].GetChunkCoordinates().x + 1), DoorList[0].GetChunkCoordinates().y);
        Vector2Int LeftDoor2 = new Vector2Int(LeftDoor.x, LeftDoor.y + 1);
        Vector2Int RightDoor = new Vector2Int((DoorList[1].GetChunkCoordinates().x - 1), DoorList[1].GetChunkCoordinates().y);
        Vector2Int RightDoor2 = new Vector2Int(RightDoor.x, RightDoor.y + 1);
        Vector2Int BottomDoor = new Vector2Int(DoorList[2].GetChunkCoordinates().x, DoorList[2].GetChunkCoordinates().y + 1);
        Vector2Int BottomDoor2 = new Vector2Int((BottomDoor.x+1), BottomDoor.y);
        Vector2Int TopDoor = new Vector2Int(DoorList[3].GetChunkCoordinates().x, DoorList[3].GetChunkCoordinates().y - 1);
        Vector2Int TopDoor2 = new Vector2Int(TopDoor.x+1, TopDoor.y);


        //this is a list of grid coords that are RIGHT IN FRONT OF EACH DOOR, NOT THE DOOR COORDS
        //List<Vector2> list = new List<Vector2>() { LeftDoor, LeftDoor2, RightDoor, RightDoor2, BottomDoor, BottomDoor2, TopDoor, TopDoor2 };

        //if you use door symmetry you can make a carpet or rug tile go across to the next door

       // if (!CheckerBoardFloor)
        //{
            if (!BlockLeftDoor)
            {
                RoomGrid[LeftDoor.x, LeftDoor.y] = new FloorChunk(LeftDoor.x, LeftDoor.y, ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
                RoomGrid[LeftDoor2.x, LeftDoor2.y] = new FloorChunk(LeftDoor2.x, LeftDoor2.y, ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
            }

            if (!BlockRightDoor)
            {
                RoomGrid[RightDoor.x, RightDoor.y] = new FloorChunk(RightDoor.x, RightDoor.y, ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
                RoomGrid[RightDoor2.x, RightDoor2.y] = new FloorChunk(RightDoor2.x, RightDoor2.y, ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
            }

            if (!BlockTopDoor)
            {
                RoomGrid[TopDoor.x, TopDoor.y] = new FloorChunk(TopDoor.x, TopDoor.y, ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
                RoomGrid[TopDoor2.x, TopDoor2.y] = new FloorChunk(TopDoor2.x, TopDoor2.y, ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
            }

            if (!BlockBottomDoor)
            {
                RoomGrid[BottomDoor.x, BottomDoor.y] = new FloorChunk(BottomDoor.x, LeftDoor.y, ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
                RoomGrid[BottomDoor2.x, BottomDoor.y] = new FloorChunk(BottomDoor2.x, LeftDoor2.y, ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1));
            }
        //}



        if ((DoorSymmetry || CenteredDoors) && SetHRug) LayHorizontalRug(LeftDoor2, XMax);

        if ((DoorSymmetry || CenteredDoors) && SetVRug) LayVerticalRug(BottomDoor2, YMax);

        //DOOR BLOCKING
        //l and r is bottom piece
        //t and b is left piece
        //bp = bottom piece
        // lp = left piece, rp = right piece

        Vector3Int LeftBP = DoorList[0].GetChunkCoordinates();
        Vector3Int LeftTP = new Vector3Int(LeftBP.x, LeftBP.y + 1, 0);
        Vector3Int RightBP = DoorList[1].GetChunkCoordinates();
        Vector3Int RightTP = new Vector3Int(RightBP.x, RightBP.y + 1, 0);
        Vector3Int BottomLP = DoorList[2].GetChunkCoordinates();
        Vector3Int BottomRP = new Vector3Int(BottomLP.x + 1, BottomLP.y, 0);
        Vector3Int TopLP = DoorList[3].GetChunkCoordinates();
        Vector3Int TopRP = new Vector3Int(TopLP.x + 1, TopLP.y, 0);

        if (BlockLeftDoor)
        {
            //replace the left door with side walls
            ReplaceWallChunk(LeftBP.x, LeftBP.y, 0, true, false, ScriptableTileMap.SideWallTileR);
            ReplaceWallChunk(LeftTP.x, LeftTP.y, 0, true, false, ScriptableTileMap.SideWallTileR);
        }
        if (BlockRightDoor)
        {
            ReplaceWallChunk(RightBP.x, RightBP.y, 0, true, false, ScriptableTileMap.SideWallTileL);
            ReplaceWallChunk(RightTP.x, RightTP.y, 0, true, false, ScriptableTileMap.SideWallTileL);
        }
        if (BlockBottomDoor)
        {
            ReplaceWallChunk(BottomLP.x, BottomLP.y, 0, false, false, ScriptableTileMap.BotWallPiece);
            ReplaceWallChunk(BottomRP.x, BottomRP.y, 0, false, false, ScriptableTileMap.BotWallPiece);

        }
        if (BlockTopDoor)
        {
            ReplaceWallChunk(TopLP.x, TopLP.y, 0, false, true, ScriptableTileMap.BotWallPiece);
            ReplaceWallChunk(TopRP.x, TopRP.y, 0, false, true, ScriptableTileMap.BotWallPiece);
        }
        

    }

    //unused for now
    private Vector2 GetDoorLimits(int max, int limiter)
    {
        Vector2 Limits;

        int x = max - (int)(limiter * max);
        int y = (int)(limiter * max);

        Limits = new Vector2(x, y);


        return Limits;
    }

    public void DoRoomOutlining()
    {

        foreach (Vector2 V in OutlineLeft)
        {
            ReplaceFloorChunk((int)V.x, (int)V.y, ScriptableTileMap.OutlineTileLeft);
        }

        foreach(Vector2 V in OutlineRight)
        {
            ReplaceFloorChunk((int)V.x, (int)V.y, ScriptableTileMap.OutlineTileRight);
        }

        foreach (Vector2 V in OutlineTop)
        {
            ReplaceFloorChunk((int)V.x, (int)V.y, ScriptableTileMap.OutlineTileUp);
        }

        if (OutlineBottomRoom)
        {

            foreach (Vector2 V in OutlineBottom)
            {
                ReplaceFloorChunk((int)V.x, (int)V.y, ScriptableTileMap.OutlineTileDown);
            }

            RoomGrid[XMax - 1, 1] = new FloorChunk(XMax - 1, 1, ScriptableTileMap.CornerOutlineBR); //bottom right

            RoomGrid[1, 1] = new FloorChunk(1, 1, ScriptableTileMap.CornerOutlineBL); //bottom left
        }


        RoomGrid[XMax - 1, YMax - 1] = new FloorChunk(XMax - 1, YMax - 1, ScriptableTileMap.CornerOutlineTR); //top right corner

        RoomGrid[1, YMax - 1] = new FloorChunk(1, YMax - 1, ScriptableTileMap.CornerOutlineTL); //top left

        //need to take the two bottom corners and replace them with normal side outline tiles if no outlinebottomroom
        ReplaceFloorChunk(1, 1, ScriptableTileMap.OutlineTileLeft);
        ReplaceFloorChunk(XMax - 1, 1, ScriptableTileMap.OutlineTileRight);

    }

    //private void DoCornerDetail()
    //{
    //    TestTile.sprite = ScriptableTileMap.CornerDetail1;

    //    if (TopLeftDetail) DoodadLayer.SetTile(new Vector3Int((5*2) + TopLeftOffset.x, ((YMax - 5)*2) + TopLeftOffset.y, 0), TestTile);
    //    if (TopRightDetail) DoodadLayer.SetTile(new Vector3Int(((XMax - 5)*2) + TopRightOffset.x, ((YMax - 5)*2) + TopRightOffset.y, 0), TestTile);
    //    if (BottomLeftDetail) DoodadLayer.SetTile(new Vector3Int((5*2) + BottomLeftOffset.x, (5*2) + BottomLeftOffset.y, 0), TestTile);
    //    if (BottomRightDetail) DoodadLayer.SetTile(new Vector3Int(((XMax - 5)*2) + BottomRightOffset.x, (5*2) + BottomRightOffset.y, 0), TestTile);

    //}


    private void DoRandomClutter(TileChunk[,] Grid)
    {
        
        int Rand = 0;

        for (int d1 = 0; d1 < Grid.GetLength(0); d1++)
        {
            for (int d2 = 0; d2 < Grid.GetLength(1); d2++)
            {
                if(Grid[d1,d2] is FloorChunk)
                {
                    if (CleanRug && (Grid[d1, d2] is RugChunk)) continue;
                    Rand = Random.Range(0, 101);
                    if(Rand <= FloorClutterChance)
                    {
                        TestTile = ScriptableTileMap.FloorClutter[Random.Range(0, ScriptableTileMap.FloorClutter.Count)];
                        DoodadLayer.SetTile(Grid[d1, d2].GetChunkWorldPosition(d1, d2), TestTile);
                    }
                }

            }
        }
    }


    //unused
    public void DoLRug()
    {
        Vector2Int LeftDoor = new Vector2Int((DoorList[0].GetChunkCoordinates().x + 1), DoorList[0].GetChunkCoordinates().y);
        Vector2Int LeftDoor2 = new Vector2Int(LeftDoor.x, LeftDoor.y + 1);

        Vector2Int RightDoor = new Vector2Int((DoorList[1].GetChunkCoordinates().x - 1), DoorList[1].GetChunkCoordinates().y);
        Vector2Int RightDoor2 = new Vector2Int(RightDoor.x, RightDoor.y + 1);

        Vector2Int BottomDoor = new Vector2Int(DoorList[2].GetChunkCoordinates().x, DoorList[2].GetChunkCoordinates().y + 1);
        Vector2Int BottomDoor2 = new Vector2Int((BottomDoor.x + 1), BottomDoor.y);

        Vector2Int TopDoor = new Vector2Int(DoorList[3].GetChunkCoordinates().x, DoorList[3].GetChunkCoordinates().y - 1);
        //Vector2Int TopDoor2 = new Vector2Int(TopDoor.x + 1, TopDoor.y);


        //maybe i should pass in an overside replacement as a argument
        //up left or left down L rug
        LayHorizontalRug(LeftDoor2, XMax / 2);
        LayVerticalRug(BottomDoor2, YMax / 2);

        //down right or left up L rug
        LayHorizontalRug(RightDoor2, XMax / 2);
        LayVerticalRug(BottomDoor2, YMax / 2);

    }

    private void ReplaceFloorChunk(TileChunk[,] Grid, int x, int y, Tile NewTile)
    {
        Grid[x, y] = new FloorChunk(x, y, NewTile);
    }

    private void ReplaceFloorChunk(int x, int y, Tile NewTile)
    {
        RoomGrid[x, y] = new FloorChunk(x, y, NewTile);
    }  

    private void ReplaceWallChunk(int x, int y, int z, bool isSidewall, bool istopwall, Tile NewTile)
    {
        RoomGrid[x, y] = new WallChunk(x, y, z, NewTile, isSidewall, istopwall, isFractle);
    }

    private void ReplaceFractleWall(int x, int y, int z, bool isSidewall, bool istopwall, Tile NewTile)
    {
        FractleGrid[x, y] = new WallChunk(x, y, z, NewTile, isSidewall, istopwall, isFractle);
    }

    public void ClearAll()
    {
        
        if (TheGrid != null && TheGrid.gameObject.activeInHierarchy) Destroy(TheGrid.gameObject);

        if (TheGrid != null && TheGrid.gameObject.GetComponent<RoomPiece>() != null)
        {
            TheGrid.gameObject.GetComponent<RoomPiece>().AllJoints.Clear();
            Destroy(TheGrid.gameObject.GetComponent<RoomPiece>());
        }
        FractleDoorJointBlockers.Clear();
        
        FractleDoorEdges.Clear();
        DoorEdgePoints.Clear();
        RoomDoors.Clear();
        AllDoorJoints.Clear();
        FractleDoors.Clear();
        DoorList.Clear();
        FractleDoorJoints.Clear();
        OutlineTop.Clear();
        OutlineRight.Clear();
        OutlineLeft.Clear();
        OutlineBottom.Clear();

        DoFloorLayout -= DoCheckerBoardFloor;
        DoFloorLayout -= DoSpiralFloor;
    }


    public void LayHorizontalRug(Vector2 LeftDoor2, int Limit)
    {
        //left door 2 is the top piece
        Vector2 LeftDoorBot = new Vector2(LeftDoor2.x, LeftDoor2.y-1);


        for (int i = 0; i < Limit-1; i++)
        {
            int x = (int)LeftDoor2.x + i;
            int y = (int)LeftDoor2.y;

            RoomGrid[x, y] = new RugChunk(x, y, ScriptableTileMap.RugTileUp);

            x = (int)LeftDoorBot.x + i;
            y = (int)LeftDoorBot.y;

            RoomGrid[x, y] = new RugChunk(x, y, ScriptableTileMap.RugTileDown);

                
        }

    }

    public void LayVerticalRug(Vector2 BottomDoor2, int Limit)
    {
        //left most piece (I said right most before...i dunno what i was refering to)
        Vector2 BottomDoorLeft = new Vector2(BottomDoor2.x - 1, BottomDoor2.y);
        Vector2Int OversidedRugFixCoords = new Vector2Int();
        bool flag = true;

        for (int i = 0; i < Limit - 1; i++)
        {
            int x = (int)BottomDoor2.x;
            int y = (int)BottomDoor2.y + i;

            RoomGrid[x, y] = new RugChunk(x, y, ScriptableTileMap.RugTileRight);

            x = (int)BottomDoorLeft.x;
            y = (int)BottomDoorLeft.y + i;

            //this is the leftmost tile on the rug
            if (flag && RoomGrid[x, y] is RugChunk)
            {
                OversidedRugFixCoords = new Vector2Int(x, y);
                flag = false;
                Debug.Log("overside coords have been set");
            }
            RoomGrid[x, y] = new RugChunk(x, y, ScriptableTileMap.RugTileLeft);



        }

        //ReplaceFloorChunk(OversidedRugFixCoords.x, OversidedRugFixCoords.y, ScriptableTileMap.BigRug1); //just for testing
        if(SetHRug) RoomGrid[OversidedRugFixCoords.x, OversidedRugFixCoords.y] = new RugChunk(OversidedRugFixCoords.x, OversidedRugFixCoords.y, -1, ScriptableTileMap.BigRug1);

        //need to get this chunks world position and add 1.05f to each x and y coord, that new coord is the top right corner of the chunk
        //at that location, place a tile
    }


    public void FillTileMap()
    {

        //this fixes the topwalls and layers them on top
        FinishTopPieceWallLayering(RoomGrid, XMax, YMax);

        Color OriginalColor = Color.white;
        Tile Floor1 = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1);
        Tile Floor2 = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex2);

        for (int d1 = 0; d1 < XMax+1; d1++)
        {
            for (int d2 = 0; d2 < YMax+1; d2++)
            {
                if (RoomGrid[d1, d2] == null)
                {
                    Debug.Log("room grid is null at x: " + d1 + " and y: " + d2);
                    continue;
                }
                TestTile = RoomGrid[d1, d2].GetChunkTile();
                TestTile.color = Color.white;
                if (RoomGrid[d1, d2] is DoorChunk)
                {
                    TestTile.color = Color.white;
                    TopLayerMap.SetTile(RoomGrid[d1, d2].GetChunkWorldPosition(d1, d2), TestTile);

                    TestTile = ScriptableTileMap.DungeonSpecificFloorTiles.GetTile(FloorSpriteIndex1); //underneath door

                    if (TestTile == Floor1) TestTile.color = FloorSprite1Color;
                    else if (TestTile == Floor2) TestTile.color = FloorSprite2Color;
                    else TestTile.color = OriginalColor;

                    TheTileMap.SetTile(RoomGrid[d1, d2].GetChunkWorldPosition(d1, d2), TestTile);
                    continue;
                }
                else if (RoomGrid[d1, d2] is WallChunk)
                {
                    WallChunk C = RoomGrid[d1, d2] as WallChunk;
                    if (C.IsSideWall)
                    {
                        TopLayerMap.SetTile(RoomGrid[d1, d2].GetChunkWorldPosition(d1, d2), TestTile);
                        continue;
                    }
                }



                if (TestTile.sprite == Floor1) TestTile.color = FloorSprite1Color;
                else if (TestTile.sprite == Floor2) TestTile.color = FloorSprite2Color;
                else TestTile.color = OriginalColor;

                TestTile.colliderType = RoomGrid[d1, d2].GetChunkCollider();
                TheTileMap.SetTile(RoomGrid[d1, d2].GetChunkWorldPosition(d1, d2), TestTile);

            }
        } // end of for

    }

    public void FinishTopPieceWallLayering(TileChunk[,] Grid, int xlimit, int ylimit)
    {
        //i could put this on a new grid that overlays on top.....hmmmm
        Vector3Int V = new Vector3Int();
        int SpClutter = 0;

        Tile t1 = ScriptableTileMap.TopWallPiece;
        Tile t2 = ScriptableTileMap.TopWallPiece;
        Tile b1 = ScriptableTileMap.BotWallPiece;


        //iterate over the top walls in the x 
        for (int i = 1; i < xlimit; i++)
        {

            //this is a little complicated...so this is how it works
            //this function uses a sperate tile variable, (t1 and t2) and sets the tiles manually,
            //then this special wall clutter repalces the tile in the grid with the bottome special piece
            // the top piece and bottom piece share the same grid location so the top piece is set manually while
            // the bottom piece just has its tile set and is set later in the layering function 

            SpClutter = Random.Range(0, 101);
            if (SpecialWallClutter && (ScriptableTileMap.SpecialBotWallPieces.Count > 0) && (ScriptableTileMap.SpecialTopWallPieces.Count > 0) && (SpClutter <= SpecialClutterChance))
            {
                int rand = Random.Range(0, ScriptableTileMap.SpecialTopWallPieces.Count);
                t1 = ScriptableTileMap.SpecialTopWallPieces[rand];
                b1 = ScriptableTileMap.SpecialBotWallPieces[rand];

                if (!(Grid[i, ylimit] is DoorChunk)) Grid[i, ylimit].SetChunkTile(b1); // set the wall chunk top piece to random special top piece

                SpClutter = Random.Range(0, 101);
                if (SpClutter <= SpecialClutterChance*2) //roll again for top part
                {
                    rand = Random.Range(0, ScriptableTileMap.SpecialTopWallPieces.Count);
                    t2 = ScriptableTileMap.SpecialTopWallPieces[rand];
                    b1 = ScriptableTileMap.SpecialBotWallPieces[rand];
                    if (!(Grid[i, 0] is DoorChunk)) Grid[i, 0].SetChunkTile(b1);
                }
            }
            else
            {
                t1 = ScriptableTileMap.TopWallPiece;
                t2 = ScriptableTileMap.TopWallPiece;
            }

            if (Grid[i, ylimit] == null || Grid[i, 0] == null)
            {
                Debug.Log("skipping");
                continue;
            }

            //else if (Grid[i, YMax] is DoorChunk || Grid[i,0] is DoorChunk) continue;


            
            //set lowerbound top chunk
            if (!(Grid[i, 0] is DoorChunk))
            {
                V = Grid[i, ylimit].GetChunkWorldPosition(i, 0);
                TestTile = t1;
                TopLayerMap.SetTile(V, TestTile);
            }
            //set upperbound top chunk
            if (!(Grid[i, ylimit] is DoorChunk))
            {
                V = Grid[i, ylimit].GetChunkWorldPosition(i, ylimit);
                TestTile = t2;
                TopLayerMap.SetTile(V, TestTile);
            }
        }
    
    }

    //private float y1 = 2.97f;
    //private float y2 = 2.25f;

    //sloppy but it works
    public void FinalizeRoomSetup()
    {
        //add room component


        

        Vector3 LeftJoint = TheTileMap.CellToWorld(DoorList[0].GetChunkCoordinates());//left
        LeftJoint.y *= 2;
        LeftJoint.y += Jy;
        LeftJoint.x *= 2;
        LeftJoint.x -= LRx;
        DoorEdgePoints.Add(DoorChunk.DoorTypeSet.LeftDoor, LeftJoint);

        Vector3 RightJoint = TheTileMap.CellToWorld(DoorList[1].GetChunkCoordinates()); //right
        RightJoint.x *= 2;
        RightJoint.y *= 2;
        RightJoint.y += Jy;
        RightJoint.x += Jx;
        DoorEdgePoints.Add(DoorChunk.DoorTypeSet.RightDoor, RightJoint);

        Vector3 BottomJoint = TheTileMap.CellToWorld(DoorList[2].GetChunkCoordinates()); //bot
        BottomJoint.x *= 2;
        BottomJoint.y *= 2;
        BottomJoint.x += Jx;
        BottomJoint.y += Jy;
        DoorEdgePoints.Add(DoorChunk.DoorTypeSet.BottomDoor, BottomJoint);

        Vector3 TopJoint = TheTileMap.CellToWorld(DoorList[3].GetChunkCoordinates()); //top
        TopJoint.x *= 2;
        TopJoint.x += Jx;
        TopJoint.y *= 2;
        TopJoint.y += Jy;
        DoorEdgePoints.Add(DoorChunk.DoorTypeSet.TopDoor, TopJoint);

        List<bool> DoorBools = new List<bool>() { !BlockBottomDoor, !BlockRightDoor, !BlockTopDoor, !BlockLeftDoor };

        if (!BlockLeftDoor)
        {
            Joint J = new Joint();
            J.InitJoint(LeftJoint, Joint.OrientationEnum.Left, TheGrid.transform, DoorList[0].GetChunkCoordinates());
            AllDoorJoints.Add(J);
            RoomDoors.Add(DoorChunk.DoorTypeSet.LeftDoor, LeftJoint);
        }
        
        if (!BlockRightDoor)
        {
            Joint J = new Joint();
            J.InitJoint(RightJoint, Joint.OrientationEnum.Right, TheGrid.transform, DoorList[1].GetChunkCoordinates());
            AllDoorJoints.Add(J);
            RoomDoors.Add(DoorChunk.DoorTypeSet.RightDoor, RightJoint);
        }


        if (!BlockBottomDoor)
        {
            //top and bottom may need adjusting different that of left and right
            Joint J = new Joint();
            J.InitJoint(BottomJoint, Joint.OrientationEnum.Down, TheGrid.transform, DoorList[2].GetChunkCoordinates());
            AllDoorJoints.Add(J);
            RoomDoors.Add(DoorChunk.DoorTypeSet.BottomDoor, BottomJoint);
        }

        //top is -=
        if (!BlockTopDoor)
        {
            Joint J = new Joint();
            J.InitJoint(TopJoint, Joint.OrientationEnum.Up, TheGrid.transform, DoorList[3].GetChunkCoordinates());
            AllDoorJoints.Add(J);
            RoomDoors.Add(DoorChunk.DoorTypeSet.TopDoor, TopJoint);
        }


        List<bool> FractleDoorBools = new List<bool>() { !FBlockBotDoor, !FBlockRightDoor, !FBlockTopDoor, !FBlockLeftDoor };
        //FRACLTE DOOR STUFF


        if (isFractle)
        {

            if (!FBlockBotDoor)
            {
                Vector3Int v1 = FractleDoorJointBlockers[DoorChunk.DoorTypeSet.BottomDoor];
                FractleDoors[DoorChunk.DoorTypeSet.BottomDoor] = FixFractleDoorOffset(FractleDoors[DoorChunk.DoorTypeSet.BottomDoor], -1.225f, 3.225f);
                Vector3 v = FractleDoors[DoorChunk.DoorTypeSet.BottomDoor];
                Joint J = new Joint();
                J.InitJoint(v, Joint.OrientationEnum.Down, TheGrid.transform, v1);
                FractleDoorJoints.Add(J);
                //AllDoorJoints.Add(J);
            }

            if (!FBlockTopDoor)
            {
                Vector3Int v1 = FractleDoorJointBlockers[DoorChunk.DoorTypeSet.TopDoor];
                FractleDoors[DoorChunk.DoorTypeSet.TopDoor] = FixFractleDoorOffset(FractleDoors[DoorChunk.DoorTypeSet.TopDoor], -1.225f, 3.225f);
                Vector3 v = FractleDoors[DoorChunk.DoorTypeSet.TopDoor];
                Joint J = new Joint();
                J.InitJoint(v, Joint.OrientationEnum.Up, TheGrid.transform, v1);
                FractleDoorJoints.Add(J);
                //AllDoorJoints.Add(J);
            }

            if (!FBlockLeftDoor)
            {
                Vector3Int v1 = FractleDoorJointBlockers[DoorChunk.DoorTypeSet.LeftDoor];
                FractleDoors[DoorChunk.DoorTypeSet.LeftDoor] = FixFractleDoorOffset(FractleDoors[DoorChunk.DoorTypeSet.LeftDoor], -1.225f, 1.125f);
                Vector3 v = FractleDoors[DoorChunk.DoorTypeSet.LeftDoor];
                Joint J = new Joint();
                J.InitJoint(v, Joint.OrientationEnum.Left, TheGrid.transform, v1);
                FractleDoorJoints.Add(J);
                //AllDoorJoints.Add(J);
            }

            //right door is off by 0.05 at < 0 in fxoffset need +0.05 if < 0 and -0.05 if > 0
            if (!FBlockRightDoor)
            {
                Vector3Int v1 = FractleDoorJointBlockers[DoorChunk.DoorTypeSet.RightDoor];
                
                FractleDoors[DoorChunk.DoorTypeSet.RightDoor] = FixFractleDoorOffset(FractleDoors[DoorChunk.DoorTypeSet.RightDoor], 0.875f, 1.125f);
                Vector3 v = FractleDoors[DoorChunk.DoorTypeSet.RightDoor];
                Joint J = new Joint();
                J.InitJoint(v, Joint.OrientationEnum.Right, TheGrid.transform, v1);
                FractleDoorJoints.Add(J);
                //AllDoorJoints.Add(J);
            }
        }

        DrawCollision(TheTileMap, XMax, YMax, DoorBools, TheGrid.gameObject, DoorEdgePoints);

        //int FXmax = (YMax / 2) + (FatnessFactor);
        //int FYmax = (YMax / 2) + ThiccnessFactor;

        if (isFractle) DrawFractleCollision(FractleDoorBools, FractleMap.gameObject, FractleDoorEdges);

        RoomPiece R;

        if (isFractle) R = TheGrid.gameObject.AddComponent<FractleRoomPiece>();
        else R = TheGrid.gameObject.AddComponent<RoomPiece>();

        BoxCollider2D Box = TheTileMap.gameObject.AddComponent<BoxCollider2D>();
        TheTileMap.tag = "GroundLevel";
        FractleMap.tag = "GroundLevel";
        TheTileMap.gameObject.layer = 9;
        FractleMap.gameObject.layer = 9;

        Rigidbody2D Rbody = TheGrid.gameObject.AddComponent<Rigidbody2D>();
        Rbody.simulated = false;
        Rbody.freezeRotation = true;
        Rbody.constraints = RigidbodyConstraints2D.FreezeAll;

        if (isFractle)
        {
            BoxCollider2D Fbox = FractleMap.gameObject.AddComponent<BoxCollider2D>();
            Fbox.isTrigger = true;
            R.GetComponent<FractleRoomPiece>().FractleInit(Box, Fbox, ScriptableTileMap, RoomTypeSize);
        }
        else R.InitRoom(Box, ScriptableTileMap, RoomTypeSize);

        if (!isFractle) FractleMap.gameObject.SetActive(false);

        R.SpawnJoints(JointRef, AllDoorJoints, FractleDoorJoints);

    }

    private Vector3 FixFractleDoorOffset(Vector3 V, float xoffset, float yoffset)
    {
        Vector3 V2 = V;

        //take int value before manual offset
        if (xprev == 0) xprev = (int)V2.x;
        if (yprev == 0) yprev = (int)YMax;

        XDif = (xprev - (int)V2.x);
        YDif = (yprev - (int)V2.y);

        V2.y += yoffset;
        V2.x += xoffset;

        V2.x += ((0.05f) * XDif) * -1;
        V2.y += ((0.05f) * YDif) * -1;

        return V2;
    }


    //1.225 and 3.225f are magic numbers
    //public float TopDoorOffset = 0f;
    //public float FJX = 1.225f;
    //public float FJY = 1.225f;

    private int YDif = 0;
    private int XDif = 0;
    private int xprev = 0;
    private int yprev = 0;

    internal List<Vector3> c = new List<Vector3>();


    private void DrawFractleCollision(List<bool> DoorBools, GameObject ParentObject, Dictionary<DoorChunk.DoorTypeSet, Vector3> DoorDictionary)
    {
        int FXmax = (YMax / 2) + (FatnessFactor);
        //int FYmax = (YMax / 2) + ThiccnessFactor;
        c.Clear();

        List<Vector2> FPoints = new List<Vector2>();

        Vector3 vb = DoorDictionary[DoorChunk.DoorTypeSet.BottomDoor];
        Vector3 vr = DoorDictionary[DoorChunk.DoorTypeSet.RightDoor];
        Vector3 vt = DoorDictionary[DoorChunk.DoorTypeSet.TopDoor];
        Vector3 vl = DoorDictionary[DoorChunk.DoorTypeSet.LeftDoor];

        Vector2[] p1 = new Vector2[2];
        Vector2[] p2 = new Vector2[2];
        Vector2 bcl = new Vector2();
        Vector2 bcr = new Vector2();

        Vector2 tcr = new Vector2();
        Vector2 tcl = new Vector2();
        Vector2 vb2 = new Vector2();
        Vector2 vt2 = new Vector2();

        Vector2 vr2 = new Vector2();
        Vector2 vl2 = new Vector2();

        vb2 = vb;

        //door skip if exists
        if (!FBlockBotDoor)
        {
            vb2.x += 2.0f;
            vb.x -= 2.19f;
            bcr = vb2;
            bcl = vb;
        }
        else
        {
            bcr = vb2;
            bcl = vb;
            bcr.x += 2.0f;
            bcl.x -= 2.19f;
        }

        bcl.x -= 2.1f * ((FXmax / 2) - 1);

        //its -= instead of += cuz math.abs doesnt return a non negative for watever reason, stupid, thats also y its +2 and not -2
        bcr.x -= 2.1f * ((Mathf.Abs(((FXmax / 2) + 1)) - FractleGrid.GetLength(0))+2);


        vt2 = vt;

        if (!FBlockTopDoor)
        {
            vt2.x += 2.0f;
            vt.x -= 2.19f;

            tcl = vt;
            tcr = vt2;
        }
        else
        {
            tcl = vt;
            tcr = vt2;
            tcl.x += 2.19f;
            tcr.x -= 2.0f;
        }


        tcl.x = bcl.x;
        tcr.x = bcr.x;

        Debug.Log(" using fxmas: " + FXmax + " with  half: " + FXmax / 2 + " and " + (Mathf.Abs(((FXmax / 2) + 1)) - FractleGrid.GetLength(0))*-1);


        vr2 = vr;
        vl2 = vl;

        if (!FBlockLeftDoor)
        {
            vl2.y += -2.1f;
            vl.y += 2.1f;
        }


        if (!FBlockRightDoor)
        {
            vr.y += 2.1f;
            vr2.y += -2.1f;
        }

        //for debug
        //c.Add(vb);
        //c.Add(vb2);
        //c.Add(bcl);
        //c.Add(bcr);
        //c.Add(vt);
        //c.Add(vt2);
        //c.Add(tcr);
        //c.Add(tcl);
        //c.Add(vl);
        //c.Add(vl2);
        //c.Add(vr);
        //c.Add(vr2);

        //specific order
        FPoints.Add(bcl);
        FPoints.Add(vb);
        FPoints.Add(vb2);
        FPoints.Add(bcr);

        FPoints.Add(bcr);
        FPoints.Add(vr2);
        FPoints.Add(vr);
        FPoints.Add(tcr);
        //FPoints.Add(tcr);//skip
        FPoints.Add(tcr);
        FPoints.Add(vt2);
        FPoints.Add(vt);
        FPoints.Add(tcl);

        FPoints.Add(tcl);
        FPoints.Add(vl);
        FPoints.Add(vl2);
        FPoints.Add(bcl);



        for (int i = 0; i < FPoints.Count; i++)
        {
            EdgeCollider2D E1 = FractleMap.gameObject.AddComponent<EdgeCollider2D>();
            p1[0] = FPoints[i]*1.428f;
            p1[1] = FPoints[i + 1]*1.428f;
            E1.points = p1;
            i++;
        }

        p1[0] = vb*1.428f;
        p1[1] = bcl*1.428f;
        p2[0] = vb2*1.428f;
        p2[1] = bcr*1.428f;


    }

    //pass in a tilemap, x1, x2, y1, xlimit, ylimit, might not need x1, x2 and y1
    //also need to pass in what gameobject to attach objects too and what dictionary of doors
    private void DrawCollision(Tilemap Map, int XLimit, int YLimit, List<bool> DoorBools, GameObject ParentObject, Dictionary<DoorChunk.DoorTypeSet, Vector3> DoorDictionary)
    {
        List<Vector3> PointList = new List<Vector3>();
        c.Clear();

        Vector3 BottomLeftCorner = DoCornerCollision(Map.CellToWorld(new Vector3Int(0, 0, 0)), 1.575f, 0.5f);
        Vector3 BottomRightCorner = DoCornerCollision(Map.CellToWorld(new Vector3Int(XLimit, 0, 0)), -0.525f, 0.5f);
        Vector3 TopLeftCorner = DoCornerCollision(Map.CellToWorld(new Vector3Int(0, YLimit, 0)), 1.575f, 0.5f);
        Vector3 TopRightCorner = DoCornerCollision(Map.CellToWorld(new Vector3Int(XLimit, YLimit, 0)), -0.525f, 0.5f);

        Vector3 BotDoorPointLeft = GetTopBotDoorPoint(DoorDictionary, DoorChunk.DoorTypeSet.BottomDoor);
        Vector3 BotDoorPointRight = new Vector3();
        BotDoorPointRight = BotDoorPointLeft;
        BotDoorPointRight.x += 4.1f;

        Vector3 TopDoorPointLeft = GetTopBotDoorPoint(DoorDictionary, DoorChunk.DoorTypeSet.TopDoor);
        Vector3 TopDoorPointRight = new Vector3();
        TopDoorPointRight = TopDoorPointLeft;
        TopDoorPointRight.x += 4.1f;

        Vector3 LeftDoorPointBot = GetSideDoorCollision(DoorDictionary, DoorChunk.DoorTypeSet.LeftDoor, 1, -1);
        Vector3 LeftDoorPointTop = new Vector3();
        LeftDoorPointTop = LeftDoorPointBot;
        LeftDoorPointTop.y += 4.2f;

        Vector3 RightDoorPointTop = GetSideDoorCollision(DoorDictionary, DoorChunk.DoorTypeSet.RightDoor, -1, 1);
        Vector3 RightDoorPointBot = new Vector3();
        RightDoorPointBot = RightDoorPointTop;
        RightDoorPointBot.y += -4.2f;

        //just for debug
        //c.Add(BottomLeftCorner);
        //c.Add(BottomRightCorner);
        //c.Add(TopLeftCorner);
        //c.Add(TopRightCorner);

        //c.Add(BotDoorPointLeft);
        //c.Add(BotDoorPointRight);
        //c.Add(TopDoorPointLeft);
        //c.Add(TopDoorPointRight);
        //c.Add(LeftDoorPointBot);
        //c.Add(LeftDoorPointTop);
        //c.Add(RightDoorPointBot);
        //c.Add(RightDoorPointTop);


        //need to structure this in a certain way, add in points in a specific order
        //bot list
        PointList.Add(BottomLeftCorner);
        PointList.Add(BotDoorPointLeft);
        PointList.Add(BotDoorPointRight);
        PointList.Add(BottomRightCorner);

        //right list
        PointList.Add(BottomRightCorner);
        PointList.Add(RightDoorPointBot);
        PointList.Add(RightDoorPointTop);
        PointList.Add(TopRightCorner);

        //top list
        PointList.Add(TopRightCorner);
        PointList.Add(TopDoorPointRight);
        PointList.Add(TopDoorPointLeft);
        PointList.Add(TopLeftCorner);

        //left list
        PointList.Add(TopLeftCorner);
        PointList.Add(LeftDoorPointTop);
        PointList.Add(LeftDoorPointBot);
        PointList.Add(BottomLeftCorner);


        CreateEdgeCollision(DoorBools, PointList, ParentObject);


    }

    private void CreateEdgeCollision(List<bool> OpenDoors, List<Vector3> PointsLits, GameObject Parent)
    {
        List<Vector3> Top = new List<Vector3>() { PointsLits[8], PointsLits[9], PointsLits[10], PointsLits[11] };
        List<Vector3> Bottom = new List<Vector3>() { PointsLits[0], PointsLits[1], PointsLits[2], PointsLits[3] };
        List<Vector3> Left = new List<Vector3>() { PointsLits[12], PointsLits[13], PointsLits[14], PointsLits[15] };
        List<Vector3> Right = new List<Vector3>() { PointsLits[4], PointsLits[5], PointsLits[6], PointsLits[7] };

        List<List<Vector3>> List = new List<List<Vector3>>() { Bottom, Right, Top, Left };
        //block door order will be Bottom, Right, Top, Left
        for (int i = 0; i < OpenDoors.Count; i++)
        {
            if(!OpenDoors[i])
            {
                //List.RemoveRange(1, 2);
                //Debug.Log("removing points: " + List[i][1] + " " + List[i][2]);
                List[i].RemoveAt(1);
                List[i].RemoveAt(1);
            }
        }

        foreach(List<Vector3> L in List)
        {
            for (int i = 0; i < L.Count; i++)
            {
                EdgeCollider2D E = Parent.AddComponent<EdgeCollider2D>();
                Vector2[] Points = new Vector2[2];
                Points[0] = L[i]*1.428f;
                Points[1] = L[i + 1]*1.428f;
                //Debug.Log("points: " + L[i] + "  " + L[i + 1]);
                E.points = Points;
                i++;

            }
        }



    }

    private Vector3 DoCornerCollision(Vector3 v1, float xoffset, float yoffset)
    {
        //the same offsets should work for fractle room
        v1.x *= 2;
        v1.y *= 2;
        v1.x += xoffset;
        v1.y += yoffset;
        return v1;
    }

    private Vector3 GetTopBotDoorPoint(Dictionary<DoorChunk.DoorTypeSet, Vector3> Dict, DoorChunk.DoorTypeSet D)
    {       
        float xd = -2.05f;
        float yd = -1.075f;

        Vector3 v1 = Dict[D];

        v1.x += xd;
        v1.y += yd;

        return v1;
    }

    //CHANGE FROM == NULL TO == VECTOR3.ZERO
    private Vector3 GetSideDoorCollision(Dictionary<DoorChunk.DoorTypeSet, Vector3> Dict, DoorChunk.DoorTypeSet D, int xsign, int ysign)
    {
        if (Dict[D] == Vector3.zero) return new Vector3();
        Vector3 v = Dict[D];

        v.x += 2.1f * xsign;
        v.y += 2.1f * ysign;

        return v;
    }


    //door list order is left right bot top
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        if (DoorList.Count > 0 && c.Count > 0)
        {

            foreach(Vector2 V in c)
            {
                Gizmos.DrawWireSphere(V, 0.1f);
            }


            ////p2 at bot left side door
            //Vector3 v = DoorEdgePoints[DoorChunk.DoorTypeSet.BottomDoor];
            //v.x += -2.05f;
            //v.y += -1.075f;

            //Gizmos.color = Color.red;
            //Gizmos.DrawWireSphere(v, 0.1f);

            ////p3 on right side bot door
            //Vector3 v2 = new Vector3();
            //v2 = v;
            //v2.x += 4.1f;
            //Gizmos.DrawWireSphere(v2, 0.1f);

            ////just left of the top door
            //Vector3 v7 = new Vector3();
            //v7 = DoorEdgePoints[DoorChunk.DoorTypeSet.TopDoor];
            //v7.x += -2.05f;
            //v7.y += -1.075f;
            //Gizmos.DrawWireSphere(v7, 0.1f);

            ////just right of the top door
            //Vector3 v8 = new Vector3();
            //v8 = v7;
            //v8.x += 4.1f;
            //Gizmos.DrawWireSphere(v8, 0.1f);

            ////side wall stuff
            ////left point below left door
            //Vector3 v4 = DoorEdgePoints[DoorChunk.DoorTypeSet.LeftDoor];
            //v4.x += 2.1f;
            //v4.y += -2.1f;
            //Gizmos.DrawWireSphere(v4, 0.1f);

            ////left point above left door
            //Vector3 v5 = new Vector3();
            //v5 = v4;
            //v5.y += 4.2f;
            //Gizmos.DrawWireSphere(v5, 0.1f);

            //Vector3 v10 = new Vector3();
            //v10 = DoorEdgePoints[DoorChunk.DoorTypeSet.RightDoor];

            //v10.x += -2.1f;
            //v10.y += 2.1f;

            //Gizmos.DrawWireSphere(v10, 0.1f);

            //Vector3 v11 = new Vector3();
            //v11 = v10;
            //v11.y += -4.2f;

            //Gizmos.DrawWireSphere(v11, 0.1f);

        }
    }

    private float Jy = 1.575f;
    private float Jx = 1.575f;
    private float LRx = 0.525f;

}
