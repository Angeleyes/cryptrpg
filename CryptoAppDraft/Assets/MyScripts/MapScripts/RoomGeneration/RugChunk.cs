﻿
using UnityEngine;
using UnityEngine.Tilemaps;

public class RugChunk : FloorChunk
{
    public RugChunk(int x, int y, Tile tile) : base(x, y, tile)
    {
        ChunkCoordinates = new Vector3Int(x, y, 0);
        ChunkSprite = tile.sprite;
        ChunkTile = tile;
    }

    public RugChunk(int x, int y, int z, Tile tile) : base(x, y, tile)
    {
        ChunkCoordinates = new Vector3Int(x, y, z);
        ChunkSprite = tile.sprite;
        ChunkTile = tile;
    }

    //public override Vector3 GetChunkWorldPosition(int d1, int d2)
    //{
    //    Vector3 OversidePos = base.GetChunkWorldPosition(d1, d2);

    //    OversidePos = new Vector3(OversidePos.x + 1.05f, OversidePos.y + 1.05f, OversidePos.z);

    //    return OversidePos;
    //}

}
