﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "NewTileMapObj")]
public class ScriptableTileMap : ScriptableObject {

    public Tile SideWallTileL; //Side wall Tile that points left
    public Tile SideWallTileR; //Side wall Tile that points right 
    //public Tile DoorSideWallCornerDR;
    //public Tile DoorSideWallCornerUR;
    //public Tile TopBotWallTile; //Tiles for the top and bottom walls
    //public Tile TopBotWallTile2; //A deviation of the top bot wall Tiles
    //public Tile TopBotWallTile3; //A 3rd deviation of the top bot wall Tiles

    public Tile TopWallPiece;
    public Tile BotWallPiece;

    public List<Tile> SpecialTopWallPieces;
    public List<Tile> SpecialBotWallPieces;

    //public Tile FloorTile; //The ground floor Tile
    //public Tile FloorTile2; //A 2nd deviation of a ground floor Tile

    public ScriptableFloorSprites DungeonSpecificFloorTiles;
    public ScriptableFloorSprites AllFloorTiles;
    public ScriptableFloorSprites RandomizedFloorTiles;

    //public ScriptableFloorTiles TopAssetsThin;
    //public ScriptableFloorTiles TopAssetsWide;

    //public ScriptableFloorSprites MatAssets;

    public Tile OutlineTileUp; //Tile that goes on the outer edge of the room as a detail on the top of the room edge
    public Tile OutlineTileDown; //Tile that goes on the outer edge of the room as a deail on the bottom of the room edge
    public Tile OutlineTileLeft;
    public Tile OutlineTileRight;

    public Tile CornerOutlineTL; //Tile that goes in the very top left (TL = top left) corner of the room to complete the outline detail
    public Tile CornerOutlineTR;
    public Tile CornerOutlineBR;
    public Tile CornerOutlineBL;

    public Tile TBDoorL1;
    public Tile TBDoorR2;

    public Tile SideDoorRightTop; //Top piece of the right side door
    public Tile SideDoorRightBottom; //bottom piece of the right side door
    public Tile SideDoorLeftTop; //top piece of the left side door
    public Tile SideDoorLeftBottom; //bottom piece of the left side door

    public Tile RugTileLeft; //rug tile chunk that points its detail left <-
    public Tile RugTileRight;//rug tile chunk that points its detail to the right
    public Tile RugTileUp; //up
    public Tile RugTileDown; //down 

    public Tile BigRug1; //center fill (oversided) chunk to fix the overlaping of the two rugs

    //public Tile CornerDetail1; //oversided detail chunk that goes in the corners of the room

    //public List<Tile> WallClutter = new List<Tile>();
    public List<Tile> FloorClutter = new List<Tile>();

    //public Tile WideCollisionTile;
    //public Tile ThinCollisionTile;


}
