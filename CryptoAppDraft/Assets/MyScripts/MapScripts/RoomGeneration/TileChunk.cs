﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Tiled2Unity;

public abstract class TileChunk {

    protected Tile.ColliderType ChunkCollider;
    protected Sprite ChunkSprite;
    protected Tile ChunkTile;
    protected Vector2Int GridCoords;
    protected Vector3Int ChunkCoordinates = new Vector3Int();

    private const int ChunkWidth = 2;
    private const int ChunkHeight = 2;
    //protected Tile ChunkTile;

    public TileChunk() { }

    public void SetChunkTile(Tile thetile)
    {
        ChunkSprite = thetile.sprite;
        ChunkTile = thetile;
        //ChunkTile.sprite = sprite;
    }

    public Sprite GetChunkSprite()
    {
        return ChunkSprite;    
    }

    public Tile GetChunkTile()
    {
        return ChunkTile;
    }

    public virtual Vector3Int GetChunkCoordinates()
    {
        return ChunkCoordinates;
    }

    public Tile.ColliderType GetChunkCollider()
    {
        return ChunkCollider;
    }
    public virtual Vector3Int GetChunkWorldPosition(int d1, int d2)
    {
        int Z = GetChunkCoordinates().z;
        Vector3Int ChunkPosition = new Vector3Int(((d1) * (ChunkWidth)), ((d2) * (ChunkHeight)), Z);
        //Vector3Int ChunkPosition = new Vector3Int((X) * (ChunkWidth), (Y) * (ChunkHeight), Z);
        return ChunkPosition;
    }

    public Vector3Int GetChunkWorldPosition(int d1, int d2, int xoffset, int yoffset)
    {
        Vector3Int ChunkPosition = new Vector3Int((d1 + xoffset), (d2 + yoffset), 0);
        //Vector3Int ChunkPosition = new Vector3Int((X) * (ChunkWidth), (Y) * (ChunkHeight), Z);
        return ChunkPosition;
    }

}
