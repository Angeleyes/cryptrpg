﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "NewFloorMapObj")]
public class ScriptableFloorSprites : ScriptableObject
{

    public List<Tile> TileList = new List<Tile>(15);

    internal Dictionary<string, Tile> TileTable1 = new Dictionary<string, Tile>();
    internal Dictionary<string, Tile> TileTable2 = new Dictionary<string, Tile>();


    public Tile GetTile(int index)
    {
        if (index >= TileList.Count) return TileList[TileList.Count - 1];
        else return TileList[index];
    }

    public int GetTileIndex(Tile S)
    {
        for (int i = 0; i < TileList.Count; i++)
        {
            if (TileList[i] == S) return i;
        }

        return 1;
    }


}
