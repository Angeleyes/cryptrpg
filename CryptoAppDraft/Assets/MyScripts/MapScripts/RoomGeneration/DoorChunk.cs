﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DoorChunk : TileChunk {

    public enum DoorTypeSet
    {
        LeftDoor,
        RightDoor,
        TopDoor,
        BottomDoor,
    }

    private Vector3 DoorJointPosition;

    DoorTypeSet DoorType = DoorTypeSet.LeftDoor;

    public DoorChunk(int x, int y, DoorTypeSet Type, Tile tile)
    {
        ChunkCoordinates = new Vector3Int(x, y, 0);
        ChunkSprite = tile.sprite;
        ChunkTile = tile;
        ChunkCollider = Tile.ColliderType.None;
        DoorType = Type;

        switch(DoorType)
        {
            case DoorTypeSet.BottomDoor: DoorJointPosition = new Vector3();
                break;
            case DoorTypeSet.TopDoor: DoorJointPosition = new Vector3();
                break;
            case DoorTypeSet.RightDoor: DoorJointPosition = new Vector3();
                break;
            case DoorTypeSet.LeftDoor: DoorJointPosition = new Vector3();
                break;
            default: Debug.Log("cannot find correct door type in door chunk constructor");
                break;
        }

    }

    public DoorTypeSet GetDoorType()
    {
        return DoorType;
    }

    public Vector3 GetJointSpawnPoint()
    {
        return DoorJointPosition;
    }

}
