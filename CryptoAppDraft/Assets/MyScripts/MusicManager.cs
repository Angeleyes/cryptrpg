﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {


    public List<AudioClip> Clips = new List<AudioClip>();
    internal AudioSource AudioPlayer;
    internal AudioSource SecondAudioPlayer;
    internal Queue<AudioClip> ClipQue = new Queue<AudioClip>();
    public AudioClip BossMusic;

    public float MinPauseTime = 15f;
    public float MaxPauseTime = 45f; //Maximum time between song pauses
    IEnumerator ClipPlayer;

    public static bool BossEncounter = false;

	void Start ()
    {
        AudioSource[] Sources = GetComponentsInChildren<AudioSource>();
        AudioPlayer = Sources[0];
        SecondAudioPlayer = Sources[1];

        ClipQue = RandomizeList();
        ClipPlayer = PlayClips();
        StartCoroutine(ClipPlayer);
	}

    public void MuteMusic(bool toggle)
    {
        if (toggle) AudioPlayer.volume = 0;
        else AudioPlayer.volume = 0.5f;
    }

    public void SetVolume(float volume)
    {
        AudioPlayer.volume = volume;
    }

    IEnumerator PlayClips()
    {
        while(true)
        {
            if(BossEncounter)
            {
                //StopCoroutine(ClipPlayer);
                yield return StartCoroutine(PlayBossMusic());
            }

            yield return new WaitForSeconds(Random.Range(MinPauseTime, MaxPauseTime + 1));
            //yield return StartCoroutine(PlayerSecondaryAmbience(Random.Range(MinPauseTime, MaxPauseTime + 1)));
            AudioClip CurrentClip = ClipQue.Dequeue();
            AudioPlayer.clip = CurrentClip;
            AudioPlayer.Play();
            ClipQue.Enqueue(CurrentClip);

            Debug.Log("playing clip: " + CurrentClip.name);

            yield return new WaitForSeconds(AudioPlayer.clip.length + 1);

        }

    }

    IEnumerator PlayerSecondaryAmbience(float time)
    {
        float timer = 0;
        while(timer < time)
        {
            timer += Time.deltaTime;
            SecondAudioPlayer.clip = null;
            SecondAudioPlayer.Play();
            yield return null;
        }

        yield break;
    }

    IEnumerator PlayBossMusic()
    {
        AudioPlayer.clip = BossMusic;
        AudioPlayer.Play();

        yield return new WaitForSeconds(AudioPlayer.clip.length);

        StartCoroutine(PlayBossMusic());
        yield break;
    }


    private Queue<AudioClip> RandomizeList()
    {
        Queue<AudioClip> NewClips = new Queue<AudioClip>();
        int k = Clips.Count;

        for (int i = 0; i < k; i++)
        {
            if (Clips.Count == 1)
            {
                NewClips.Enqueue(Clips[0]);
                break;
            }

            int j = Random.Range(0, Clips.Count);
            NewClips.Enqueue(Clips[j]);
            Clips.RemoveAt(j);

        }

        return NewClips;
    }

}
