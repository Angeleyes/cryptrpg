﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class UIElement : MonoBehaviour, ITooltipable, IPointerEnterHandler, IPointerExitHandler
{
    private Text TipText;
    private RectTransform Background;
    internal Vector2 CustomTipOffset = new Vector2();
    public CharacterHUD CharHudRef;

    public abstract string GetTooltipData(out UIElement U);


    private void Awake()
    {
        if(CharHudRef == null) CharHudRef = GetComponentInParent(typeof(CharacterHUD)) as CharacterHUD;

        //Debug.Log("element awake, found hud?: " + this.name + " hud is : " + (CharHudRef == null) + " with tip panel: " + (CharHudRef.ToolTipPanel == null));
        Background = CharHudRef.ToolTipPanel;
        //Debug.Log("just set tip panel background is null ? : " + (Background == null));
        TipText = Background.GetComponentInChildren<Text>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ShowToolTip(eventData);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HideToolTip();
    }

    private void ShowToolTip(PointerEventData PointerData)
    {
        Background.gameObject.SetActive(true);
        UIElement UiObject;

        TipText.text = GetTooltipData(out UiObject);

        if (TipText.text == "")
        {
            Background.gameObject.SetActive(false);
            return;
        }


        //Debug.Log(" ui object pos: " + new Vector3(UiObject.transform.position.x, UiObject.transform.position.y, UiObject.transform.position.z));
        //Debug.Log(" versus pointer pos: " + PointerData.position);
        //Debug.Log(" versus pointer data: " + PointerData.selectedObject.GetComponent<RectTransform>().position);

        //Background.transform.position = new Vector3(UiObject.transform.position.x, UiObject.transform.position.y + 3f, UiObject.transform.position.z);
        Background.position = PointerData.position;
        Background.position += (Vector3.up * 70f);
        Background.position += new Vector3(CustomTipOffset.x, CustomTipOffset.y+25, 0);

        float Padding = 10f;
        Vector2 BackgroundSize = new Vector2(TipText.preferredWidth + Padding, TipText.preferredHeight + Padding/2);
        TipText.rectTransform.sizeDelta = BackgroundSize;
        //Background.sizeDelta = BackgroundSize;
        Background.sizeDelta = new Vector2(BackgroundSize.x + Padding*2, BackgroundSize.y + Padding*2);
        //Background.localScale = BackgroundSize;
    }


    private void HideToolTip()
    {
        Background.gameObject.SetActive(false);
    }

}
