﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
public class OptionsMenu : MonoBehaviour
{

    public AudioMixer MasterMixer;
    public AudioMixerGroup MusicMixer;
    public AudioMixerGroup EffectsMixer;

    public Slider MasterSlider;
    public Slider MusicSlider;
    public Slider EffectsSlider;

    private float CurrentMasterVolume = 0f;
    private float CurrentMusicVolume = 0f;
    private float CurrentEffectsVolume = 0f;

    public void SetMasterVolume(float volume)
    {
        MasterMixer.SetFloat("MasterVolume", volume);
        CurrentMasterVolume = volume;
    }

    public void ToggleMasterMute(bool toggle)
    {
        if (toggle) MasterMixer.SetFloat("MasterVolume", -80f);
        else MasterMixer.SetFloat("MasterVolume", CurrentMasterVolume);
        MasterSlider.enabled = !toggle;

        //MasterMixer
    }

    public void SetMusicVolume(float volume)
    {
        MusicMixer.audioMixer.SetFloat("MusicVolume", volume);
        CurrentMusicVolume = volume;
    }

    public void SetEffectsVolume(float volume)
    {
        EffectsMixer.audioMixer.SetFloat("EffectsVolume", volume);
        CurrentEffectsVolume = volume;
    }

    public void ToggleMusicMute(bool toggle)
    {
        if (toggle) MusicMixer.audioMixer.SetFloat("MusicVolume", -80f);
        else MusicMixer.audioMixer.SetFloat("MusicVolume", CurrentMusicVolume);

        MusicSlider.enabled = !toggle;
    }

    public void ToggleEffectsMute(bool toggle)
    {
        if (toggle) EffectsMixer.audioMixer.SetFloat("EffectsVolume", -80f);
        else EffectsMixer.audioMixer.SetFloat("EffectsVolume", CurrentEffectsVolume);

        EffectsSlider.enabled = !toggle;
    }


    //public void SetMixerVolume(AudioMixerGroup Mixer, string MixerString, float volume)
    //{
    //    Mixer.audioMixer.SetFloat(MixerString, volume);
    //}

    //public void SetMixerMute(AudioMixerGroup Mixer, string MixerString, bool Toggle)
    //{
    //    if (Toggle) Mixer.audioMixer.SetFloat(MixerString, -80f);
    //    else Mixer.audioMixer.SetFloat(MixerString, -40f);
    //}

}
