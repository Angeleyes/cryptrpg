﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour {


    InventorySlot[] Slots;
    public GameObject Parent;
    internal Inventory InvRef;
    internal CharacterSheet CharSheetRef;

    private void Awake()
    {
        //inventory = Inventory.PlayerInventory;
        //inventory.OnItemChangedCallback += UpdateUI;
        Slots = Parent.GetComponentsInChildren<InventorySlot>();
        gameObject.SetActive(false);
    }

    public void SetCharSheetRef(CharacterSheet C)
    {
        CharSheetRef = C;
    }

    public void UpdateUI()
    {
        for (int i = 0; i < Slots.Length; i++)
        {
            if (i < InvRef.ListOfItems.Count)
            {
                if (InvRef.ListOfItems[i] is EquipableItem)
                {
                    //Debug.Log("inside equipable inventory ui");
                    EquipableItem I = InvRef.ListOfItems[i] as EquipableItem;
                    I.OnEquipAction = () => CharSheetRef.Equip(I);
 
                }
                Slots[i].AddItem(InvRef.ListOfItems[i]);
            }
            else Slots[i].ClearSlot();
        }
    }

    public void SetInventoryRef(Inventory I)
    {
        InvRef = I;
    }

    public Inventory GetInv()
    {
        return InvRef;
    }









}
