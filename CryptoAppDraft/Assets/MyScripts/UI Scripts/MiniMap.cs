﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour {

    //public RectTransform MiniMapHeaderPanel;
    public Image Border;
    public Image BigModeBackDrop;

    public Sprite HideButtonUp;
    public Sprite HideButtonDown;
    public Button HideShowButton;


    private Vector3 MiniMapPos;

    private int BorderOriginScale;
    private float MiniMapOriginScale;


    private float CamMaxZoomIn = 20f;
    private float CamMaxZoomOut = 80f;
    private float CamZoomFactor = 20f;

    internal MiniMapCamera MiniMapCam;
    internal bool IsBigMode = false;

    public Button ZoomInButton;
    public Button ZoomOutButton;
    internal bool IsHidden = false;

    internal Canvas MiniMapCanvas;
    internal Vector3 OriginButMinus;
    internal Vector3 OriginButPlus;
    internal RectTransform MinimapRect;
    internal RawImage MiniMapProjection;

    internal RectTransform PlusButtonRect, MinusButtonRect;

    internal SpriteRenderer ClassIcon;

    public void Awake()
    {

        MiniMapPos = this.GetComponent<RectTransform>().position;

        OriginButMinus = ZoomOutButton.GetComponent<RectTransform>().position;
        OriginButPlus = ZoomInButton.GetComponent<RectTransform>().position;
        MiniMapCanvas = GetComponentInParent<Canvas>();
        MinimapRect = GetComponent<RectTransform>();

        PlusButtonRect = ZoomInButton.GetComponent<RectTransform>();
        MinusButtonRect = ZoomOutButton.GetComponent<RectTransform>();
        MiniMapProjection = GetComponent<RawImage>();
        ZoomInButton.interactable = false;
        //need to assign the ca
    }

    public void AssignCamera(MiniMapCamera Mc, SpriteRenderer Classicon)
    {
        MiniMapCam = Mc;
        ClassIcon = Classicon;
        CheckButtons();
    }



    public void ZoomCamera(int sign) // + zoom in/ - out
    {
        if (MiniMapCam != null)
        {
            CheckButtons();

            MiniMapCam.TheCamera.orthographicSize += CamZoomFactor * sign;
            ClassIcon.transform.localScale = new Vector3(ClassIcon.transform.localScale.x + (3 * sign), ClassIcon.transform.localScale.y + (3 * sign), 1);
            MiniMapCam.TheCamera.orthographicSize = Mathf.Clamp(MiniMapCam.TheCamera.orthographicSize, CamMaxZoomIn, CamMaxZoomOut);

            CheckButtons();

        }
        else Debug.Log("Minimap cam is null in zoom camera");
    }


    private void CheckButtons()
    {
        if (MiniMapCam.TheCamera.orthographicSize >= CamMaxZoomOut) ZoomOutButton.interactable = false;
        else ZoomOutButton.interactable = true;

        if (MiniMapCam.TheCamera.orthographicSize <= CamMaxZoomIn) ZoomInButton.interactable = false;
        else ZoomInButton.interactable = true;
    }


    public void EmbiggenMap()
    {
        //ya ya using magic numbers for now, fix later
        Border.gameObject.SetActive(false);
        BigModeBackDrop.gameObject.SetActive(true);
        if (IsHidden)
        {
            ZoomInButton.gameObject.SetActive(true);
            ZoomOutButton.gameObject.SetActive(true);
            MiniMapProjection.enabled = true;
         
        }

        this.GetComponent<RectTransform>().localPosition = new Vector3(10, -2, 0); //im being dumb by using localpos, oh well it works

        this.transform.localScale = new Vector3(6, 4.7f, 0);
        MinimapRect.sizeDelta = new Vector2(150, 130);
        //BigModeBackDrop.rectTransform.sizeDelta = new Vector2(BigModeBackDrop.rectTransform.sizeDelta.x, 130);
        //150, 130 for minimap projection

        MiniMapCanvas.sortingOrder = 2;

        PlusButtonRect.localPosition = new Vector3(400, -280);
        MinusButtonRect.localPosition = new Vector3(433, -280);

        IsBigMode = true;
    }


    public void DebiggenMap()
    {
        Border.gameObject.SetActive(true);
        BigModeBackDrop.gameObject.SetActive(false);
        MinimapRect.position = MiniMapPos;


        MinimapRect.sizeDelta = new Vector2(150, 150);
        MiniMapCanvas.sortingOrder = 0;
        //BigModeBackDrop.rectTransform.sizeDelta = new Vector2(BigModeBackDrop.rectTransform.sizeDelta.x, 660);

        this.transform.localScale = Vector3.one;

        PlusButtonRect.position = OriginButPlus;
        MinusButtonRect.position = OriginButMinus;

        if (IsHidden)
        {
            ZoomInButton.gameObject.SetActive(false);
            ZoomOutButton.gameObject.SetActive(false);
            MiniMapProjection.enabled = false;
            Border.gameObject.SetActive(false);
        }

        IsBigMode = false;
    }


    public void ToggleMiniMap()
    {
        if (HideShowButton.image.sprite == HideButtonUp) HideShowButton.image.sprite = HideButtonDown;
        else HideShowButton.image.sprite = HideButtonUp;

        Border.gameObject.SetActive(!Border.isActiveAndEnabled);
        MiniMapProjection.enabled = !MiniMapProjection.enabled;
        ZoomInButton.gameObject.SetActive(!ZoomOutButton.isActiveAndEnabled);
        ZoomOutButton.gameObject.SetActive(!ZoomOutButton.isActiveAndEnabled);
        IsHidden = !IsHidden;
    }

}
