﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapCamera : MonoBehaviour {

    public Canvas MiniMapCanvas;
    internal Camera TheCamera;
    internal MiniMap TheMap;

    public void CreateMiniMap(GameObject HudParent, SpriteRenderer ClassIcon, CharacterHUD PlayerHudRef)
    {
        Canvas C = Instantiate(MiniMapCanvas, HudParent.transform);
        TheCamera = GetComponent<Camera>();
        TheMap = C.GetComponentInChildren<MiniMap>();
        TheMap.AssignCamera(this, ClassIcon);
        PlayerHudRef.TheMiniMap = TheMap;
    }
}
