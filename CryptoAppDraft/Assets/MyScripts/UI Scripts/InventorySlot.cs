﻿using UnityEngine;
using UnityEngine.UI;
public class InventorySlot : UIElement {

    internal Item item;
    public Image Icon;


    public virtual void AddItem(Item NewItem)
    {
        item = NewItem; //do i need a copy constructor?
        //item.transform.parent = this.transform;     //parents are not getting set properly 
        item.transform.SetParent(this.transform); //testing this out
        Icon.sprite = NewItem.ItemIcon;
        Icon.enabled = true;
        Icon.color = new Color(Icon.color.r, Icon.color.g, Icon.color.b, 1);
        //Debug.Log("adding item..is null? : " + (item == null) + " adding " + NewItem.name);

        MoveToParent();
    }

    public void MoveToParent()
    {
        //getting a null ref here after chaning inventory ui image, it happens after a small delay
        if (item == null) Debug.Log("item is null at this point...why");
        if(item.transform.parent != null) item.transform.position = item.transform.parent.position;
    }

    public virtual void ClearSlot()
    {
        item = null;
        Icon.sprite = null;
        Icon.enabled = false;
    }

    //called by a button press on the inventory slot
    public virtual void UseItem()
    {
        //Debug.Log("inside slot use item" + "..item is :" + item.name + " with player " + item.ItemOwner);
        //this needs to be written with context in mind, the type of inventory
        if (item != null)
        {
            item.Use(item.ItemOwner);
        }
    }

    public override string GetTooltipData(out UIElement U)
    {
        U = this;
        string DataString = "";
        if (item != null) DataString += item.GetItemData();

        return DataString;

    }
}
