﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Icon Data")]
public class InterfaceIcon : ScriptableObject {

    //needs refactoring, turn into item
    //depreciated atm

    public int PriceToDisplay;
    public string IconName;
    public string Description;
    public Image IconImage;
    public RawImage IconRawImage;
    public Sprite IconSprite;
    public Button IconButton;


    void Start()
    {
        //Getcomponent to define image if null;
    }

}
