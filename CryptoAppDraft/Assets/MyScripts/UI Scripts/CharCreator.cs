﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Barebones.MasterServer;
//using Barebones.Networking;

//: ServerModuleBehaviour
public class CharCreator : MonoBehaviour {

    public List<ScriptableClass> ClassList;
    internal Dictionary<string, ScriptableClass> ClassDictionary;
    public Text OnScreenDescription;
    public Button EnterButton;

    //public Button ClassButton;
    //public Vector3 StartingButtonPos;
    public Vector3 OwnedCharListPos;
    internal int MoveButtonInXby = 100;

    internal bool IsNewChar = false;
    public GameObject ClassButtonObj;

    private ScriptableClass LoadedCharacter;

    //internal ObservableProfile PlayerProfileRef;

    void Start()
    {
        //need to insantiate a temp character object to hold inventory data and other data

    }

    private void Awake()
    {

        ClassDictionary = new Dictionary<string, ScriptableClass>();
        foreach (ScriptableClass S in ClassList)
        {
            ClassDictionary.Add(S.ClassName, S);
        }

        //instantiates all the possible choices for character classes that the player can create
        //OUT
        //foreach (KeyValuePair<string, ScriptableClass> S in ClassDictionary)
        //{
        //    if (ProfileManager.Instance.HasCharacterClass(S.Key)) //check if player has created this a char before
        //    {
        //        GameObject A = Instantiate(ClassButtonObj, OwnedCharListPos, Quaternion.identity, this.transform);
        //        A.GetComponent<Image>().color = S.Value.ClassColorTheme;
        //        A.GetComponentInChildren<Text>().text = S.Value.ClassName;
        //        A.GetComponentInChildren<Button>().onClick.AddListener(delegate () { LoadClassData(S.Key);  });
        //        A.transform.position = new Vector3(OwnedCharListPos.x, 120, A.transform.position.z);
        //        OwnedCharListPos.x += MoveButtonInXby;
        //    }           
        //}


    }

    public void LoadClassData(string ClassName) //maybe change param to int ID or something
    {
        //IsNewChar = toggle;
        LoadedCharacter = ClassDictionary[ClassName];
        UpdateOnScreenDescription(LoadedCharacter);
        EnterButtonActive(true);
    }

    public void ToggleIsNew(bool toggle)
    {
        IsNewChar = toggle;
    }

    private void LoadProfile(string ClassName)
    {
        //if an existing profile is found, the main player profile will already be filled with values from the db
        //extract class data from the profile based on passed in class name
        //List<int> CharaData = ProfileManager.Instance.GetExistingCharData(ClassName);
        //if (CharaData == null) Debug.Log("chara data is null in load profile....");

        //OUT
        //ProfileManager.Instance.SetExistingCharData(ClassName);
    }


    private void CreateNewCharProfile(string ClassName)
    {
        //OUT
        //ProfileManager.Instance.AddNewCharProfile(ClassName);
    }

    private void UpdateOnScreenDescription(ScriptableClass S)
    {
        //OUT
        //OnScreenDescription.text = S.ClassName + "\n" + S.ClassDescription + "\n"
        //    + "\n" + ProfileManager.Instance.GetCharacterDataAsString(S.ClassName);
    }

    private void EnterButtonActive(bool toggle)
    {
        EnterButton.interactable = toggle;
    }

    public void OnEnterHUB()
    {
        //set the hub canvas active (deactivate all other stuff)
        if (IsNewChar) CreateNewCharProfile(LoadedCharacter.ClassName);
        else LoadProfile(LoadedCharacter.ClassName);

        //Set player controller classs to loaded class character
        Debug.Log("loaded lass is: " + LoadedCharacter.ClassName);

    }


}
