﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyableIcon : Icon {

    private const bool Success = true;
    private const bool Fail = false;



    internal int Price;
    //private long BTCprice;
    //private long ETHprice;

    public string PriceTag;
    public string NameLabel;

    internal string PurchaseErrors;

    internal int ConversionVariable = 1;

    public Button BuyButton;

    internal Text[] Labels;
    internal Text PriceLabel;
    internal Text IconLabel;

    private void Start()
    {
        //Labels = GetComponentsInChildren<Text>();
        //Debug.Log(Labels.Length);
        //IconLabel.text = Labels[0].text;
        //PriceLabel.text = Labels[1].text;



        SetPrice(Price);
        Instantiate(BuyButton, new Vector3(this.transform.position.x, this.transform.position.y - 3, this.transform.position.z), Quaternion.identity);

    }
    public void SetPrice(int ItemPrice)
    {
        Price = ItemPrice;
        //BTCprice = ItemPrice * ConversionVariable;
        //ETHprice = ItemPrice * ConversionVariable;
    }

    public bool Buy()
    {
        //check balance

        PurchaseErrors = CheckValidPurchase();
        if(PurchaseErrors.Length > 0) return Fail;


        Debug.Log("ok you bought this..");   

        return Success;
    }

    internal void OnBuySuccess()
    {
        //send item to inventory
        Debug.Log("calling parent buybable");
    }

    private string CheckValidPurchase()
    {
        string Errors = "";
        //if(balance < Price) Errros += " The required amount is not present.";
        //else if(somethingelsewrong) Errors += " 

        return Errors;
    }




}
