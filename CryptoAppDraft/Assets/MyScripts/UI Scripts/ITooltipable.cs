﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface ITooltipable
{
    string GetTooltipData(out UIElement U);
}
