﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSheet : MonoBehaviour {

    internal SheetSlot[] Slots;

    internal Dictionary<SlotType, SheetSlot> EquipmentSlots;

    public GameObject DataSheet;
    public GameObject SlotParent;

    private Text CharacterData;

    public InventoryUI InvUIRef;


    public delegate void OnEquipmentChanged(EquipableItem NewItem, EquipableItem OldItem);
    public OnEquipmentChanged EquipmentChanged;

    public delegate void OnWeaponEquip(Weapon W);
    public OnWeaponEquip WeaponEquipped;

    public delegate void OnOffhandEquip(Offhand O);
    public OnOffhandEquip OffhandEquipped;

    public delegate void OnArmorEquip(Armor A);
    public OnArmorEquip ArmorEquipped;

    private void Awake()
    {
        EquipmentSlots = new Dictionary<SlotType, SheetSlot>();
        Slots = SlotParent.GetComponentsInChildren<SheetSlot>();
        CharacterData = DataSheet.GetComponentInChildren<Text>();

        foreach(SheetSlot S in Slots)
        {
            S.SetSheetRef(this);
            EquipmentSlots.Add(S.Type, S);
        }


        this.gameObject.SetActive(false);

    }

    public void SetInvUI(InventoryUI I)
    {
        InvUIRef = I;
    }


    public void UpdateDataSheet(string s)
    {
        CharacterData.text = s;
    }

    public void Equip(EquipableItem ItemToEquip)
    {
        Debug.Log("calling equip in char sheet");
        EquipableItem OldItem = null; //if swapping
        //should only be callable from the inventory slot
        if(EquipmentSlots[ItemToEquip.ItemType].IsFilled)
        {
            if (ItemToEquip.ItemType == SlotType.Ring && !EquipmentSlots[SlotType.Ring2].IsFilled) //if is filled but its a ring1 and ring2 is not filled
            {
                EquipmentSlots[SlotType.Ring2].AddItem(ItemToEquip); //equip ring1 into ring2 slot
                ItemToEquip.ItemType = SlotType.Ring2;
                //if slot 2 is filled, it will just swap with ring1
                ItemToEquip.IsEquipped = true;
                InvUIRef.InvRef.RemoveItem(ItemToEquip);
                EquipmentChanged.Invoke(ItemToEquip, OldItem); //for rings only
                return;
            }
            else
            {
                //swap items
                Debug.Log("swapping " + EquipmentSlots[ItemToEquip.ItemType].item.name + " with " + ItemToEquip.name);
                OldItem = EquipmentSlots[ItemToEquip.ItemType].GetEquippedItem();
                if (OldItem.ItemType == SlotType.Ring2) OldItem.ItemType = SlotType.Ring;
                UnEquip(OldItem);
            }
        }
        Debug.Log("equpping the item " + ItemToEquip);
        EquipmentSlots[ItemToEquip.ItemType].AddItem(ItemToEquip); //equip item in the sheet slot
        ItemToEquip.IsEquipped = true;
        InvUIRef.InvRef.RemoveItem(ItemToEquip);

        EquipmentChanged.Invoke(ItemToEquip, OldItem);
        if(ItemToEquip is Weapon)
        {
            WeaponEquipped(ItemToEquip as Weapon);
        }

        if(ItemToEquip is Offhand)
        {
            OffhandEquipped(ItemToEquip as Offhand);
        }
        else if(ItemToEquip is Armor)
        {
            Debug.Log("calling equip armor in char sheet");
            ArmorEquipped(ItemToEquip as Armor);
        }




    }


    public EquipableItem GetWeapon()
    {
        return EquipmentSlots[SlotType.Weapon].item as EquipableItem;
    }



    public void UnEquip(EquipableItem ItemToUnEquip)
    {
        //should only be called when the item is in a sheet slot....might be prone to errors later
        Debug.Log("unequipping :" + ItemToUnEquip.name);
        ItemToUnEquip.IsEquipped = false;
        InvUIRef.InvRef.AddItem(ItemToUnEquip); //add back to inventory to unequip
        EquipmentSlots[ItemToUnEquip.ItemType].ClearSlot(); //unequip item


        //itemtoequip is null when unequipping
        EquipmentChanged.Invoke(null, ItemToUnEquip); //changes the stats

        if (ItemToUnEquip is Offhand)
        {
            OffhandEquipped(null);
        }


    }
}

//only assign ring items with ring1
public enum SlotType
{
    Weapon = 0,
    ChestArmor = 1,
    Helm = 2,
    Boots = 3,
    Pants = 4,
    Offhand = 5,
    Ring = 6,
    Ring2 = 7,
    Special = 8,
    Emblem = 9,
}
