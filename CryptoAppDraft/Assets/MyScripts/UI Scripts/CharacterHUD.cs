﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CharacterHUD : MonoBehaviour {


    public CharacterSheet CharSheet;
    private Text CharDataText;

    public Slider ResourceBar;
    public Slider Healthbar;
    public Slider ExpBar;
    private Text ResourceDisplayText;
    private Text HealthDisplayText;
    private Text ExpBarText;
    public Slider[] Bars;

    public InventoryUI InvUI;
    private Vector3 InventoryPos;
    private Vector3 CharSheetPos;
    internal PlayerEntity PlayerToWatch;
    public AbilityUI AbilityBar;
    public OptionsMenu TheOptionsMenu;
    public RectTransform ToolTipPanel;
    internal MiniMap TheMiniMap;
    public BuffHandler BuffUI;

    internal event Action<int> ResourceChanged;


    string Hpmax;

    private void Awake()
    {
        //CharDataText = CharacterSheet.GetComponentInChildren<Text>();
        HealthDisplayText = Healthbar.GetComponentInChildren<Text>();
        ResourceDisplayText = ResourceBar.GetComponentInChildren<Text>();
        ExpBarText = ExpBar.GetComponentInChildren<Text>();
        InventoryPos = InvUI.transform.position;
        CharSheetPos = CharSheet.transform.position;

        CharSheet.SetInvUI(InvUI);
        InvUI.SetCharSheetRef(CharSheet);
        TheOptionsMenu.gameObject.SetActive(false);

        //Debug.Log("char hud awake done");

    }

    private void Start()
    {
        //Ability.OnResourceUsed += DrainResource;
        StartCoroutine(RegenerateResources());
    }

    IEnumerator RegenerateResources()
    {
        while(true)
        {
            yield return new WaitForSeconds(3); //regen rate
            if (Healthbar.value < PlayerToWatch.HealthMax.GetValue())
            {
                PlayerToWatch.Health.AddToBase(PlayerToWatch.HealthRegen.GetValue());
                //Debug.Log("updating player health in regen, new value: " + PlayerToWatch.Health.GetValue() + PlayerToWatch.HealthRegen.GetValue());
                UpdateHP(PlayerToWatch.Health.GetValue());
            }
            if (ResourceBar.value < PlayerToWatch.ResourceMax.GetValue())
            {
                //Debug.Log("adding " + PlayerToWatch.ResourceRegen.GetValue() + " to current resource: " + PlayerToWatch.Resource.GetValue());
                PlayerToWatch.Resource.AddToBase(PlayerToWatch.ResourceRegen.GetValue());
                UpdateResource(PlayerToWatch.Resource.GetValue());
            }
        }
    }


    private void ToggleMiniMap()
    {
        TheMiniMap.MiniMapCam.MiniMapCanvas.gameObject.SetActive(!TheMiniMap.MiniMapCam.MiniMapCanvas.gameObject.activeInHierarchy);
    }

    public int GetResourceValue()
    {
        return (int)ResourceBar.value;
    }

    private void InitalizeClassAbilites()
    {

        AbilityBar.SetUpAbilitesBar(PlayerToWatch.EntityClass.Abilities, PlayerToWatch.EntityClass.OffHandAbility, PlayerToWatch);
        ResourceChanged += AbilityBar.OffhandCooldownSlot.CheckResource;
        PlayerToWatch.EntityClass.OffHandAbility.OnResourceUsed += DrainResource;
        if (PlayerToWatch.OffHand == null) PlayerToWatch.EntityClass.OffHandAbility.IsAvaliable = false;

        foreach (AbilityCoolDown A in AbilityBar.AbilityCooldownSlots)
        {
            A.TheAbility.OnResourceUsed += DrainResource;
            ResourceChanged += A.CheckResource;
        }

    }

    public void DrainResource(int amount)
    {
        PlayerToWatch.Resource.AddModifier(amount * -1);
        ResourceBar.value = PlayerToWatch.Resource.GetValue();
        UpdateResource(PlayerToWatch.Resource.GetValue());
    }

    public void ToggleDataSheet()
    {
        //CharSheet.UpdateInfo();
        CharSheet.transform.position = CharSheetPos;
        CharSheet.gameObject.SetActive(!CharSheet.gameObject.activeInHierarchy);
    }

    public void ToggleDataSheet(bool toggle)
    {
        //CharSheet.UpdateInfo();
        CharSheet.transform.position = CharSheetPos;
        CharSheet.gameObject.SetActive(toggle);
    }

    public bool HudUiOpen()
    {
        if (CharSheet.isActiveAndEnabled || InvUI.isActiveAndEnabled) return true;
        else return false;
    }


    public void ToggleOptions()
    {
        PlayerToWatch.CanMove = !PlayerToWatch.CanMove;
        TheOptionsMenu.gameObject.SetActive(!TheOptionsMenu.gameObject.activeInHierarchy);
    }

    internal void ToggleOptions(bool toggle)
    {
        TheOptionsMenu.gameObject.SetActive(toggle);
    }

    public void SetWatchMe(PlayerEntity P)
    {
        PlayerToWatch = P;
        //Stat.StatChanged += CharSheet.UpdateDataSheet();

    }

    public void ToggleBars()
    {
        foreach(Slider S in Bars)
        {
            S.gameObject.SetActive(!S.gameObject.activeInHierarchy);
        }
    }

    public void ToggleAllOff()
    {
        ToggleInventory(false);
        ToggleDataSheet(false);
        //ToggleOptions(false);
    }

    public void ToggleInventory()
    {
        InvUI.transform.position = InventoryPos;
        InvUI.gameObject.SetActive(!InvUI.gameObject.activeInHierarchy);
        //ToggleMiniMap();
    }


    public void ToggleInventory(bool toggle)
    {
        InvUI.transform.position = InventoryPos;
        InvUI.gameObject.SetActive(toggle);
        ToggleMiniMap();
    }

    public void InitBarValues(float hp, float hpmax, float resource)
    {
        InitalizeClassAbilites();
        Healthbar.maxValue = hpmax;
        ResourceBar.maxValue = resource;
        UpdateHP(hp);
        UpdateResource(resource);
        Hpmax = "/" + hpmax.ToString();
    }

    public void UpdateHUD(float hp, float maxhp, float resource)
    {
        UpdateHP(hp);
        UpdateResource(resource);
        Healthbar.maxValue = maxhp;
        ResourceBar.maxValue = PlayerToWatch.ResourceMax.GetValue();
        Hpmax = "/" + maxhp.ToString();
    }

    public void UpdateHP(float hp)
    {
        hp = Mathf.RoundToInt(hp);
        Healthbar.value = hp;
        if(HealthDisplayText != null) HealthDisplayText.text = hp.ToString() + Hpmax;
    }

    public void UpdateExp(float exp, float xpnext)
    {
        ExpBar.value = exp;
        ExpBarText.text = ExpBar.value + "/" + xpnext;
    }

    public void ResetExpBar(float maxexp)
    {
        ExpBar.maxValue = maxexp;
        UpdateExp(0, maxexp);
    }

    public void UpdateResource(float r)
    {
        r = Mathf.RoundToInt(r);
        ResourceBar.value = r;
        ResourceDisplayText.text = r.ToString();
        if (ResourceChanged != null) ResourceChanged((int)ResourceBar.value); //notifies the spell bar to go red or not
    }


}
