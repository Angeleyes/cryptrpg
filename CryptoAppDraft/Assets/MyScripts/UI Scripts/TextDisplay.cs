﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplay : MonoBehaviour
{


    internal Text TextField = null;
    private string TheText = "...";


    void Start()
    {
        TextField = GetComponentInChildren<Text>();
        DisplayText();

    }

    private void DisplayText()
    {
        TextField.text = TheText;
    }

    public void SetText(string NewText)
    {
        TheText = NewText;
        DisplayText();
    }


}

