﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class BuffSlot : UIElement
{
    internal Image BuffImage;
    internal string BuffTip;
    internal StringBuilder Sb = new StringBuilder();
    public Text TimerText;


    public void SetBuff(Sprite iconimage, float timertext, string bufftip)
    {
        BuffImage.sprite = iconimage;
        SetText(timertext);
        BuffTip = bufftip;
    }

    public void SetText(float timertext)
    {
        Sb.Length = 0;
        if (timertext <= 0) TimerText.enabled = false;
        else
        {
            int remainder = (int)timertext % 60;
            timertext = timertext / 60;
            Sb.Append(((int)timertext).ToString());
            Sb.Append(":");
            Sb.Append(remainder.ToString());
            TimerText.enabled = true;
            TimerText.text = Sb.ToString();
            Sb.Length = 0;
        }
    }

    public override string GetTooltipData(out UIElement ConcreteElement)
    {
        ConcreteElement = this;
        return BuffTip;

    }
}
