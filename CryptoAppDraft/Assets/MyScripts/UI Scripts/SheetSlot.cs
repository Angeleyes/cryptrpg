﻿using System;
using UnityEngine;

public class SheetSlot : InventorySlot {


    public SlotType Type;
    private CharacterSheet SheetReference;
    public bool IsFilled = false;
    public EquipableItem EquippedItem;

    public override void AddItem(Item NewItem)
    {
        //EquipableItem I = NewItem as EquipableItem;
        base.AddItem(NewItem);
        EquippedItem = NewItem as EquipableItem;

        EquippedItem.OnEquipAction = () => SheetReference.UnEquip(EquippedItem);
        IsFilled = true;

        Debug.Log("adding from sheet slot");
    }

    public EquipableItem GetEquippedItem()
    {
        return item as EquipableItem;
    }

    public override void ClearSlot()
    {

        base.ClearSlot();
        IsFilled = false;
    }

    public override void UseItem()
    {
        base.UseItem();
    }

    public void SetSheetRef(CharacterSheet C)
    {
        SheetReference = C;
    }

    public string GetEquippmentStatData()
    {
        if (EquippedItem != null) return EquippedItem.GetItemData();
        else return "";
    }

}
