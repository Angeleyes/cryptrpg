﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
////using Barebones.Networking;
////using Barebones.MasterServer;
//using System;


//public class ProfileManager : ServerModuleBehaviour {

//    public static ProfileManager Instance;
//    internal IClientSocket ClientSock;
//    internal AccountInfoPacket Account;

//    internal static string TheClassToLoad;

//    private static ObservableServerProfile ThePlayerProfile;

//    private Dictionary<string, ScriptableClass> ScriptableClassDict;
//    public List<ScriptableClass> Classes;

//    void Awake()
//    {

//        if (Instance != null) Destroy(Instance);

//        Instance = this;

//        DontDestroyOnLoad(this.gameObject);

//        ScriptableClassDict = new Dictionary<string, ScriptableClass>();

//        foreach (ScriptableClass S in Classes)
//        {
//            ScriptableClassDict.Add(S.ClassName, S);
//        }


//    }

//    public bool HasCharacterClass(string Class)
//    {
//        if (ThePlayerProfile == null) Debug.Log("player prof is null in hascharclass");
//        if (ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).UnderlyingDictionary == null) Debug.Log("underlying is null");
//        Dictionary<string, int> D = new Dictionary<string, int>();
//        D = ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).UnderlyingDictionary;
//        return (D.ContainsKey(Class));
//    }

//    //public class Tuple
//    //{
//    //    int item1;
//    //    int item2;
//    //    public Tuple Create(int a, int b)
//    //    {
//    //        Tuple T = new Tuple();
//    //        T.item1 = a;
//    //        T.item2 = b;
//    //        return T;
//    //    }
//    //}

//    public void SetUpPlayer(PlayerEntity P)
//    {
//        //at some point maybe i should trigger the client interval update....
//        Debug.Log("setting up player");
//        P.SetUpPlayerProfile(ScriptableClassDict[TheClassToLoad], GetLevel(TheClassToLoad), GetExp(TheClassToLoad), Account.Username);

//        P.OnLevelUp += (int lvl) => SetCharLevel(P.EntityClass.ClassName, lvl);
//        P.OnExpChange += (int exp) => SetCharExp(P.EntityClass.ClassName, exp);
//        P.OnDefeatedBoss += () => AddToCryptLvl();
//        P.OnPickedUpItem += (Item I) => ProcessAddToInventoryRequest(I);

//        Debug.Log("finished setting up player");
//    }

//    public void ProcessAddToInventoryRequest(Item TheItem)
//    {
//        Debug.Log("profile manager processing the item you picked up...." + TheItem.name);
//    }

//    public string GetLoadedCharacter()
//    {
//        return TheClassToLoad;
//    }

//    public void AddNewCharProfile(string newclass)
//    {
//        NewClassEntry(newclass);
//        TheClassToLoad = newclass;
//    }

//    private void NewClassEntry(string newclass)
//    {
//        ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).SetValue(newclass, 1); //sets profile to have new class at lvl 1
//        ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.ExpKey).SetValue(newclass, 0); //sets the new classe xp to 0
//        //if(ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey) == null || ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey).Value <= 0) //then this is the first character made for this profile
//        //{
//        //    Debug.Log("setting crypt key to 1");
//        //    ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey).Set(1);
//        //}
//        ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey).MarkDirty();

//        //maybe mark the inventory as dirty
//    }

//    private int GetLevel(string Class)
//    {
//        //if class doesnt exist, return a values for a new class...but may cause problems later
//        if (!ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).UnderlyingDictionary.ContainsKey(Class)) return 1;

//        int ClassLevel = ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).GetValue(Class);
//        return ClassLevel;
//    }

//    private int GetExp(string Class)
//    {
//        if (!ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.ExpKey).UnderlyingDictionary.ContainsKey(Class)) return 0;

//        int ClassExp = ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.ExpKey).GetValue(Class);
//        return ClassExp;
//    }

//    private int GetCryptLevel(string Class) //look at this if statement again...
//    {
//        if (ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey) == null || ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey).Value <= 0)
//        {
//            Debug.Log("crypt key check is null or less than zero in getxryptlvl");
//            return 1;
//        }

//        int CryptLvl = ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey).Value;
//        return CryptLvl;
//    }

//    public List<string> GetAllProfileChars()
//    {
//        Dictionary<string, int> D = ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).UnderlyingDictionary;
//        List<string> List = new List<string>();
//        foreach (KeyValuePair<string, int> K in D)
//        {
//            List.Add(K.Key);
//        }
//        return List;

//    }

//    private bool ClassExists(string Class)
//    {
//        if (string.IsNullOrEmpty(Class))
//        {
//            Debug.Log("class is null in class exists, maybe null in chartoload");
//            return false;
//        }
//        bool b = ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).UnderlyingDictionary.ContainsKey(Class);
//        return b;
//    }

//    public void SetExistingCharData(string Class) //potential BUG in this function if the data is mis-ordered
//    {
//        TheClassToLoad = Class;
//    }

//    public List<int> LoadCharacterData()
//    {
//        if (!ClassExists(TheClassToLoad))
//        {
//            Debug.Log("class to load is not in the profile");
//            return null;
//        }

//        List<int> L = new List<int>() { GetLevel(TheClassToLoad), GetExp(TheClassToLoad), GetCryptLevel(TheClassToLoad) }; //if class doesnt exist, values will be 1,0,1

//        Debug.Log("getting data: " + TheClassToLoad + " " + "lvl: " + L[0] + " exp: " + L[1] + " cryptlvl: " + L[2]);

//        return L;
//    }


//    private void AddToCryptLvl()
//    {
//        ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey).Add(1);
//    }

//    private void SetTestInventory(string ItemName, int HashKey)
//    {
//        ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.InvKey).SetValue(ItemName, HashKey);
//    }

//    private void SetCharLevel(string Class, int lvl)
//    {
//        ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).SetValue(Class, lvl);
//    }

//    private void SetCharExp(string Class, int exp)
//    {
//        ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.ExpKey).SetValue(Class, exp);
//    }
        
//    private void AddToInventory()
//    {

//    }
    
//    private void RemoveFromInventory()
//    {

//    }


//    public void StartSetup()
//    {
//        StartCoroutine(SetupProfileData());
//    }

//    IEnumerator SetupProfileData()
//    {
//        yield return new WaitForSeconds(2); //need a callback instead of this
//        OnEnterLogin();
//    }


//    public void OnEnterLogin()
//    {
//        Account = Msf.Client.Auth.AccountInfo;
//        ClientSock = Msf.Client.Connection;

//        ThePlayerProfile = PlayerProfile.CreatePlayerProfileInServer(Account.Username, ClientSock.Peer); //make a default profile
//        //GetProfileFromDatabase(); //fill with data from database, if fails, that means its a new profile, need char creator to make new data
//        SetServerProfileToBeSavedToDB();

//        if (ThePlayerProfile == null) Debug.Log("player prof is null");
//    }


//    public void Store() // for debug purproses only
//    {
//        //ThePlayerProfile.Modified
//        SetServerProfileToBeSavedToDB();
//    }



//    public void DebugShowRestore()
//    {
//        List<string> L = GetAllProfileChars();
//        foreach (string S in L)
//        {
//            Debug.Log("this class is a : " + S);
//            Debug.Log("profile: " + ThePlayerProfile.Username + "  has exp value: " + ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.ExpKey).GetValue(S));
//            Debug.Log("and has level: " + ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).GetValue(S));
            
            
//        }
//        //string s = ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).
//        //Debug.Log(" and has class: " + ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).GetValue(s);
//    }

//    public string GetCharacterDataAsString(string classname)
//    {

//        string exp = GetExp(classname).ToString();
//        string lvl = GetLevel(classname).ToString();
//        string cryptlvl = GetCryptLevel(classname).ToString();

//        //will be null if selected new char

//        return "Level: " + lvl + "\n" + "Exp: " + exp + "\n" + "CryptLevel: " + cryptlvl;
//    }

//    private void SetServerProfileToBeSavedToDB()
//    {
        
//        //ThePlayerProfile = P;
//        Msf.Server.Profiles.FillProfileValues(ThePlayerProfile, (successful, error) =>
//        {
//            if (!successful) Debug.Log("error..oh no");
//            else if (successful)
//            {
//                DebugShowRestore();
//                StartCoroutine(DebugShow());
//                //perhaps put a callback in here instead of the 2 second wait at the select char screen

//            }

//        });
//    }

//    public void ChangeProfile() //debug only
//    {
//        //Debug.Log("Unsaved props: " + ThePlayerProfile.UnsavedProperties.Count + " ....has property updates: " + ThePlayerProfile.HasDirtyProperties);
//        //byte[] data = ThePlayerProfile.ToBytes();
//        ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).MarkDirty();
//        ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.ExpKey).MarkDirty();
//        ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.InvKey).MarkDirty();
//        ThePlayerProfile.GetProperty<ObservableInt>(PlayerProfile.CryptLvlKey).MarkDirty();
//        Debug.Log("Unsaved props: " + ThePlayerProfile.UnsavedProperties.Count + " ....has property updates: " + ThePlayerProfile.HasDirtyProperties);


//        //ThePlayerProfile.GetProperty<ObservableDictStringInt>(PlayerProfile.CharListKey).
//    }

//    public void GetFromDB() // debug only
//    {
//        SetClientSaveToDBInterval();
//    }


//    private void SetClientSaveToDBInterval()
//    {
//        Debug.Log("calling get from db");
//        //var EmptyProfile = PlayerProfile.CreatePlayerProfileInServer(Account.Username, ClientSock.Peer); //try with empty later
//        if (ThePlayerProfile == null) Debug.Log("player prof is null insdie getprofilefromdb (before get call)");
//        Msf.Client.Profiles.GetProfileValues(ThePlayerProfile, (successful, error) =>
//        {
//            if (!successful) Debug.Log("failed to get profile from db, so this is brand new profile");
//            else if (successful)
//            {
//                Debug.Log("applying updates from handel client request");
//                byte[] data = ThePlayerProfile.GetUpdates();
//                ThePlayerProfile.ApplyUpdates(data);
//                StartCoroutine(DebugShow());
//                //maybe call save to db in here
//            }

//        });

//        if (ThePlayerProfile == null) Debug.Log("this is where i suspect its null (after get call)");
//             //maybe need to write a copyto function

//    }

//    IEnumerator DebugShow()
//    {
//        yield return new WaitForSeconds(2);
//        if (ThePlayerProfile == null) Debug.Log("this is where i suspect its null after 3");
//        DebugShowRestore();

//    }



//}
