﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityUI : MonoBehaviour {


    public List<AbilityCoolDown> AbilityCooldownSlots;
    public AbilityCoolDown OffhandCooldownSlot;
    public GameObject CooldownPanel;



    public void SetUpAbilitesBar(List<Ability> ClassAbilities, Ability OffhandAbility, PlayerEntity ThePlayer)
    {
        foreach(AbilityCoolDown A in CooldownPanel.GetComponentsInChildren<AbilityCoolDown>())
        {
            AbilityCooldownSlots.Add(A);
        }

        foreach(Ability A in ClassAbilities)
        {

            if (A != null) A.Initalize(ThePlayer);
            else Debug.Log("have a null ability");
        }

        if (AbilityCooldownSlots.Count <= 0) Debug.Log("! ability cooldown slots is empty !");
        for (int i = 0; i < AbilityCooldownSlots.Count; i++)
        {
            AbilityCooldownSlots[i].InitalizeAbility(ClassAbilities[i]);
        }

        OffhandAbility.Initalize(ThePlayer);
        OffhandCooldownSlot.InitalizeAbility(OffhandAbility);
    }

}
