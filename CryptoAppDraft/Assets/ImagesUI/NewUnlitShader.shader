﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/ShadowFallOff"{


	Properties{
		_MainTex("Texture", 2D) = "white"{ }
		_Color("Color", Color) = (1,1,1,1)

		_Alpha1("StartAlpha", Range(0.0,1.0)) = 0.5
		_Alpha2("EndAlpha", Range(0.0,1.0)) = 0.5

			_Color1("StartColor", Color) = (1,1,1,1)
			_Color2("EndColor", Color) = (1,1,1,1)

	}

		SubShader{
			Cull Off


			Tags{"RenderType" = "Transparent" "Queue" = "Transparent"}

			Pass
			{
			ZWrite Off
			Blend One OneMinusSrcAlpha
			

			CGPROGRAM

			#pragma vertex vertexFunc
			#pragma fragment fragmentFunc
			#include "UnityCG.cginc"

			sampler2D _MainTex;
		
		float4 _MainTex_ST;

		fixed4 _TopAlpha;
		fixed4 _BottomAlpha;
		fixed4 _TopColor;
		fixed4 _BottomColor;

		half _Value;


		struct v2f
		{
			float4 pos : SV_POSITION;
			fixed4 color : COLOR;
			half2 uv : TEXCOOR0;
		};


		v2f vertexFunc(appdata_base v)
		{
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
			o.color = lerp(_TopColor, _BottomColor, v.texcoord.y);

			return o;
		}

		//fixed4 _Color;
		//float4 _MainTex_TexelSize;
		

		fixed4 fragmentFunc(v2f i) : SV_Target{
			half4 color = tex2D(_MainTex, i.uv);

			color.rgb *= color.a;
			//float4 color;
			color.rgb = i.color.rgb;
			color.a = tex2D(_MainTex, i.uv).a * i.color.a;


			return color;

		}

		ENDCG

			}
		}



}
