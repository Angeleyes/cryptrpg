﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)


// Default Unity sprite shader with a second pass for offset shadow
// Edit by @_Dickie

Shader "Sprites/Default Shadowed"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
	_Color("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
		[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
		[PerRendererData] _AlphaTex("External Alpha", 2D) = "white" {}
	[PerRendererData] _EnableExternalAlpha("Enable External Alpha", Float) = 0

		_ShadowDistance("Shadow Distance", vector) = (-0.1,-0.1,0.1,0)
		_ShadowColor("Shadow Color", color) = (0.05,0.1,0.25,0.9)

		//my props

		//_MainTex("Texture", 2D) = "white" {}
	    _Color1("First Color", Color) = (1,1,1,1)
		_Color2("Second Color", Color) = (1,1,1,1)
		_Height("Height", Float) = 10.0
		_Height2("Height2", Float) = 1.0
		_Scale("Scale", Float) = 1.0
	}

		SubShader
	{
		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "Transparent"
		"PreviewType" = "Plane"
		"CanUseSpriteAtlas" = "True"
	}

		Cull Off
		LOD 100
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		// shadow pass
		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 2.0
#pragma multi_compile_instancing
#pragma multi_compile _ PIXELSNAP_ON
#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
#include "UnitySprites.cginc"


		struct v2
	{
		float3 worldPos : TEXCOORD2;
	};

		float4 _ShadowDistance;
		half4 _ShadowColor;
		half4 _Scale;
	float4 _Color1;
	float4 _Color2;
	float _Height;
	float _Height2;


	v2f vert(appdata_t IN)
	{
		v2f OUT;

		UNITY_SETUP_INSTANCE_ID(IN);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

#ifdef UNITY_INSTANCING_ENABLED
		IN.vertex.xy *= _Flip.xy;
#endif

		OUT.vertex = UnityObjectToClipPos(IN.vertex);
		OUT.texcoord = IN.texcoord;
		OUT.color = IN.color * _Color * _RendererColor;

		OUT.vertex += mul(UNITY_MATRIX_VP, _ShadowDistance); // offsetting the shadow pass by shadow distance

		//OUT.texcoord = TRANSFORM_TEX(OUT.texcoord, IN.texcoord);



		UNITY_TRANSFER_FOG(OUT, OUT.vertex);



		//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
		//o.worldPos = mul(_Height2, v.vertex).xyz;




#ifdef PIXELSNAP_ON
		OUT.vertex = UnityPixelSnap(OUT.vertex);
#endif

		return OUT;
	}

	fixed4 frag(v2f IN) : SV_Target
	{
		fixed4 c = SampleSpriteTexture(IN.texcoord) * IN.color;

	c.rgb = _ShadowColor.rgb;  // replace texture colour with shadow colour
	c.rgb *= c.a;
	c *= _ShadowColor.a; // multiply it all by shadow's alpha colour so you can have transparent shadows


	fixed4 gradient = lerp(_ShadowColor, _Color2,  _Height);
	c *= gradient;

	UNITY_APPLY_FOG(IN.UNITY_FOG_COORDS(1), c);

	//col = col * gradient;
	//// apply fog
	//UNITY_APPLY_FOG(i.fogCoord, col);


	return c;
	}

		ENDCG
	}

	//gradient pass







		// standard pass
		Pass
	{
		CGPROGRAM
#pragma vertex SpriteVert
#pragma fragment SpriteFrag
#pragma target 2.0
#pragma multi_compile_instancing
#pragma multi_compile _ PIXELSNAP_ON
#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
#include "UnitySprites.cginc"





		ENDCG
	}











	}
}