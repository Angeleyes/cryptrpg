﻿

public class CameraLocator 
{

    static CameraEffects Service;

    public static CameraEffects GetCamEffects() { return Service; }
    public static void Provide(CameraEffects service) { Service = service; }
}
