﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientCamera : CameraEffects
{
    bool Shaking = false;
    Vector3 StartPos = new Vector3();
    public float mx = 0.0001f;
    public float my = -0.0001f;
    public float lerpf = 0.1f;
    public override void ShakeCamera()
    {
        StartPos = this.transform.localPosition;
        if (!Shaking) StartCoroutine(CamShake(3,  my, mx));
    }


    IEnumerator CamShake(int framecount, float min, float max)
    {
        Shaking = true;
        int frames = 0;
        Vector3 LerpPoint = new Vector3();
        while (frames < framecount)
        {
            float x, y;
            x = Random.Range(min, max);
            y = Random.Range(min, max);
            LerpPoint = new Vector3(this.transform.localPosition.x + x, this.transform.localPosition.y + y, this.transform.localPosition.z);
            //Debug.Log("using camera shake coords x: " + x + " and y : " + y + " with frame count: " + frames);
            this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, new Vector3(x, y, this.transform.localPosition.z), lerpf);

            frames++;
            yield return new WaitForEndOfFrame();
            this.transform.localPosition = StartPos;
            yield return null;
        }

        this.transform.localPosition = StartPos;
        Shaking = false;

        yield break;
    }
}
